<?php
return array (
  'service_manager' => 
  array (
    'abstract_factories' => 
    array (
      0 => 'Zend\\Session\\Service\\ContainerAbstractServiceFactory',
      1 => 'Zend\\Db\\Adapter\\AdapterAbstractServiceFactory',
      2 => 'Zend\\Cache\\Service\\StorageCacheAbstractServiceFactory',
    ),
    'aliases' => 
    array (
      'Zend\\Session\\SessionManager' => 'Zend\\Session\\ManagerInterface',
      'MvcTranslator' => 'Zend\\Mvc\\I18n\\Translator',
      'Zend\\Db\\Adapter\\Adapter' => 'Zend\\Db\\Adapter\\AdapterInterface',
      'HttpRouter' => 'Zend\\Router\\Http\\TreeRouteStack',
      'router' => 'Zend\\Router\\RouteStackInterface',
      'Router' => 'Zend\\Router\\RouteStackInterface',
      'RoutePluginManager' => 'Zend\\Router\\RoutePluginManager',
      'cache' => 'ZendCacheStorageFactory',
    ),
    'factories' => 
    array (
      'Zend\\Session\\Config\\ConfigInterface' => 'Zend\\Session\\Service\\SessionConfigFactory',
      'Zend\\Session\\ManagerInterface' => 'Zend\\Session\\Service\\SessionManagerFactory',
      'Zend\\Session\\Storage\\StorageInterface' => 'Zend\\Session\\Service\\StorageFactory',
      'Zend\\Mvc\\I18n\\Translator' => 'Zend\\Mvc\\I18n\\TranslatorFactory',
      'Zend\\Db\\Adapter\\AdapterInterface' => 'Zend\\Db\\Adapter\\AdapterServiceFactory',
      'Zend\\Cache\\PatternPluginManager' => 'Zend\\Cache\\Service\\PatternPluginManagerFactory',
      'Zend\\Cache\\Storage\\AdapterPluginManager' => 'Zend\\Cache\\Service\\StorageAdapterPluginManagerFactory',
      'Zend\\Cache\\Storage\\PluginManager' => 'Zend\\Cache\\Service\\StoragePluginManagerFactory',
      'Zend\\Router\\Http\\TreeRouteStack' => 'Zend\\Router\\Http\\HttpRouterFactory',
      'Zend\\Router\\RoutePluginManager' => 'Zend\\Router\\RoutePluginManagerFactory',
      'Zend\\Router\\RouteStackInterface' => 'Zend\\Router\\RouterFactory',
      'ValidatorManager' => 'Zend\\Validator\\ValidatorPluginManagerFactory',
      'User\\Model\\RoleRepository' => 
      Closure::__set_state(array(
      )),
      'User\\Model\\UserRepository' => 
      Closure::__set_state(array(
      )),
      'User\\Model\\ResourceActionRepository' => 
      Closure::__set_state(array(
      )),
      'User\\Model\\ResourceRepository' => 
      Closure::__set_state(array(
      )),
      'User\\Model\\RolePermissionRepository' => 
      Closure::__set_state(array(
      )),
      'User\\Model\\UserPermissionRepository' => 
      Closure::__set_state(array(
      )),
      'User\\Model\\GroupResourceRepository' => 
      Closure::__set_state(array(
      )),
      'User\\Model\\ResourceControllersRepository' => 
      Closure::__set_state(array(
      )),
      'SystemSetting\\Model\\BranchRepository' => 
      Closure::__set_state(array(
      )),
      'SystemSetting\\Model\\NationalityRepository' => 
      Closure::__set_state(array(
      )),
      'SystemSetting\\Model\\SubjectRepository' => 
      Closure::__set_state(array(
      )),
      'SystemSetting\\Model\\FormatRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\SupplierRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemCategoryRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemMasterRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemBrokenReasonRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemUnitMeasurementRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemAdjustmentRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemMissingBrokenRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemMissingBrokenDetailRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemReceiveRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemTakeUseRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\ItemReceiveGroupRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\StockBalanceMasterRepository' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Model\\SaleStockRepository' => 
      Closure::__set_state(array(
      )),
      'Sale\\Model\\SaleRepository' => 
      Closure::__set_state(array(
      )),
      'Sale\\Model\\SaleDetailRepository' => 
      Closure::__set_state(array(
      )),
      'Sale\\Model\\ItemCategoryRepository' => 
      Closure::__set_state(array(
      )),
      'Sale\\Model\\ItemTypeRepository' => 
      Closure::__set_state(array(
      )),
      'Sale\\Model\\SaleTypeRepository' => 
      Closure::__set_state(array(
      )),
      'Sale\\Model\\SaleItemRepository' => 
      Closure::__set_state(array(
      )),
      'Sale\\Model\\ItemSizeRepository' => 
      Closure::__set_state(array(
      )),
      'Accountant\\Model\\SalePriceRepository' => 
      Closure::__set_state(array(
      )),
      'Accountant\\Model\\DiscountRepository' => 
      Closure::__set_state(array(
      )),
      'Accountant\\Model\\DiscountTypeRepository' => 
      Closure::__set_state(array(
      )),
      'Accountant\\Model\\IncomeRepository' => 
      Closure::__set_state(array(
      )),
      'Contact\\Model\\ContactRepository' => 
      Closure::__set_state(array(
      )),
      'ZendCacheStorageFactory' => 
      Closure::__set_state(array(
      )),
    ),
    'delegators' => 
    array (
      'HttpRouter' => 
      array (
        0 => 'Zend\\Mvc\\I18n\\Router\\HttpRouterDelegatorFactory',
      ),
      'Zend\\Router\\Http\\TreeRouteStack' => 
      array (
        0 => 'Zend\\Mvc\\I18n\\Router\\HttpRouterDelegatorFactory',
      ),
    ),
  ),
  'route_manager' => 
  array (
  ),
  'router' => 
  array (
    'routes' => 
    array (
      'home' => 
      array (
        'type' => 'Zend\\Router\\Http\\Literal',
        'options' => 
        array (
          'route' => '/',
          'defaults' => 
          array (
            'controller' => 'Application\\Controller\\IndexController',
            'action' => 'index',
          ),
        ),
      ),
      'application' => 
      array (
        'type' => 'Zend\\Router\\Http\\Segment',
        'options' => 
        array (
          'route' => '/application[/:action]',
          'defaults' => 
          array (
            'controller' => 'Application\\Controller\\IndexController',
            'action' => 'index',
          ),
        ),
      ),
      'user' => 
      array (
        'type' => 'Zend\\Router\\Http\\Literal',
        'options' => 
        array (
          'route' => '/user',
          'defaults' => 
          array (
            'controller' => 'User\\Controller\\UserController',
            'action' => 'list',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => 
        array (
          'user' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/user[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'User\\Controller\\UserController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'role' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/role[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'User\\Controller\\RoleController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'group-resource' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/group-resource[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'User\\Controller\\GroupResourceController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'resource-action' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/resource-action[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'User\\Controller\\ResourceActionController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'resource-controllers' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/resource-controllers[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'User\\Controller\\ResourceControllersController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'resource' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/resource[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'User\\Controller\\ResourceController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
        ),
      ),
      'login' => 
      array (
        'type' => 'Zend\\Router\\Http\\Segment',
        'options' => 
        array (
          'route' => '/login[/]',
          'defaults' => 
          array (
            'controller' => 'User\\Controller\\LoginController',
            'action' => 'login',
          ),
        ),
      ),
      'auth-deny' => 
      array (
        'type' => 'Zend\\Router\\Http\\Segment',
        'options' => 
        array (
          'route' => '/auth-deny[/]',
          'defaults' => 
          array (
            'controller' => 'User\\Controller\\LoginController',
            'action' => 'accessDenyPage',
          ),
        ),
      ),
      'logout' => 
      array (
        'type' => 'Zend\\Router\\Http\\Segment',
        'options' => 
        array (
          'route' => '/logout[/]',
          'defaults' => 
          array (
            'controller' => 'User\\Controller\\LoginController',
            'action' => 'logout',
          ),
        ),
      ),
      'system-setting' => 
      array (
        'type' => 'Zend\\Router\\Http\\Literal',
        'options' => 
        array (
          'route' => '/system-setting',
          'defaults' => 
          array (
            'controller' => 'SystemSetting\\Controller\\SystemSettingController',
            'action' => 'list',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => 
        array (
          'branch' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/branch[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'SystemSetting\\Controller\\BranchController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'nationality' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/nationality[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'SystemSetting\\Controller\\NationalityController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'subject' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/subject[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'SystemSetting\\Controller\\SubjectController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'format' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/format[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'SystemSetting\\Controller\\FormatController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
        ),
      ),
      'inventory' => 
      array (
        'type' => 'Zend\\Router\\Http\\Literal',
        'options' => 
        array (
          'route' => '/inventory',
          'defaults' => 
          array (
            'controller' => 'Inventory\\Controller\\InventoryController',
            'action' => 'list',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => 
        array (
          'inventory' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/inventory[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\InventoryController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-unit-measurement' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-unit-measurement[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\ItemUnitMeasurementController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-category' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-category[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\ItemCategoryController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'supplier' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/supplier[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\SupplierController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-broken-reason' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-broken-reason[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\ItemBrokenReasonController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-master' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-master[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\ItemMasterController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-receive' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-receive[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\ItemReceiveController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-adjustment' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-adjustment[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\ItemAdjustmentController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-missing-broken' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-missing-broken[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\ItemMissingBrokenController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-take' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-take[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\ItemTakeController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'stock-balance-master' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/stock-balance-master[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\StockBalanceMasterController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'sale-stock' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/sale-stock[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\SaleStockController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'report' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/report[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\ReportController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'stock-alert' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/stock-alert[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Inventory\\Controller\\StockAlertController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
        ),
      ),
      'sale' => 
      array (
        'type' => 'Zend\\Router\\Http\\Literal',
        'options' => 
        array (
          'route' => '/sale',
          'defaults' => 
          array (
            'controller' => 'Sale\\Controller\\SaleController',
            'action' => 'list',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => 
        array (
          'sale' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/sale[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Sale\\Controller\\SaleController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-category' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-category[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Sale\\Controller\\ItemCategoryController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-type' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-type[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Sale\\Controller\\ItemTypeController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'sale-price' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/sale-price[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Sale\\Controller\\SalePriceController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'sale-type' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/sale-type[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Sale\\Controller\\SaleTypeController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'sale-item' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/sale-item[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Sale\\Controller\\SaleItemController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'item-size' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/item-size[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Sale\\Controller\\ItemSizeController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
        ),
      ),
      'accountant' => 
      array (
        'type' => 'Zend\\Router\\Http\\Literal',
        'options' => 
        array (
          'route' => '/accountant',
          'defaults' => 
          array (
            'controller' => 'Accountant\\Controller\\AccountantController',
            'action' => 'list',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => 
        array (
          'accountant' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/accountant[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Accountant\\Controller\\AccountantController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'sale-price' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/sale-price[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Accountant\\Controller\\SalePriceController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'discount' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/discount[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Accountant\\Controller\\DiscountController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'income' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/income[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Accountant\\Controller\\IncomeController',
                'action' => 'list',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
        ),
      ),
      'pos' => 
      array (
        'type' => 'Zend\\Router\\Http\\Literal',
        'options' => 
        array (
          'route' => '/pos',
          'defaults' => 
          array (
            'controller' => 'Pos\\Controller\\PosController',
            'action' => 'screen',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => 
        array (
          'pos' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/pos[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Pos\\Controller\\PosController',
                'action' => 'screen',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
          'login' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/login[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Pos\\Controller\\LoginController',
                'action' => 'index',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
        ),
      ),
      'contact' => 
      array (
        'type' => 'Zend\\Router\\Http\\Literal',
        'options' => 
        array (
          'route' => '/contact',
          'defaults' => 
          array (
            'controller' => 'Contact\\Controller\\ContactController',
            'action' => 'screen',
          ),
        ),
        'may_terminate' => true,
        'child_routes' => 
        array (
          'contact' => 
          array (
            'type' => 'Zend\\Router\\Http\\Segment',
            'options' => 
            array (
              'route' => '/contact[/:action[/:id]]',
              'defaults' => 
              array (
                'controller' => 'Contact\\Controller\\ContactController',
                'action' => 'screen',
              ),
              'constraints' => 
              array (
                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id' => '[0-9]+',
              ),
            ),
          ),
        ),
      ),
    ),
  ),
  'controllers' => 
  array (
    'factories' => 
    array (
      'Application\\Controller\\IndexController' => 
      Closure::__set_state(array(
      )),
      'User\\Controller\\LoginController' => 
      Closure::__set_state(array(
      )),
      'User\\Controller\\UserController' => 
      Closure::__set_state(array(
      )),
      'User\\Controller\\RoleController' => 
      Closure::__set_state(array(
      )),
      'User\\Controller\\GroupResourceController' => 
      Closure::__set_state(array(
      )),
      'User\\Controller\\ResourceActionController' => 
      Closure::__set_state(array(
      )),
      'User\\Controller\\ResourceControllersController' => 
      Closure::__set_state(array(
      )),
      'User\\Controller\\ResourceController' => 
      Closure::__set_state(array(
      )),
      'SystemSetting\\Controller\\SystemSettingController' => 
      Closure::__set_state(array(
      )),
      'SystemSetting\\Controller\\BranchController' => 
      Closure::__set_state(array(
      )),
      'SystemSetting\\Controller\\NationalityController' => 
      Closure::__set_state(array(
      )),
      'SystemSetting\\Controller\\SubjectController' => 
      Closure::__set_state(array(
      )),
      'SystemSetting\\Controller\\FormatController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\InventoryController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\ItemCategoryController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\ItemBrokenReasonController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\ItemUnitMeasurementController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\SupplierController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\ItemMasterController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\ItemReceiveController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\ItemAdjustmentController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\ItemMissingBrokenController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\ItemTakeController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\StockBalanceMasterController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\SaleStockController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\ReportController' => 
      Closure::__set_state(array(
      )),
      'Inventory\\Controller\\StockAlertController' => 
      Closure::__set_state(array(
      )),
      'Sale\\Controller\\SaleController' => 
      Closure::__set_state(array(
      )),
      'Sale\\Controller\\ItemCategoryController' => 
      Closure::__set_state(array(
      )),
      'Sale\\Controller\\ItemTypeController' => 
      Closure::__set_state(array(
      )),
      'Sale\\Controller\\SalePriceController' => 
      Closure::__set_state(array(
      )),
      'Sale\\Controller\\SaleTypeController' => 
      Closure::__set_state(array(
      )),
      'Sale\\Controller\\SaleItemController' => 
      Closure::__set_state(array(
      )),
      'Sale\\Controller\\ItemSizeController' => 
      Closure::__set_state(array(
      )),
      'Accountant\\Controller\\AccountantController' => 
      Closure::__set_state(array(
      )),
      'Accountant\\Controller\\SalePriceController' => 
      Closure::__set_state(array(
      )),
      'Accountant\\Controller\\DiscountController' => 
      Closure::__set_state(array(
      )),
      'Accountant\\Controller\\IncomeController' => 
      Closure::__set_state(array(
      )),
      'Pos\\Controller\\PosController' => 
      Closure::__set_state(array(
      )),
      'Pos\\Controller\\LoginController' => 
      Closure::__set_state(array(
      )),
      'Contact\\Controller\\ContactController' => 
      Closure::__set_state(array(
      )),
    ),
  ),
  'view_manager' => 
  array (
    'display_not_found_reason' => true,
    'display_exceptions' => true,
    'doctype' => 'HTML5',
    'not_found_template' => 'error/404',
    'exception_template' => 'error/index',
    'template_map' => 
    array (
      'layout/layout' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Application\\config/../view/layout/layout.phtml',
      'application/index/index' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Application\\config/../view/application/index/index.phtml',
      'error/404' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Application\\config/../view/error/404.phtml',
      'error/index' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Application\\config/../view/error/index.phtml',
    ),
    'template_path_stack' => 
    array (
      0 => 'C:\\wamp\\www\\inventory_system_pos\\module\\Application\\config/../view',
      'user' => 'C:\\wamp\\www\\inventory_system_pos\\module\\User\\config/../view',
      'system-setting' => 'C:\\wamp\\www\\inventory_system_pos\\module\\SystemSetting\\config/../view',
      'inventory' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Inventory\\config/../view',
      'sale' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Sale\\config/../view',
      'accountant' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Accountant\\config/../view',
      'pos' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Pos\\config/../view',
      'contact' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Contact\\config/../view',
    ),
    'strategies' => 
    array (
      0 => 'ViewJsonStrategy',
    ),
  ),
  'translator' => 
  array (
    'locale' => 'en_US',
    'translation_file_patterns' => 
    array (
      0 => 
      array (
        'type' => 'gettext',
        'base_dir' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Application\\config/../language',
        'pattern' => '%s.mo',
      ),
      1 => 
      array (
        'type' => 'gettext',
        'base_dir' => 'C:\\wamp\\www\\inventory_system_pos\\module\\User\\config/../language',
        'pattern' => '%s.mo',
      ),
      2 => 
      array (
        'type' => 'gettext',
        'base_dir' => 'C:\\wamp\\www\\inventory_system_pos\\module\\SystemSetting\\config/../language',
        'pattern' => '%s.mo',
      ),
      3 => 
      array (
        'type' => 'gettext',
        'base_dir' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Inventory\\config/../language',
        'pattern' => '%s.mo',
      ),
      4 => 
      array (
        'type' => 'gettext',
        'base_dir' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Sale\\config/../language',
        'pattern' => '%s.mo',
      ),
      5 => 
      array (
        'type' => 'gettext',
        'base_dir' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Accountant\\config/../language',
        'pattern' => '%s.mo',
      ),
      6 => 
      array (
        'type' => 'gettext',
        'base_dir' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Pos\\config/../language',
        'pattern' => '%s.mo',
      ),
      7 => 
      array (
        'type' => 'gettext',
        'base_dir' => 'C:\\wamp\\www\\inventory_system_pos\\module\\Contact\\config/../language',
        'pattern' => '%s.mo',
      ),
    ),
  ),
  'db' => 
  array (
    'driver' => 'Pdo',
    'dsn' => 'mysql:dbname=inventory_system_pos_db;host=localhost',
  ),
  'view_helpers' => 
  array (
    'invokables' => 
    array (
      'translate' => 'Zend\\I18n\\View\\Helper\\Translate',
      'pcsp-cache' => 'PCSPLib\\PCSPCache',
    ),
  ),
);