

ALTER TABLE heng_sal_sale_detail ADD COLUMN DiscountName varchar(255); 
ALTER TABLE heng_sal_sale_detail ADD COLUMN DiscountId int(11); 
ALTER TABLE heng_sal_sale_detail ADD COLUMN DiscountValue decimal(10,2); 
ALTER TABLE heng_sal_sale_detail ADD COLUMN DiscountTypeId int(11); 