-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2017 at 04:07 PM
-- Server version: 5.6.17-log
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_system_pos_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `heng_us_group_resource`
--

DROP TABLE `heng_us_group_resource`

CREATE TABLE `heng_us_group_resource` (
  `Id` int(11) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `OrderNum` int(11) DEFAULT NULL,
  `Note` text,
  `IsDeleted` int(11) DEFAULT '0',
  `IsActive` tinyint(1) DEFAULT '1',
  `AddDate` datetime DEFAULT NULL,
  `AddUserId` int(11) DEFAULT NULL,
  `ModifyDate` datetime DEFAULT NULL,
  `ModifyUserId` int(11) DEFAULT NULL,
  `DeleteUserId` int(11) DEFAULT NULL,
  `DeleteDate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `heng_us_group_resource`
--

INSERT INTO `heng_us_group_resource` (`Id`, `Name`, `OrderNum`, `Note`, `IsDeleted`, `IsActive`, `AddDate`, `AddUserId`, `ModifyDate`, `ModifyUserId`, `DeleteUserId`, `DeleteDate`) VALUES
(1, 'User Management', 5, '', 0, 1, '2017-01-15 04:19:34', 1, NULL, NULL, NULL, NULL),
(2, 'Inventory Management', 10, '', 0, 1, '2017-01-15 04:33:09', 1, '2017-01-15 04:33:19', 1, NULL, NULL),
(3, 'Accountant', 15, '', 0, 1, '2017-01-15 04:33:42', 1, NULL, NULL, NULL, NULL),
(4, 'Sale Management', 20, '', 0, 1, '2017-01-15 04:33:59', 1, NULL, NULL, NULL, NULL),
(5, 'Report', 25, '', 0, 1, '2017-01-15 04:34:35', 1, NULL, NULL, NULL, NULL),
(6, 'Settings', 30, '', 0, 1, '2017-01-15 04:36:08', 1, '2017-02-21 16:58:37', 1, NULL, NULL),
(7, 'Contact', 26, '', 0, 1, '2017-02-21 17:13:21', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `heng_us_resource`
--
DROP TABLE `heng_us_resource`

CREATE TABLE `heng_us_resource` (
  `Id` bigint(20) NOT NULL,
  `ResourceActionId` int(11) DEFAULT NULL,
  `ResourceControllerId` int(11) DEFAULT NULL,
  `OrderNum` int(11) DEFAULT '0',
  `IsActive` tinyint(1) DEFAULT '1',
  `IsDeleted` int(11) DEFAULT '0',
  `AddDate` datetime DEFAULT NULL,
  `AddUserId` int(11) DEFAULT NULL,
  `ModifyDate` datetime DEFAULT NULL,
  `ModifyUserId` int(11) DEFAULT NULL,
  `DeleteDate` datetime DEFAULT NULL,
  `DeleteUserId` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `heng_us_resource`
--

INSERT INTO `heng_us_resource` (`Id`, `ResourceActionId`, `ResourceControllerId`, `OrderNum`, `IsActive`, `IsDeleted`, `AddDate`, `AddUserId`, `ModifyDate`, `ModifyUserId`, `DeleteDate`, `DeleteUserId`) VALUES
(1, 1, 1, 5, 1, 0, '2017-01-15 05:04:07', 1, NULL, NULL, NULL, NULL),
(2, 2, 1, 10, 1, 0, '2017-01-15 05:05:02', 1, '2017-01-15 05:07:19', 1, NULL, NULL),
(3, 8, 1, 20, 1, 0, '2017-01-15 05:08:11', 1, NULL, NULL, NULL, NULL),
(4, 4, 1, 25, 1, 0, '2017-01-15 05:08:19', 1, NULL, NULL, NULL, NULL),
(5, 5, 1, 30, 1, 0, '2017-01-15 05:08:26', 1, NULL, NULL, NULL, NULL),
(6, 6, 1, 35, 1, 0, '2017-01-15 05:08:33', 1, NULL, NULL, NULL, NULL),
(7, 7, 1, 40, 1, 0, '2017-01-15 05:08:40', 1, NULL, NULL, NULL, NULL),
(8, 1, 2, 5, 1, 0, '2017-01-15 05:09:03', 1, NULL, NULL, NULL, NULL),
(9, 2, 2, 10, 1, 0, '2017-01-15 05:09:08', 1, NULL, NULL, NULL, NULL),
(10, 3, 2, 15, 1, 0, '2017-01-15 05:09:13', 1, NULL, NULL, NULL, NULL),
(11, 8, 2, 20, 1, 0, '2017-01-15 05:09:21', 1, NULL, NULL, NULL, NULL),
(12, 4, 2, 25, 1, 0, '2017-01-15 05:09:28', 1, NULL, NULL, NULL, NULL),
(13, 5, 2, 30, 1, 0, '2017-01-15 05:09:35', 1, NULL, NULL, NULL, NULL),
(14, 6, 2, 35, 1, 0, '2017-01-15 05:09:42', 1, NULL, NULL, NULL, NULL),
(15, 7, 2, 40, 1, 0, '2017-01-15 05:09:48', 1, NULL, NULL, NULL, NULL),
(16, 1, 3, 5, 0, 1, '2017-01-15 05:10:34', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(17, 2, 3, 10, 0, 1, '2017-01-15 05:10:41', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(18, 4, 3, 25, 0, 1, '2017-01-15 05:10:57', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(19, 1, 4, 5, 0, 1, '2017-01-15 05:14:57', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(20, 2, 4, 10, 0, 1, '2017-01-15 05:15:03', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(21, 4, 4, 25, 0, 1, '2017-01-15 05:15:15', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(22, 1, 5, 5, 0, 1, '2017-01-15 05:15:31', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(23, 2, 5, 10, 0, 1, '2017-01-15 05:15:37', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(24, 4, 5, 25, 0, 1, '2017-01-15 05:15:55', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(25, 1, 6, 5, 0, 1, '2017-01-15 05:16:34', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(26, 2, 6, 10, 0, 1, '2017-01-15 05:16:41', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(27, 4, 6, 25, 0, 1, '2017-01-15 05:17:01', 1, NULL, NULL, '2017-07-22 16:49:43', 1),
(28, 1, 7, 5, 1, 0, '2017-01-15 05:17:11', 1, NULL, NULL, NULL, NULL),
(29, 4, 7, 25, 1, 0, '2017-01-15 05:17:22', 1, NULL, NULL, NULL, NULL),
(30, 1, 8, 5, 1, 0, '2017-01-15 05:17:48', 1, NULL, NULL, NULL, NULL),
(31, 2, 8, 10, 1, 0, '2017-01-15 05:17:54', 1, NULL, NULL, NULL, NULL),
(32, 4, 8, 25, 1, 0, '2017-01-15 05:18:00', 1, NULL, NULL, NULL, NULL),
(33, 1, 15, 5, 1, 0, '2017-01-15 05:26:46', 1, NULL, NULL, NULL, NULL),
(34, 2, 15, 10, 1, 0, '2017-01-15 05:26:53', 1, NULL, NULL, NULL, NULL),
(35, 3, 15, 15, 1, 0, '2017-01-15 05:26:59', 1, NULL, NULL, NULL, NULL),
(36, 8, 15, 15, 1, 0, '2017-01-15 05:27:10', 1, NULL, NULL, NULL, NULL),
(37, 4, 15, 25, 1, 0, '2017-01-15 05:27:17', 1, NULL, NULL, NULL, NULL),
(38, 5, 15, 30, 1, 0, '2017-01-15 05:27:26', 1, NULL, NULL, NULL, NULL),
(39, 6, 15, 35, 1, 0, '2017-01-15 05:27:34', 1, NULL, NULL, NULL, NULL),
(40, 7, 15, 40, 1, 0, '2017-01-15 05:27:42', 1, NULL, NULL, NULL, NULL),
(41, 1, 9, 5, 1, 0, '2017-01-15 05:28:57', 1, NULL, NULL, NULL, NULL),
(42, 2, 9, 10, 1, 0, '2017-01-15 05:29:09', 1, NULL, NULL, NULL, NULL),
(43, 8, 9, 20, 1, 0, '2017-01-15 05:29:20', 1, NULL, NULL, NULL, NULL),
(44, 1, 10, 5, 1, 0, '2017-01-15 05:29:35', 1, NULL, NULL, NULL, NULL),
(45, 2, 10, 10, 1, 0, '2017-01-15 05:30:11', 1, NULL, NULL, NULL, NULL),
(46, 3, 10, 15, 1, 0, '2017-01-15 05:30:18', 1, NULL, NULL, NULL, NULL),
(47, 8, 10, 20, 1, 0, '2017-01-15 05:30:30', 1, NULL, NULL, NULL, NULL),
(48, 5, 10, 30, 1, 0, '2017-01-15 05:30:49', 1, NULL, NULL, NULL, NULL),
(49, 6, 10, 35, 1, 0, '2017-01-15 05:30:55', 1, NULL, NULL, NULL, NULL),
(50, 7, 10, 40, 1, 0, '2017-01-15 05:31:02', 1, NULL, NULL, NULL, NULL),
(51, 1, 11, 5, 1, 0, '2017-01-15 06:02:15', 1, NULL, NULL, NULL, NULL),
(52, 2, 11, 10, 1, 0, '2017-01-15 06:02:20', 1, NULL, NULL, NULL, NULL),
(53, 3, 11, 15, 1, 0, '2017-01-15 06:02:26', 1, NULL, NULL, NULL, NULL),
(54, 8, 11, 20, 1, 0, '2017-01-15 06:02:32', 1, NULL, NULL, NULL, NULL),
(55, 4, 11, 30, 1, 0, '2017-01-15 06:02:38', 1, NULL, NULL, NULL, NULL),
(56, 5, 11, 35, 1, 0, '2017-01-15 06:02:44', 1, NULL, NULL, NULL, NULL),
(57, 6, 11, 40, 1, 0, '2017-01-15 06:02:50', 1, NULL, NULL, NULL, NULL),
(58, 7, 11, 45, 1, 0, '2017-01-15 06:02:56', 1, NULL, NULL, NULL, NULL),
(59, 1, 12, 5, 1, 0, '2017-01-15 06:03:09', 1, NULL, NULL, NULL, NULL),
(60, 4, 12, 25, 1, 0, '2017-01-15 06:03:27', 1, NULL, NULL, NULL, NULL),
(61, 1, 13, 5, 1, 0, '2017-01-15 06:03:40', 1, NULL, NULL, NULL, NULL),
(62, 2, 13, 10, 1, 0, '2017-01-15 06:03:48', 1, NULL, NULL, NULL, NULL),
(63, 3, 13, 15, 1, 0, '2017-01-15 06:03:53', 1, NULL, NULL, NULL, NULL),
(64, 8, 13, 20, 1, 0, '2017-01-15 06:03:58', 1, NULL, NULL, NULL, NULL),
(65, 4, 13, 25, 0, 0, '2017-01-15 06:04:04', 1, NULL, NULL, NULL, NULL),
(66, 5, 13, 30, 1, 0, '2017-01-15 06:04:09', 1, NULL, NULL, NULL, NULL),
(67, 6, 13, 35, 1, 0, '2017-01-15 06:04:14', 1, NULL, NULL, NULL, NULL),
(68, 7, 13, 40, 1, 0, '2017-01-15 06:04:20', 1, NULL, NULL, NULL, NULL),
(69, 1, 14, 5, 1, 0, '2017-01-15 06:04:30', 1, NULL, NULL, NULL, NULL),
(70, 2, 14, 10, 1, 0, '2017-01-15 06:04:35', 1, NULL, NULL, NULL, NULL),
(71, 3, 14, 15, 1, 0, '2017-01-15 06:04:41', 1, NULL, NULL, NULL, NULL),
(72, 8, 14, 20, 1, 0, '2017-01-15 06:04:46', 1, NULL, NULL, NULL, NULL),
(73, 5, 14, 30, 1, 0, '2017-01-15 06:04:59', 1, NULL, NULL, NULL, NULL),
(74, 6, 14, 35, 1, 0, '2017-01-15 06:05:04', 1, NULL, NULL, NULL, NULL),
(75, 7, 14, 40, 1, 0, '2017-01-15 06:05:11', 1, NULL, NULL, NULL, NULL),
(76, 12, 16, 5, 1, 1, '2017-02-21 16:35:44', 1, NULL, NULL, '2017-07-22 16:52:02', 1),
(77, 13, 16, 10, 1, 1, '2017-02-21 16:35:54', 1, NULL, NULL, '2017-07-22 16:52:02', 1),
(78, 1, 17, 5, 1, 0, '2017-02-21 16:49:55', 1, NULL, NULL, NULL, NULL),
(79, 1, 18, 5, 1, 0, '2017-02-21 16:54:31', 1, NULL, NULL, NULL, NULL),
(80, 2, 18, 10, 1, 0, '2017-02-21 16:54:41', 1, NULL, NULL, NULL, NULL),
(81, 3, 18, 15, 1, 0, '2017-02-21 16:54:56', 1, NULL, NULL, NULL, NULL),
(82, 8, 18, 20, 1, 0, '2017-02-21 16:55:02', 1, NULL, NULL, NULL, NULL),
(83, 5, 18, 30, 1, 0, '2017-02-21 16:55:10', 1, NULL, NULL, NULL, NULL),
(84, 6, 18, 35, 1, 0, '2017-02-21 16:55:16', 1, NULL, NULL, NULL, NULL),
(85, 7, 18, 40, 1, 0, '2017-02-21 16:55:25', 1, NULL, NULL, NULL, NULL),
(86, 1, 19, 5, 1, 0, '2017-02-21 16:55:36', 1, NULL, NULL, NULL, NULL),
(87, 2, 19, 10, 1, 0, '2017-02-21 16:55:42', 1, NULL, NULL, NULL, NULL),
(88, 3, 19, 15, 1, 0, '2017-02-21 16:55:48', 1, NULL, NULL, NULL, NULL),
(89, 8, 19, 20, 1, 0, '2017-02-21 16:55:56', 1, NULL, NULL, NULL, NULL),
(90, 5, 19, 30, 1, 0, '2017-02-21 16:56:03', 1, NULL, NULL, NULL, NULL),
(91, 6, 19, 35, 1, 0, '2017-02-21 16:56:09', 1, NULL, NULL, NULL, NULL),
(92, 7, 19, 40, 1, 0, '2017-02-21 16:56:15', 1, NULL, NULL, NULL, NULL),
(93, 1, 20, 5, 1, 1, '2017-02-21 16:56:24', 1, NULL, NULL, '2017-07-22 16:50:35', 1),
(94, 2, 20, 10, 1, 1, '2017-02-21 16:56:29', 1, NULL, NULL, '2017-07-22 16:50:35', 1),
(95, 3, 20, 15, 1, 1, '2017-02-21 16:56:35', 1, NULL, NULL, '2017-07-22 16:50:35', 1),
(96, 8, 20, 20, 1, 1, '2017-02-21 16:56:41', 1, NULL, NULL, '2017-07-22 16:50:35', 1),
(97, 5, 20, 30, 1, 1, '2017-02-21 16:56:49', 1, NULL, NULL, '2017-07-22 16:50:35', 1),
(98, 6, 20, 35, 1, 1, '2017-02-21 16:56:55', 1, NULL, NULL, '2017-07-22 16:50:35', 1),
(99, 7, 20, 40, 1, 1, '2017-02-21 16:57:01', 1, NULL, NULL, '2017-07-22 16:50:35', 1),
(100, 1, 21, 5, 1, 0, '2017-02-21 16:57:08', 1, NULL, NULL, NULL, NULL),
(101, 2, 21, 10, 1, 0, '2017-02-21 16:57:13', 1, NULL, NULL, NULL, NULL),
(102, 3, 21, 15, 1, 0, '2017-02-21 16:57:19', 1, NULL, NULL, NULL, NULL),
(103, 8, 21, 20, 1, 0, '2017-02-21 16:57:27', 1, NULL, NULL, NULL, NULL),
(104, 5, 21, 30, 1, 0, '2017-02-21 16:57:34', 1, NULL, NULL, NULL, NULL),
(105, 6, 21, 35, 1, 0, '2017-02-21 16:57:40', 1, NULL, NULL, NULL, NULL),
(106, 7, 21, 40, 1, 0, '2017-02-21 16:57:48', 1, NULL, NULL, NULL, NULL),
(107, 1, 22, 5, 1, 0, '2017-02-21 17:14:30', 1, NULL, NULL, NULL, NULL),
(108, 14, 23, 5, 1, 0, '2017-02-21 17:18:51', 1, NULL, NULL, NULL, NULL),
(109, 8, 7, 15, 1, 0, '2017-07-22 17:41:11', 1, NULL, NULL, NULL, NULL),
(110, 2, 7, 10, 1, 0, '2017-08-06 21:17:00', 1, '2017-08-06 21:18:00', 1, NULL, NULL),
(111, 1, 24, 0, 1, 0, '2017-08-06 21:26:58', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `heng_us_resource_action`
--

DROP TABLE `heng_us_resource_action`

CREATE TABLE `heng_us_resource_action` (
  `Id` int(11) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Prefix` varchar(100) DEFAULT NULL,
  `OrderNum` int(11) DEFAULT NULL,
  `IsDeleted` int(11) DEFAULT '0',
  `IsActive` tinyint(1) DEFAULT '1',
  `AddDate` datetime DEFAULT NULL,
  `AddUserId` int(11) DEFAULT NULL,
  `ModifyDate` datetime DEFAULT NULL,
  `ModifyUserId` int(11) DEFAULT NULL,
  `DeleteUserId` int(11) DEFAULT NULL,
  `DeleteDate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `heng_us_resource_action`
--

INSERT INTO `heng_us_resource_action` (`Id`, `Name`, `Prefix`, `OrderNum`, `IsDeleted`, `IsActive`, `AddDate`, `AddUserId`, `ModifyDate`, `ModifyUserId`, `DeleteUserId`, `DeleteDate`) VALUES
(1, 'List', 'list', 1, 0, 1, '2016-12-30 01:51:08', 1, NULL, NULL, NULL, NULL),
(2, 'New', 'new', 2, 0, 1, '2016-12-30 01:51:17', 1, NULL, NULL, NULL, NULL),
(3, 'Copy', 'copy', 3, 0, 1, '2016-12-30 01:51:30', 1, NULL, NULL, NULL, NULL),
(4, 'View', 'view', 5, 0, 1, '2016-12-30 01:51:45', 1, '2016-12-30 03:54:09', 1, NULL, NULL),
(5, 'Deleted', 'deleteMultipleRecords', 6, 0, 1, '2016-12-30 01:52:28', 1, '2016-12-30 03:54:23', 1, NULL, NULL),
(6, 'Enabled', 'enableMultipleRecords', 7, 0, 1, '2016-12-30 01:52:46', 1, '2016-12-30 03:54:28', 1, NULL, NULL),
(7, 'Disabled', 'disableMultipleRecords', 8, 0, 1, '2016-12-30 01:53:03', 1, '2016-12-30 03:54:34', 1, NULL, NULL),
(8, 'Edit', 'exit', 4, 0, 1, '2016-12-30 03:53:58', 1, NULL, NULL, NULL, NULL),
(9, 'Invoice', 'invoice', 20, 0, 1, '2016-12-30 07:29:58', 1, NULL, NULL, NULL, NULL),
(10, 'Payment', 'payment', 25, 0, 1, '2017-01-11 10:15:24', 1, NULL, NULL, NULL, NULL),
(11, 'Receipt', 'receipt', 22, 0, 1, '2017-01-11 10:24:15', 1, NULL, NULL, NULL, NULL),
(12, 'Income', 'income', 30, 0, 1, '2017-02-21 16:31:50', 1, NULL, NULL, NULL, NULL),
(13, 'Outcome', 'outcome', 35, 0, 1, '2017-02-21 16:32:02', 1, NULL, NULL, NULL, NULL),
(14, 'Screen', 'screen', 40, 0, 1, '2017-02-21 17:16:11', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `heng_us_resource_controller`
--

DROP TABLE `heng_us_resource_controller`

CREATE TABLE `heng_us_resource_controller` (
  `Id` int(11) NOT NULL,
  `GroupResourceId` int(11) DEFAULT NULL,
  `GroupResourceName` varchar(255) DEFAULT NULL,
  `GroupResourceOrderNum` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `CustomController` varchar(255) DEFAULT NULL,
  `PrefixResource` varchar(255) DEFAULT NULL,
  `Note` varchar(255) DEFAULT NULL,
  `OrderNum` int(11) DEFAULT '0',
  `IsDeleted` int(11) DEFAULT '0',
  `IsActive` tinyint(1) DEFAULT '1',
  `AddDate` datetime DEFAULT NULL,
  `AddUserId` int(11) DEFAULT NULL,
  `ModifyDate` datetime DEFAULT NULL,
  `ModifyUserId` int(11) DEFAULT NULL,
  `DeleteUserId` int(11) DEFAULT NULL,
  `DeleteDate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `heng_us_resource_controller`
--

INSERT INTO `heng_us_resource_controller` (`Id`, `GroupResourceId`, `GroupResourceName`, `GroupResourceOrderNum`, `Name`, `CustomController`, `PrefixResource`, `Note`, `OrderNum`, `IsDeleted`, `IsActive`, `AddDate`, `AddUserId`, `ModifyDate`, `ModifyUserId`, `DeleteUserId`, `DeleteDate`) VALUES
(1, 1, 'User Management', 5, 'Users', 'User\\Controller\\UserController', 'user', NULL, 5, 0, 1, '2017-01-15 04:37:35', 1, NULL, NULL, NULL, NULL),
(2, 1, 'User Management', 5, 'User Role', 'User\\Controller\\RoleController', 'user-role', NULL, 10, 0, 1, '2017-01-15 04:38:01', 1, NULL, NULL, NULL, NULL),
(3, 2, 'Inventory Management', 10, 'Receive', 'Inventory\\Controller\\ItemReceiveController', 'item-receive', NULL, 25, 0, 1, '2017-01-15 04:39:43', 1, '2017-02-23 17:27:48', 1, NULL, NULL),
(4, 2, 'Inventory Management', 10, 'Adjustment', 'Inventory\\Controller\\ItemAdjustmentController', 'item-adjustment', NULL, 30, 0, 0, '2017-01-15 04:40:13', 1, '2017-02-23 17:27:59', 1, NULL, NULL),
(5, 2, 'Inventory Management', 10, 'Take', 'Inventory\\Controller\\ItemTakeController', 'item-take', NULL, 35, 0, 0, '2017-01-15 04:40:34', 1, '2017-02-23 17:28:08', 1, NULL, NULL),
(6, 2, 'Inventory Management', 10, 'Missing/Broken', 'Inventory\\Controller\\ItemMissingBrokenController', 'item-missing-broken', NULL, 40, 0, 0, '2017-01-15 04:41:30', 1, '2017-02-23 17:28:20', 1, NULL, NULL),
(7, 2, 'Inventory Management', 10, 'Stock Balance Master', 'Inventory\\Controller\\StockBalanceMasterController', 'stock-balance-master', NULL, 45, 0, 1, '2017-01-15 04:47:53', 1, NULL, NULL, NULL, NULL),
(8, 2, 'Inventory Management', 10, 'Sale Stock', 'Inventory\\Controller\\SaleStockController', 'sale-stock', NULL, 50, 0, 1, '2017-01-15 04:48:46', 1, NULL, NULL, NULL, NULL),
(9, 3, 'Accountant', 15, 'Sale Price', 'Accountant\\Controller\\SalePriceController', 'sale-price', NULL, 55, 0, 1, '2017-01-15 04:49:27', 1, NULL, NULL, NULL, NULL),
(10, 3, 'Accountant', 15, 'Discount', 'Accountant\\Controller\\DiscountController', 'discount', NULL, 60, 0, 1, '2017-01-15 04:50:13', 1, NULL, NULL, NULL, NULL),
(11, 4, 'Sale Management', 20, 'Sale Item', 'Sale\\Controller\\SaleItemController', 'sale-item', NULL, 65, 0, 1, '2017-01-15 04:53:03', 1, NULL, NULL, NULL, NULL),
(12, 4, 'Sale Management', 20, 'Sale', 'Sale\\Controller\\SaleController', 'sale', NULL, 70, 0, 1, '2017-01-15 04:53:33', 1, NULL, NULL, NULL, NULL),
(13, 4, 'Sale Management', 20, 'Sale Item Category', 'Sale\\Controller\\ItemCategoryController', 'sale-item-category', NULL, 74, 0, 1, '2017-01-15 04:55:43', 1, '2017-02-21 17:17:58', 1, NULL, NULL),
(14, 4, 'Sale Management', 20, 'Sale Item Type', 'Sale\\Controller\\ItemTypeController', 'sale-item-type', NULL, 75, 0, 1, '2017-01-15 04:56:10', 1, '2017-02-23 17:31:56', 1, NULL, NULL),
(15, 2, 'Inventory Management', 10, 'Item Master', 'Inventory\\Controller\\ItemMasterController', 'item-master', NULL, 80, 0, 1, '2017-01-15 04:57:13', 1, NULL, NULL, NULL, NULL),
(16, 5, 'Report', 25, 'Report', 'Inventory\\Controller\\ReportController', 'report', NULL, 85, 0, 1, '2017-02-21 16:34:22', 1, NULL, NULL, NULL, NULL),
(17, 2, 'Inventory Management', 10, 'Stock Alert', 'Inventory\\Controller\\StockAlertController', 'stock-alert', NULL, 90, 0, 1, '2017-02-21 16:49:17', 1, NULL, NULL, NULL, NULL),
(18, 6, 'Settings', 30, 'Supplier', 'Inventory\\Controller\\SupplierController', 'supplier', NULL, 95, 0, 1, '2017-02-21 16:51:59', 1, NULL, NULL, NULL, NULL),
(19, 6, 'Settings', 30, 'Item Category', 'Inventory\\Controller\\ItemCategoryController', 'item-category', NULL, 100, 0, 1, '2017-02-21 16:52:43', 1, NULL, NULL, NULL, NULL),
(20, 6, 'Settings', 30, 'Item Broken Reason', 'Inventory\\Controller\\ItemBrokenReasonController', 'item-broken-reason', NULL, 105, 0, 1, '2017-02-21 16:53:30', 1, NULL, NULL, NULL, NULL),
(21, 6, 'Settings', 30, 'Item Unit Measurement', 'Inventory\\Controller\\ItemUnitMeasurementController', 'item-unit-measurement', NULL, 110, 0, 1, '2017-02-21 16:53:59', 1, NULL, NULL, NULL, NULL),
(22, 7, 'Contact', 26, 'Contact', 'Contact\\Controller\\ContactController', 'contact', NULL, 5, 0, 1, '2017-02-21 17:14:03', 1, NULL, NULL, NULL, NULL),
(23, 4, 'Sale Management', 20, 'POS', 'Pos\\Controller\\PosController', 'pos-screen', NULL, 71, 0, 1, '2017-02-21 17:18:07', 1, NULL, NULL, NULL, NULL),
(24, 3, 'Accountant', 15, 'Income', 'Accountant\\Controller\\IncomeController', 'income', NULL, 0, 0, 1, '2017-08-06 21:20:52', 1, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `heng_us_group_resource`
--
ALTER TABLE `heng_us_group_resource`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `heng_us_resource`
--
ALTER TABLE `heng_us_resource`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `heng_us_resource_action`
--
ALTER TABLE `heng_us_resource_action`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `heng_us_resource_controller`
--
ALTER TABLE `heng_us_resource_controller`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `heng_us_group_resource`
--
ALTER TABLE `heng_us_group_resource`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `heng_us_resource`
--
ALTER TABLE `heng_us_resource`
  MODIFY `Id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `heng_us_resource_action`
--
ALTER TABLE `heng_us_resource_action`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `heng_us_resource_controller`
--
ALTER TABLE `heng_us_resource_controller`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
