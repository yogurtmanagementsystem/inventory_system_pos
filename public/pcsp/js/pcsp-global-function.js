var heightContentAptional = 0;

function printBlockContent(divContentToPrint,type)
{
  if('print' in window)
  {
    var contents = "";
    if( type*1 == 0 )
    {
      contents = document.getElementById(divContentToPrint).innerHTML;
    }
    else
    {
      contents = divContentToPrint;
    }

      var frame1 = $('<iframe />');
      frame1[0].name = "frame1";
      frame1.css({ "position": "absolute", "top": "-1000000px" });

      $("body").append(frame1);

      var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
      
      frameDoc.document.open();
      //Create a new HTML document.
      frameDoc.document.write('<html><head><title></title>');
      //Append the external CSS file.
      // frameDoc.document.write('<link rel="stylesheet" href="'+basePath+'/pcsp/css/printcss.css" />');
      frameDoc.document.write('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />');
      frameDoc.document.write('<link href="https://fonts.googleapis.com/css?family=Grand Hotel" rel="stylesheet"></link>');
      frameDoc.document.write('<style media="print">@page { size: auto;  margin: 0mm; } .pageA4 {padding: 0;margin: 0 auto;}</style>');
      frameDoc.document.write('</head><body><div class="pageA4">');

      //Append the DIV contents.
      frameDoc.document.write(contents);
      frameDoc.document.write('</div></body></html>');
      frameDoc.document.close();

      setTimeout(function () 
      {
          window.frames["frame1"].focus();
          window.frames["frame1"].print();
          frame1.remove();
      }, 500);
  }
}

function roundNumber(num, numFixed)
{
  try 
  {
    var number = num.toString().indexOf(".") != -1 ? num.toFixed(numFixed) : num;

    return number;
  }
  catch(err) 
  {
    return num;
  }
}
$('.panel-heading span.clickable').on("click", function (e) 
{
  if ($(this).hasClass('panel-collapsed')) 
  {
    // expand the panel
    $(this).parents('.panel').find('.panel-body').slideDown();
    $(this).removeClass('panel-collapsed');
    $(this).find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
  }
  else 
  {
    // collapse the panel
    $(this).parents('.panel').find('.panel-body').slideUp();
    $(this).addClass('panel-collapsed');
    $(this).find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
  }
});

function readImageURL(input, imgId) 
{
  if (input.files && input.files[0]) 
  {
    var reader = new FileReader();
    reader.onload = function (e)
    {
      $(imgId).attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function isAllowNumerice(event,e,type)
{
  var charCode = ( event.which ) ? event.which : event.keyCode;
  var bool = true;
  if( charCode != 8 )
  {
    if( type*1 == 1 )// allow enter integory
    {
      if ( charCode > 31 && ( charCode < 48 || charCode > 57 ) )
        bool = false;
    }
    else if( type*1 == 2 ) // allow enter float number
    {
      if ((event.which != 46 || $(e).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) 
      {
        bool = false;
      }
    }
  }

  return bool;
}

function isNumberKey(evt)
{
  if(evt.which == 0) return true;
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if(charCode == 46)
    return true;
  if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
  return true;
}

function runWaitMe(obj, effect){//none,bounce,rotateplane,stretch,orbit,roundBounce,win8,win8_linear,ios
  $(obj).waitMe({
    effect: effect,
    //text: 'Please waiting...',
    bg: 'rgba(255,255,255,0.7)',
    color:'#3C8DBC'
  });
}
function closeWaitMe(obj)
{
  $(obj).waitMe('hide');
}

