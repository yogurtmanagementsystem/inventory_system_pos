var jqxTheme = "darkblue";
var basePath = '/inventory_system_pos/public';

var activeInactiveList = [
			{ Id: "1", Name: "Active" },
            { Id: "0", Name: "Inactive" },
];
	
var activeRecord = function (row, datafield, value) 
{
  var str = "<div style='position:relative;text-align:center;top:30%;'>";

    str += value==0?'<i style="color: red;" class="glyphicon glyphicon-remove" title=""></i>':'<i style="color: #0099FF;" class="glyphicon glyphicon-ok" title=""></i>'

  str += "</div>";

    return str;
};

var numberRenderer = function (row, column, value, defaulthtml, properties, rowData)
{
    var html = $.parseHTML(defaulthtml);
    var cellDefaultMarginTop = $(html).css('margin-top');

  return '<div class="jqx-grid-cell-left-align" style="position:relative;text-align:center;margin-top:'+cellDefaultMarginTop+';">' + (1 + value) + '</div>';
};

function showMessage(title, msg, width, height)
{
  if($('#showMessage').length!=0){
    $('#showMessage').jqxWindow('destroy');
  }

    var x = ($(window).width())/ 2 + $(window).scrollLeft();
    var y = ($(window).height())/ 2 + $(window).scrollTop();

  var content = '<div>'+ msg +'</div>'+
          '<div>'+
          '<div style="position:absolute;left:0px;bottom:0px;height:28px;width:100%;"><div style="float: right;margin: 2px;">'+
              '<input type="button" id="showMessageOk" name="showMessageOk" value="OK" style="margin-right: 10px" />'+
          '</div></div>'+
          '</div>';
  $('body').append('<div id="showMessage"><div>'+title+'</div><div id="showMessageContent" style="margin-bottom:28px;">'+content+'</div></div>');
  $('#showMessage').css('visibility','visible');
  $('#showMessage').jqxWindow({theme: jqxTheme, width:width, height:height,
        resizable: false, isModal: true, modalOpacity: 0.3, zIndex:99999, 
        initContent: function () {
            $('#showMessageOk').jqxButton({ theme: jqxTheme, width: '65px' });
            $('#showMessage').focus();
            
            $('#showMessageOk').click(function(){
              $('#showMessage').jqxWindow('close');
            });
            $('#showMessage').on('close',function(){
              $('#showMessage').jqxWindow('destroy');
            });
        }
    });
};

function pageLoading(){
  closePageLoading();
  // var content = '<div>'+
  //         '<div style="margin: 0 auto;text-align:center;padding:3px;"><img src="'+basePath+'/images/icons/ajax_loader_blue.gif" > &nbsp;please wait...</div>'+
  //         '</div>';
  var content = '<div>'+
          '<div style="margin: 0 auto;text-align:center;padding:3px;"><i class="fa fa-spinner fa-pulse"></i> &nbsp;please wait...</div>'+
          '</div>';

  $('body').append('<div id="pageLoading"><div>Loading</div><div id="pageLoadingContent">'+content+'</div></div>');
  $('#pageLoading').css('visibility','visible');
  $('#pageLoading').jqxWindow({theme: jqxTheme, width:150, height:80, showCloseButton: false,resizable: false, isModal: true, modalOpacity: 0.1,
        initContent: function () {
            $('#pageLoading').focus();    
        }
    });
};

function closePageLoading(){
  if($('#pageLoading').length!=0){
    $('#pageLoading').jqxWindow('destroy');
  }
};

function createJqxWindowId(id){
  if($('#'+id).length>0){
    $('#'+id).jqxWindow('destroy');
  }
  $('body').append('<div id="'+id+'"><div>Loading</div><div class="window-magin-bottom"><div style="margin: 0 auto;text-align:center;padding:3px;"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i> Please wait...</div></div></div>');
};

function closeJqxWindowId(id){
  if($('#'+id).length!=0){
    $('#'+id).jqxWindow('destroy');
  }
};

//tab ajax function for set title content to specific tab
function tabAjaxDataLoading(url, id, title, content, index){
  $.ajax({
        type: 'post',
        url: url,
        data:{},
        beforeSend: function(){
          pageLoading();
        },
        //On success - adding the new tab with the loaded content
        success: function (data) {
          closePageLoading();
            var title = title;var content = data;
            $("#"+id).jqxTabs('setTitleAt', index, title);
            $("#"+id).jqxTabs('setContentAt', index, content);
            
        },
        //On error - alerting an error message
        error: function (errs) {//alert(jqxTheme);
          closePageLoading();
          showMessage('Message','Sorry this page is currenly not available. Please contact to administrator to check.','270','120');
            //alert('Error (probably the file you want to load is missing)!');
        }
    });
};

var jqxTreeDropDownList = function(jqxTheme, width, height, dropDownList, jqxTreeId, jsonList, hiddenId, hasRoot, hasMainRoot, mainRootText, isSelectFirstItem, isExpandAll){
  
  $(dropDownList).jqxDropDownButton('setContent', '');
    var jqxTree = jqxTreeId;
    var defaultHiddenVal = (hiddenId != null && hiddenId!="" ? $(hiddenId).val() : '');
    $(dropDownList).jqxDropDownButton({ theme: jqxTheme, width: width, height: 25});
    
    $(jqxTree).bind('select', function (event) {
        var args = event.args;
        var item = $(jqxTree).jqxTree('getItem', args.element);
        (hiddenId!=null && hiddenId!=""?$(hiddenId).val(item.id):'');
        var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + item.label + '</div>';
        $(dropDownList).jqxDropDownButton('setContent', dropDownContent);
        $(dropDownList).jqxDropDownButton('close'); 
    });
    
    var records = [];
    if(jsonList!=null && jsonList.length > 0){
      var source = jsonList;                                                                                                                
        var dataAdapter = new $.jqx.dataAdapter(source);
        dataAdapter.dataBind(); 
        records = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label'}]);
  }
    
    if(hasRoot===true){
      if(hasMainRoot!=null && hasMainRoot===true){
        if(mainRootText!=null && mainRootText!=''){
          records = [{ label: mainRootText, id:0, parentid:0, icon: basePath+"/images/icons/folder.png", expanded: false, items: records}];
        }else{
          records = [{ label: "Main Root", id:0, parentid:0, icon: basePath+"/images/icons/folder.png", expanded: false, items: records}];
        }
      }else{
        records = [{ label: " ", id:0, parentid:0, icon: basePath+"/images/icons/folder.png", expanded: false, items: records}];
      }
    }
    
    if(isExpandAll===true){
      $(jqxTree).jqxTree({ theme: jqxTheme, source: records, width: width-2, height: height}).jqxTree('expandAll');
    }else{
      $(jqxTree).jqxTree({ theme: jqxTheme, source: records, width: width-2, height: height}).jqxTree('collapseAll');
    }
   
    
    if(isSelectFirstItem===true){
      $(jqxTree).jqxTree('selectItem', $(jqxTree).find('li:first')[0]);
    }
    
    if(defaultHiddenVal != "" && defaultHiddenVal != "0"){
      var element = $(jqxTree).find('#'+defaultHiddenVal)[0];
        $(jqxTree).jqxTree('selectItem', element);
    } 
    
};

var clearTreeCheckbox = function (jqxTree, hiddenId){
   $(jqxTree).jqxTree('uncheckAll');
   $(jqxTree).jqxTree('selectItem', null); 
   (hiddenId!=null && hiddenId!=""?$(hiddenId).val(''):'');
};

var populateTreeCheckboxContent = function (event, jqxTree, dropDownList, hiddenId, isSelect){
  var args = event.args;
   var item = $(jqxTree).jqxTree('getItem', args.element);
   if(isSelect===true){
    $(jqxTree).jqxTree('checkItem', item, true);
     $(jqxTree).jqxTree('selectItem', null); 
   }
   
   var arrText = [];
  var items = $(jqxTree).jqxTree('getCheckedItems');
  if(items!=null && items.length > 0){
       $.each(items, function (index) {
        arrText.push(this.label);                         
       });
  }
  var text = arrText.join(',');
   
   
   (hiddenId!=null && hiddenId!=""?$(hiddenId).val(getChbTreeCheckItem(jqxTree)):'');
   var dropDownContent = '<div style="position: relative; margin-left: 3px; margin-top: 5px;">' + text + '</div>';
   $(dropDownList).jqxDropDownButton('setContent', dropDownContent); 
};

var jqxTreeDropDownListCheckbox = function(jqxTheme, width, height, dropDownList, jqxTreeId, jsonList, hiddenId, hasRoot, hasMainRoot, mainRootText,isSelectFirstItem, isExpandAll)
{
  $(dropDownList).jqxDropDownButton('setContent', '');
   var jqxTree = jqxTreeId;
   var defaultHiddenVal = (hiddenId != null && hiddenId!="" ? $(hiddenId).val() : '');
   $(dropDownList).jqxDropDownButton({ theme: jqxTheme, width: width, height: 25});
   
   $(jqxTree).bind('checkChange', function (event) {
    populateTreeCheckboxContent(event, jqxTree, dropDownList, hiddenId, false);
   });
   
   $(jqxTree).bind('select', function (event) {
    populateTreeCheckboxContent(event, jqxTree, dropDownList, hiddenId, true);
   });
   
   var records = [];
   if(jsonList!=null && jsonList.length > 0){
    var source = jsonList;                                                                                                                
       var dataAdapter = new $.jqx.dataAdapter(source);
       dataAdapter.dataBind(); 
       records = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label'}]);
  }
   
   if(hasRoot===true){
    if(hasMainRoot!=null && hasMainRoot===true){
      if(mainRootText!=null && mainRootText!=''){
        records = [{ label: mainRootText, id:0, parentid:0, icon: basePath+"/images/icons/folder.png", expanded: false, items: records}];
      }else{
        records = [{ label: "Main Root", id:0, parentid:0, icon: basePath+"/images/icons/folder.png", expanded: false, items: records}];
      }
    }else{
      records = [{ label: " ", id:0, parentid:0, icon: basePath+"/images/icons/folder.png", expanded: false, items: records}];
    }
   }
   
   if(isExpandAll===true){
    $(jqxTree).jqxTree({ hasThreeStates: true, checkboxes: true, theme: jqxTheme, source: records, width: width-2, height: height}).jqxTree('expandAll');
   }else{
    $(jqxTree).jqxTree({ hasThreeStates: true, checkboxes: true, theme: jqxTheme, source: records, width: width-2, height: height});//.jqxTree('expandAll');
   }
  
   
   if(isSelectFirstItem===true){
    $(jqxTree).jqxTree('selectItem', $(jqxTree).find('li:first')[0]);
   }
   
   if(defaultHiddenVal != "" && defaultHiddenVal != "0"){
      var element = $(jqxTree).find('#'+defaultHiddenVal)[0];
       $(jqxTree).jqxTree('selectItem', element);
   }
  
   if(defaultHiddenVal != "" && defaultHiddenVal != "0"){
    var arrId = defaultHiddenVal.split(",");
    for(i=0; i < arrId.length; i++){
      var element = $(jqxTree).find('#'+arrId[i])[0];
          $(jqxTree).jqxTree('selectItem', element);
    }
  }   
};
                                  
var initDropDownList = function(jqxTheme, width, height, jqxdropDownListId, source, displayField, valueField, hasDefaultItem, defaultItemText, defaultItemValue, hiddenFeildId){
  
  if(source==null || source.length <= 0){
    $(jqxdropDownListId).jqxDropDownList('selectIndex', 0);
    $(jqxdropDownListId).jqxDropDownList({filterable: false, selectedIndex: 0, source:[], width: width, dropDownHeight: 1, placeHolder: "", theme: jqxTheme}); 
    return false;
  }
  
  var dataAdapter = new $.jqx.dataAdapter(source,
        {
            beforeLoadComplete: function (records) {
              if(hasDefaultItem===true){
                var defaultItem = '{"'+valueField+'":"'+defaultItemValue+'","'+displayField+'":"'+defaultItemText+'"}';
                records.unshift(JSON.parse(defaultItem));
              }
                return records;
            }
        }
  );
  dataAdapter.dataBind(); 
   
  $(jqxdropDownListId).jqxDropDownList({
    filterable: true, selectedIndex: 0, source: dataAdapter, displayMember: displayField, valueMember: valueField, width: width, dropDownHeight: height, placeHolder: "", theme: jqxTheme
  });
  
  $(jqxdropDownListId).bind('select', function (event) {
        var item = event.args.item;
        ((hiddenFeildId!="" && item != null) ? $(hiddenFeildId).val(item.value) : '');
    });
  
  if(hiddenFeildId!="" && $(hiddenFeildId).val()!= ""){
      $(jqxdropDownListId).jqxDropDownList('selectItem', $(hiddenFeildId).val());
    }
  
};

var initDropDownListCategories = function(jqxTheme, width, height, jqxdropDownListId, source, displayField, valueField, hasDefaultItem, defaultItemText, defaultItemValue, hiddenFeildId){
  
  if(source==null || source.length <= 0){
    $(jqxdropDownListId).jqxDropDownList('selectIndex', 0);
    $(jqxdropDownListId).jqxDropDownList({filterable: false, source:[], width: width, dropDownHeight: 1, placeHolder: "", theme: jqxTheme}); 
    return false;
  }
  
  var dataAdapter = new $.jqx.dataAdapter(source,
        {
            beforeLoadComplete: function (records) {
              if(hasDefaultItem===true){
                var defaultItem = '{"'+valueField+'":"'+defaultItemValue+'","'+displayField+'":"'+defaultItemText+'", "group":" "}';
                records.unshift(JSON.parse(defaultItem));
              }
                return records;
            }
        }
  );
  dataAdapter.dataBind(); 
   
  $(jqxdropDownListId).jqxDropDownList({
    filterable: true, source: dataAdapter, displayMember: displayField, valueMember: valueField, width: width, dropDownHeight: height, placeHolder: "", theme: jqxTheme
  });
  
  $(jqxdropDownListId).bind('select', function (event) {
        var item = event.args.item;
        ((hiddenFeildId!="" && item != null) ? $(hiddenFeildId).val(item.value) : '');
        
    });
  
  if(hiddenFeildId!="" && $(hiddenFeildId).val()!= ""){
      $(jqxdropDownListId).jqxDropDownList('selectItem', $(hiddenFeildId).val());
    }
  
}

var initDropDownListCategoriesCheckbox = function(jqxTheme, width, height, jqxdropDownListId, source, displayField, valueField, hasSelectAll, defaultItemText, defaultItemValue, listIds){
  if(source==null || source.length <= 0){
    $(jqxdropDownListId).jqxDropDownList({ source:[], width: width, dropDownHeight: 1, placeHolder: "", theme: jqxTheme}); 
    return false;
  }
  
  
   
  $(jqxdropDownListId).jqxDropDownList({
    filterable: true, selectedIndex: 0, checkboxes: true, source: source, displayMember: displayField, valueMember: valueField, width: width, dropDownHeight: height, placeHolder: '', theme: jqxTheme
  });
  
  $(jqxdropDownListId).on('checkChange', function (event) {
        if (event.args) {
            var item = event.args.item;
            if (item) {
              if(item.value == 0 && item.checked){
                $(jqxdropDownListId).jqxDropDownList('checkAll');
                $(jqxdropDownListId).jqxDropDownList("close" );
              }else if (item.value == 0 && !item.checked){
                $(jqxdropDownListId).jqxDropDownList('uncheckAll');
                $(jqxdropDownListId).jqxDropDownList("close" );
              }
            }
        }
    });
  if(listIds!="" && listIds!="0"){
    var arrId = listIds.split(",");
    if(hasSelectAll === true){
      var items = $(jqxdropDownListId).jqxDropDownList('getItems');
      if(items.length == arrId.length+1){
        $(jqxdropDownListId).jqxDropDownList('checkItem', 0);
        return;
      }
    }
    
    for(i=0; i < arrId.length; i++){
      $(jqxdropDownListId).jqxDropDownList('checkItem', arrId[i]);
    }
    
  }
   
};

var initDropDownListCheckbox = function(jqxTheme, width, height, jqxdropDownListId, source, displayField, valueField, hasSelectAll, defaultItemText, defaultItemValue, listIds){

	if(source==null || source.length <= 0){
		$(jqxdropDownListId).jqxDropDownList('clear');
		$(jqxdropDownListId).jqxDropDownList({source:[], width: width, dropDownHeight: 1, autoDropDownHeight: true, placeHolder: "", theme: jqxTheme}); 
		return;
	}
  
	var dataAdapter = new $.jqx.dataAdapter(source,{
        autoBind: true
    });
    var uniqueRecords = dataAdapter.records;
	if(hasSelectAll===true){
		if(defaultItemText==""){
			defaultItemText = "(Select All)";
		}
		var defaultItem = '{"'+valueField+'":"'+defaultItemValue+'","'+displayField+'":"'+defaultItemText+'"}';
		uniqueRecords.splice(0, 0, JSON.parse(defaultItem));
	}
                
   
	$(jqxdropDownListId).jqxDropDownList({
		filterable: true, selectedIndex: 0, checkboxes: true, source: uniqueRecords, displayMember: displayField, valueMember: valueField, width: width, dropDownHeight: height, autoDropDownHeight: false, placeHolder: '', theme: jqxTheme
	});
  
	
	var handleCheckChange = true;
    $(jqxdropDownListId).on('checkChange', function (event) {
        if (!handleCheckChange)
            return;
        if (event.args.label != defaultItemText) {
            handleCheckChange = false;
            if(hasSelectAll===true) $(jqxdropDownListId).jqxDropDownList('checkIndex', 0);
            var checkedItems = $(jqxdropDownListId).jqxDropDownList('getCheckedItems');
            var items = $(jqxdropDownListId).jqxDropDownList('getItems');
            if (checkedItems.length == 1) {
                if(hasSelectAll===true) $(jqxdropDownListId).jqxDropDownList('uncheckIndex', 0);
            }
            else if (items.length != checkedItems.length) {
                if(hasSelectAll===true) $(jqxdropDownListId).jqxDropDownList('indeterminateIndex', 0);
            }
            handleCheckChange = true;
        }
        else {
            handleCheckChange = false;
            if (event.args.checked) {
                $(jqxdropDownListId).jqxDropDownList('checkAll');
            }
            else {
                $(jqxdropDownListId).jqxDropDownList('uncheckAll');
            }
                handleCheckChange = true;
        }
    });
	
	if(listIds!="" && listIds!="0"){
		var arrId = listIds.split(",");
		if(hasSelectAll === true){
			var items = $(jqxdropDownListId).jqxDropDownList('getItems');
			if(items.length == arrId.length+1){
				$(jqxdropDownListId).jqxDropDownList('checkItem', 0);
				return;
			}
		}	
    
		for(i=0; i < arrId.length; i++){
			$(jqxdropDownListId).jqxDropDownList('checkItem', arrId[i]);
		}
    
	}
   
};

function initialContent(divContentId, url, param){
  $(divContentId).html('');
  $.ajax({
        type: 'post',
        url: url,
        data:param,
        beforeSend: function(){
          pageLoading();
        },
        success: function (data) {
          closePageLoading();
            $(divContentId).html(data);
        },
        error: function (errs) {
          closePageLoading();
          showMessage('Error','Error Loading Content','270','120');
        }
    });
};

function jqxClearPrefixElement(id){
  if($('[id*='+id+']').length>0){
    //console.log($('[id*='+id+']').val());
    $('[id*='+id+']').remove();
  }
}

function jqxCalChangeAutoId(id, prefix){
  var ariaOwns = $('div[id="'+id+prefix+'"]').attr('aria-owns');
  var newAriaOwns = ariaOwns+prefix;
  
  $('div[id="'+id+prefix+'"]').attr('aria-owns', newAriaOwns);
  $('div[id='+ariaOwns+']').attr('id', newAriaOwns);
}

function removeJqxDivElement(pre, type){
  switch(type) {
      case 'listBox':
        $("div[id^=listBox]").filter(function(){ 
          var regexp= new RegExp('listBox(.*)'+pre+'(.*)');
          if(this.id.match(regexp)){
            //if(!this.id.match(/^listBoxgridpagerlistjqxgrid*/) && !this.id.match(/^listBoxContentinnerListBoxgridpagerlistjqxgrid*/)){
              $(this).jqxDropDownList('destroy'); 
              $(this).remove();
            //}
          }
        });
        break;
      case 'calendarjqxWidget':
        $("div[id^=calendarjqxWidget]").filter(function(){ 
          var regexp= new RegExp('calendarjqxWidget(.*)'+pre+'(.*)');
          if(this.id.match(regexp)){
            $(this).jqxDateTimeInput('destroy'); 
          $(this).remove();
          }
        });
        break;
      case 'dropDownButton':
        $("div[id^=dropDownButton]").filter(function(){ 
          var regexp= new RegExp('dropDownButton(.*)'+pre+'(.*)');
          if(this.id.match(regexp)){
            $(this).jqxDropDownButton('destroy'); 
          $(this).remove();
          }
        });
        break;  
      default:
          break;
  }
} 

function removeHiddenJqxDivElement(pre){
  removeJqxDivElement(pre, 'listBox');
  removeJqxDivElement(pre, 'calendarjqxWidget');
  removeJqxDivElement(pre, 'dropDownButton');
} 

function saveData(pre, nextUrl, divContentId, saveUrl, gridId, gridType, formId, hiddenField, isCloseAfterSave)
{
  var validationResult = function (isValid){
      if (isValid) {
          var json = ($(formId).serializeJSON());
          var data = $.param(json);
          $.ajax({
              type: 'post',
              dataType: 'json',
              url: saveUrl,
              data: data,
              beforeSend: function(){
                  pageLoading();
              },
              success: function (data) {
                  closePageLoading();
                  if(data.msg && data.msg!=''){
                      showMessage('Error',data.msg,'270','120');
                      return false;
                  }
                  if(hiddenField!=null && hiddenField!=""){
                      $(hiddenField).val(data.Id);
                  }

                  if(nextUrl!=""){

                      //removeHiddenJqxDivElement(pre);
                      jqxClearPrefixElement(pre);

                      initialContent(divContentId, nextUrl, data);
                  }
                  if( gridId != '' && $(gridId).length > 0 )
                  {
                      if(gridType==1){
                          $(gridId).jqxGrid({ autoshowloadelement: false});
                          $(gridId).jqxGrid('updatebounddata');
                          $(gridId).jqxGrid('clearselection');
                      }
                      else if(gridType==2){
                          $(gridId).jqxTreeGrid('updateBoundData');
                          $(gridId).jqxTreeGrid('selectRow', -1);
                      }

                  }

                  if(isCloseAfterSave!=null && isCloseAfterSave===true){
                      $("#jqxWindowPopUp"+pre).trigger('close');
                  }
              },
              error: function (errs) {
                  closePageLoading();
                  showMessage('Error','Error Save Data','270','120');
              }
          });
      }
  }

    $(formId).jqxValidator('validate', validationResult);
};

function saveDataWithAttachement(pre, nextUrl, divContentId, saveUrl, gridId, gridType, formId, hiddenField, isCloseAfterSave){

    var validationResult = function (isValid){
        if(isValid){
            var data = new window.FormData($(formId)[0]);

            $.ajax({
                type: "post",
                data: data,
                contentType: false,
                cache: false,
                processData:false,
                url: saveUrl,
                beforeSend: function(){
                    pageLoading();
                },
                success: function (data) {
                    closePageLoading();
                    if(data.msg && data.msg!=''){
                        showMessage('Error',data.msg,'270','120');
                        return false;
                    }
                    if(hiddenField!=null && hiddenField!=""){
                        $(hiddenField).val(data.Id);
                    }
                    if(nextUrl!="")
                    {
                        // removeHiddenJqxDivElement(pre);
                        jqxClearPrefixElement(pre);

                        initialContent(divContentId, nextUrl, data);
                    }
                    if(gridId!='')
                    {
                        if(gridType==1){
                            $(gridId).jqxGrid({ autoshowloadelement: true});
                            $(gridId).jqxGrid('updatebounddata');
                            $(gridId).jqxGrid('clearselection');
                        }
                        else if(gridType==2){
                            $(gridId).jqxTreeGrid('updateBoundData');
                            $(gridId).jqxTreeGrid('selectRow', -1);
                        }

                    }

                    if(isCloseAfterSave!=null && isCloseAfterSave===true)
                    {
                        $("#jqxWindowPopUp"+pre).trigger('close');
                    }
                },
                error: function (errs) {
                    closePageLoading();
                    showMessage('Error','Error Save Data','270','120');
                }
            });
        }
    }

    $(formId).jqxValidator('validate', validationResult);
};

function getChbCheckItem(ddlChbName){
  var arrId = [];
  var items = $(ddlChbName).jqxDropDownList('getCheckedItems');
  if(items!=null && items.length > 0){
        $.each(items, function (index) {
          arrId.push(this.value);                         
        });
  }
  return arrId.join(',');
};

function getChbTreeCheckItem(ddlChbName)
{
  var arrId = [];
  var items = $(ddlChbName).jqxTree('getCheckedItems');
  if(items!=null && items.length > 0){
        $.each(items, function (index) {
          arrId.push(this.id);                         
        });
  }
  return arrId.join(',');
};


function enableDisableGridRow(listId, gridId, gridType, url, errorTitle, errorMessage){
  var data = "listId=" + listId;
  $.ajax({
    type: "post",
        dataType: 'json',
        url: url,
        cache: false,
        data: data,
        beforeSend: function(){
          pageLoading();
        },
        success: function (data, status, xhr) {
          closePageLoading();
          if(gridType==1){
            $(gridId).jqxGrid({ autoshowloadelement: false});
            $(gridId).jqxGrid('updatebounddata');
                $(gridId).jqxGrid('clearselection');
          }
          else if(gridType==2){
            $(gridId).jqxTreeGrid('updateBoundData');
              $(gridId).jqxTreeGrid('selectRow', -1);
          }
        },
        error: function () {
          closePageLoading();
          showMessage(errorTitle,errorMessage, '300', '100');
        }
    });
};

function deleteGridRow(listId, gridId, gridType, url, errorTitle, errorMessage){
  var data = "listId=" + listId;
  $.ajax({
    type: "post",
        dataType: 'json',
        url: url,
        cache: false,
        data: data,
        beforeSend: function(){
          pageLoading();
        },
        success: function (data, status, xhr) {
          closePageLoading();
          if((data.countDeleted == 1) && (data.title!=null || data.msg!=null))
          {
            var width = '300';
            var height = '100';
            
              showMessage(data.title,data.msg, width, height);
              if(gridId == '')return;
              if(gridType==1){
                    $(gridId).jqxGrid('clearselection');
              }
              else if(gridType==2){
                  $(gridId).jqxTreeGrid('selectRow', -1);
              }
            return false;
          
          }
          else if((data.countDeleted == 2) && (data.title!=null || data.msg!=null)){
            showMessage(data.title,data.msg, '300', '100');
            if(gridId != '')
            {
              if(gridType==1){
                    $(gridId).jqxGrid('clearselection');
              }
              else if(gridType==2){
                  $(gridId).jqxTreeGrid('selectRow', -1);
              }
            }
          }
          if(gridId == '')return;
          if(gridType==1){
            $(gridId).jqxGrid({ autoshowloadelement: false});
            $(gridId).jqxGrid('updatebounddata');
                $(gridId).jqxGrid('clearselection');
          }
          else if(gridType==2){
            $(gridId).jqxTreeGrid('updateBoundData');
              $(gridId).jqxTreeGrid('selectRow', -1);
          }
        },
        error: function () {
          closePageLoading();
          showMessage(errorTitle,errorMessage, '300', '100');
        }
    });
};

function addNewRecord(pre, windowTitle, windowWidth, windowHeight, url, param, errorTitle, errorMessage) {
  
  windowWidth = parseInt(windowWidth);
  windowHeight = parseInt(windowHeight);
  var id = "jqxWindowPopUp"+pre;
  createJqxWindowId(id);
  var x = ($(window).width() - windowWidth)/ 2 + $(window).scrollLeft();
  var y = ($(window).height() - windowHeight)/ 2 + $(window).scrollTop();

  // $('#'+id).jqxWindow({ theme: jqxTheme,position: { x: x, y: y}, maxWidth: windowWidth, minWidth: windowWidth, Height: windowHeight, resizable: true, isModal: true, modalOpacity: 0.3,
   
  $('#'+id).jqxWindow({ theme: jqxTheme,maxWidth: windowWidth, minWidth: windowWidth, height: windowHeight, width: windowWidth, position: { x: x, y: y}, resizable: true, isModal: true, modalOpacity: 0.3,
   
   initContent: function(){
      $('#'+id).jqxWindow('setTitle',windowTitle);
      $.ajax({
                type: 'post',
                url: url,
                data: param,
                success: function (data) {
                    $('#'+id).jqxWindow('setContent',data);
                },
                error: function (errs) {
                  showMessage(errorTitle,errorMessage,'270','120');
                }
            });
      
      $('#'+id).on('close',function(){
        $('#'+id).jqxWindow('destroy');
        jqxClearPrefixElement(pre);
      });
    }
  });

};

function newPopupWizardWindow(pre, windowTitle, windowWidth, windowHeight, urlWizardScreen, divContentWizardId, urlFirstContent, param, errorTitle, errorMessage) {
  
  windowWidth = parseInt(windowWidth);
  windowHeight = parseInt(windowHeight);
  var id = "jqxWindowPopUp"+pre;
  createJqxWindowId(id);
  $('#'+id).jqxWindow({ theme: jqxTheme, maxWidth: windowWidth, minWidth: windowWidth, minHeight: windowHeight, maxHeight: windowHeight, height: windowHeight, width: windowWidth, resizable: true, isModal: false, modalOpacity: 0.3,
    initContent: function(){
      $('#'+id).jqxWindow('setTitle',windowTitle);
      $.ajax({
                type: 'post',
                url: urlWizardScreen,
                data:param,
                success: function (data) {
                    $('#'+id).jqxWindow('setContent',data);
                    if(divContentWizardId != '')
                    {
                      initialContent(divContentWizardId, urlFirstContent, param);
                    }
                },
                error: function (errs) {
                  showMessage(errorTitle,errorMessage,'270','120');
                }
            });
      
      $('#'+id).on('close',function(){
        //removeHiddenJqxDivElement(pre);
        $('#'+id).jqxWindow('destroy');
        jqxClearPrefixElement(pre);
      });
    }
  });
};

function refreshDropDownList(ddlIdName, hiddenField, displayFieldName, valueFieldName, url, param, width, height) 
{
  $(ddlIdName).jqxDropDownList('selectIndex', 0 ); 
  $.ajax({
        type: 'post',
        datatype: 'json',
        url: url,
        data: param,
        beforeSend: function(){
            pageLoading();
        },
        success: function (data) 
        {
              initDropDownList(jqxTheme, width, height, ddlIdName, data, displayFieldName, valueFieldName, true, '', '0', hiddenField);
              closePageLoading();
        },
        error: function (errs) 
        {
            closePageLoading();
            showMessage('Error','Error Loading data','270','120');
         }
    });
  
};

function refreshDropDownListCategory(width, height, url, param, ddlIdName, displayField, valueField, hasDefaultItem, defaultItemText, defaultItemValue, hiddenFeildId){
  
  $(ddlIdName).jqxDropDownList('selectIndex', 0 ); 
  $.ajax({
        type: 'post',
        datatype: 'json',
        url: url,
        data: param,
        beforeSend: function(){
            pageLoading();
        },
        success: function (data) 
        {
          initDropDownListCategories(jqxTheme, width, height, ddlIdName, data, displayField, valueField, hasDefaultItem, defaultItemText, defaultItemValue, hiddenFeildId);
            closePageLoading();
        },
        error: function (errs) {
            closePageLoading();
            showMessage('Error','Error Loading data','270','120');
         }
    });
  
};

function refreshDropDownListCheckbox(width, height, url, param, ddlIdName, displayField, valueField, hasSelectAll, defaultItemText, defaultItemValue, listIds){
  
  $(ddlIdName).jqxDropDownList('selectIndex', 0 ); 
  $.ajax({
        type: 'post',
        datatype: 'json',
        url: url,
        data: param,
        beforeSend: function(){
            pageLoading();
        },
        success: function (data) 
        {
              initDropDownListCheckbox(jqxTheme, width, height, ddlIdName, data, displayField, valueField, hasSelectAll, defaultItemText, defaultItemValue, listIds);
              closePageLoading();
        },
        error: function (errs) {
            closePageLoading();
            showMessage('Error','Error Loading data','270','120');
         }
    });
  
};

function refreshTreeDropDownList(ddlIdName, ddlTreeIdName, hiddenField, url, param, hasRoot, hasMainRoot, mainRootText,isSelectFirstItem, isExpandAll,width,height) 
{
  
  $(ddlTreeIdName).jqxTree('selectItem', $(ddlTreeIdName).find('li:first')[0]);
  $.ajax({
        type: 'post',
        datatype: 'json',
        url: url,
        data: param,
        beforeSend: function(){
            pageLoading();
        },
        success: function (data) 
        {
            jqxTreeDropDownList(jqxTheme, width, height, ddlIdName, ddlTreeIdName, data, hiddenField, hasRoot, hasMainRoot, mainRootText, isSelectFirstItem, isExpandAll);
              closePageLoading();
        },
        error: function (errs) {
            closePageLoading();
            showMessage('Error','Error Loading data','270','120');
         }
    });
  
};

function refreshTreeDropDownListCheckbox(ddlIdName, ddlTreeIdName, hiddenField, url, param, hasRoot, hasMainRoot, mainRootText,isSelectFirstItem, isExpandAll,width,height) 
{
  $.ajax({
        type: 'post',
        datatype: 'json',
        url: url,
        data: param,
        beforeSend: function(){
            pageLoading();
        },
        success: function (data) 
        {
            jqxTreeDropDownListCheckbox(jqxTheme, width, height, ddlIdName, ddlTreeIdName, data, hiddenField, hasRoot, hasMainRoot, mainRootText, isSelectFirstItem, isExpandAll);
              closePageLoading();
        },
        error: function (errs) {
            closePageLoading();
            showMessage('Error','Error Loading data','270','120');
         }
    });
  
};

function refreshContentData(windowIdName, url, param) {
  
  $(windowIdName).jqxWindow('setContent','');
  $.ajax({
    type: "post",
        url: url,
        data: param,
        cache: false,
        beforeSend: function(){
          pageLoading();
        },
        success: function (data) {
          $(windowIdName).jqxWindow('setContent',data);
          closePageLoading();
        },
        error: function (errs) {
          closePageLoading();
           showMessage('Error','Error Loading data','270','120');
        }
    });
  
};

function getJqxGridDataSource(getGridDataUrl, gridIdName, param)
{
  var source = {
      type: "post",
      datatype: "json",
      cache: false,
      id: 'Id',
      data: param,
      url: getGridDataUrl,
      root: 'rows',
      beforeprocessing: function(data)
      {
		 //source.data = [];
         source.totalrecords = (data != null)? data[0].total:0;
      },
      filter: function()
      {
        $(gridIdName).jqxGrid('updatebounddata', 'filter');
      },
      sort: function()
      {
        $(gridIdName).jqxGrid('updatebounddata', 'sort');
      },
  };
  //console.log(source);
  return source;
};

function getJqxTreeGridDataSource(getTreeGridDataUrl, treeGridIdName, param){

  var source = {
      type: "post",
      datatype: "json",
      cache: false,
      data: param,
      hierarchy:
        {
        keyDataField: { name: 'Id' },
        parentDataField: { name: 'ParentId' },
           
        },
      id: 'Id',
      url: getTreeGridDataUrl,
  };
  return source;
};

function getSelectedSingleRowGrid(gridType, gridIdName, selectRowMessage, selectOnlyOneRowMessage){
	
	var row = "";
	
	if(gridType*1==2){ // jqxTreeGrid
		
		var jqxrow = $(gridIdName).jqxTreeGrid('getSelection');
        var jqxdatarow = jqxrow[0];
        if(jqxdatarow==null)
        {
            showMessage('Warning', selectRowMessage, '300', '100');
            return false;
        }
		row = jqxdatarow;
		
	}else{ // jqxGrid
		
		var selectedRows = $(gridIdName).jqxGrid('getselectedrowindexes');
        if(selectedRows.length==0){
            showMessage('Warning', selectRowMessage, '300', '100');
            return false;
        }else if(selectedRows.length > 1){
            showMessage('Warning', selectOnlyOneRowMessage, '300', '100');
            return false;
        }else if(selectedRows.length == 1){
            var jqxdatarow = $(gridIdName).jqxGrid('getrowdata', selectedRows[0]);
			row = jqxdatarow;
        }
	}
	return row;
};

function getSelectedMultipleRowGrid(gridType, gridIdName, selectRowMessage, selectOnlyOneRowMessage){
	
	var rows = "";
	
	if(gridType==2){ // jqxTreeGrid
		
		var jqxrow = $(gridIdName).jqxTreeGrid('getSelection');
        var jqxdatarow = jqxrow[0];
        if(jqxdatarow==null)
        {
            showMessage('Warning', selectRowMessage, '300', '100');
            return false;
        }
		rows = jqxdatarow;
		
	}else{ // jqxGrid
		
		var selectedRows = $(gridIdName).jqxGrid('getselectedrowindexes');
        if(selectedRows.length==0){
            showMessage('Warning', selectRowMessage, '300', '100');
            return false;
        }
		rows= selectedRows;
	}
	return rows;
}

//=======Clear Controls Function========================
function clearDropDownListCheckbox(ddlChbIdName){
	$(ddlChbIdName).jqxDropDownList('uncheckAll');
}

function clearDropDownList(ddlIdName){
	$(ddlIdName).jqxDropDownList({selectedIndex: 0 });
}

function clearDate(txtDateIdName){
	$(txtDateIdName).jqxDateTimeInput('setDate', null);
}

function clearTreeDropDownListCheckbox(ddlTreeIdName)
{
  $(ddlTreeIdName).jqxTree("uncheckAll");
}

function clearTreeDropDownList(ddlTreeIdName)
{
  $(ddlTreeIdName).jqxTree('selectItem', $(ddlTreeIdName).find('li:first')[0]);
}

 var initGridDropDownListCheckbox = function(jqxdropDownListId, source, displayField, valueField)
{
    if(source==null || source.length <= 0)
    {
        $(jqxdropDownListId).jqxDropDownList({source:[]});
        return;
    }
  
    var dataAdapter = new $.jqx.dataAdapter(source);        
   
    $(jqxdropDownListId).jqxDropDownList
    ({
        filterable: true, selectedIndex: 0, checkboxes: true, source: dataAdapter, displayMember: displayField, valueMember: valueField
    });
};

var initGridFilterItems = function(source)
{
    if(source == null || source.length <= 0 )
    {
        return [];
    }
    var dataAdapter = new $.jqx.dataAdapter(source);

    return dataAdapter;
}
