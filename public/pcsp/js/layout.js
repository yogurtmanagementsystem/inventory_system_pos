
$(document).ready(function()
{
	initContent();
});

//content init
var initContent = function()
{
	var screenHeight = $(window).height();
	var contentCustomHeaderHeight = $(".content-custom-header").length>0? $(".content-custom-header").height()*1+35 : 0;
	var respAdvanceSearchHeight = $(".respAdvanceSearch").length>0? $(".respAdvanceSearch").height()*1 : 0;
	var respActionGridHeight = $(".respActionGrid").length>0? $('.respActionGrid').height()*1 : 0;
	
	var contentHeight = (screenHeight-180)-contentCustomHeaderHeight-respAdvanceSearchHeight-respActionGridHeight+heightContentAptional*1;
		//contentHeight = contentHeight + "px";
	$(".respContent").height(contentHeight);
	$(".respContent").css({"background-color":"transparent", "min-height": "300px"});
}
