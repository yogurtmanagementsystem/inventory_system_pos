function multiUploader(config){
  
	this.config = config;
	this.items = "";
	this.all = [];
	var self = this;
	
	multiUploader.prototype._init = function(){
		if (window.File && 
			window.FileReader && 
			window.FileList && 
			window.Blob) {		
			 var inputId = $("#"+this.config.form).find("input[type='file']").eq(0).attr("id");
			 document.getElementById(inputId).addEventListener("change", this._read, false);
			 document.getElementById(this.config.dragArea).addEventListener("dragover", function(e){ e.stopPropagation(); e.preventDefault(); }, false);
			 document.getElementById(this.config.dragArea).addEventListener("drop", this._dropFiles, false);
			 document.getElementById(this.config.form).addEventListener("submit", this._submit, false);
		} else
			console.log("Browser supports failed");
	}
	
	multiUploader.prototype._submit = function(e){
		e.stopPropagation(); e.preventDefault();
		self._startUpload();
	}
	
	multiUploader.prototype._preview = function(data){
		this.items = data;
		if(this.items.length > 0){
			var html = "";		
			var uId = "";
 			for(var i = 0; i<this.items.length; i++){
				uId = this.items[i].name._unique();
				var sampleIcon = '<img src="'+this.config.basePath+'/images/uploads/image.png" />';
				var errorClass = "";
				if(typeof this.items[i] != undefined){
					if(self._validate(this.items[i].type) <= 0) {
						sampleIcon = '<img src="'+this.config.basePath+'/images/uploads/unknown.png" />';
						errorClass =" invalid";
					} 
					html += '<div class="dfiles'+errorClass+'" rel="'+uId+'"><h5>'+sampleIcon+this.items[i].name+'</h5><div id="'+uId+'" class="progress" style="display:none;"><img src="'+this.config.basePath+'/images/uploads/ajax-loader.gif" /></div></div>';
				}
			}
			$("#dragAndDropFiles").append(html);
		}
	}

	multiUploader.prototype._read = function(evt){
		if(evt.target.files){
			self.all = [];$("#dragAndDropFiles").html('');
			self._preview(evt.target.files);
			self.all.push(evt.target.files);
		} else 
			console.log("Failed file reading");
	}
	
	multiUploader.prototype._validate = function(format){
		var arr = this.config.support.split(",");
		return arr.indexOf(format);
	}
	
	multiUploader.prototype._dropFiles = function(e){
		e.stopPropagation(); e.preventDefault();
		self.all = [];$("#dragAndDropFiles").html('');
		self._preview(e.dataTransfer.files);
		self.all.push(e.dataTransfer.files);
	}
	
	multiUploader.prototype._uploader = function(file,f){
		if(typeof file[f] != undefined && self._validate(file[f].type) > 0){
			var data = new FormData();
			var ids = file[f].name._unique();
			data.append('file',file[f]);
			data.append('index',ids);
			data.append('testTakingId',this.config.testTakingId);
			$(".dfiles[rel='"+ids+"']").find(".progress").show();
			$.ajax({
				type:"POST",
				url:this.config.uploadUrl,
				data:data,
				cache: false,
				contentType: false,
				processData: false,
				success:function(rponse){
					$("#"+ids).hide();
					var obj = $(".dfiles").get();
					$.each(obj,function(k,fle){
						if($(fle).attr("rel") == rponse.index){
							$(fle).slideUp("normal", function(){ $(this).remove(); });
							addFileUploadToList(rponse.fileTestRecord);
						}else{
							if(rponse.index=='') {
								$(".dfiles[rel='" + ids + "']").find(".progress").hide();
								self._addMessageValidator(ids, rponse.msg);
							}
						}
					});
					if (f+1 < file.length) {
						self._uploader(file,f+1);
					}

				}
			});
		} else {
			//var data = new FormData();
			//var ids = file[f].name._unique();

			self._addMessageValidator(ids, '(Invalid  file format)');
			console.log("Invalid file format - "+file[f].name);
		}
	}
	
	multiUploader.prototype._startUpload = function(){
		if(this.all.length > 0){
			for(var k=0; k<this.all.length; k++){
				var file = this.all[k];
				this._uploader(file,0);
			}
		}
		this.all = [];
	}

	multiUploader.prototype._addMessageValidator = function(ids, msg) {
		if ($(".invalidFormat[rel='" + ids + "']").length > 0) $(".invalidFormat[rel='" + ids + "']").remove();
		$(".dfiles[rel='" + ids + "'] h5").append('<span rel="' + ids + '" class="invalidFormat"> <a href="javascript:void(0)" onclick="removeInvalidFileFormat(\'' + ids + '\')" class="label label-danger small">x</a><br><span class="small" style="color:red"> '+msg+'</span></span>');
	}

	String.prototype._unique = function(){
		return this.replace(/[a-zA-Z]/g, function(c){
     	   return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
    	});
	}

	this._init();
}

function initMultiUploader(){
	new multiUploader(config);
}

var removeInvalidFileFormat = function(ids){
	$(".dfiles[rel='"+ids+"']").slideUp("normal", function(){ $(this).remove(); });
};

var addFileUploadToList = function(row){
	var url = this.config.basePath+'/resource/test-taking/'+row.SystemFileName;

	var html = '<div class="pull-left" id="testFileRecords[]">'+
					'<div class="alert alert-success" style="padding: 0px 3px;">'+
						'<a id="downloadFileTest[]" href="javascript:void(0)" title="download" onclick="downloadFileTest(\''+url+'\', \''+row.FileName+'\');">'+
						'<strong style="text-decoration: underline; padding-right: 10px;">'+row.FileName+'</strong>'+
						'</a>'+
						'<a id="removeFileTest[]" href="javascript:void(0)" title="delete" onclick="removeFileTest(this, '+row.Id+');"> <i class="fa fa-trash"></i> </a>'+
					'</div>'+
				'</div>';
	$(".fileTestList").slideDown('slow', function(){$(this).append(html)});
}

var downloadFileTest = function(filePath, fileName){
	var url = filePath;
	var anchor = document.createElement('a');
	anchor.setAttribute('href', url);
	anchor.setAttribute('download', fileName);


	$(anchor)[0].click();

	var ev = document.createEvent("MouseEvents");
	ev.initMouseEvent("click", true, false, self, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

	anchor.dispatchEvent(ev);
}

var removeFileTest = function(obj, id){
	var ind = $('[id="'+obj.id+'"]').index(obj)*1;


	var confirm = window.confirm('Are you sure to delete this file?');

	if(confirm){
		var params = {};
			params.Id = id;
		$.ajax({
			type: "post",
			dataType: 'json',
			url: this.config.baseUrl+'/deleteFileTest',
			cache: false,
			data: params,
			beforeSend: function(){
				runWaitMe('.fileTestLoading','bounce');
			},
			success: function (data, status, xhr) {
				if(data.status==true)
					$($('[id="testFileRecords[]"')[ind]).slideUp('normal').remove();
				closeWaitMe('.fileTestLoading');
			},
			error: function () {
				closeWaitMe('.fileTestLoading');
				showMessage('Warning','Delete failed.', '300', '100');
			}
		});
	}
}