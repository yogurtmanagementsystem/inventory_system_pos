<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
use PCSPLib\PHPConstant;
use PCSPLib\PCSPCache;
return [
    'db' => [
        'driver' => 'Pdo',
        // 'dsn'	 =>'mysql:dbname=inventory_system_pos_db;host=localhost',
        'dsn'	 =>'mysql:dbname='.PHPConstant::DB_NAME.';host=localhost',
    ],
	
	'service_manager' => [
        /* 'factories' => [
            'dbAdapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ], */
			'factories' => [
				'ZendCacheStorageFactory' => function () {
					return \Zend\Cache\StorageFactory::factory(array(
						'adapter' => array(
							'name' => 'filesystem',
							'options' => array(
							'cache_dir' => 'data/cache',
							),
						),
						/* 'plugins' => array(
							'exception_handler' => array(
								'throw_exceptions' => false
							),
							'Serializer', // dont forget this one as on my example im caching an array
						), */
					));
				},
			],
			'aliases' => [
					'cache' => 'ZendCacheStorageFactory'
			]
    ],
	
	'view_manager' => [
		'strategies' => [
                'ViewJsonStrategy',
        ],
    ],
    
	/* ======== For Translation ================*/
	'view_helpers' => [
		'invokables' => [
			'translate' => \Zend\I18n\View\Helper\Translate::class,
			'pcsp-cache'=> PCSPCache::class
		]
	],
	/* ======== For Translation ================*/
	
];
