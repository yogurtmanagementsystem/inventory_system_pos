<?php
namespace PCSPLib\BaseClasses;

use InvalidArgumentException;
use RuntimeException;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\Adapter\Driver\Pdo\Result;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Update;
use Zend\Db\Sql\Delete;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Hydrator\HydratorInterface;
use Zend\Authentication\AuthenticationService;

use PCSPLib\BaseClasses\BaseRepositoryInterface;

class BaseRepository implements BaseRepositoryInterface
{
    protected $tableName;
    protected $dbAdapter;
	protected $hydrator;
	protected $objectPrototype;
    protected $sql;
    protected $userLogin;
    protected $userLoginId;

    /**
     * @param AdapterInterface $adaperInterface
     */
    public function __construct(AdapterInterface $dbAdapter, $tableName, HydratorInterface $hydrator, $objectPrototype)
    {
        $this->dbAdapter = $dbAdapter;
        $this->sql = new Sql($dbAdapter);
		$this->tableName = $tableName;
		$this->hydrator      = $hydrator;
        $this->objectPrototype = $objectPrototype;
		
        $auth   =   new AuthenticationService();
        if($auth->hasIdentity())
        {
            $this->userLogin = $auth->getIdentity();
        }
        if(isset($this->userLogin->Id))
        {
            $this->userLoginId = ($this->userLogin!=null)?$this->userLogin->Id:0;
        }
    }

    public function getTableObject()
    {
        return $this->objectPrototype;
    }

    public function getTableName()
    {
        return $this->tableName;
    }
	
	public function getDBAdaper()
    {
        return $this->dbAdapter;
    }

    public function saveRecord($condition, $data)
    {
    	try
        {
            $id = $condition;

            if(!is_array($condition) && $condition != 0 )
            {
                $condition = array("Id"=>(int)$condition);
            }
            if(isset($data['Password']))
            {
                if(empty($data['Password']))
                {
                    unset($data['Password']);
                }
            }
            if(isset($data['Id']))unset($data['Id']);

            if (!is_array($condition))
            {
                $data['AddDate'] = date("Y-m-d H:i:s");
                $data['AddUserId'] = $this->userLoginId;
                $insert = new Insert($this->tableName);
                $insert->values($data);

                $statement = $this->sql->prepareStatementForSqlObject($insert);
                $result = $statement->execute();

                if(!$result instanceof ResultInterface )
                {
                    throw new RuntimeException("Database error occurred during item insert operation");
                }
                $id = $this->getDBAdaper()->getDriver()->getLastGeneratedValue();
            }
            else
            {
                $data['ModifyDate']    = date("Y-m-d H:i:s");
                $data['ModifyUserId']  = $this->userLoginId;
                unset($data["AddUserId"]);
                unset($data["AddDate"]);

                $update = new Update($this->tableName);
                $update->set($data);
                $update->where($condition);
                $statement = $this->sql->prepareStatementForSqlObject($update);
                $result = $statement->execute();
                if (! $result instanceof ResultInterface)
                {
                    throw new RuntimeException('Database error occurred during item update operation');
                }
            }

            return $id;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function getRecord($condition, $columns = array(),$orderField = "",$orderType = "",$groupField = "" )
    {
        if(is_numeric($condition))
        {
            $condition = array("Id"=>$condition);
        }
        try
        {
            $select    = $this->sql->select($this->tableName);
            if(count($columns)>0)
                $select->columns($columns);
            $select->where(["IsDeleted"=>0]);
            $select->where($condition);

            $stmt   = $this->sql->prepareStatementForSqlObject($select);

            $result = $stmt->execute();

            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                throw new RuntimeException(sprintf(
                    'Failed retrieving item with identifier "%s"; unknown database error.', serialize($condition)
                ));
            }

            $resultSet = new HydratingResultSet($this->hydrator, $this->objectPrototype);
            $resultSet->initialize($result);
            $item = $resultSet->current();

            if (!$item)
            {
                return $this->objectPrototype;
            }
            
            return $item;
        }
        catch (\Exception $e)
        {
             die($e->getMessage());exit();
        }
    }

    public function getAllRecords($condition = array(), $columns = array(), $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        try
        {
            $select = $this->sql->select($this->tableName);

            if(count($columns))
                $select->columns($columns);

            if( count( $condition ) )
                $select->where($condition);

            $select->where(["IsDeleted"=>0])->order($orderField." ".$orderType);
            
            if(!empty($groupField))
            {
                $select->group($groupField);
            }
            $stmt   = $this->sql->prepareStatementForSqlObject($select);

            $result = $stmt->execute();

            $resultSet = new HydratingResultSet($this->hydrator, $this->objectPrototype);
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function getAllActiveRecords($condition = "",$columns = array(), $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        try
        {
            $select = $this->sql->select($this->tableName);
            if(count($columns))
                $select->columns($columns);
            $select->where(["IsDeleted"=>0,"IsActive"=>1]);
            if(!empty($condition))
                $select->where($condition);
            $select->order($orderField." ".$orderType);

            if(!empty($groupField))
            {
                $select->group($groupField);
            }
            $stmt   = $this->sql->prepareStatementForSqlObject($select);

            $result = $stmt->execute();

            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                return [];
            }

            $resultSet = new HydratingResultSet($this->hydrator, $this->objectPrototype);
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
    
    public function getLastRecord($condition = array(),$columns = array(), $orderField = "Id", $orderType = "DESC")
    {
        try
        {
            $select    = $this->sql->select($this->tableName);

            if(count($columns))
                $select->columns($columns);

            if(count($condition))
            {
                $select->where($condition);
            }

            $select->where(["IsDeleted" => 0])->order($orderField . " " . $orderType);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();

            $resultSet = new HydratingResultSet($this->hydrator, $this->objectPrototype);
            $resultSet->initialize($result);
            $item = $resultSet->current();

            if (!$item)
            {
                return $this->objectPrototype;
            }
            
            return $item;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function updateRecords($arrUpdate, $condition, $typeOfCondition = 1 )
    {
        try
        {
            if($typeOfCondition)
            {
                if(is_string($condition) || is_numeric($condition))
                {
                    $condition = [" Id IN (" . $condition . ")"];
                }
            }
            $update = new Update($this->tableName);
            $update->set($arrUpdate);

            if( count( $condition ) )
                $update->where($condition);

            $statement = $this->sql->prepareStatementForSqlObject($update);
            $result = $statement->execute();
            if (! $result instanceof ResultInterface)
            {
                throw new RuntimeException(
                    'Database error occurred during item update operation'
                );
            }
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function getCountRecord($condition = array(), $countField = "Id", $groupField = '')
    {
        try
        {
            $select = $this->sql->select($this->tableName);
            $select->columns(array('Id' => new \Zend\Db\Sql\Expression('COUNT('.$countField.')')));
            if(!is_array($condition))
            {
                $condition = array();
            }
            $condition = array_merge($condition, ["IsDeleted"=>0]);
            $select->where($condition);
            if(!empty($groupField))
            {
                $select->group($groupField);
            }
            $select->limit(1);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();

            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                throw new RuntimeException(sprintf(
                    'Failed retrieving item with identifier "%s"; unknown database error.', serialize($condition)
                ));
            }

            $resultSet = new HydratingResultSet($this->hydrator, $this->objectPrototype);
            $resultSet->initialize($result);
            $item = $resultSet->current();

            return $item->Id;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function  deleteRecord($condition, $type = 1)
    {
        if(is_numeric($condition))
        {
            $condition = array("Id"=>$condition);
        }
        try
        {
            if($type == 1)
            {
                $update = new Update($this->tableName);
                $update->set(["IsDeleted"=>1]);
                $update->where($condition);
                $statement = $this->sql->prepareStatementForSqlObject($update);
                $result = $statement->execute();
                if (! $result instanceof ResultInterface)
                {
                    return false;
                }
                return true;
            }
            else
            {
                $delete = new Delete($this->tableName);
                $delete->where($condition);

                $statement = $this->sql->prepareStatementForSqlObject($delete);
                $result = $statement->execute();

                if (! $result instanceof ResultInterface)
                {
                    return false;
                }

                return true;
            }
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function isForeingKeyExist($condition, $countField ="Id")
    {
        try
        {
            $select = $this->sql->select($this->tableName);
            $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT('.$countField.')')));
            if(!is_array($condition))
            {
                $condition = array();
            }
            $condition = array_merge($condition, ["IsDeleted"=>0]);
            $select->where($condition);
            $select->limit(1);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();

            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                throw new RuntimeException(sprintf(
                    'Failed retrieving item with identifier "%s"; unknown database error.', serialize($condition)
                ));
            }

            $resultSet = new HydratingResultSet();
            $resultSet->initialize($result);
            $item = $resultSet->current();

            return $item['Num'] > 0 ? true : false;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        try
        {
            $select = $this->sql->select($this->tableName);
            if(!empty($filterString)) $select->where($filterString);
            $select->where("IsDeleted = 0");
            $select->order($sort.' '.$order);
            $select->limit($rows)->offset($offset);
			//echo $select->getSqlString();exit;
			
            $stmt   = $this->sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();
            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                return [];
            }

            $resultSet = new HydratingResultSet($this->hydrator, $this->objectPrototype);
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function getRowCountForGrid($filterString)
    {
        try
        {
            $select = $this->sql->select($this->tableName);
            $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT(Id)')));
            $select->where("IsDeleted = 0");
            if(!empty($filterString))$select->where($filterString);
            $select->limit(1);
			//echo $select->getSqlString();exit;
			
            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();

            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                throw new RuntimeException(sprintf(
                    'Failed retrieving item with identifier "%s"; unknown database error.', $filterString
                ));
            }
			
			$resultSet = new HydratingResultSet();
            $resultSet->initialize($result);
            $item = $resultSet->current();
            return $item['Num'];
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
	
	/*=========== Other option: Use as sql query statement below======================*/
	/*public function getRowCountForGrid($filterString)
    {
        try
        {
            $query = "SELECT COUNT(Id) AS Num FROM ".$this->tableName." WHERE IsDeleted = 0";
			if(!empty($filterString)) $query .= " AND ".$filterString;
			$query .= " LIMIT 1";
			$statement = $this->dbAdaper->createStatement($query);
			$result = $statement->execute();
			$resultSet = new ResultSet();
			$resultSet->initialize($result);
			$item = $resultSet->current();
            return $item['Num'];
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }*/
	/*========================================================================================= */
	
    public function isExist($condition = "")
    {
        try
        {
			if(empty($condition)) return false;
			
            $sql = $this->sql;
            $select = $sql->select($this->tableName);
            $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT(Id)')));
            $select->where("IsDeleted = 0");
            $select->where($condition);
            $select->limit(1);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();

            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                throw new RuntimeException(sprintf(
                    'Failed retrieving item with identifier "%s"; unknown database error.', serialize($condition)
                ));
            }

			$resultSet = new HydratingResultSet();
            $resultSet->initialize($result);
            $item = $resultSet->current();

            return $item['Num'] > 0 ? true : false;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
	
	public function isInUsed($condition = "", $relationTable = "")
    {
        try
        {
			if(empty($condition)) return false;
			
            $sql = $this->sql;
            if( empty( $relationTable ) ) $relationTable = $this->getTableName();
            $select = $sql->select($relationTable);
            $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT(Id)')));
            $select->where("IsDeleted = 0");
            $select->where($condition);
            $select->limit(1);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();

            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                throw new RuntimeException(sprintf(
                    'Failed retrieving item with identifier "%s"; unknown database error.', serialize($condition)
                ));
            }

			$resultSet = new HydratingResultSet();
            $resultSet->initialize($result);
            $item = $resultSet->current();

            return $item['Num'] > 0 ? true : false;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function getActiveRecordsForTreeDropDown($condition = array(),$fieldValue ="Id")
    {
        try
        {
            $condition = array_merge($condition, ["IsDeleted"=>0,"IsActive"=>1]);

            $select = $this->sql->select($this->tableName);
            $select->columns(array('*', new Expression($fieldValue.' as id'), new Expression('ParentId as parentid'), new Expression('Name as text')));
            
            $select->where($condition);

            $stmt   = $this->sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();
            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                return [];
            }
            // $resultSet = new HydratingResultSet($this->hydrator, $this->objectPrototype);
            $resultSet = new ResultSet();
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}