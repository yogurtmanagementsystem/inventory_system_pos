<?php
namespace PCSPLib\BaseClasses;

interface BaseRepositoryInterface
{
    public function getTableObject();
    public function getTableName();
    public function saveRecord($condition, $data);
    public function getRecord($condition);
    public function deleteRecord($condition, $type = 1);
    public function getAllRecords($condition = array(), $orderField = '', $orderType = '', $groupField = '');
    public function getAllActiveRecords($condition = array(), $orderField = '', $orderType = '', $groupField = '');
    public function getLastRecord($condition = array(), $orderField = "Id", $orderType = "DESC");
    public function getCountRecord($condition = array(), $countField = "Id", $groupField = '');
    public function isForeingKeyExist($condition);
    public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString);
    public function getRowCountForGrid($filterString);
	public function isExist($condition);
	public function isInUsed($condition, $relationTables);
}