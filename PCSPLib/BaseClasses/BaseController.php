<?php
namespace PCSPLib\BaseClasses;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Authentication\AuthenticationService;
use PCSPLib\PHPConstant;
use Zend\Session\Container;
use Interop\Container\ContainerInterface;

use PCSPLib\PHPClass;
use PCSPLib\PasswordHelper;
use Inventory\Model AS Inventory;

class BaseController extends AbstractActionController
{
    protected $serviceLocator;
    protected $dbAdapter;
    protected $config;
    protected $translator;
    protected $objectRepository;
    protected $tableObject;
    protected $tableName;
    protected $userLogin;
    protected $userLoginId;
    protected $userRoleId;
    protected $defaultSortGridField;
    protected $defaultSortGridType;
    protected $phpClass;
    protected $cache;
    protected $userFullName;

    public function __construct(ContainerInterface $serviceLocator, $objectRepository = null, $defaultSortGridField = "Id", $defaultSortGridType = "DESC")
    {
        date_default_timezone_set("Asia/Bangkok");
        
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $serviceLocator->get('dbAdapter');
        $this->config = $serviceLocator->get('Config');
        $this->cache = $serviceLocator->get("cache");
        $this->translator = $serviceLocator->get('Zend\Mvc\I18n\Translator');
        $this->objectRepository = $objectRepository;
        $this->tableObject = $objectRepository->getTableObject();
        $this->tableName = $objectRepository->getTableName();
        $this->defaultSortGridField = empty($defaultSortGridField)?"Id":$defaultSortGridField;
        $this->defaultSortGridType = empty($defaultSortGridType)?"ASC":$defaultSortGridType;
        
        $auth = new AuthenticationService();
        if($auth->hasIdentity())
        {
            $this->userLogin = $auth->getIdentity();
        }
        $this->userLoginId = ($this->userLogin != null ) ? $this->userLogin->Id:0;
        $this->userRoleId  = ($this->userLogin != null ) ? $this->userLogin->RoleId:0;
        $this->userFullName= ($this->userLogin != null ) ? $this->userLogin->LastName." ".$this->userLogin->FirstName:0;
        
        $this->phpClass = new PHPClass();

        // $languageKey = $this->getCacheLanguageKey();

        // if($this->cache->hasItem($languageKey))
        // {
        //     $this->local = $this->cache->getItem($languageKey);
        // }
        // else
        // {
        //     $this->local = "En";
        // }
        
        // $this->setLocation();
    }
    public function getBasePath()
    {
        $path = $_SERVER['DOCUMENT_ROOT'];

        return $path;
    }
    public function getCacheLanguageKey($optional = '')
    {
        $userId = $this->userLoginId;

        return md5($userId.$optional);
    }

    public function setLocation()
    {
        $local = "";
        switch($this->local)
        {
            case "Kh":
                $local = "km_KH";
                break;
            case "En":
                $local = "en_US";
                break;
        }
        
        $this->translator->setLocale($local);
    }
    
    public function getService($repository)
    {
        return $this->getServiceLocator()->get($repository);
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function getRequestController()
    {
        $controller = $this->getServiceLocator()->get('application')->getMvcEvent()->getRouteMatch()->getParam('controller');
    
        return $controller;
    }

    public function getRequestAction()
    {
        $action = $this->getServiceLocator()->get("application")->getMvcEvent()->getRouteMatch()->getParam("action");

        return $action;
    }

    public function getCachekey($optional = '')
    {
        $controller = $this->getRequestController();
        $userId     = $this->userLoginId;
        $str        = $controller.$optional.$userId;

        return md5($str);
    }

    public function getGridColumnRecord()
    {
        $cacheKey   = $this->getCachekey();
        $item       = [];
        if($this->cache->hasItem($cacheKey))
        {
            $item   = (array)json_decode($this->cache->getItem($cacheKey));
        }

        return $item;
    }

    public function gridPropertyChangeAction()
    {
        $field = isset($_POST["Field"])?$_POST["Field"]:"";
        $value = isset($_POST["Value"])?(int)$_POST["Value"]:0;

        $cacheKey = $this->getCachekey();
        $data = [$field=>1];

        if($this->cache->hasItem($cacheKey))
        {
            $item = $this->cache->getItem($cacheKey);
            $item = (array)json_decode($item);

            if($value)
            {
                if(isset($item[$field]))unset($item[$field]);
            }
            else
            {
                $item = array_merge($item, $data);                
            }
            $this->cache->replaceItem($cacheKey, json_encode($item));
        }
        else
        {
            $this->cache->addItem($cacheKey, json_encode($data));
        }

        return new JsonModel();
    }

    public function getLayoutVariable()
    {        
        $stockAlertRepository = $this->getService(Inventory\StockBalanceMasterRepository::class);

        $stockAlertRecord = [];

        $condition = "";

        $countStockAlert  = $stockAlertRepository->getCountAlertRecord($condition);
        if( $countStockAlert )
            $stockAlertRecord = $stockAlertRepository->getAllAlertRecord($condition,10)->toArray();

        $arrVariable = [
                'idenMenu'  => $this->idenMenu,
                'translate' => $this->translator,
                "stockAlertRecord"=> $stockAlertRecord,
                "countStockAlert" => $countStockAlert,
            ];
        
        $this->layout()->setVariable('arrVariable',$arrVariable);
    }
    
    public function getCacheName($action=null)
    {
        $routeMatch = $this->getServiceLocator()->get('application')->getMvcEvent()->getRouteMatch()->getmatchedRouteName();
        if($action != null)
        {
            $routeMatch .= '/'.$action;
        }
        
        $encrypt = new PasswordHelper('sha1');
        $cacheKey = $encrypt->createPassword($routeMatch.$this->userLoginId);
        
        return $cacheKey;
    }
    
    public function isItemExist()
    {
        return $this->objectRepository->isExist();
    }
    
    public function isItemInUsed($id)
    {
        return $this->objectRepository->isInUsed();
    }

    public function indexAction()
    {
        $viewModel = new ViewModel();
        $this->getLayoutVariable();
        return $viewModel;
    }

    public function newAction()
    {
        $viewModel = new ViewModel();
        $id = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $isCopy = isset($_POST['isCopy'])?$_POST['isCopy']:0;
        $isPopup = isset($_POST['isPopup'])?$_POST['isPopup']:1;
        
        if($isPopup) $viewModel->setTerminal(true);
        if(isset($_POST['isPopup']))
            $viewModel->setTerminal(true);
        else
            $this->getLayoutVariable();
        $record = $this->objectRepository->getRecord($id);
        $viewModel->setVariables(
                        [
                            'record'=>$record,
                            'isCopy'=>$isCopy,
                        ]
                    );

        return $viewModel;
    }

    public function editAction()
    {
        $view = $this->newAction();

        return $view;
    }

    public function copyAction()
    {
        $view = $this->newAction();

        return $view;
    }

    public function listAction()
    {
        $viewModel = new ViewModel();
        $this->getLayoutVariable();

        $gridColumnRecord = $this->getGridColumnRecord();

        $viewModel->setVariables(
                            [
                                "gridColumnRecord"  =>  $gridColumnRecord,
                            ]
                        );

        return $viewModel;
    }

    public function newPopupAction()
    {
        $viewModel = new ViewModel();
        
        $viewModel->setTerminal(true);

        return $viewModel;
    }
    
    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] )?$formData["Record"]:[];

        $tableName = $this->tableName;
        
        $filterString = "";

        if( count( $record ) )
        {
            foreach( $record as $key => $value )
            {
                if( !empty( $value["Name"] ) )
                {
                    if ( !empty( $filterString ) ) $filterString .= " OR ";

                    $filterString .= $tableName.".Name LIKE '%".trim($value["Name"])."%' ";
                }
            }
        }

        return $filterString;
    }

    public function getMergeRecordForGrid($record)
    {
        return $record;
    }

    public function getRecordsForGridAction()
    {
        $page = isset($_POST['pagenum']) ? intval($_POST['pagenum']) : 0;
        $rows = isset($_POST['pagesize'])? intval($_POST['pagesize']) : PHPConstant::DEFAULT_PAGE_SIZE;
        $offset = $page*$rows;
        $sort = (isset($_POST['sortdatafield']) & !empty($_POST['sortdatafield'])) ? strval($_POST['sortdatafield']) : $this->defaultSortGridField;
        $order = (isset($_POST['sortorder']) & !empty($_POST['sortorder'])) ? strval($_POST['sortorder']) : $this->defaultSortGridType;

        $filterString = $this->getFilterCondition();

        $records  = $this->objectRepository->getRecordsForGrid($offset, $rows, $sort, $order, $filterString)->toArray();
            
        $records = $this->getMergeRecordForGrid($records);

        $totalRow = $this->objectRepository->getRowCountForGrid($filterString);
        $data[]   = ['total'=>$totalRow,'rows'=>$records];
        $result   = new JsonModel($data);
        
        return $result;
    }

    public function saveRecordAction()
    {
        $id = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $formData = $this->request->getPost();
        $data=[];
        
        try
        {
            $record = $this->objectRepository->getTableObject();
            $record->exchangeArray($formData);
            $arrayTableField = $record->removeNoneTableField((array)$record);

            if(!$this->isItemExist())
            {
                $id = $this->objectRepository->saveRecord($id, $arrayTableField);
                $data = ['Id'=>$id];
            }
            else
            {
                $data = ['msg'=>$this->translator->translate('ITEM_EXIST')];
            }

            $result   = new JsonModel($data);
            return $result;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function deleteMultipleRecordsAction()
    {
        $strIds = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $formData = $this->request->getPost();
        $countDeleted = 0;
        try
        {
            $listId = explode(',',$strIds);
            $arrId = array("0");

            foreach ($listId as $id)
            {
                $bool = $this->isItemInUsed($id);
                if($bool){
                    $countDeleted++;
                }
                else {
                    array_push($arrId, $id);
                }
            }
            $strIdList = implode(',', $arrId);

            $this->objectRepository->updateRecords(['IsDeleted'=>1, 'DeleteUserId'=>$this->userLoginId, 'DeleteDate'=>date("Y-m-d H:i:s")], $strIdList);

            $data = ['title'=>null,'msg'=>null, 'countDeleted'=>0];
            if($countDeleted >= 1 && count($listId) > 1)
            {
                $data = ['title'=>$this->translator->translate('ERROR'),'msg'=>$this->translator->translate('SOME_ITEMS_DELETED_EXCEPT_CURRENT_USE'), 'countDeleted'=>2];
            }
            else if($countDeleted == 1 && count($listId) == 1)
            {
                $data = ['title'=>$this->translator->translate('ERROR'),'msg'=>$this->translator->translate('ITEM_IN_USED_CANNOT_DELETE'), 'countDeleted'=>1];
            }

            $result = new JsonModel($data);
            return $result;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function disableMultipleRecordsAction()
    {
        $strIds   = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $formData = $this->request->getPost();
        try
        {
            $arrUpdate = array('IsActive'=>0);
            $this->objectRepository->updateRecords($arrUpdate, $strIds);

            $data = array('title'=>null,'msg'=>null);
            $result = new JsonModel($data);
            return $result;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function enableMultipleRecordsAction()
    {
        $strIds   = isset($_POST['listId']) ? $_POST['listId'] : 0;
        $formData = $this->request->getPost();
        try
        {
            $arrUpdate = array('IsActive'=>1);
            $this->objectRepository->updateRecords($arrUpdate, $strIds);

            $data = array('title'=>null,'msg'=>null);
            $result = new JsonModel($data);
            return $result;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
    
    public function getAllActiveRecordsAction()
    {
        $record = $this->objectRepository->getAllActiveRecords()->toArray();

        return new JsonModel($record);
    }
}