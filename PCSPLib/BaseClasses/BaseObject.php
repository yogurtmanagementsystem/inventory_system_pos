<?php
namespace PCSPLib\BaseClasses;

class BaseObject
{
    public $Id;
    public $Name;
    public $Note;
    public $OrderNum;
    public $IsActive;
    public $IsDeleted;
    public $AddDate;
    public $ModifyDate;
    public $AddUserId;
    public $ModifyUserId;
    public $DeleteDate;
    public $DeleteUserId;

    public function __construct()
    {
        $this->Id = 0;
    }
    public function exchangeArray($data)
    {
        $this->Id       = isset($data['Id'])? (!empty($data['Id']) ? trim($data['Id']):0) : 0;
        $this->Name     = isset($data['Name']) ? trim($data['Name']) : "";
        $this->Note     = isset($data['Note'])?  trim($data['Note']): "";
        $this->OrderNum = isset($data['OrderNum'])? (!empty($data['OrderNum'])? trim($data['OrderNum']):0) : 0;
        $this->IsActive = isset($data['IsActive'])? (!empty($data['IsActive'])? trim($data['IsActive']):0) : 0;
        $this->IsDeleted= isset($data['IsDeleted'])? (!empty($data['IsDeleted'])? trim($data['IsDeleted']):0) : 0;
        $this->AddDate  = isset($data['AddDate'])? (!empty($data['AddDate'])? trim($data['AddDate']):null) : null;
        $this->ModifyDate   = isset($data['ModifyDate'])? (!empty($data['ModifyDate'])? trim($data['ModifyDate']):null) : null;
        $this->AddUserId    = isset($data['AddUserId'])? (!empty($data['AddUserId'])? trim($data['AddUserId']):null) : null;
        $this->ModifyUserId = isset($data['ModifyUserId'])? (!empty($data['ModifyUserId'])? trim($data['ModifyUserId']):null) : null;
        $this->DeleteDate   = isset($data['DeleteDate'])? (!empty($data['DeleteDate'])? trim($data['DeleteDate']):null) : null;
        $this->DeleteUserId = isset($data['DeleteUserId'])? (!empty($data['DeleteUserId'])? trim($data['DeleteUserId']):null) : null;
    }

    public function getNoneTableField()
    {
        return [];
    }
	
    public function removeNoneTableField($arrayObjectField)
    {
        $arrayNoneTableField = $this->getNoneTableField();
        foreach($arrayNoneTableField as $key => $value)
        {
            unset($arrayObjectField[$value]);
        }

        return $arrayObjectField;
    }
	
    public function setId($value)
    {
        $this->Id = $value;
    }
}