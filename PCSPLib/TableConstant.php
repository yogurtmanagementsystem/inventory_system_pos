<?php
namespace PCSPLib;

class TableConstant
{
	const SEX_TABLE = 'heng_sex';

	// User
	const US_USER_TABLE = 'heng_us_user';
	const US_ROLE_TABLE = 'heng_us_role';

	const US_GROUP_RESOURCE_TABLE 		= 'heng_us_group_resource';
	const US_RESOURCE_ACTION_TABLE 		= 'heng_us_resource_action';
	const US_RESOURCE_CONTROLLER_TABLE 	= 'heng_us_resource_controller';
	const US_RESOURCE_TABLE 			= 'heng_us_resource';
	const US_ROLE_PERMISSION_TABLE 		= 'heng_us_role_permission';

	// Setting 
	const FORMAT_TABLE      = "heng_format";

	// Inventory
	const INV_SUPPLIER_TABLE  		= "heng_inv_supplier";
	const INV_ITEM_BROKEN_REASON_TABLE = "heng_inv_item_broken_reason";
	const INV_ITEM_CATEGORY_TABLE 	= "heng_inv_item_category";

	const INV_ITEM_MASTER_TABLE 	= "heng_inv_item_master";

	const INV_ITEM_ADJUSTMENT_TABLE = "heng_inv_item_adjustment";
	const INV_ITEM_RECEIVE_TABLE 	= "heng_inv_item_receive";
	const INV_ITEM_UNIT_MEASUREMENT_TABLE = "heng_inv_item_unit_measurement";
	const INV_ITEM_RECEIVE_GROUP_TABLE 	= "heng_inv_receive_group";
	const INV_STOCK_BALANCE_MASTER_TABLE  = "heng_inv_stock_balance_master";

	const INV_ITEM_MISSING_BROKEN_TABLE = "heng_inv_item_missing_broken";
	
	const INV_ITEM_TAKE_USE_TABLE  = "heng_inv_item_take_use";

	const INV_SALE_STOCK_TABLE  = "heng_inv_sale_stock";

	// accountant
	const SAL_SALE_PRICE_TABLE = "heng_sal_sale_price";
	const SAL_DISCOUNT_TYPE_TABLE = "heng_sal_discount_type";
	const SAL_DISCOUNT_VALUE_TABLE = "heng_sal_discount";

	// customer
	const CUS_CUSTOMER_GROUP_TABLE = "heng_cus_customer_group";

	// Sale
	const SAL_ITEM_CATEGORY_TABLE = "heng_sal_item_category";
	const SAL_ITEM_TYPE_TABLE = "heng_sal_item_type";
	const SAL_SALE_TYPE_TABLE = "heng_sal_sale_type";
	const SAL_SALE_TABLE = "heng_sal_sale";
	const SAL_SALE_DETAIL_TABLE = "heng_sal_sale_detail";
	const SAL_SALE_ITEM_TABLE = "heng_sal_sale_item";
	const SAL_ITEM_SIZE_TABLE = "heng_sal_item_size";
	const SAL_FIX_SALE_TYPE_TABLE = "heng_sal_fix_sale_type";

	// Cotnact
	const CON_CONTACT_TABLE = "heng_con_contact";
}