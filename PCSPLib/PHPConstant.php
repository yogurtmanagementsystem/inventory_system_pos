<?php
namespace PCSPLib;

class PHPConstant
{
	// global constant
	const IMAGE_EXTENSION = "jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF";
	const STATIC_SALT = "abcde0123456789";
	const DATE_FORMAT = '%d-%b-%Y';
	const FIX_DATE_FORMAT = 'd-M-Y';
	const CURRENCY_FIX = '$';
	const PAGE_SIZE_OPTIONS = '[20,50,80,100,150]';
	const DEFAULT_PAGE_SIZE = 20;
	const RESOURCE_FILES_PATH = 'public/resource/';
	const RESOURCE_FILES_FOLDER = '/resource';
	const THUMNAIL_WIDTH = 50;
	const THUMNAIL_HEIGHT = 50;
	// const SMALL_IMAGE_PREFIX = "th_";
    const USER_IMAGE_BLANK = "resource/user-blank-image.jpg";
    const ITEM_IMAGE_BLANK = "item-blank-image.png";

    const FIX_CURRENCY = "$";

	// upload file folder name
	const USER_PHOTO_FOLDER = "user/";
	const USER_PHOTO_FILE_PREFIX = "user";

	const GROUP_RESOURCE_CONTROLLER			=	'group-resource';
	const RESOURCE_ACTION_CONTROLLER		=	'resource-action';
	const RESOURCE_CONTROLLERS_CONTROLLER	=	'resource-controllers';
	const RESOURCE_CONTROLLER			    =	'resource';
	
	// user module
	const USER_MODULE = "user";
	const USER_CONTROLLER = "user";
	const ROLE_CONTROLLER = "role";
	const RESOURCES_CONTROLLER = "resources";

	// inventory
	const INVENTORY_MODULE 		= 'inventory';
	const INVENTORY_CONTROLLER 	= 'inventory';
	const ITEM_CLASS_CONTROLLER = 'item-class';
	const ITEM_BROKEN_REASON_CONTROLLER = 'item-broken-reason';
	const ITEM_CATEGORY_CONTROLLER 		= 'item-category';
	const WAREHOUSE_CONTROLLER 			= 'warehouse';
	const SUPPLIER_CONTROLLER 			= 'supplier';
	const REQUEST_DEPARTMENT_CONTROLLER = 'request-department';
	const ITEM_UNIT_MEASUREMENT_CONTROLLER = 'item-unit-measurement';
	const ITEM_METHOD_CONTROLLER  = 'item-method';
	const ASSET_FORMAT_CONTROLLER = 'asset-format';
	const ITEM_GROUP_CONTROLLER   = 'item-group';
	const ITEM_TYPE_CONTROLLER    = 'item-type';
	const ITEM_VARIANCE_CONTROLLER= 'item-variance';

	const ITEM_MASTER_CONTROLLER  = 'item-master';
	const ITEM_RECEIVE_CONTROLLER = 'item-receive';
	const ITEM_ADJUSTMENT_CONTROLLER = 'item-adjustment';
	const ITEM_TAKE_CONTROLLER 	  = 'item-take';
	const ITEM_MISSING_BROKEN_CONTROLLER  = 'item-missing-broken';
	const STOCK_BALANCE_MASTER_CONTROLLER = 'stock-balance-master';
	const STOCK_BALANCE_DETAIL_CONTROLLER = 'stock-balance-detail';

	const STOCK_ALERT_CONTROLLER = 'stock-alert';

	const SALE_STOCK_CONTROLLER = 'sale-stock';
	
	// accountant
	const ACCOUNTANT_MODULE = 'accountant';
	const DISCOUNT_CONTROLLER = 'discount';

	// report
	const REPORT_MODULE = 'report';
	
	// system setting
	const SYSTEM_SETTING_MODULE = 'system-setting';
	const FORMAT_CONTROLLER = 'format';

	// Sale
	const SALE_MODULE = 'sale';
	const SALE_CONTROLLER = 'sale';
	const SALE_ITEM_CONTROLLER = 'sale-item';
	const SALE_PRICE_CONTROLLER = 'sale-price';
	const SALE_TYPE_CONTROLLER = 'sale-type';
	const ITEM_SIZE_CONTROLLER = 'item-size';

	// pos
	const POS_MODULE = 'pos';
	const POS_CONTROLLER = 'pos';

	// Contact
	const CONTACT_MODULE = 'contact';
	const CONTACT_CONTROLLER = 'contact';
	const SMALL_LOGO_PREFIX = "th_";
    const CONTACT_LOGO_BLANK = "resource/contact-blank-logo.png";

	// Database connection
	const DB_NAME = "inventory_system_pos_db";
	const DB_USERNAME = "root";
	const DB_PASSWORD = "";


	// const DB_NAME = "yogurt_db";
	// const DB_USERNAME = "Dara12345";
	// const DB_PASSWORD = "12345@@Dara";

	// const DB_NAME = "bunthorn_yogurt";
	// const DB_USERNAME = "bunthorn_yogurt";
	// const DB_PASSWORD = "yogurt";

	// ALTER TABLE heng_con_contact ADD WiFiName char(100)
	// ALTER TABLE heng_con_contact ADD WiFiPassword char(100)
}