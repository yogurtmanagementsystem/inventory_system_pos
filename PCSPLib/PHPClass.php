<?php

namespace PCSPLib;

class PHPClass
{
	
	public $pricePrecision = 5;

	public function __construct()
	{
	}

	public function dateFormat($dateString, $formatString=PHPConstant::FIX_DATE_FORMAT )
	{

		if($dateString=='' || $dateString==null || $dateString=='0000-00-00' || !strtotime($dateString)){
			return null;
		}else{
			$timestamp = strtotime($dateString);
			return date($formatString, $timestamp);
		}
		 
	}
	
	public function currencyRound($Value,$Precision){
		if(is_numeric($Value)){
			return round($Value,$Precision);
		}else{
			return 0;
		}
	
	}
	
	public function displayKhmerDate($date){
		//$date = PHPClass::dateFormat($date, 'd/F/Y');
		$date = PHPClass::dateFormat($date, 'd-F-Y'); // Make it consistency for whole application of date format with dash not slash
	
		$en_month = array('January','February','March','April','May','June','July','August','September','October','November','December');
		$kh_month = array('áž˜áž€ážšáž¶','áž€áž»áž˜áŸ’áž—áŸˆ','áž˜áž·áž“áž¶','áž˜áŸ�ážŸáž¶','áž§ážŸáž—áž¶','áž˜áž·áž�áž»áž“áž¶','áž€áž€áŸ’áž€ážŠáž¶','ážŸáž¸áž áž¶','áž€áž‰áŸ’áž‰áž¶','áž�áž»áž›áž¶','ážœáž·áž…áŸ’áž†áž·áž€áž¶','áž’áŸ’áž“áž¼');
	
		$new_date = str_ireplace($en_month, $kh_month, $date);
	
		return $new_date;
	}
	
	public function displayKhmerDateOfBirthFormat($date){
		
		$khDOB = "";
		if(!empty($date)){
			$dob = $this->displayKhmerDate($date);
			$wDay = ''; $wMonth =''; $wYear='';
			if(!empty($dob)){
				$delimiters = array('/','-','.');
				$dobArr = $this->explodeX($delimiters, $dob);
				$wDay = $dobArr[0]; $wMonth = $dobArr[1]; $wYear = $dobArr[2];
				$khDOB = 'áž€áž¾áž�áž�áŸ’áž„áŸƒáž‘áž¸ '.$wDay.' áž�áŸ‚ '.$wMonth.' áž†áŸ’áž“áž¶áŸ† '.$wYear;
			}
		}
		return $khDOB;
	}
	
	//explode array of delimiters
	function explodeX( $delimiters, $string )
	{
		return explode( chr( 1 ), str_replace( $delimiters, chr( 1 ), $string ) );
	}
	
	function createThumbnailWithFlexibleSize($src, $dst, $width, $height, $crop=0){
	
	    if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";
	
	    $type = strtolower(substr(strrchr($src,"."),1));
	    if($type == 'jpeg') $type = 'jpg';
	    switch($type){
	        case 'bmp': $img = imagecreatefromwbmp($src); break;
	        case 'gif': $img = imagecreatefromgif($src); break;
	        case 'jpg': $img = imagecreatefromjpeg($src); break;
	        case 'png': $img = imagecreatefrompng($src); break;
	        default : return "Unsupported picture type!";
	    }
	
	    // resize
	    if($crop){
	        if($w < $width or $h < $height) return "Picture is too small!";
	        $ratio = max($width/$w, $height/$h);
	        $h = $height / $ratio;
	        $x = ($w - $width / $ratio) / 2;
	        $w = $width / $ratio;
	    }
	    else{
	        if($w < $width and $h < $height) return "Picture is too small!";
	        $ratio = min($width/$w, $height/$h);
	        $width = $w * $ratio;
	        $height = $h * $ratio;
	        $x = 0;
	    }
	
	    $new = imagecreatetruecolor($width, $height);
	
	    // preserve transparency
	    if($type == "gif" or $type == "png"){
	        imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
	        imagealphablending($new, false);
	        imagesavealpha($new, true);
	    }
	
	    imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);
	
	    switch($type){
	        case 'bmp': imagewbmp($new, $dst); break;
	        case 'gif': imagegif($new, $dst); break;
	        case 'jpg': imagejpeg($new, $dst); break;
	        case 'png': imagepng($new, $dst); break;
	    }
	    
	    ImageDestroy($new);
	    ImageDestroy($img);
	    
	    return true;
	}
	
	function createThumbnailWithPadding($filepath, $thumbpath, $thumbnail_width, $thumbnail_height) {
	    list($original_width, $original_height, $original_type) = getimagesize($filepath);
	    if ($original_width > $original_height) {
	        $new_width = $thumbnail_width;
	        $new_height = intval($original_height * $new_width / $original_width);
	    } else {
	        $new_height = $thumbnail_height;
	        $new_width = intval($original_width * $new_height / $original_height);
	    }
	    $dest_x = intval(($thumbnail_width - $new_width) / 2);
	    $dest_y = intval(($thumbnail_height - $new_height) / 2);
	
	    if ($original_type === 1) {
	        $imgt = "ImageGIF";
	        $imgcreatefrom = "ImageCreateFromGIF";
	    } else if ($original_type === 2) {
	        $imgt = "ImageJPEG";
	        $imgcreatefrom = "ImageCreateFromJPEG";
	    } else if ($original_type === 3) {
	        $imgt = "ImagePNG";
	        $imgcreatefrom = "ImageCreateFromPNG";
	    } else {
	        return false;
	    }
	
	    $old_image = $imgcreatefrom($filepath);
	    $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
	    imagecopyresampled($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
	    //imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $thumbnail_width, $thumbnail_height, $original_width, $original_height);
	
	    $imgt($new_image, $thumbpath);
	    
	    ImageDestroy($old_image);
	    ImageDestroy($new_image);
	    //return file_exists($thumbpath);
	    return true;
	}
	
	function createThumbnailWithFixSize1111($filepath, $thumbpath, $thumbnail_width, $thumbnail_height) {
	    list($original_width, $original_height, $original_type) = getimagesize($filepath);
	    if ($original_width > $original_height) {
	        $new_width = $thumbnail_width;
	        $new_height = intval($original_height * $new_width / $original_width);
	    } else {
	        $new_height = $thumbnail_height;
	        $new_width = intval($original_width * $new_height / $original_height);
	    }
	    $dest_x = intval(($thumbnail_width - $new_width) / 2);
	    $dest_y = intval(($thumbnail_height - $new_height) / 2);
	
	    if ($original_type === 1) {
	        $imgt = "ImageGIF";
	        $imgcreatefrom = "ImageCreateFromGIF";
	    } else if ($original_type === 2) {
	        $imgt = "ImageJPEG";
	        $imgcreatefrom = "ImageCreateFromJPEG";
	    } else if ($original_type === 3) {
	        $imgt = "ImagePNG";
	        $imgcreatefrom = "ImageCreateFromPNG";
	    } else {
	        return false;
	    }
	
	    $old_image = $imgcreatefrom($filepath);
	    $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
	    imagecopyresampled($new_image, $old_image, 0, 0, 0, 0, $thumbnail_width, $thumbnail_height, $original_width, $original_height);
	
	    $imgt($new_image, $thumbpath);
	   
	    ImageDestroy($old_image);
	    ImageDestroy($new_image);
	    
	    return true;
	}
	
	function createThumbnailWithFixSize($src, $dst, $width, $height, $crop=0){
	
	    if(!list($w, $h) = getimagesize($src)) return "Unsupported picture type!";
	
	    $type = strtolower(substr(strrchr($src,"."),1));
	    if($type == 'jpeg') $type = 'jpg';
	    switch($type){
	        case 'bmp': $img = imagecreatefromwbmp($src); break;
	        case 'gif': $img = imagecreatefromgif($src); break;
	        case 'jpg': $img = imagecreatefromjpeg($src); break;
	        case 'png': $img = imagecreatefrompng($src); break;
	        default : return "Unsupported picture type!";
	    }
	
	    // resize
	    if($crop){
	        if($w < $width or $h < $height) return "Picture is too small!";
// 	        $ratio = max($width/$w, $height/$h);
// 	        $h = $height / $ratio;
// 	        $x = ($w - $width / $ratio) / 2;
// 	        $w = $width / $ratio;
	    }
	    else{
	        if($w < $width and $h < $height) return "Picture is too small!";
// 	        $ratio = min($width/$w, $height/$h);
// 	        $width = $w * $ratio;
// 	        $height = $h * $ratio;
	        $x = 0;
	    }
	    
	    $new = imagecreatetruecolor($width, $height);
	
	    // preserve transparency
	    if($type == "gif" or $type == "png"){
	        imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
	        imagealphablending($new, false);
	        imagesavealpha($new, true);
	    }
	
	    imagecopyresampled($new, $img, 0, 0, $x, 0, $width, $height, $w, $h);
	
	    switch($type){
	        case 'bmp': imagewbmp($new, $dst); break;
	        case 'gif': imagegif($new, $dst); break;
	        case 'jpg': imagejpeg($new, $dst); break;
	        case 'png': imagepng($new, $dst); break;
	    }
	     
	    ImageDestroy($new);
	    ImageDestroy($img);
	     
	    return true;
	}
	
	function jqxFilterFormat($filterCondition, $filterValue){
		
		$formatStr = "";
		switch ($filterCondition){
			case "CONTAINS":
				$condition = " LIKE ";
				$value = "%{$filterValue}%";
				break;
			case "DOES_NOT_CONTAIN":
				$condition = " NOT LIKE ";
				$value = "%{$filterValue}%";
				break;
			case "EQUAL":
				$condition = " = ";
				$value = $filterValue;
				break;
			case "NOT_EQUAL":
				$condition = " <> ";
				$value = $filterValue;
				break;
			case "GREATER_THAN":
				$condition = " > ";
				$value = $filterValue;
				break;
			case "LESS_THAN":
				$condition = " < ";
				$value = $filterValue;
				break;
			case "GREATER_THAN_OR_EQUAL":
				$condition = " >= ";
				$value = $filterValue;
				break;
			case "LESS_THAN_OR_EQUAL":
				$condition = " <= ";
				$value = $filterValue;
				break;
			case "STARTS_WITH":
				$condition = " LIKE ";
				$value = "{$filterValue}%";
				break;
			case "ENDS_WITH":
				$condition = " LIKE ";
				$value = "%{$filterValue}";
				break;
			case "NULL":
				$condition = " IS NULL ";
				$value = "%{$filterValue}%";
				break;
			case "NOT_NULL":
				$condition = " IS NOT NULL ";
				$value = "%{$filterValue}%";
				break;
		}
		$formatStr = $condition . "'".$value. "'";
		return $formatStr;
	}
}