<?php 

namespace PCSPLib;

use PCSPLib\PHPConstant;
use Zend\Crypt\Password\Bcrypt;
use Zend\Session\Container;

/**
 * @author Phin Heng
 * @since  01-Jan-2016
 * Helper class for use in view 
 * Generate Button 
 * Flash Message etc
 * Check Permission
 */
 class Helper 
 {
    public static function buildAdminMenu($menu_name)
    {
        $menu = $menu_name;

        $permissionRecord =  new Container('UserPermissionRecord');
        $record = $permissionRecord->Record;
        
        echo "<ul class='sidebar-menu'>\n";
        self::displayMenu($menu, $record);

        echo "</ul>";
        
    }
    public static function getMenuItem($item)
    {
        $class = "";
        $suffix = '';
        $icon   = '';
        if(isset($item['class'])) $class = $class." ".$item['class'];
        if(isset($item['hasChild']) && $item['hasChild'])
        {
            $class.=" treeview ";
            $suffix= '<span class="pull-right-container"><i class="fa fa-angle-left pull-right" style="color:black !important;"></i></span>';
        }
        if( isset( $item["img"] ) )
        {
            $icon = "<img src='".$item['img']."' style='width: 20px;height: 20px;'/>";
        }
        else
        {
            $icon = "<i class='fa ".$item['icon']."'></i>";
        }
        $url = ($item['url']=='#')?'javascript:;':$item['url'];
        $id = isset($item['id'])?$item['id']:"";

        $text   = "<a href='".$url."'> ".$icon." <span>".$item['text']."</span> $suffix </a>";
        return  "<li class='$class' id='$id'> $text \n" ;
    }

    public static function  displayMenu(array $menu, $permissionRecord = array())
    { 
        foreach ($menu as $key => $item) 
        {
            switch ($item['type'])
            {
                case 'seperator':
                        echo self::getSeperator();
                        break;
                default:
                    $hasPermission   = true;
                    
                    if(isset($item['permission']))
                    {
                        $permission = $item['permission'];
                        $hasPermission =  self::checkMenuPermission($permission, $permissionRecord);
                    }
                    if($hasPermission)
                    {
                        echo self::getMenuItem($item);
                            if(isset($item['hasChild']) && $item['hasChild'])
                            {
                                echo '<ul class="treeview-menu "> ';
                                echo "\n";
                                self::displayMenu($item['children'], $permissionRecord);
                                echo "</ul>\n";
                            }
                         echo "</li>\n";
                    }
                    break;
            }
        }
    }

    public static function checkMenuPermission($permission, $permissionRecord = array())
    {
        $can_display = false;
        $user_permission = $permissionRecord; 

        $type = isset($permission['type'])?$permission['type']:'';

        if( isset( $permission['AutoAllow']) && $permission['AutoAllow'] )
        {
            $can_display = true;
        }
        elseif( $type == 'group' )
        {
            $modules = $permission['modules'];
            foreach ($modules as $key => $value) 
            {
                $tmp_permission = array();
                if(isset($user_permission[$key]))
                {
                    $tmp_permission = $user_permission[$key];
                }       
                  
                if(is_array($value))
                {
                    foreach ($value as $action_name) 
                    {
                        if(isset($tmp_permission[$action_name]) && $tmp_permission[$action_name])
                        {
                            $can_display = true;  
                            break;
                        } 
                        
                    }
                }
                else
                {
                    if(isset($tmp_permission['none_all']) && !$tmp_permission['none_all'])
                    {
                        $can_display = true;
                        break;
                    }
                }
            }
        }
        elseif( $type == 'groups' )
        {
            $modules = $permission['module'];
            $tmp_permission = $permission['action'];
            foreach($modules as $key => $value)
            {
                foreach($tmp_permission as $action)
                {
                    if(isset($user_permission[$value][$action]) && $user_permission[$value][$action])
                    {
                        $can_display = true;
                        break;
                    }
                }
            }
        }
        elseif( $type == 'single' )
        {
            $module     = $permission['module'];
            $action     = $permission['action'];
            $tmp_permission = array();
            if(is_array($action))
            {
                foreach($action as $item)
                {
                    if(isset($user_permission[$module][$item]) && $user_permission[$module][$item])
                    {
                        $can_display = true;
                        break;
                    }
                }
            }
            else
            {
                if(isset($user_permission[$module][$action]))
                {
                    $tmp_permission  = $user_permission[$module][$action];
                }
                if(isset($tmp_permission[$module][$action]) && $tmp_permission[$module][$action])
                {
                    $can_display = true;
                }
            }
        }

        return $can_display;
    }

    public static function getDateFormat($date)
    {
        return date('m/d/Y H:i:s',strtotime($date));
    }

    public static function checkPagePermission($record, $module, $action)
    {
        return true;
    }
}