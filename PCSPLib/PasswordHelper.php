<?php

namespace PCSPLib;

use Zend\Crypt\Password\Bcrypt;
use PCSPLib\PHPConstant;

class PasswordHelper
{
    public $salt = PHPConstant::STATIC_SALT;
    public $method = 'sha1';

    public function __construct($method = null)
    {
        if (! is_null($method)) {
            $this->method = $method;
        }
    }

    public function createPassword($password)
    {
        if ($this->method == 'md5') {
            return md5($this->salt . $password);
        } elseif ($this->method == 'sha1') {
            return sha1($this->salt . $password);
        } elseif ($this->method == 'bcrypt') {
            $bcrypt = new Bcrypt();
            $bcrypt->setCost(14);
            return $bcrypt->create($password);
        }
    }

    public function verifyPassword($password, $hash)
    {
        if ($this->method == 'md5') {
            return $hash == md5($this->salt . $password);
        } elseif ($this->method == 'sha1') {
            return $hash == sha1($this->salt . $password);
        } elseif ($this->method == 'bcrypt') {
            $bcrypt = new Bcrypt();
            $bcrypt->setCost(14);
            return $bcrypt->verify($password, $hash);
        }
    }
}
