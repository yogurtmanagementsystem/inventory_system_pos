<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace SystemSetting;

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\BaseClasses\BaseObject;
use PCSPLib\TableConstant;
use Zend\Db\Adapter\AdapterInterface;
use Interop\Container\ContainerInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;

return [
    
    'service_manager' => 
    [
        'aliases' => 
        [
            
        ],
        'factories' => 
        [
            Model\BranchRepository::class => function(ContainerInterface $container)
            {
                return new Model\BranchRepository($container->get(AdapterInterface::class), TableConstant::BRANCH_TABLE,new ReflectionHydrator(), new Model\Branch());
            },
            Model\NationalityRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::NATIONALITY_TABLE,new ReflectionHydrator(), new BaseObject());
            },
            Model\SubjectRepository::class => function(ContainerInterface $container)
            {
                return new Model\SubjectRepository($container->get(AdapterInterface::class), TableConstant::SUBJECT_TABLE,new ReflectionHydrator(), new BaseObject());
            },
            Model\SubjectRepository::class => function(ContainerInterface $container)
            {
                return new Model\SubjectRepository($container->get(AdapterInterface::class), TableConstant::SUBJECT_TABLE,new ReflectionHydrator(), new BaseObject());
            },
            Model\FormatRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::FORMAT_TABLE,new ReflectionHydrator(), new Model\Formats());
            },
        ],
    ],
    'controllers' => 
    [
        'factories' => 
        [
            Controller\SystemSettingController::class => function(ContainerInterface $container)
            {
                return new Controller\SystemSettingController($container);
            },
            Controller\BranchController::class => function(ContainerInterface $container)
            {
                return new Controller\BranchController($container);
            }, 
            Controller\NationalityController::class => function(ContainerInterface $container)
            {
                return new Controller\NationalityController($container);
            },
            Controller\SubjectController::class => function(ContainerInterface $container)
            {
                return new Controller\SubjectController($container);
            },
            Controller\FormatController::class => function(ContainerInterface $container)
            {
                return new Controller\FormatController($container);
            },
        ],      
    ],
    'router' => 
    [
        'routes' => 
        [
            'system-setting' => 
            [
                'type' => Literal::class,
                'options' => 
                [
                    'route' => '/system-setting',
                    'defaults' => 
                    [
                        'controller' => Controller\SystemSettingController::class,
                        'action'     => 'list',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => 
                [
                    'branch' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/branch[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\BranchController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],   
                    'nationality' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/nationality[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\NationalityController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ], 
                    'subject' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/subject[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\SubjectController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],   
                    'format' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/format[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\FormatController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],           
                ],
            ],
        ],
    ],
    'view_manager' => 
    [
        'template_path_stack' => 
        [
            'system-setting' => __DIR__ . '/../view',
        ],
    ],
    'translator' =>
    [
        'locale' => 'en_US',
        'translation_file_patterns' =>
            [
                [
                    'type' => 'gettext',
                    'base_dir' => __DIR__ . '/../language',
                    'pattern' => '%s.mo'
                ]
            ]
    ],
];
