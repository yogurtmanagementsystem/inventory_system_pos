<?php
/**
 * @author Phin Heng
 * @since  20 Oct 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace SystemSetting\Model;

use PCSPLib\BaseClasses\BaseObject;

class Formats extends BaseObject
{
	public $FormatType;
    public $Identify;
    public $StartNum;
    public $Digit;
    public $Text;
	public $DateFormat;
	
    public function exchangeArray($data)
    {
    	parent::exchangeArray($data);

    	$this->FormatType   = isset($data['FormatType']) ? trim($data['FormatType']," ") : "";
        $this->Identify     = isset($data['Identify'])? (!empty($data['Identify']) ? trim($data['Identify']," "):0) : 0;
        $this->StartNum     = isset($data['StartNum'])? (!empty($data['StartNum']) ? trim($data['StartNum']," "):0) : 0;
        $this->Digit        = isset($data['Digit'])? (!empty($data['Digit']) ? trim($data['Digit']," "):0) : 0;
        
        $this->Text         = isset($data['Text'])?trim($data['Text']):"";
        $this->DateFormat   = isset($data['DateFormat'])?trim($data['DateFormat']):"";
    }
    
    public function getNoneTableField()
    {
        return array("Note");
    }
}