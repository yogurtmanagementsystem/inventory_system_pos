<?php
/**
 * @author Phin Heng
 * @since  20 Oct 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace SystemSetting\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;

use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class SubjectRepository extends BaseRepository
{
    public function getActiveRecordsForTreeDropDown($condition = array())
    {
        try
        {
            $condition = array_merge($condition, ["IsDeleted"=>0,"IsActive"=>1]);

            $select = $this->sql->select($this->tableName);
            $select->columns(
                        [
                            "*",
                            "id"        => new Expression("Id"),
                            "parentid"  => new Expression("IsDeleted")
                        ]
                    );
            if(count($condition))
                $select->where($condition);

            $stmt   = $this->sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();
           
            $resultSet = new ResultSet();
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}