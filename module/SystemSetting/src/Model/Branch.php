<?php
/***
 * 
 * @author Sopheak Chhin
 * @date Aug 23, 2016
 * @time 5:20:39 PM
 */
namespace SystemSetting\Model;

use PCSPLib\BaseClasses\BaseObject;

class Branch extends BaseObject
{
	public $Tel;
	public $Email;
	public $Address;
	
	public function exchangeArray($data)
	{
		parent::exchangeArray($data);
		
		$this->Tel 		= isset($data['Tel'])?trim($data['Tel']):'';
		$this->Email 		= isset($data['Email'])?trim($data['Email']):'';
		$this->Address 		= isset($data['Address'])?trim($data['Address']):''; 
	}
}