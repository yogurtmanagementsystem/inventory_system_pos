<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace SystemSetting\Controller;

use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use SystemSetting\Model AS SystemSetting;
use PCSPLib\PasswordHelper;
use Zend\View\Model\JsonModel;

class BranchController extends BaseController
{
    protected $idenMenu = "setting";

    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(SystemSetting\BranchRepository::class);

        parent::__construct($serviceLocator, $table);
    }
    
    public function newAction()
    {
    	$view = parent::newAction();
    	$gridIdName = isset($_POST["gridIdName"])?$_POST["gridIdName"]:0;

    	$view->setVariables(
    					[
    						"gridIdName"	=>	$gridIdName,

    					]
    				);

    	return $view;
    }
}