<?php
/**
 * @author Phin Heng
 * @since  20 Oct 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace SystemSetting\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use SystemSetting\Model AS SystemSetting;

class FormatController extends BaseController
{
	protected $idenMenu = "setting";
	// identify 1 = student, 2 = teacher, 3 = invoice, 4 = receipt
	public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(SystemSetting\FormatRepository::class);

        parent::__construct($serviceLocator, $table);
    }

	public function studentAction()
	{
		return $this->format(1);
	}

	public function teacherAction()
	{
		return $this->format(2);
	}

    public function invoiceAction()
    {
        return $this->format(3);
    }

    public function receiptAction()
    {
        return $this->format(4);
    }

    public function assetAction()
    {
        return $this->format(5);
    }

    public function format($identify)
    {
        $view = parent::listAction();

        $record = $this->objectRepository->getAllRecords(["Identify"=>$identify],[],"OrderNum");

        $fixDateFormat = $this->fixDateFormat();

        $view->setVariables(
                        [
                            "identify"  =>  $identify,
                            "record"    =>  $record,
                            "fixDateFormat" =>  $fixDateFormat,
                        ]
                    );
        return $view;
    }

    public function newAction()
    {
        $view = parent::newAction();
        $identify = isset($_POST["Identify"])?(int)$_POST["Identify"]:0;

        $fixDateFormat = $this->fixDateFormat();

        $view->setVariables(
                        [
                            "identify"      =>  $identify,
                            "fixDateFormat" =>  $fixDateFormat,

                        ]
                    );
        return $view;
    }

    public function fixDateFormat()
    {
        $fixDateFormat = [
                ["Name"  =>  "Ymd (".date("Ymd").")","Id"   => "Ymd"],
                ["Name"  =>  "dmY (".date("dmY").")","Id"   => "dmY"],
                ["Name"  =>  "dmY (".date("dmY").")","Id"   => "dmY"],
                ["Name"  =>  "Y-m-d (".date("Y-m-d").")","Id" => "Y-m-d"],
                ["Name"  =>  "d-m-Y (".date("d-m-Y").")","Id" => "d-m-Y"]
            ];

        return $fixDateFormat;
    }
	public function saveRecordAction()
    {
        $id = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $formData = $this->request->getPost();

        $obj = $this->objectRepository->getTableObject();

        if(isset($formData["Record"]))
        {
        	$record = $formData["Record"];
        	foreach( $record as $key => $value )
        	{
        		$obj->exchangeArray($value);
	        	$arrayTableField = $obj->removeNoneTableField((array)$obj);
	        	
                $id = $this->objectRepository->saveRecord($value["Id"], $arrayTableField);
        	}
        }
        else
        {
            $formatType = isset($formData["FormatType"])?(int)$formData["FormatType"]:0;
            
            $record = [];

            switch($formatType)
            {
                case 2:
                    $record = $formData["DigitRecord"];
                    break;
                case 1:
                    $record = $formData["TextRecord"];
                    break;
                case 3:
                    $record = $formData["DateFormatRecord"];
                    break;
            }
            if(count($record))
            {
                $record["FormatType"] = $formatType;
                $record["Identify"]   = $formData["Identify"];

    	        $obj->exchangeArray($record);
    	        $arrayTableField = $obj->removeNoneTableField((array)$obj);
    	        $id = $this->objectRepository->saveRecord($id, $arrayTableField);
            }
        }
        
        $result = new JsonModel();
        return $result;
    }

    public function previewAction()
    {
    	$view = parent::listAction();
    	$identify = isset($_POST["Identify"])?$_POST["Identify"]:0;
    	$record = PreBuildTable::getFormatsTable()->getRecordByCondition(array("Identify"=>$identify));
    	$format = $this->getFormat($record);
    	$view->setVariables(
    					array
    					(
    						"format"	=>	$format,
    						'identify'	=>	$identify,
    						
    					)
    				);
    	return $view;
    }

    public function getFormat($record)
    {
    	$format = "";
    	foreach( $record as $key => $value )
    	{
    		$formatType = $value->FormatType;
    		switch($formatType)
    		{
    			case 1:$format .= $value->Name;break;
    			case 2:
    				$sLen = strlen($value->StartNum)+1;
    				for($i = $sLen; $i <= $value->Digit ; $i ++)
    				{
    					$format .= "0";
    				}
                    $format .= $value->StartNum;
    				break;
    		}
    	}
    	return $format;
    }
}