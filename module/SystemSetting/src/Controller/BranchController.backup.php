<?php
namespace SystemSetting\Controller;

use Zend\View\Model\ViewModel;
use PCSPLib\BaseClasses\BaseController;
use Interop\Container\ContainerInterface;
use SystemSetting\Model as SystemSetting;
use Zend\InputFilter\InputFilter;
use Zend\View\Model\JsonModel;
use SystemSetting\Form\BranchInputFilter;
use Zend\Mvc\MvcEvent;
use Zend\EventManager\EventManager;

class BranchController extends BaseController {
	
	protected $idenMenu = "setting";
	private $branchRepository;
	private $cache;
	private $cacheKey;
	
	public function __construct(ContainerInterface $serviceLocator)
	{
		$this->branchRepository = $serviceLocator->get(SystemSetting\BranchRepository::class);
		parent::__construct($serviceLocator, $this->branchRepository, 'Name', 'ASC');
		
		$this->cache = $serviceLocator->get('cache');
		$this->cacheKey = 'unique-cache-key';
	}
	
	public function indexAction()
	{
		$result = $this->cache->getItem($this->cacheKey, $success);
		print_r($result);
		return new ViewModel();
	}
	
	public function listAction()
	{
		$view = parent::listAction();
		
		$path = $this->getRequest()->getUri()->getPath();
		$path = $this->getRequest()->getUri()->getPath();
		$key = $this->bCrypt->createPassword($this->userLoginId).str_replace('/', '-', $path);
		$setKey = $this->setCatchKey($key);
		echo str_replace('/', '.', $path);exit;
		$gridHeaderProperties = $this->cache->getItem($this->cacheKey, $success);
		
		$view->setVariables(['gridHeaderProperties' => $gridHeaderProperties]);
		return $view;
	}
	
	public function gridPropertyChangeAction(){
		$postData = $this->request->getPost();
		if($this->request->isPost() && isset($postData['data'])){
			$key = 'unique-cache-key';
			$this->cache->removeItem($key);
			$this->cache->addItem($key, $postData['data']);
		}
		return new JsonModel(null);
	}
	
	public function newAction()
	{
		$view = parent::newAction();
		return $view;
	}
	
	public function saveRecordAction()
	{
		//input filter and validator
		$postArray = $this->request->getPost();
		if($this->request->isPost() && isset($postArray['Name'])){
			
			$inputFilter = new BranchInputFilter();
			$inputFilter->setData($postArray);

			
			if($inputFilter->isValid()){
				
				parent::saveRecordAction();
				exit;
				
			}else{
				$msg = "The form is invalid <br>";
				foreach ($inputFilter->getInvalidInput() as $invalidInput) {
					$msg .= "<p>".$invalidInput->getName(). ": " . implode(',', $invalidInput->getMessages())."</p><br>"; 
				}
				$data = ['msg'=>$msg];
				return new JsonModel($data);
			}
			
		}
		
	}
	
}