<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace SystemSetting\Controller;

use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use SystemSetting\Model AS SystemSetting;

class SystemSettingController extends BaseController
{
    protected $idenMenu = "setting";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(SystemSetting\BranchRepository::class);
        parent::__construct($serviceLocator, $table);
    }
}