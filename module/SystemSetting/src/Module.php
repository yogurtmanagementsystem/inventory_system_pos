<?php
/***
 * 
 * @author Sopheak Chhin
 * @date Aug 23, 2016
 * @time 1:55:59 PM
 */
namespace SystemSetting;

class Module
{
	public function getConfig()
	{
		return include __DIR__ . '/../config/module.config.php';
	}
}