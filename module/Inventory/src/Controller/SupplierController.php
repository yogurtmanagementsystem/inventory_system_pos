<?php
/**
 * @author Phin Heng
 * @since  Oct 20 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Interop\Container\ContainerInterface;
use Inventory\Model AS Inventory;
use PCSPLib\BaseClasses\BaseController;
use Zend\View\Model\JsonModel;

class SupplierController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Inventory\SupplierRepository::class);

        parent::__construct($container, $table);
    }

    public function isItemExist()
    {
        $formData = $this->request->getPost();
        
        $condition = "Name = '".trim($formData['Name']," ")."' AND Id != ".trim($formData['Id']," ");
        
        return $this->objectRepository->isExist($condition);
    }

    public function newAction()
    {
        $view = parent::newAction();
        $gridIdName = isset($_POST["gridIdName"])?$_POST["gridIdName"]:0;

        $view->setVariables(
                        [
                            "gridIdName"    =>  $gridIdName,
                            
                        ]
                    );

        return $view;
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] )?$formData["Record"]:[];
        $filterString = " 1 = 1 ";
        $tableName = $this->tableName;

        if( count( $record ) )
        {
            $condition = "";
            foreach( $record as $key => $value )
            {
                $s = "";
                if( !empty( $value["Name"] ) )
                {
                    $s = $tableName.".Name LIKE '%".trim($value["Name"],"")."%' ";
                }
                if( !empty( $value["Tel"] ) )
                {
                    if ( !empty( $s ) ) $s .= " AND ";
                    $s .= $tableName.".Tel LIKE '%".trim($value["Tel"])."%' ";
                }
                if( !empty( $value["Email"] ) )
                {
                    if ( !empty( $s ) ) $s .= " AND ";
                    $s .= $tableName.".Email LIKE '%".trim($value["Email"])."%' ";
                }
                if( !empty( $value["Website"] ) )
                {
                    if ( !empty( $s ) ) $s .= " AND ";
                    $s .= $tableName.".Website LIKE '%".trim($value["Website"])."%' ";
                }
                if( !empty( $value["Address"] ) )
                {
                    if ( !empty( $s ) ) $s .= " AND ";
                    $s .= $tableName.".Address LIKE '%".trim($value["Address"])."%' ";
                }
                if( !empty( $condition ) && !empty( $s ) )$condition .= " OR ";
                $condition .= $s;
            }
            if( !empty( $condition ) )
            {
                $filterString .= " AND ({$condition}) ";
            }
        }

        return $filterString;
    }
}