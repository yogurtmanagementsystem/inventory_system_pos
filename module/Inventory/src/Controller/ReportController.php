<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use Inventory\Model AS Inventory;
use Sale\Model AS Sale;

class ReportController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $repository = $serviceLocator->get(Sale\SaleRepository::class);

        parent::__construct($serviceLocator, $repository);
    }

    public function incomeAction()
    {
    	$view = parent::listAction();

    	return $view;
    }

    public function incomeContentAction()
    {
        $view = parent::newPopupAction();

        $fromDate = isset( $_POST["FromDate"] ) ? $_POST["FromDate"] : "";
        $toDate   = isset( $_POST["ToDate"]   ) ? $_POST["ToDate"]   : "";

        $condition = " 1 = 1 ";

        if( !empty( $fromDate ) ) 
        {
            $fromDate = date("Y-m-d", strtotime( $fromDate ) ) ;

            $condition .= " AND InvoiceDate >= '{$fromDate}' "; 
        }
        if( !empty( $toDate ) )
        {
            $toDate = date("Y-m-d", strtotime( $toDate ) );

            $condition .= " AND InvoiceDate <= '{$toDate}' ";
        }

        $record = $this->objectRepository->getAllRecords($condition)->toArray();

        $view->setVariables(
                [
                    "record" => $record,

                ]
            );

        return $view;
    }

    public function outcomeAction()
    {
    	$view = parent::listAction();

    	return $view;
    }

    public function outcomeContentAction()
    {
        $view = parent::newPopupAction();

        $fromDate = isset($_POST["FromDate"])?$_POST["FromDate"]:"";
        $toDate   = isset($_POST["ToDate"])?$_POST["ToDate"]:"";

        $condition = " 1 = 1 ";

        if( !empty( $fromDate ) )
        {
            $fromDate = date("Y-m-d", strtotime( $fromDate ) );

            $condition .= " AND ReceivedDate >= '{$fromDate}' ";
        }
        if( !empty( $toDate ) )
        {
            $toDate = date("Y-m-d", strtotime( $toDate ) );

            $condition .= " AND ReceivedDate <= '{$toDate}' ";
        }

        $record = $this->getService(Inventory\ItemReceiveRepository::class)->getRecordForReport($condition)->toArray();

        $record = $this->getMergeRecordForOutcome($record);

        $view->setVariables(
                [
                    "record" => $record,
                    
                ]
            );

        return $view;
    }

    public function getMergeRecordForOutcome($record)
    {
        $item = [];

        foreach( $record as $key => $value )
        {
            $receivedDate = strtotime($value["ReceivedDate"]);

            $total = $value["SystemQty"]*$value["SystemUnitPrice"];

            if( !isset( $item[$receivedDate] ) ) 
            {
                $item[$receivedDate]["ReceivedDate"] = $receivedDate;
                $item[$receivedDate]["Total"] = $total;
            }
            else
            {
                $item[$receivedDate]["Total"] += $total;
            }
        }

        return $item;
    }

    public function profitAction()
    {
    	$view = parent::listAction();

    	return $view;
    }

    public function profitContentAction()
    {
        $view = parent::newPopupAction();

        $fromDate = isset($_POST["FromDate"])?$_POST["FromDate"]:"";
        $toDate   = isset($_POST["ToDate"])?$_POST["ToDate"]:"";

        $condition = " 1 = 1 ";
        $condition1 = " 1 = 1 ";

        if( !empty( $fromDate ) )
        {
            $fromDate = date("Y-m-d", strtotime( $fromDate ) );

            $condition .= " AND ReceivedDate >= '{$fromDate}' ";
            $condition1.= " AND InvoiceDate  >= '{$fromDate}' ";
        }
        if( !empty( $toDate ) )
        {
            $toDate = date("Y-m-d", strtotime( $toDate ) );

            $condition .= " AND ReceivedDate <= '{$toDate}' ";
            $condition1.= " AND InvoiceDate  <= '{$toDate}' ";
        }

        $incomeRecord = $this->objectRepository->getAllRecords($condition1)->toArray();

        $outcomeRecord = $this->getService(Inventory\ItemReceiveRepository::class)->getRecordForReport($condition)->toArray();

        $record = $this->getMergeProfitRecord($incomeRecord,$outcomeRecord);

        $view->setVariables(
                [
                    "record" => $record,
                    
                ]
            );

        return $view;
    }

    public function getMergeProfitRecord($incomeRecord,$outcomeRecord)
    {
        $item = [];
        $tmpIncomeRecord = [];
        foreach( $incomeRecord as $key => $value )
        {
            $date = strtotime(date("Y-m", strtotime($value["InvoiceDate"])));

            if(!isset($tmpIncomeRecord[$date]))
                $tmpIncomeRecord[$date] = 0;

            $tmpIncomeRecord[$date] += $value["Total"];
        }
        $tmpOutcomeRecord = [];
        foreach( $outcomeRecord as $key => $value )
        {
            $date = strtotime(date("Y-m", strtotime($value["ReceivedDate"])));
 
            if(!isset($tmpOutcomeRecord[$date]))
                $tmpOutcomeRecord[$date] = 0;

            $tmpOutcomeRecord[$date] += $value["SystemA Q1QQty"]*$value["SystemUnitPrice"];
        }

        foreach( $tmpIncomeRecord as $key => $value )
        {
            $income  = $value;

            $outcome = 0;
            if(isset($tmpOutcomeRecord[$key]))
            {
                $outcome = $tmpOutcomeRecord[$key];

                unset($tmpOutcomeRecord[$key]);
            }

            $item[$key]["Date"]     = date(PHPConstant::FIX_DATE_FORMAT,$key);  ;       
            $item[$key]["Income"]   = $income;
            $item[$key]["Outcome"]  = $outcome;
            $item[$key]["Total"]    = $income-$outcome;
        }

        foreach( $tmpOutcomeRecord as $key => $value )
        {
            $item[$key]["Date"]     = date(PHPConstant::FIX_DATE_FORMAT,$key);       
            $item[$key]["Income"]   = 0;
            $item[$key]["Outcome"]  = $value;
            $item[$key]["Total"]    = $value;
        }

        return $item;
    }

    public function getIncomeRecordsForGridAction()
    {
        $page = isset($_POST['pagenum']) ? intval($_POST['pagenum']) : 0;
        $rows = isset($_POST['pagesize'])? intval($_POST['pagesize']) : PHPConstant::DEFAULT_PAGE_SIZE;
        $offset = $page*$rows;
        $sort = (isset($_POST['sortdatafield']) & !empty($_POST['sortdatafield'])) ? strval($_POST['sortdatafield']) : $this->defaultSortGridField;
        $order = (isset($_POST['sortorder']) & !empty($_POST['sortorder'])) ? strval($_POST['sortorder']) : $this->defaultSortGridType;

        $repository = $this->getService(Sale\SaleRepository::class);

        $fromDate = isset( $_POST["FromDate"] ) ? $_POST["FromDate"] : "";
        $toDate   = isset( $_POST["ToDate"] ) ? $_POST["ToDate"] : "";

        $filterString = " 1 = 1 ";
        if( !empty( $fromDate ) )
        {
            $fromDate = date("Y-m-d", strtotime( $fromDate ) );

            $filterString .= " AND InvoiceDate >= '{$fromDate}' ";
        }
        if( !empty( $toDate ) )
        {
            $toDate = date("Y-m-d", strtotime( $toDate ) );
            
            $filterString .= " AND InvoiceDate <= '{$toDate}' ";
        }

        $records  = $repository->getRecordsForGrid($offset, $rows, $sort, $order, $filterString)->toArray();
        $totalRow = $repository->getRowCountForGrid($filterString);

        $data[]   = ['total'=>$totalRow,'rows'=>$records];
        $result   = new JsonModel($data);
        
        return $result;
    }

    public function getOutcomeRecordsForGridAction()
    {
        $page = isset($_POST['pagenum']) ? intval($_POST['pagenum']) : 0;
        $rows = isset($_POST['pagesize'])? intval($_POST['pagesize']) : PHPConstant::DEFAULT_PAGE_SIZE;
        $offset = $page*$rows;
        $sort = (isset($_POST['sortdatafield']) & !empty($_POST['sortdatafield'])) ? strval($_POST['sortdatafield']) : $this->defaultSortGridField;
        $order = (isset($_POST['sortorder']) & !empty($_POST['sortorder'])) ? strval($_POST['sortorder']) : $this->defaultSortGridType;

        $repository = $this->getService(Inventory\ItemReceiveGroupRepository::class);

        $fromDate = isset( $_POST["FromDate"] ) ? $_POST["FromDate"] : "";
        $toDate   = isset( $_POST["ToDate"] ) ? $_POST["ToDate"] : "";

        $filterString = " 1 = 1 ";
        if( !empty( $fromDate ) )
        {
            $fromDate = date("Y-m-d", strtotime( $fromDate ) );

            $filterString .= " AND ReceivedDate >= '{$fromDate}' ";
        }
        if( !empty( $toDate ) )
        {
            $toDate = date("Y-m-d", strtotime( $toDate ) );
            
            $filterString .= " AND ReceivedDate <= '{$toDate}' ";
        }

        $records  = $repository->getOutcomeRecordsForGrid($offset, $rows, $sort, $order, $filterString)->toArray();
        $totalRow = $repository->getOutcomeRowCountForGrid($filterString);

        $data[]   = ['total' => $totalRow,'rows' => $records];

        $result   = new JsonModel($data);
        
        return $result;
    }

    public function incomeExportAction()
    {
        $fromDate = isset( $_GET["FromDate"] ) ? $_GET["FromDate"] : "";
        $toDate   = isset( $_GET["ToDate"]) ? $_GET["ToDate"] : "";

        $repository = $this->getService(Sale\SaleRepository::class);
        $tableName = $repository->getTableName();

        $condition = " 1 = 1 ";

        if( !empty( $fromDate ) )
        {
            $fromDate = date("Y-m-d", strtotime( $fromDate ) );

            $condition .= " AND {$tableName}.InvoiceDate >= '{$fromDate}' ";
        }
        if( !empty( $toDate ) )
        {
            $toDate = date("Y-m-d", strtotime( $toDate ) );
            
            $condition .= " AND {$tableName}.InvoiceDate <= '{$toDate}' ";
        }

        $record = $repository->getRecordForReport($condition)->toArray();

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $this->translator->translate("Income Date"));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', $this->translator->translate("Sub Total"));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', $this->translator->translate("Discount"));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', $this->translator->translate("Total"));

        $index = 2;

        foreach( $record as $key => $value )
        {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$index, $value["IncomeDate"]);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$index, $value["SubTotal"]);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$index, $value["TotalDiscount"]);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$index, $value["Total"]);

            $index ++;
        }

        $worksheet = "income-".date('YmdHis').".xls";

        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Disposition: attachment;filename="'.$worksheet.'"');
        header("Content-Type: application/vnd.ms-excel");
        header("Cache-Control: max-age=0");

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');
        
        return $view;
    }

    public function outcomeExportAction()
    {
        $fromDate = isset( $_GET["FromDate"] ) ? $_GET["FromDate"] : "";
        $toDate   = isset( $_GET["ToDate"]) ? $_GET["ToDate"] : "";

        $repository = $this->getService(Inventory\ItemReceiveGroupRepository::class);
        $tableName = $repository->getTableName();

        $condition = " 1 = 1 ";

        if( !empty( $fromDate ) )
        {
            $fromDate = date("Y-m-d", strtotime( $fromDate ) );

            $condition .= " AND {$tableName}.ReceivedDate >= '{$fromDate}' ";
        }
        if( !empty( $toDate ) )
        {
            $toDate = date("Y-m-d", strtotime( $toDate ) );
            
            $condition .= " AND {$tableName}.ReceivedDate <= '{$toDate}' ";
        }

        $record = $repository->getRecordForReport($condition)->toArray();

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $this->translator->translate("Outcome Date"));
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', $this->translator->translate("Total"));

        $index = 2;

        foreach( $record as $key => $value )
        {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$index, $value["OutcomeDate"]);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$index, $value["Total"]);

            $index ++;
        }

        $worksheet = "outcome-".date('YmdHis').".xls";

        $objPHPExcel->setActiveSheetIndex(0);
        header('Content-Disposition: attachment;filename="'.$worksheet.'"');
        header("Content-Type: application/vnd.ms-excel");
        header("Cache-Control: max-age=0");

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');
        
        return $view;
    }
}