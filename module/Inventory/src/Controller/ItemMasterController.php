<?php
/**
 * @author Phin Heng
 * @since  03 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Interop\Container\ContainerInterface;
use Inventory\Model AS Inventory;
use PCSPLib\BaseClasses\BaseController;
use Zend\View\Model\JsonModel;

class ItemMasterController extends BaseController
{
    protected $idenMenu = "inventory";
    private   $label    = "";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Inventory\ItemMasterRepository::class);

        parent::__construct($container, $table);
    }

    public function listAction()
    {
        $view = parent::listAction();

        $categoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();
        $unitRecord  = $this->getService(Inventory\ItemUnitMeasurementRepository::class)->getActiveRecordsForTreeDropDown()->toArray();

        $view->setVariables(
                    [
                        "categoryRecord" => $categoryRecord,
                        "unitRecord"     => $unitRecord,

                    ]
                );

        return $view;
    }

    public function newAction()
    {
    	$view = parent::newAction();
        $itemCode = isset($_POST["ItemCode"])?$_POST["ItemCode"]:"";

    	$itemCategoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();
        $itemMeasurementRecord = $this->getService(Inventory\ItemUnitMeasurementRepository::class)->getAllActiveRecords()->toArray();
        $itemMeasurementRecord = $this->getMergeGroupRecord($itemMeasurementRecord);
        
        $isInUsed = false;

        if( !empty( $itemCode ) )
        {
            $isInUsed = $this->isInUsed($itemCode);
        }

    	$view->setVariables(
    				[
    					"itemCategoryRecord"	=>	$itemCategoryRecord,
    					"itemMeasurementRecord" =>  $itemMeasurementRecord,
                        "isInUsed"   => $isInUsed,

    				]
    			);

    	return $view;
    }

    public function isInUsed($itemCode)
    {
        $repository = $this->getService(Inventory\StockBalanceMasterRepository::class);

        $isInUsed = $repository->isInUsed("ItemCode = '".$itemCode."'");

        return $isInUsed;
    }

    public function getMergeGroupRecord($record)
    {
        $item = [];
        $tmp  = [];
        $groupName = "";
        $index = 0;

        foreach( $record as $key => $value )
        {
            $id = $value["Id"];
            $parentId = $value["ParentId"];
            if($parentId)
            {
                $id = $parentId;
            }
            if(!isset($tmp[$id]))
            {
                $groupName = $value["Name"];
                $tmp[$id]["Name"] = $value["Name"];
                continue;
            }
            else
            {
                $groupName = $tmp[$id]["Name"];
            }
            $item[$index] = $value;
            $item[$index]["group"] = $groupName;
            $index ++;
        }

        return $item;
    }

    public function itemUnitMeasurementContentAction()
    {
        $view = parent::newPopupAction();

        $id  = isset($_POST["Id"])?(int)$_POST["Id"]:0;
        $isInUsed = isset($_POST["IsInUsed"])?$_POST["IsInUsed"]:0;
        $systemUnitLabel = isset($_POST["SystemUnitLabel"])?$_POST["SystemUnitLabel"]:0;
        $otherUsableUnit = isset($_POST["OtherUsableUnit"])?$_POST["OtherUsableUnit"]:"";
        $convertedValues = isset($_POST["ConvertedValues"])?$_POST["ConvertedValues"]:"";
        $defaultReceiveUnit = isset($_POST["DefaultReceiveUnit"])?$_POST["DefaultReceiveUnit"]:0;
        $defaultTakeUnit = isset($_POST["DefaultTakeUnit"])?$_POST["DefaultTakeUnit"]:0;

        $condition = " Id = {$id} OR ParentId = {$id}";
        $record = $this->getService(Inventory\ItemUnitMeasurementRepository::class)->getAllActiveRecords($condition)->toArray();
        $record = $this->getMergeRecordForItemMeasurementContent($record,$systemUnitLabel,$otherUsableUnit,$convertedValues);
        
        $view->setVariables(
                    [
                        "record"            =>  $record,
                        "systemUnitLabel"   =>  $systemUnitLabel,
                        "parentLabel"       =>  $this->label,
                        "defaultReceiveUnit"=>  $defaultReceiveUnit,
                        "defaultTakeUnit"   =>  $defaultTakeUnit,
                        "isInUsed" => $isInUsed,

                    ]
                );

        return $view;
    }

    public function getMergeRecordForItemMeasurementContent($record,$systemUnitLabel,$otherUsableUnit,$convertedValues)
    {
        $item = [];
        $otherUsableUnitArr = explode(",",$otherUsableUnit);
        $convertedValuesArr = explode(",",$convertedValues);
        $otherTmp = [];
        foreach( $otherUsableUnitArr as $key => $value )
        {
            $otherTmp[$value] = $value;
            $convertedValuesArr[$value] = $convertedValuesArr[$key];
        } 
        $index = 0;
        foreach( $record as $key => $value )
        {
            if(!$value["ParentId"])
            {
                $this->label = $value["Name"];
            }
            elseif( $value["Name"] != $systemUnitLabel )
            {
                $item[$index] = $value;
                $item[$index]["IsUsabled"] = isset($otherTmp[$value["Name"]])?true:false;
                $item[$index]["Value"] = isset($convertedValuesArr[$value["Name"]])?$convertedValuesArr[$value["Name"]]:0; 

                $index ++;
            }
        }

        return $item;
    }

    public function isItemExist()
    {
        $formData = $this->request->getPost();

        $condition = "Id <> ".$formData['Id']." AND Name = '".trim($formData['Name']," ")."'";
        
        return $this->objectRepository->isExist($condition);
    }

    public function saveRecordAction()
    {
        $id = isset($_POST['Id'])?intval($_POST['Id']) : 0;
        $formData = $this->request->getPost();
        
        $isInUsed = isset($formData["IsInUsed"])?$formData["IsInUsed"]:"";

        $isItemExist = $this->isItemExist();
        if($isItemExist)
        {
            return new JsonModel(
                            [
                                'msg'=>$this->translator->translate('ITEM_EXIST')
                            ]
                        );
        }

        $unitRecord = isset($formData["UnitRecord"])?$formData["UnitRecord"]:[];
        $otherUsableUnit = "";
        $convertedValues = "";
        foreach( $unitRecord as $key => $value )
        {
            if( !isset($value["Name"]) ) continue;
            if( !empty($otherUsableUnit) ) 
            {
                $otherUsableUnit .= ",";
                $convertedValues .= ",";
            }
            $otherUsableUnit .= $value["Name"];
            $convertedValues .= $value["Value"];
        }
        $object = $this->objectRepository->getTableObject();
        
        $object->exchangeArray($formData);
        $arrayTableField = $object->removeNoneTableField((array)$object);

        $arrayTableField["OtherUsableUnit"] = $otherUsableUnit;
        $arrayTableField["ConvertedValues"] = $convertedValues;

        $id = $this->objectRepository->saveRecord($id, $arrayTableField);
        
        $result   = new JsonModel();

        return $result;
    }

    public function isItemInUsed($id)
    {
        $repository = $this->getService(Inventory\StockBalanceMasterRepository::class);

        return $repository->isInUsed("Id = ".$id);
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] )?$formData["Record"]:[];
        $filterString = " 1 = 1 ";
        $tableName = $this->tableName;
        
        if( isset( $formData["Name"] ) && !empty( $formData["Name"] ) )
        {
            $filterString .= " AND {$tableName}.Name LIKE '%{$formData["Name"]}%'";
        }
        if( isset( $formData["ItemCategoryId"] ) && !empty( $formData["ItemCategoryId"] ) )
        {
            $filterString .= " AND {$tableName}.ItemCategoryId IN({$formData["ItemCategoryId"]})";
        }
        if( isset( $formData["ItemUnitMeasurement"] ) && !empty( $formData["ItemUnitMeasurement"] ) )
        {
            $filterString .= $this->getItemUnitMeasurementCondition($formData["ItemUnitMeasurement"],$tableName);
        }

        return $filterString;
    }

    public function getItemUnitMeasurementCondition($variance,$tableName)
    {
        $condition = "";

        $arr = explode(",", $variance);
        for( $i = 0 ; $i < count( $arr ) ; $i ++ )
        {
            if( !empty( $condition ) ) $condition .= " OR ";
            $condition .= " {$tableName}.OtherUsableUnit LIKE '".trim($arr[$i],"")."%'";
        }
        $condition = " AND ( ".$condition." ) ";
        return $condition;
    }
}