<?php
/**
 * @author Phin Heng
 * @since  Oct 20 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Interop\Container\ContainerInterface;
use PCSPLib\BaseClasses\BaseController;
use Zend\View\Model\JsonModel;
use Zend\Db\Sql\Expression;

use Inventory\Model AS Inventory;

class ItemTakeController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Inventory\ItemTakeUseRepository::class);

        parent::__construct($container, $table);
    }

    public function listAction()
    {
        $view = parent::listAction();

        $categoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();

        $view->setVariables(
                [
                    "categoryRecord" => $categoryRecord,

                ]
            );

        return $view;
    }

    public function newAction()
    {
        $view = parent::newAction();

        $itemCategoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();

        $view->setVariables(
                [
                    "itemCategoryRecord" => $itemCategoryRecord,

                ]
            );

        return $view;
    }

    public function getAllItemActiveRecordAction()
    {
        $categoryId = isset($_POST["CategoryId"])?(int)$_POST["CategoryId"]:0;
        $condition  = [ "ItemCategoryId" => $categoryId ];

        $record = $this->getService(Inventory\ItemMasterRepository::class)->getAllActiveRecords($condition)->toArray();

        return new JsonModel($record);
    }

    public function getAllItemUnitRecordAction()
    {
        $itemMasterId = isset($_POST["ItemMasterId"])?(int)$_POST["ItemMasterId"]:"";
        $condition    = [ "Id" => $itemMasterId ];
        $defaultUnit  = "";

        $record = $this->getService(Inventory\ItemMasterRepository::class)->getRecord($itemMasterId);
        
        $defaultSystemUnit = $record->DefaultSystemUnit;
        $convertedValues  = $record->ConvertedValues;

        $arr1 = [];
        if( !empty( $otherUsableUnit ) )
           $arr1 = explode(",", $otherUsableUnit);
        $arr4 = explode(",", $convertedValues);

        $currentQty = isset($record->CurrentQty)?$record->CurrentQty:0;

        $itemUnitRecord = [];

        if( $record->Id )
        {
            $defaultUnitId = "1".$defaultSystemUnit." = 1".$defaultSystemUnit.";$;1".";$;".$defaultSystemUnit;

            $itemUnitRecord[] = ["Id" => $defaultUnitId,"Name" => $defaultSystemUnit,"Value"=>1];
            $bool = false;
            foreach( $arr1 as $key => $value )
            {
                $id = "1".$value." = ".$arr4[$key].$defaultSystemUnit.";$;".$arr4[$key].";$;".$defaultSystemUnit;
                $itemUnitRecord[] = ["Id"=>$id,"Name"=>$value,"Value"=>$arr4[$key]];
                if( !$bool && $value == $defaultUnit )
                {
                    $defaultUnit = $id;
                    $bool = true;
                }
            }
            if( !$bool )
            {
                $defaultUnit = $defaultUnitId;
            }

            $itemUnitRecord = $this->sortItemUnitMeasurement($itemUnitRecord);
        }

        $item = [
                "ItemUnitRecord"    =>  $itemUnitRecord,
                "DefaultUnit"       =>  $defaultUnit,
                "CurrentQty" => $currentQty,

            ];

        return new JsonModel($item);
    }

    public function sortItemUnitMeasurement($record) 
    {
        $count = count($record);
        for ( $i = 0; $i < $count; $i ++ )
        {
            $j = $i;
            $value = $record[$i]["Value"];
            $tmp = $record[$i];
            while (($j > 0) && ($record[$j-1]["Value"] > $value))
            {
                $record[$j] = $record[$j-1];
                $j -- ;
            }
            $record[$j] = $tmp;
        }

        return $record;
    }

    public function saveRecordAction()
    {
        $formData = $this->request->getPost();

        $record = isset($formData["Record"])?$formData["Record"]:[];
        $takeUseDate = isset($formData["TakeUseDate"])?$formData["TakeUseDate"]:"";

        $object = $this->objectRepository->getTableObject();

        $groupRecord = date("YmdHi");

        $stockBalanceMasterRepository = $this->getService(Inventory\StockBalanceMasterRepository::class);
        $itemMasterRepository = $this->getService(Inventory\ItemMasterRepository::class);

        foreach( $record as $key => $value )
        {
            $systemQty    = $value["SystemQty"];
            $itemMasterId = $value["ItemMasterId"];

            $item = (array)$itemMasterRepository->getRecord($itemMasterId);

            $item = array_merge($item,$value);
            $item["GroupRecord"] = $groupRecord;
            $item["TakeUseDate"]   = $takeUseDate;

            $stockBalanceMasterRepository->updateQauntity(["ItemMasterId" => $itemMasterId],["CurrentQty" => new Expression("CurrentQty-".$systemQty)]);

            $object->exchangeArray($item);
            $arrayTableField = $object->removeNoneTableField((array)$object);

            $id = $this->objectRepository->saveRecord(0, $arrayTableField);
        }

        return new JsonModel();
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] )?$formData["Record"]:[];
        $filterString = " 1 = 1 ";
        $tableName = $this->tableName;
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;
        
        if( isset( $formData["FromDate"] ) && !empty( $formData["FromDate"] ) )
        {
            $filterString .= " AND {$tableName}.ReceivedDate >='".date("Y-m-d",strtotime( $formData["FromDate"] ) )."'";
        }
        if( isset( $formData["ToDate"] ) && !empty( $formData["ToDate"] ) )
        {
            $filterString .= " AND {$tableName}.ReceivedDate <='".date("Y-m-d",strtotime( $formData["ToDate"] ) )."'";
        }
        if( isset( $formData["ItemCategoryId"] ) && !empty( $formData["ItemCategoryId"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.ItemCategoryId IN({$formData["ItemCategoryId"]})";
        }
        if( isset( $formData["Name"] ) && !empty( $formData["Name"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.Name LIKE '%{$formData["Name"]}%' ";
        }

        return $filterString;
    }
}