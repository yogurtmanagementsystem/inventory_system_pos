<?php
/**
 * @author Phin Heng
 * @since  25 Oct 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\JsonModel;

use PCSPLib\BaseClasses\BaseController;
use Inventory\Model AS Inventory;

class ItemBrokenReasonController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Inventory\ItemBrokenReasonRepository::class);

        parent::__construct($container, $table);
    }

    public function isItemExist()
    {
        $formData = $this->request->getPost();
        $condition = "Name = '".trim($formData['Name']," ")."' AND Id != ".trim($formData['Id']," ");
        
        return $this->objectRepository->isExist($condition);
    }

    public function newAction()
    {
        $view = parent::newAction();
        $gridIdName = isset($_POST["gridIdName"])?$_POST["gridIdName"]:0;

        $view->setVariables(
            [
                "gridIdName"    =>  $gridIdName,

            ]
        );

        return $view;
    }
}