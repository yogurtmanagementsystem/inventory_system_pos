<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;
use Zend\Db\Sql\Expression;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;
use Inventory\Model AS Inventory;

class ItemReceiveController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $repository = $serviceLocator->get(Inventory\ItemReceiveRepository::class);

        parent::__construct($serviceLocator, $repository);
    }

    public function listAction()
    {
        $view = parent::listAction();

        $categoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();
        $supplierRecord = $this->getService(Inventory\SupplierRepository::class)->getAllActiveRecords()->toArray();
        $unitRecord = $this->getService(Inventory\ItemUnitMeasurementRepository::class)->getActiveRecordsForTreeDropDown()->toArray();

        $view->setVariables(
                    [
                        "categoryRecord" => $categoryRecord,
                        "supplierRecord" => $supplierRecord,
                        "unitRecord"     => $unitRecord,

                    ]
                );

        return $view;
    }
    public function newAction()
    {
    	$view = parent::newAction();

    	$itemCategoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();
    	$supplierRecord = $this->getService(Inventory\SupplierRepository::class)->getAllActiveRecords()->toArray();
    	
    	$view->setVariables(
    				[
    					"itemCategoryRecord" =>	$itemCategoryRecord,
    					"supplierRecord"	 => $supplierRecord,

    				]
    			);

    	return $view;
    }
    public function getAllItemActiveRecordAction()
    {
    	$categoryId = isset( $_POST["CategoryId"] ) ? (int)$_POST["CategoryId"]:0;
    	$condition  = [ "ItemCategoryId" => $categoryId ];

    	$record = $this->getService(Inventory\ItemMasterRepository::class)->getAllActiveRecords($condition,[],"Name","ASC")->toArray();

    	return new JsonModel($record);
    }
    public function getAllItemUnitRecordAction()
    {
    	$itemMasterId = isset($_POST["ItemMasterId"])?$_POST["ItemMasterId"]:"";

        $itemMasterRepository = $this->getService(Inventory\ItemMasterRepository::class);
        $tableName = $itemMasterRepository->getTableName();

    	$condition= ["{$tableName}.Id" => $itemMasterId];
        $defaultUnit = "";

    	$record = $itemMasterRepository->getRecord($condition);

    	$otherUsableUnit = $record->OtherUsableUnit;
	    $defaultSystemUnit = $record->DefaultSystemUnit;
	    $convertedValues  = $record->ConvertedValues;
        $arr1 = [];
        $arr2 = [];
        if( !empty( $otherUsableUnit ) )
    	   $arr1 = explode(",", $otherUsableUnit);
    	$arr4 = explode(",", $convertedValues);

    	$itemUnitRecord = [];

    	if( $record->Id )
    	{
            if( !empty( $record->DefaultReceiveUnit ) ) $defaultUnit = $record->DefaultReceiveUnit;
            else $defaultUnit = $defaultSystemUnit;

    		$itemUnitRecord[] = ["Id" => "1".$defaultSystemUnit." = 1".$defaultSystemUnit.";$;1".";$;".$defaultSystemUnit,"Name" => $defaultSystemUnit,"Value"=>1];
	    	$bool = false;
            foreach( $arr1 as $key => $value )
	    	{
                $id = "1".$value." = ".$arr4[$key].$defaultSystemUnit.";$;".$arr4[$key].";$;".$defaultSystemUnit;
	    		$itemUnitRecord[] = ["Id"=>$id,"Name"=>$value,"Value"=>$arr4[$key]];
                if( !$bool && $value == $defaultUnit )
                {
                    $defaultUnit = $id;
                    $bool = true;
                }
	    	}
            if( !$bool )
            {
                $defaultUnit = "1".$defaultSystemUnit." = 1".$defaultSystemUnit.";$;1".";$;".$defaultSystemUnit;
            }

	    	$itemUnitRecord = $this->sortItemUnitMeasurement($itemUnitRecord);
	    }

        $record = [
        		"ItemUnitRecord"	=>	$itemUnitRecord,
                "DefaultUnit"       =>  $defaultUnit,

        	];

    	return new JsonModel($record);
    }

    public function sortItemUnitMeasurement($record) 
    {
    	$count = count($record);
	    for ( $i = 0; $i < $count; $i ++ )
	    {
		    $j = $i;
		    $value = $record[$i]["Value"];
		    $tmp = $record[$i];
		    while (($j > 0) && ($record[$j-1]["Value"] > $value))
		    {
		        $record[$j] = $record[$j-1];
		        $j -- ;
		    }
		    $record[$j] = $tmp;
        }

        return $record;
    }

    public function saveItemReceiveGroup($repository,$record)
    {
        $object = $repository->getTableObject();
        $object->exchangeArray($record);
        $arrayTableField = $object->removeNoneTableField((array)$object);
        $arrayTableField["SystemReceivedDateTime"] = date("Y-m-d H:i");

        $id = $repository->saveRecord(0, $arrayTableField);

        return $id;
    }

    public function saveStockBalanceMasterRecord($repository,$record)
    {
        $itemMasterId = $record["ItemMasterId"];
        $systemQty    = $record["SystemQty"];
        $receiveDate  = strtotime($record["LastReceivedDate"]);
        $totalQty     = $systemQty;
        $currentQty   = $systemQty;

        $item = $repository->getRecord(["ItemMasterId"=>$itemMasterId]);
        $id = 0;

        if( $item->Id > 0 )
        {
            $totalQty   += $item->TotalQty;
            $currentQty += $item->CurrentQty;
            $currentReceiveDate = strtotime($item->LastReceivedDate);
            if( $receiveDate > $currentReceiveDate )
            {
                $receiveDate = date("Y-m-d",$receiveDate);
            }
            else
            {
                $receiveDate = date("Y-m-d",$currentReceiveDate);
            }
            $id = $item->Id;
        }
        else
        {
            $receiveDate = date("Y-m-d",$receiveDate);

            $this->getService(Inventory\ItemMasterRepository::class)->updateRecords(["IsInUsed" => 1],["Id" => $itemMasterId]);
        }

        $object = $repository->getTableObject();
        $object->exchangeArray($record);
        $arrayTableField = $object->removeNoneTableField((array)$object);

        $arrayTableField["TotalQty"]   = $totalQty;
        $arrayTableField["CurrentQty"] = $currentQty;
        $arrayTableField["LastReceivedDate"] = $receiveDate;
        
        $id = $repository->saveRecord($id, $arrayTableField);

        $object->resetObject();

        return $id;
    }

    public function saveItemReceiveRecord($repository,$record)
    {
        $object = $repository->getTableObject();
        $object->exchangeArray($record);
        $arrayTableField = $object->removeNoneTableField((array)$object);

        $id = $repository->saveRecord(0, $arrayTableField);

        $object->resetObject();

        return $id;
    }
    public function saveRecordAction()
    {
        $formData = $this->request->getPost();

        $record = isset( $formData["Record"] ) ? $formData["Record"] : [];

        $itemMasterId = isset( $record["ItemMasterId"] ) ? (int)$record["ItemMasterId"] : 0 ;

        $systemQty = isset( $record["SystemQty"] ) ? (int)$record["SystemQty"] : 0 ;

        $itemMasterRepository = $this->getService(Inventory\ItemMasterRepository::class);
        $sbmRepository = $this->getService(Inventory\StockBalanceMasterRepository::class);
        
        $mTableName = $itemMasterRepository->getTableName();
        $msTableName = $sbmRepository->getTableName();
        
        $condition = ["{$msTableName}.ItemMasterId" => $itemMasterId];
        $stockRecord = (array)$sbmRepository->getRecord($condition);

        $itemMasterRecord = (array)$itemMasterRepository->getRecord(["{$mTableName}.Id"=>$itemMasterId]);

        $item = array_merge($record,$itemMasterRecord);

        $stockBalanceMasterId = $stockRecord["Id"];

        if( $stockBalanceMasterId )
        {
            $item1 = [
                    "CurrentQty" => new Expression("CurrentQty+".$systemQty)
                ];

            $sbmRepository->updateRecords($item1,["Id"=>$stockBalanceMasterId]);
        }
        else
        { 
            $item["CurrentQty"] = $systemQty;

            $object1 = $sbmRepository->getTableObject();
            $object1->exchangeArray($item);
            $arrayTableField = $object1->removeNoneTableField((array)$object1);

            $stockBalanceMasterId = $sbmRepository->saveRecord(0,$arrayTableField);
        }

        $item["StockBalanceMasterId"] = $stockBalanceMasterId;
        
        $object = $this->objectRepository->getTableObject();
        $object->exchangeArray($item);
        $arrayTableField = $object->removeNoneTableField((array)$object);

        $this->objectRepository->saveRecord(0, $arrayTableField);

        return new JsonModel();
    }
    public function saveRecordAction_backup1()
    {
        $formData = $this->request->getPost();
        $record   = isset($formData["Record"])?$formData["Record"]:[];
        $itemRecieveGroupRecord = isset($formData["ItemRecieveGroup"])?$formData["ItemRecieveGroup"]:[];
        $receiveDate = $itemRecieveGroupRecord["ReceivedDate"];
        $supplierId  = $itemRecieveGroupRecord["SupplierId"];

        $irgRepository = $this->getService(Inventory\ItemReceiveGroupRepository::class);
        $itemMasterRepository = $this->getService(Inventory\ItemMasterRepository::class);
        $sbmRepository = $this->getService(Inventory\StockBalanceMasterRepository::class);
        
        $itemReceiveGroupId = $this->saveItemReceiveGroup($irgRepository,$itemRecieveGroupRecord);

        $itemMasterTable = $itemMasterRepository->getTableName();
        
        foreach( $record as $key => $value )
        {
            $categoryId = $value["ItemCategoryId"];

            foreach( $value["Child"] as $k => $val )
            {
                $item = $val;
                $systemQty = $val["SystemQty"];

                $itemMasterId = $val["ItemMasterId"];
                $condition = ["{$itemMasterTable}.Id" => $itemMasterId];
                
                $itemMasterRecord = (array)$itemMasterRepository->getRecord($condition);
                
                $item = array_merge($item,$itemMasterRecord);

                $item["ItemMasterId"]     = $itemMasterId;
                $item["LastReceivedDate"] = $receiveDate;

                // stock balance master
                $stockBalanceMasterId = $this->saveStockBalanceMasterRecord($sbmRepository,$item);

                $item["StockBalanceMasterId"] = $stockBalanceMasterId;
                $item = array_merge($item, $itemRecieveGroupRecord);
                // item receive
                $item["ReceiveGroupId"] = $itemReceiveGroupId;
                $itemReceiveId = $this->saveItemReceiveRecord($this->objectRepository,$item);
            }
        }

        return new JsonModel();
    }
    public function saveRecordAction_backup()
    {
        $formData = $this->request->getPost();
        $record   = isset($formData["Record"])?$formData["Record"]:[];
        $itemRecieveGroupRecord = isset($formData["ItemRecieveGroup"])?$formData["ItemRecieveGroup"]:[];
        $receiveDate = $itemRecieveGroupRecord["ReceivedDate"];
        $supplierId  = $itemRecieveGroupRecord["SupplierId"];

        $irgRepository = $this->getService(Inventory\ItemReceiveGroupRepository::class);
        $itemMasterRepository = $this->getService(Inventory\ItemMasterRepository::class);
        $sbmRepository = $this->getService(Inventory\StockBalanceMasterRepository::class);
        
        $itemReceiveGroupId = $this->saveItemReceiveGroup($irgRepository,$itemRecieveGroupRecord);

        $itemMasterTable = $itemMasterRepository->getTableName();
        
        foreach( $record as $key => $value )
        {
            $item = $value;
            $systemQty = $value["SystemQty"];
            $note = $value["Note"];

            $itemMasterId = $value["ItemMasterId"];
            $condition = ["{$itemMasterTable}.Id" => $itemMasterId];
            
            $itemMasterRecord = (array)$itemMasterRepository->getRecord($condition);
            
            $item = array_merge($item,$itemMasterRecord);

            $item["ItemMasterId"]     = $itemMasterId;
            $item["LastReceivedDate"] = $receiveDate;

            // stock balance master
            $stockBalanceMasterId = $this->saveStockBalanceMasterRecord($sbmRepository,$item);

            $item["StockBalanceMasterId"] = $stockBalanceMasterId;
            $item = array_merge($item, $itemRecieveGroupRecord);
            // item receive
            $item["ReceiveGroupId"] = $itemReceiveGroupId;
            $item["Note"]  = $note;
            $itemReceiveId = $this->saveItemReceiveRecord($this->objectRepository,$item);
        }

        return new JsonModel();
    }

    public function viewAction()
    {
        $view = parent::newAction();

        $receiveGroupId = isset($_POST["Id"])?(int)$_POST["Id"]:0;

        $groupRecord = $this->getService(Inventory\ItemReceiveGroupRepository::class)->getRecord($receiveGroupId);

        $record = $this->objectRepository->getAllActiveRecords(["ReceiveGroupId"=>$receiveGroupId])->toArray();
        
        $view->setVariables(
                    [
                        "record"        =>  $record,
                        "groupRecord"   =>  $groupRecord,
                        
                    ]
                );

        return $view;
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] )?$formData["Record"]:[];
        $filterString = " 1 = 1 ";
        $tableName = $this->tableName;
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;
        
        if( isset( $formData["FromDate"] ) && !empty( $formData["FromDate"] ) )
        {
            $filterString .= " AND {$tableName}.ReceivedDate >='".date("Y-m-d",strtotime( $formData["FromDate"] ) )."'";
        }
        if( isset( $formData["ToDate"] ) && !empty( $formData["ToDate"] ) )
        {
            $filterString .= " AND {$tableName}.ReceivedDate <='".date("Y-m-d",strtotime( $formData["ToDate"] ) )."'";
        }
        if( isset( $formData["ItemCategoryId"] ) && !empty( $formData["ItemCategoryId"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.ItemCategoryId IN({$formData["ItemCategoryId"]})";
        }
        if( isset( $formData["Name"] ) && !empty( $formData["Name"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.Name LIKE '%{$formData["Name"]}%' ";
        }

        return $filterString;
    }
}