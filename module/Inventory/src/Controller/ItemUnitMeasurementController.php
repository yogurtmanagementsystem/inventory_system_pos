<?php
/**
 * @author Phin Heng
 * @since  Oct 20 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Interop\Container\ContainerInterface;
use Inventory\Model AS Inventory;
use PCSPLib\BaseClasses\BaseController;
use Zend\View\Model\JsonModel;

class ItemUnitMeasurementController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Inventory\ItemUnitMeasurementRepository::class);

        parent::__construct($container, $table);
    }

    public function isItemExist()
    {
        $formData = $this->request->getPost();
        
        $condition = "Name = '".trim($formData['Name']," ")."' AND Id != ".trim($formData['Id']," ");
        
        return $this->objectRepository->isExist($condition);
    }

    public function newAction()
    {
        $view = parent::newAction();
        $gridIdName = isset($_POST["gridIdName"])?$_POST["gridIdName"]:0;

        $parentRecord = $this->objectRepository->getActiveRecordsForTreeDropDown(["ParentId"=>0])->toArray();
        
        $view->setVariables(
            [
                "gridIdName"	=>	$gridIdName,
                'parentRecord'  => $parentRecord,
                
            ]
        );

        return $view;
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] ) ? $formData["Record"] : [];

        $tableName = $this->tableName;
        
        $filterString = "";

        if( count( $record ) )
        {
            foreach( $record as $key => $value )
            {
                if( !empty( $value["Name"] ) )
                {
                    if ( !empty( $filterString ) ) $filterString .= " OR ";

                    $filterString .= " Name LIKE '%".trim( $value["Name"] )."%' ";
                }
            }

            if( !empty( $filterString ) )
            {
                $filterString = "(".$filterString.")";
            }
        }

        return $filterString;
    }

    public function getRecordsForTreeGridAction()
    {
        $condition = $this->getFilterCondition();

        if( isset( $_POST["ParentOnly"] ) && $_POST["ParentOnly"] )
        {
            $condition .= " AND ParentId = 0 ";
        }

        $records = $this->objectRepository->getRecordsForTreeGrid($condition)->toArray();

        $result = new JsonModel($records);

        return $result;
    }

    public function getAllActiveRecordsAction()
    {
        $record = $this->objectRepository->getAllActiveRecords()->toArray();
        $record = $this->getMergeGroupRecord($record);

        return new JsonModel($record);
    }

    public function getMergeGroupRecord($record)
    {
        $item = [];
        $tmp  = [];
        $groupName = "";
        $index = 0;

        foreach( $record as $key => $value )
        {
            $id = $value["Id"];
            $parentId = $value["ParentId"];
            if($parentId)
            {
                $id = $parentId;
            }
            if(!isset($tmp[$id]))
            {
                $groupName = $value["Name"];
                $tmp[$id]["Name"] = $value["Name"];
                continue;
            }
            else
            {
                $groupName = $tmp[$id]["Name"];
            }
            $item[$index] = $value;
            $item[$index]["group"] = $groupName;
            $index ++;
        }

        return $item;
    }

    public function getActiveRecordsForTreeDropDownAction()
    {
        $condition = " ParentId = 0 AND IsActive = 1 ";
        
        $records = $this->objectRepository->getRecordsForTreeGrid($condition)->toArray();

        $result = new JsonModel($records);

        return $result;
    }
}