<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use Inventory\Model AS Inventory;

class InventoryController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $repository = $serviceLocator->get(Inventory\SupplierRepository::class);

        parent::__construct($serviceLocator, $repository);
    }
}