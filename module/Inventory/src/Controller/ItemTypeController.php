<?php
/**
 * @author Phin Heng
 * @since  25 Oct 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\JsonModel;

use PCSPLib\BaseClasses\BaseController;
use Inventory\Model AS Inventory;

class ItemTypeController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Inventory\ItemTypeRepository::class);

        parent::__construct($container, $table);
    }

    public function isItemExist()
    {
        $formData = $this->request->getPost();
        $condition = "Name = '".trim($formData['Name']," ")."' AND Id != ".trim($formData['Id']," ");
        
        return $this->objectRepository->isExist($condition);
    }

    public function newAction()
    {
        $view = parent::newAction();
        $gridIdName = isset($_POST["gridIdName"])?$_POST["gridIdName"]:0;

        $view->setVariables(
            [
                "gridIdName"    =>  $gridIdName,

            ]
        );

        return $view;
    }

    public function saveRecordAction()
    {
        $id   = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $formData = $this->request->getPost();
        $data = [];
        
        try
        {
            $record = $this->objectRepository->getTableObject();
            $record->exchangeArray($formData);
            $arrayTableField = $record->removeNoneTableField((array)$record);

            if(!$this->isItemExist())
            {
                $id = $this->objectRepository->saveRecord($id, $arrayTableField);
                $data = ['Id'=>$id];

                if( isset( $formData["OldName"] ) && trim( $formData["OldName"],"" ) != trim( $formData["Name"],"" ) )
                {
                    $itemMasterRepository = $this->getService(Inventory\ItemMasterRepository::class);
                    $itemMasterRepository->updateRecords(["TypeName" => trim($formData["Name"],"") ], ["TypeName" => trim($formData["OldName"],"") ] );
                }
            }
            else
            {
                $data = ['msg'=>$this->translator->translate('ITEM_EXIST')];
            }

            $result   = new JsonModel($data);
            return $result;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}