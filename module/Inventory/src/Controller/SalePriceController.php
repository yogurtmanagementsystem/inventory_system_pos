<?php
/**
 * @author Phin Heng
 * @since  01 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Interop\Container\ContainerInterface;
use Inventory\Model AS Inventory;
use PCSPLib\BaseClasses\BaseController;
use Zend\View\Model\JsonModel;

class SalePriceController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Inventory\SalePriceRepository::class);

        parent::__construct($container, $table);
    }

    public function newAction()
    {
    	$view = parent::newAction();

        $id = isset($_POST["Id"])?(int)$_POST["Id"]:0;

    	$stockMasterId = isset($_POST["StockMasterId"])?(int)$_POST["StockMasterId"]:0;
    	$itemMasterId = isset($_POST["ItemMasterId"])?(int)$_POST["ItemMasterId"]:0;

    	$customerGroupRecord = $this->getService(Inventory\CustomerGroupRepository::class)->getAllActiveRecords()->toArray();
        $groupPriceRecord = $this->getService(Inventory\SaleGroupPriceRepository::class)->getAllRecords(["SalePriceId" => $id])->toArray();
        $tierPriceRecord = $this->getService(Inventory\SaleTierPriceRepository::class)->getAllRecords(["SalePriceId" => $id])->toArray();

    	$view->setVariables(
    				[
    					"stockMasterId"	=> $stockMasterId,
                        "itemMasterId"  => $itemMasterId,
                        "groupPriceRecord"    => $groupPriceRecord,
    					"tierPriceRecord"	  => $tierPriceRecord,
    					"customerGroupRecord" => $customerGroupRecord,

    				]
    			);

    	return $view;
    }

    public function getMergeRecordForGrid($record)
    {
        $item = [];
        $tmp  = [];
        $currentDate = strtotime(date("Y-m-d"));

        foreach( $record as $key => $value )
        {
        	$fromDate = $value["ActivePriceFromDate"];
        	$toDate   = $value["ActivePriceToDate"];
        	$isActive = 0;
        	if( empty( $fromDate ) && empty( $toDate ) && $value["SalePriceId"] != null )
        		$isActive = 1;
        	else
        	{
        		if( !empty( $fromDate ) && !empty( $toDate ) )
        		{
        			$fromDate = strtotime($fromDate);
        			$toDate = strtotime($toDate);
        			if( $fromDate <= $currentDate && $toDate >= $currentDate )
        				$isActive = 1;
        		}
        		elseif( !empty( $fromDate ) )
        		{
        			$fromDate = strtotime($fromDate);
        			if( $fromDate <= $currentDate )
        				$isActive = 1;
        		}
        		else
        		{
        			$toDate = strtotime($toDate);
        			if( $toDate >= $currentDate )
        				$isActive = 1;
        		}
        	}
        	if( $isActive == 1 )
        	{
        		$tmp[$value["ItemMasterId"]] = 1;
        	}
        	$item[$key] = $value;
        	$item[$key]["IsActivePrice"] = $isActive;
        }
        $record = [];
        foreach( $item as $key => $value )
        {
        	$record[$key] = $value;
        	$record[$key]["IsHasPriceActive"] = isset($tmp[$value["ItemMasterId"]])?1:0;
        }

        return $record;
    }

    public function isExistDate($type,$currentActivePriceFromDate,$currentActivePriceToDate,$activePriceFromDate,$activePriceToDate)
    {
    	$isExist = false;

    	switch( $type )
    	{
    		case 1:
    			if( !empty( $activePriceFromDate ) && !empty( $activePriceToDate ) )
    			{
    				if( ( $currentActivePriceFromDate <= $activePriceFromDate && $currentActivePriceToDate > $activePriceFromDate ) || ( $currentActivePriceFromDate >= $activePriceFromDate && ( $currentActivePriceToDate < $activePriceToDate || ( $currentActivePriceToDate >= $activePriceToDate && $currentActivePriceToDate > $activePriceFromDate ) ) ) )
    				{
    					$isExist = true;
    				}
    			}
    			elseif( !empty( $activePriceFromDate ) )
    			{
    				if( $currentActivePriceToDate > $activePriceFromDate )
    				{
    					$isExist = true;
    				}
    			}
    			elseif( $currentActivePriceFromDate < $activePriceToDate )
    			{
    				$isExist = true;
    			}

    			break;
    		case 2:
    			if( !empty( $activePriceFromDate ) && !empty( $activePriceToDate ) )
    			{
    				if( $currentActivePriceFromDate < $activePriceToDate )
    				{
    					$isExist = true;
    				}
    			}
    			elseif( !empty( $activePriceFromDate ) )
    			{
    				$isExist = true;
    			}
    			elseif( $currentActivePriceFromDate < $activePriceToDate )
    			{
    				$isExist = true;
    			}
    			break;
    		case 3:
    			if( !empty( $activePriceFromDate ) && !empty( $activePriceToDate ) )
    			{
    				if( $currentActivePriceToDate > $activePriceFromDate )
    				{
    					$isExist = true;
    				}
    			}
    			elseif( !empty( $activePriceFromDate ) )
    			{
    				if( $currentActivePriceToDate > $activePriceFromDate )
    				{
    					$isExist = true;
    				}
    			}
    			else
    			{
    				$isExist = true;
    			}
    			break;
    	}

    	return $isExist;
    }

    public function isItemExist()
    {
    	$formData = $this->request->getPost();

    	$id = isset($formData["Id"])?(int)$formData["Id"]:0;
    	$itemMasterId = isset($formData["ItemMasterId"])?(int)$formData["ItemMasterId"]:0;
    	$activePriceFromDate = isset($formData["ActivePriceFromDate"])?$formData["ActivePriceFromDate"]:"";
    	$activePriceToDate   = isset($formData["ActivePriceToDate"])?$formData["ActivePriceToDate"]:"";

    	$record = $this->objectRepository->getAllRecords( "Id <> {$id} AND ItemMasterId = ".$itemMasterId );
    	if( empty( $activePriceFromDate ) && empty( $activePriceToDate ) )
    	{
    		if( count( $record ) )
    			return true;
    		else return false;
    	}
    	if( !empty( $activePriceFromDate ) ) $activePriceFromDate = strtotime($activePriceFromDate);
    	if( !empty( $activePriceToDate ) ) $activePriceToDate = strtotime($activePriceToDate);
    	$currentDate = strtotime(date("Y-m-d"));
    	$isExist = false;
    	$type = 0;
    	foreach( $record as $key => $value )
    	{
    		if( $isExist )break;

    		$currentActivePriceFromDate = $value->ActivePriceFromDate;
    		$currentActivePriceToDate = $value->ActivePriceToDate;
    		if( !empty( $currentActivePriceFromDate ) && !empty( $currentActivePriceToDate ) )
    		{
    			$type = 1;
    			$currentActivePriceFromDate = strtotime($currentActivePriceFromDate);
    			$currentActivePriceToDate = strtotime($currentActivePriceToDate);
    		}
    		elseif( !empty( $currentActivePriceFromDate ) )
    		{
    			$type = 2;
    			$currentActivePriceFromDate = strtotime($currentActivePriceFromDate);
    		}
    		elseif( !empty( $currentActivePriceToDate ) )
    		{
    			$type = 3;
    			$currentActivePriceToDate = strtotime($currentActivePriceToDate);
    		}
    		else
    		{
    			$isExist = true;
    		}
    		if( !$isExist )
    		{
				$isExist = $this->isExistDate($type,$currentActivePriceFromDate,$currentActivePriceToDate,$activePriceFromDate,$activePriceToDate);
    		}
    	}

    	return $isExist;
    }

    public function saveRecordAction()
    {
        $id = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $stockMasterId = isset($_POST['StockMasterId']) ? intval($_POST['StockMasterId']) : 0;
        $itemMasterId  = isset($_POST['ItemMasterId']) ? intval($_POST['ItemMasterId']) : 0;
        $formData = $this->request->getPost();
        $data = [];
		
		$isExist = $this->isItemExist();
		if( !$isExist )
		{
            $groupPriceRecord = isset($formData["GroupPriceRecord"])?$formData["GroupPriceRecord"]:[];
            $tierPriceRecord = isset($formData["TierPriceRecord"])?$formData["TierPriceRecord"]:[];

			$object = $this->objectRepository->getTableObject();
			$object->exchangeArray($formData);
			$arrayTableField = $object->removeNoneTableField((array)$object);

            if( count( $groupPriceRecord ) ) $arrayTableField["IsGroupPrice"] = 1;

			$id = $this->objectRepository->saveRecord($id, $arrayTableField);

            $groupPriceRepository = $this->getService(Inventory\SaleGroupPriceRepository::class);
            $groupPriceObject = $groupPriceRepository->getTableObject();

            $groupPriceRepository->deleteRecord(["SalePriceId" => $id ],0);

            foreach( $groupPriceRecord as $key => $value )
            {
                $item = $value;
                $item["SalePriceId"] = $id;
                $item["StockMasterId"] = $stockMasterId;
                $item["ItemMasterId"]  = $itemMasterId;

                $groupPriceObject->exchangeArray($item);
                $arrayTableField = $groupPriceObject->removeNoneTableField((array)$groupPriceObject);

                $gpId = $groupPriceRepository->saveRecord(0, $arrayTableField);
            }

            $tierPriceRepository = $this->getService(Inventory\SaleTierPriceRepository::class);
            $tierPriceObject = $tierPriceRepository->getTableObject();

            $tierPriceRepository->deleteRecord(["SalePriceId" => $id ],0);

            foreach( $tierPriceRecord as $key => $value )
            {
                $item = $value;
                $item["SalePriceId"] = $id;
                $item["StockMasterId"] = $stockMasterId;
                $item["ItemMasterId"]  = $itemMasterId;

                $tierPriceObject->exchangeArray($item);
                $arrayTableField = $tierPriceObject->removeNoneTableField((array)$tierPriceObject);

                $tpId = $tierPriceRepository->saveRecord(0, $arrayTableField);
            }

			$data = [ 'Id' => $id ];
		}
		else
		{
			$data = [ 'msg' => $this->translator->translate('ITEM_EXIST')];
		}

		$result   = new JsonModel($data);

		return $result;
    }
}