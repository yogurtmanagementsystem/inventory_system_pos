<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;
use Inventory\Model AS Inventory;

class StockAlertController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $repository = $serviceLocator->get(Inventory\StockBalanceMasterRepository::class);

        parent::__construct($serviceLocator, $repository);
    }

    public function listAction()
    {
        $view = parent::listAction();

        $categoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();
        $unitRecord = $this->getService(Inventory\ItemUnitMeasurementRepository::class)->getActiveRecordsForTreeDropDown()->toArray();

        $view->setVariables(
                    [
                        "categoryRecord" => $categoryRecord,
                        "unitRecord"     => $unitRecord,

                    ]
                );

        return $view;
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] ) ? $formData["Record"] : [];

        $tableName = $this->tableName;
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;

        $filterString = "{$tableName}.CurrentQty <= {$itemMasterTable}.AlertQty";
        
        if( isset( $formData["ItemCategoryId"] ) && !empty( $formData["ItemCategoryId"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.ItemCategoryId IN({$formData["ItemCategoryId"]})";
        }
        if( isset( $formData["Name"] ) && !empty( $formData["Name"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.Name LIKE '%{$formData["Name"]}%' ";
        }

        return $filterString;
    }
}