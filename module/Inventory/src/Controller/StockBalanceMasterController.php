<?php
/**
 * @author Phin Heng
 * @since  28 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;
use Inventory\Model AS Inventory;

class StockBalanceMasterController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $repository = $serviceLocator->get(Inventory\StockBalanceMasterRepository::class);

        parent::__construct($serviceLocator, $repository);
    }
    public function listAction()
    {
        $view = parent::listAction();

        $categoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();
        $unitRecord = $this->getService(Inventory\ItemUnitMeasurementRepository::class)->getActiveRecordsForTreeDropDown()->toArray();

        $view->setVariables(
                    [
                        "categoryRecord" => $categoryRecord,
                        "unitRecord"     => $unitRecord,

                    ]
                );

        return $view;
    }
    public function newAction()
    {
        $view = parent::newAction();

        $itemCategoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();
        $supplierRecord = $this->getService(Inventory\SupplierRepository::class)->getAllActiveRecords()->toArray();
        
        $view->setVariables(
                    [
                        "itemCategoryRecord" => $itemCategoryRecord,
                        "supplierRecord"     => $supplierRecord,

                    ]
                );

        return $view;
    }
    public function viewAction()
    {
        $view = parent::newAction();

        $itemMasterId = isset( $_POST["ItemMasterId"] ) ? (int)$_POST["ItemMasterId"] : 0;

        $itemReceiveRecord = $this->getService(Inventory\ItemReceiveRepository::class)->getAllActiveRecords(["ItemMasterId"=>$itemMasterId])->toArray();

        // $itemReceiveRecord = $this->getMergeRecordForView($itemReceiveRecord);
        // echo json_encode($itemReceiveRecord);exit();
        $view->setVariables(
                    [
                        "itemReceiveRecord" => $itemReceiveRecord
                    ]
                );

        return $view;
    }
    public function getMergeRecordForView($record)
    {
        $item = [];

        foreach( $record as $key => $value )
        {
            $groupId = $value["ReceiveGroupId"];

            if( !isset( $item[$groupId] ) )
            {
                $item[$groupId]["Supplier"] = $value["Supplier"];
                $item[$groupId]["ReceivedDate"] = $value["ReceivedDate"];
            }

            $item[$groupId]["Child"][] = $value;
        }

        return $item;
    }
    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] )?$formData["Record"]:[];
        $filterString = " 1 = 1 ";
        $tableName = $this->tableName;
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;
        
        if( isset( $formData["ItemCategoryId"] ) && !empty( $formData["ItemCategoryId"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.ItemCategoryId IN({$formData["ItemCategoryId"]})";
        }
        if( isset( $formData["Name"] ) && !empty( $formData["Name"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.Name LIKE '%{$formData["Name"]}%' ";
        }

        return $filterString;
    }
    public function updateRecordAction()
    {   
        $id = isset( $_POST["Id"] ) ? (int)$_POST["Id"] : 0 ;
        $qty= isset( $_POST["Qty"] )? floatval($_POST["Qty"]) : 0;

        $this->objectRepository->updateRecords(["CurrentQty"=>$qty],["Id"=>$id]);
 
        return new JsonModel();
    }
}