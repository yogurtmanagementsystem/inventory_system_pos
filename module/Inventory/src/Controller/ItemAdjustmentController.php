<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;
use Zend\Db\Sql\Expression;
use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;
use Inventory\Model AS Inventory;

class ItemAdjustmentController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $repository = $serviceLocator->get(Inventory\ItemAdjustmentRepository::class);

        parent::__construct($serviceLocator, $repository);
    }

    public function listAction()
    {
        $view = parent::listAction();

        $categoryRecord = $this->getService(Inventory\ItemCategoryRepository::class)->getAllActiveRecords()->toArray();
        $supplierRecord = $this->getService(Inventory\SupplierRepository::class)->getAllActiveRecords()->toArray();
        $unitRecord = $this->getService(Inventory\ItemUnitMeasurementRepository::class)->getActiveRecordsForTreeDropDown()->toArray();

        $view->setVariables(
                    [
                        "categoryRecord" => $categoryRecord,
                        "supplierRecord" => $supplierRecord,
                        "unitRecord"     => $unitRecord,

                    ]
                );

        return $view;
    }

    public function newAction()
    {
        $view = parent::newAction();

        $id   = isset($_POST["Id"])?(int)$_POST["Id"]:0;
        $pre  = isset($_POST["Pre"])?$_POST["Pre"]:0;
        
        $supplierRecord = $this->getService(Inventory\SupplierRepository::class)->getAllActiveRecords()->toArray();
        $record = $this->getService(Inventory\StockBalanceMasterRepository::class)->getRecord($id);
        $unitMeasurementRecord = $this->getUnitMeasurementRecord($record);
        
        $view->setVariables(
                    [
                        "record" => $record,
                        "supplierRecord" =>  $supplierRecord,
                        "unitMeasurementRecord" => $unitMeasurementRecord,
                        "pre"   =>  $pre,

                    ]
                );

        return $view;
    }

    public function getUnitMeasurementRecord($record)
    {
        $otherUsableUnit = $record->OtherUsableUnit;
        $defaultSystemUnit = $record->DefaultSystemUnit;
        $convertedValues   = $record->ConvertedValues;
        $arr1 = [];
        if( !empty( $otherUsableUnit ) )
            $arr1 = explode(",", $otherUsableUnit);
        $arr4 = explode(",", $convertedValues);

        $itemUnitRecord[] = ["Id" => "1".$defaultSystemUnit." = 1".$defaultSystemUnit.";$;1".";$;".$defaultSystemUnit,"Name" => $defaultSystemUnit,"Value"=>1];
        foreach( $arr1 as $key => $value )
        {
            $itemUnitRecord[] = ["Id"=>"1".$value." = ".$arr4[$key].$defaultSystemUnit.";$;".$arr4[$key].";$;".$defaultSystemUnit,"Name"=>$value,"Value"=>$arr4[$key]];
        }

        $itemUnitRecord = $this->sortItemUnitMeasurement($itemUnitRecord);

        return $itemUnitRecord;
    }

    public function sortItemUnitMeasurement($record) 
    {
        $count = count($record);
        for ( $i = 0; $i < $count; $i ++ )
        {
            $j = $i;
            $value = $record[$i]["Value"];
            $tmp = $record[$i];
            while ( ( $j > 0 ) && ( $record[$j-1]["Value"] > $value ) )
            {
                $record[$j] = $record[$j-1];
                $j -- ;
            }
            $record[$j] = $tmp;
        }

        return $record;
    }
    
    public function newAdjustmentAction()
    {
    	$view = parent::newAction();

        $sbRepository = $this->getService(Inventory\StockBalanceMasterRepository::class);
    	$record = $sbRepository->getAllActiveRecords()->toArray();

        $view->setVariables(
                    [
                        "record"    =>   $record,

                    ]
                );

    	return $view;
    }

    public function getSaveAdjustmentRecord($repository,$record)
    {
        $object = $repository->getTableObject();
        $object->exchangeArray($record);
        $arrayTableField = $object->removeNoneTableField((array)$object);
        $arrayTableField["AdjustedDate"] = date("Y-m-d");

        $id = $repository->saveRecord(0, $arrayTableField);

        return $id;
    }

    public function getMergeArray($record,$item)
    {
        foreach( $item as $key => $value )
        {
            $record[$key] = $value;
        }
        return $record;
    }

    public function saveRecordAction()
    {
        $formData = $this->request->getPost();
        $stockBalanceMasterId = isset($formData["StockBalanceMasterId"])?(int)$formData["StockBalanceMasterId"]:0;
        $itemMasterId  = isset($formData["ItemMasterId "])?(int)$formData["ItemMasterId "]:0;

        $sbRepository = $this->getService(Inventory\StockBalanceMasterRepository::class);
        
        $systemQty = $formData["SystemQty"];

        
        $record = (array)$sbRepository->getRecord($stockBalanceMasterId);

        $item = $this->getMergeArray($record,$formData);
        
        $adjustmentId = $this->getSaveAdjustmentRecord($this->objectRepository,$item);

        $item = [
            "CurrentQty" => new Expression("CurrentQty+".($systemQty)),
            "TotalQty"   => new Expression("TotalQty+".($systemQty))
        ];

        $tableName = $sbRepository->getTableName();

        $condition = " {$tableName}.ItemMasterId=".$itemMasterId;
        $sbRepository->updateQauntity([ "Id" => $stockBalanceMasterId ], $item);

        return new JsonModel();
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] )?$formData["Record"]:[];
        $filterString = " 1 = 1 ";
        $tableName = $this->tableName;
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;
        
        if( isset( $formData["FromDate"] ) && !empty( $formData["FromDate"] ) )
        {
            $filterString .= " AND {$tableName}.AdjustedDate >='".date("Y-m-d",strtotime( $formData["FromDate"] ) )."'";
        }
        if( isset( $formData["ToDate"] ) && !empty( $formData["ToDate"] ) )
        {
            $filterString .= " AND {$tableName}.AdjustedDate <='".date("Y-m-d",strtotime( $formData["ToDate"] ) )."'";
        }
        if( isset( $formData["ItemCategoryId"] ) && !empty( $formData["ItemCategoryId"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.ItemCategoryId IN({$formData["ItemCategoryId"]})";
        }
        if( isset( $formData["Name"] ) && !empty( $formData["Name"] ) )
        {
            $filterString .= " AND {$itemMasterTable}.Name LIKE '%{$formData["Name"]}%' ";
        }

        return $filterString;
    }
}