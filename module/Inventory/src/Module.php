<?php
/**
 * @author Phin Heng
 * @since  Oct 06 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\Adapter\AdapterInterface;
use Interop\Container\ContainerInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;
use PCSPLib\BaseClasses\BaseController;
use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\BaseClasses\BaseObject;
use PCSPLib\TableConstant;

class Module implements ConfigProviderInterface
{
	const VERSION = '3.0.0dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}