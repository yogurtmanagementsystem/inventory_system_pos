<?php
/**
 * @author Phin Heng
 * @since  17 Oct 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use PCSPLib\BaseClasses\BaseObject;

class ItemReceiveGroup extends BaseObject
{
    public $SupplierId;
    public $ReceivedDate;
    public $SystemReceivedDateTime;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->SupplierId  = isset($data['SupplierId'])? (!empty($data['SupplierId'])?trim($data['SupplierId']," "):0) : 0;
        $this->ReceivedDate  = isset($data['ReceivedDate'])? (!empty($data['ReceivedDate'])? date("Y-m-d", strtotime(trim($data['ReceivedDate']))):null) : null;
        $this->SystemReceivedDateTime   = isset($data['SystemReceivedDateTime'])? (!empty($data['SystemReceivedDateTime'])? date("Y-m-d H:i", strtotime(trim($data['SystemReceivedDateTime']))):null) : null;
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }
}