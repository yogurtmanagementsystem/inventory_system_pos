<?php
/**
 * @author Phin Heng
 * @since  10 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Inventory\Model\BaseItemMasterObject;

class ItemReceive extends BaseItemMasterObject
{
    public $StockBalanceMasterId;
    public $ReceiveGroupId;
    public $SupplierId;
    
    public $SystemQty;
    public $SystemUnitPrice;
    public $ReceivedQty;
    public $ReceivedUnitPrice;
    public $ReceivedUnitMeasurement;
    public $ReceivedDate;
    
    public $ReceivedUnitMeasurementPrefix;
    public $ReceivedQtyPrefix;
    public $ReceivedUnitPricePrefix;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->StockBalanceMasterId 	= isset($data['StockBalanceMasterId'])? (!empty($data['StockBalanceMasterId'])?trim($data['StockBalanceMasterId']," "):0) : 0;
        
        $this->ReceiveGroupId = isset($data['ReceiveGroupId'])? (!empty($data['ReceiveGroupId'])?trim($data['ReceiveGroupId']," "):0) : 0;
        $this->SupplierId  = isset($data['SupplierId'])? (!empty($data['SupplierId'])?trim($data['SupplierId']," "):0) : 0;
        $this->SystemQty   = isset($data['SystemQty'])? (!empty($data['SystemQty'])?trim($data['SystemQty']," "):0) : 0;
        $this->ReceivedQty = isset($data['ReceivedQty'])? (!empty($data['ReceivedQty'])?trim($data['ReceivedQty']," "):0) : 0;
        $this->SystemUnitPrice = isset($data['SystemUnitPrice'])? (!empty($data['SystemUnitPrice'])?trim($data['SystemUnitPrice']," "):0) : 0;
        $this->ReceivedUnitPrice = isset($data['ReceivedUnitPrice'])? (!empty($data['ReceivedUnitPrice'])?trim($data['ReceivedUnitPrice']," "):0) : 0;
        
        $this->ReceivedUnitMeasurementPrefix = isset($data['ReceivedUnitMeasurementPrefix'])?trim($data['ReceivedUnitMeasurementPrefix']," "):"";
        $this->ReceivedQtyPrefix = isset($data['ReceivedQtyPrefix'])?trim($data['ReceivedQtyPrefix']," "):"";
        $this->ReceivedUnitPricePrefix = isset($data['ReceivedUnitPricePrefix'])?trim($data['ReceivedUnitPricePrefix']," "):"";
       
        $this->ReceivedUnitMeasurement= isset($data['ReceivedUnitMeasurement'])?trim($data['ReceivedUnitMeasurement']," "):"";
       
        $this->ReceivedDate = isset($data['ReceivedDate'])? (!empty($data['ReceivedDate'])? date("Y-m-d",strtotime(trim($data['ReceivedDate']))):null) : null;
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }

    public function resetObject()
    {
        foreach( $this as $key => $value )
        {
            $this->$key = null;
        }
    }
}