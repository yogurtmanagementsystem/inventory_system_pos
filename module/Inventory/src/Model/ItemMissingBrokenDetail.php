<?php
/**
 * @author Phin Heng
 * @since  10 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use PCSPLib\BaseClasses\BaseObject;

class ItemMissingBrokenDetail extends BaseObject
{
    public $StockBalanceMasterId;
    public $StockBalanceVarianceId;
    public $StockBalanceVarianceWarehouseId;
    public $StockBalanceDetailId;
    public $ItemMasterId;
    public $ItemMethodId;
    public $ItemMissingBrokenId;
    public $WarehouseId;
    
    public $ItemCode;
    public $ItemKey;
    public $ItemKeyByWarehouse;
    public $ItemKeyDetail;
    public $ItemBarcode;
    public $MethodName;
    public $SystemQty;
    public $MissingBrokenQty;
    public $MissingBrokenUnitMeasurement;
    public $MissingBrokenDate;

    public $DefaultSystemUnit;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->StockBalanceDetailId     = isset($data['StockBalanceDetailId'])? (!empty($data['StockBalanceDetailId'])?trim($data['StockBalanceDetailId']," "):0) : 0;
        $this->StockBalanceMasterId 	= isset($data['StockBalanceMasterId'])? (!empty($data['StockBalanceMasterId'])?trim($data['StockBalanceMasterId']," "):0) : 0;
        $this->StockBalanceVarianceId 	= isset($data['StockBalanceVarianceId'])? (!empty($data['StockBalanceVarianceId'])?trim($data['StockBalanceVarianceId']," "):0) : 0;
        $this->StockBalanceVarianceWarehouseId 		= isset($data['StockBalanceVarianceWarehouseId'])? (!empty($data['StockBalanceVarianceWarehouseId'])?trim($data['StockBalanceVarianceWarehouseId']," "):0) : 0;
        $this->ItemMasterId = isset($data['ItemMasterId'])? (!empty($data['ItemMasterId'])?trim($data['ItemMasterId']," "):0) : 0;
        $this->ItemMethodId = isset($data['ItemMethodId'])? (!empty($data['ItemMethodId'])?trim($data['ItemMethodId']," "):0) : 0;
        $this->WarehouseId 	= isset($data['WarehouseId'])? (!empty($data['WarehouseId'])?trim($data['WarehouseId']," "):0) : 0;
        $this->ItemMissingBrokenId = isset($data['ItemMissingBrokenId'])? (!empty($data['ItemMissingBrokenId'])?trim($data['ItemMissingBrokenId']," "):0) : 0;
        $this->SystemQty   = isset($data['SystemQty'])? (!empty($data['SystemQty'])?trim($data['SystemQty']," "):0) : 0;
        $this->MissingBrokenQty = isset($data['MissingBrokenQty'])? (!empty($data['MissingBrokenQty'])?trim($data['MissingBrokenQty']," "):0) : 0;
      
        $this->ItemCode = isset($data['ItemCode'])?trim($data['ItemCode']," "):"";
        $this->ItemKey 	= isset($data['ItemKey'])?trim($data['ItemKey']," "):"";
        $this->ItemKeyByWarehouse 	= isset($data['ItemKeyByWarehouse'])?trim($data['ItemKeyByWarehouse']," "):"";
        $this->ItemKeyDetail = isset($data['ItemKeyDetail'])?trim($data['ItemKeyDetail']," "):"";
        $this->ItemBarcode= isset($data['ItemBarcode'])?trim($data['ItemBarcode']," "):"";
        $this->MethodName 	= isset($data['MethodName'])?trim($data['MethodName']," "):"";
        $this->ItemCode 		= isset($data['ItemCode'])?trim($data['ItemCode']," "):"";
        $this->ItemBarcode 		= isset($data['ItemBarcode'])?trim($data['ItemBarcode']," "):"";
        $this->ItemKey 			= isset($data['ItemKey'])?trim($data['ItemKey']," "):"";
        $this->MethodName 		= isset($data['MethodName'])?trim($data['MethodName']," "):"";
        $this->MissingBrokenUnitMeasurement 	= isset($data['MissingBrokenUnitMeasurement'])?trim($data['MissingBrokenUnitMeasurement']," "):"";
        $this->DefaultSystemUnit 		= isset($data['DefaultSystemUnit'])?trim($data['DefaultSystemUnit']," "):"";
        
        $this->MissingBrokenDate = isset($data['MissingBrokenDate'])? (!empty($data['MissingBrokenDate'])? trim($data['MissingBrokenDate']):null) : null;
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }
}