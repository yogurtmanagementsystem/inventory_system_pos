<?php
/**
 * @author Phin Heng
 * @since  10 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Inventory\Model\BaseItemMasterObject;

class ItemMissingBroken extends BaseItemMasterObject
{
    public $SystemQty;
    public $MissingQty;
    public $MissingDate;
    public $MissingUnitMeasurement;
    public $GroupRecord;
    public $ItemBrokenReasonId;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->ItemBrokenReasonId   = isset($data['ItemBrokenReasonId'])? (!empty($data['ItemBrokenReasonId'])?trim($data['ItemBrokenReasonId']," "):0) : 0;
        $this->SystemQty   = isset($data['SystemQty'])? (!empty($data['SystemQty'])?trim($data['SystemQty']," "):0) : 0;
        $this->MissingQty = isset($data['MissingQty'])? (!empty($data['MissingQty'])?trim($data['MissingQty']," "):0) : 0;
        
        $this->MissingUnitMeasurement = isset($data['MissingUnitMeasurement'])?trim($data['MissingUnitMeasurement']," "):"";
        $this->GroupRecord  = isset($data['GroupRecord'])?trim($data['GroupRecord']," "):"";
        $this->MissingDate = isset($data['MissingDate'])? (!empty($data['MissingDate'])? date("Y-m-d",strtotime(trim($data['MissingDate']))):null) : null;
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }
}