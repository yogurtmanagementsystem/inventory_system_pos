<?php
/**
 * @author Phin Heng
 * @since  Oct 20 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use PCSPLib\BaseClasses\BaseObject;

class ItemUnitMeasurement extends BaseObject
{
	public $ParentId;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->ParentId = isset($data['ParentId'])? (!empty($data['ParentId'])?trim($data['ParentId']," "):0) : 0;
    }
}