<?php
/**
 * @author Phin Heng
 * @since  10 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Inventory\Model\BaseItemMasterObject;

class ItemAdjustment extends BaseItemMasterObject
{
    public $StockBalanceMasterId;
    public $SupplierId;
    
    public $SystemQty;
    public $SystemUnitPrice;
    public $AdjustedQty;
    public $AdjustedUnitPrice;
    public $AdjustedUnitMeasurement;
    public $AdjustedDate;

    public $AdjustmentUnitMeasurementPrefix;
    public $AdjustmentQtyPrefix;
    public $AdjustmentUnitPricePrefix;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->StockBalanceMasterId   = isset($data['StockBalanceMasterId'])? (!empty($data['StockBalanceMasterId'])?trim($data['StockBalanceMasterId']," "):0) : 0;
        
        $this->SupplierId  = isset($data['SupplierId'])? (!empty($data['SupplierId'])?trim($data['SupplierId']," "):0) : 0;
        $this->SystemQty   = isset($data['SystemQty'])? (!empty($data['SystemQty'])?trim($data['SystemQty']," "):0) : 0;
        $this->AdjustedQty = isset($data['AdjustedQty'])? (!empty($data['AdjustedQty'])?trim($data['AdjustedQty']," "):0) : 0;
        $this->SystemUnitPrice = isset($data['SystemUnitPrice'])? (!empty($data['SystemUnitPrice'])?trim($data['SystemUnitPrice']," "):0) : 0;
        $this->AdjustedUnitPrice = isset($data['AdjustedUnitPrice'])? (!empty($data['AdjustedUnitPrice'])?trim($data['AdjustedUnitPrice']," "):0) : 0;
         
        $this->AdjustedUnitMeasurement 	= isset($data['AdjustedUnitMeasurement'])?trim($data['AdjustedUnitMeasurement']," "):"";
        $this->AdjustedDate = isset($data['AdjustedDate'])? (!empty($data['AdjustedDate'])? trim($data['AdjustedDate']):null) : null;
    
        $this->AdjustmentUnitMeasurementPrefix = isset($data['AdjustmentUnitMeasurementPrefix'])?trim($data['AdjustmentUnitMeasurementPrefix']," "):"";
        $this->AdjustmentQtyPrefix = isset($data['AdjustmentQtyPrefix'])?trim($data['AdjustmentQtyPrefix']," "):"";
        $this->AdjustmentUnitPricePrefix = isset($data['AdjustmentUnitPricePrefix'])?trim($data['AdjustmentUnitPricePrefix']," "):"";
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }
}