<?php
/**
 * @author Phin Heng
 * @since  10 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class ItemReceiveRepository extends BaseRepository
{
	public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
	{
        $receiveGroupTable = TableConstant::INV_ITEM_RECEIVE_GROUP_TABLE;
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;
    	$supplierTable = TableConstant::INV_SUPPLIER_TABLE;
        $DATE_FORMAT = PHPConstant::DATE_FORMAT;

        $select = "SELECT
                        {$this->tableName}.*,
                        DATE_FORMAT(ReceivedDate,'{$DATE_FORMAT}') AS ReceivedDate,
                        {$itemMasterTable}.Name,
                        {$itemMasterTable}.CategoryName,
                        {$receiveGroupTable}.SupplierName
                    FROM 
                        {$this->tableName}
                    LEFT JOIN 
                        {$itemMasterTable}
                        ON 
                            {$itemMasterTable}.Id = {$this->tableName}.ItemMasterId 
                    LEFT JOIN 
                    (
                        SELECT 
                            {$receiveGroupTable}.Id,
                            {$supplierTable}.Name AS SupplierName 
                        FROM 
                            {$receiveGroupTable}
                        LEFT JOIN 
                            {$supplierTable}
                            ON 
                                {$supplierTable}.Id = {$receiveGroupTable}.SupplierId
                    ) {$receiveGroupTable}
                        ON 
                            {$receiveGroupTable}.Id = {$this->tableName}.ReceiveGroupId
                ";

                    
        $select .= " WHERE ".$this->tableName.".IsDeleted = 0 ";

        if(!empty($filterString))
            $select .= "AND ".$filterString;
        $select .= " ORDER BY ".$sort." ".$order;
        $select .= " LIMIT ".$rows;
                
        $statement = $this->getDBAdaper()->createStatement($select);
        $results = $statement->execute();
        $resultSet = new ResultSet();
        $resultSet->initialize($results);

        return $resultSet; 
	}

    public function getRowCountForGrid($filterString)
    {
        $receiveGroupTable = TableConstant::INV_ITEM_RECEIVE_GROUP_TABLE;
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;
        
        $select = "SELECT
                        COUNT({$this->tableName}.Id) AS Num 
                    FROM 
                        {$this->tableName}
                    LEFT JOIN 
                        {$itemMasterTable}
                        ON 
                            {$itemMasterTable}.Id = {$this->tableName}.ItemMasterId 
                ";

                    
        $select .= " WHERE ".$this->tableName.".IsDeleted = 0 ";

        if(!empty($filterString))
            $select .= "AND ".$filterString;
        
        $statement = $this->getDBAdaper()->createStatement($select);
        $results = $statement->execute();
        $resultSet = new ResultSet();
        $resultSet->initialize($results);

        $item = $resultSet->current();

        return $item['Num'];
    }

    public function getAllActiveRecords($condition = "",$columns = array(), $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;
        $supplierTable = TableConstant::INV_SUPPLIER_TABLE;
        
        $select = $this->sql->select($this->tableName);
        if(count($columns))
            $select->columns($columns);

        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",["Name","CategoryName"],'left');
        $select->join("{$supplierTable}","{$this->tableName}.SupplierId = {$supplierTable}.Id",["Supplier"=>"Name"],'left');

        $select->where(["{$this->tableName}.IsDeleted"=>0,"{$this->tableName}.IsActive"=>1]);
        if(!empty($condition))
            $select->where($condition);
        $select->order($orderField." ".$orderType);

        if(!empty($groupField))
        {
            $select->group($groupField);
        }
        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($result);
        
        return $resultSet;
    }
}