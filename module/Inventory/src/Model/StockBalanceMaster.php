<?php
/**
 * @author Phin Heng
 * @since  10 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use PCSPLib\BaseClasses\BaseObject;

use Inventory\Model\BaseItemMasterObject;

class StockBalanceMaster extends BaseItemMasterObject
{
    public $TotalQty;
    public $CurrentQty;
    public $LastReceivedDate;
    
    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->TotalQty     = isset($data['TotalQty'])? (!empty($data['TotalQty'])?trim($data['TotalQty']," "):0) : 0;
        $this->CurrentQty   = isset($data['CurrentQty'])? (!empty($data['CurrentQty'])?trim($data['CurrentQty']," "):0) : 0;
        
        $this->LastReceivedDate  = isset($data['LastReceivedDate'])? (!empty($data['LastReceivedDate'])? trim($data['LastReceivedDate']):null) : null;
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }

    public function resetObject()
    {
        foreach( $this as $key => $value )
        {
            $this->$key = null;
        }
    }
}