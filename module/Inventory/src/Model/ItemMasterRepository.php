<?php
/**
 * @author Phin Heng
 * @since  Oct 20 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class ItemMasterRepository extends BaseRepository
{
	public function getRecord($condition, $columns = array(),$orderField = "",$orderType = "",$groupField = "" )
    {
    	$stockBalanceMasterTable = TableConstant::INV_STOCK_BALANCE_MASTER_TABLE;

        if(is_numeric($condition))
        {
            $condition = ["{$this->tableName}.Id" => $condition];
        }
        $select = $this->sql->select($this->tableName);
        if(count($columns)>0)
            $select->columns($columns);

        $select->join("{$stockBalanceMasterTable}","{$stockBalanceMasterTable}.ItemMasterId = {$this->tableName}.Id",["CurrentQty"],"left");

        $select->where(["{$this->tableName}.IsDeleted"=>0]);
        $select->where($condition);

        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();


        $resultSet = new ResultSet();
        $resultSet->initialize($result);
        $item = $resultSet->current();

        if (!$item)
        {
            return $this->objectPrototype;
        }
        
        return $item;
    }
}