<?php
/**
 * @author Phin Heng
 * @since  Oct 20 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;


use PCSPLib\BaseClasses\BaseObject;

class ItemMaster extends BaseObject
{
    public $ItemCategoryId;
    public $IsInUsed;

    public $SystemUnitLabelId;
    public $DefaultSystemUnit;
    public $SystemUnitLabel;
    public $DefaultReceiveUnit;
    public $DefaultTakeUnit;
    public $OtherUsableUnit;
    public $ConvertedValues;
    public $CategoryName;
    public $IsTrack;
    public $IsAlert;
    public $AlertQty;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->ItemCategoryId = isset($data['ItemCategoryId'])? (!empty($data['ItemCategoryId'])?trim($data['ItemCategoryId']," "):0) : 0;
        $this->IsInUsed       = isset($data['IsInUsed'])? (!empty($data['IsInUsed'])?trim($data['IsInUsed']," "):0) : 0;
        $this->IsTrack        = isset($data['IsTrack'])? (!empty($data['IsTrack'])?trim($data['IsTrack']," "):0) : 0;
        $this->IsAlert        = isset($data['IsAlert'])? (!empty($data['IsAlert'])?trim($data['IsAlert']," "):0) : 0;
        $this->AlertQty 	  = isset($data['AlertQty'])? (!empty($data['AlertQty'])?trim($data['AlertQty']," "):0) : 0;
        
        $this->DefaultSystemUnit= isset($data['DefaultSystemUnit'])?trim($data['DefaultSystemUnit']," "):"";
        $this->SystemUnitLabel 	= isset($data['SystemUnitLabel'])?trim($data['SystemUnitLabel']," "):"";
        $this->DefaultReceiveUnit=isset($data['DefaultReceiveUnit'])?trim($data['DefaultReceiveUnit']," "):"";
        $this->DefaultTakeUnit 	= isset($data['DefaultTakeUnit'])?trim($data['DefaultTakeUnit']," "):"";
        $this->OtherUsableUnit 	= isset($data['OtherUsableUnit'])?trim($data['OtherUsableUnit']," "):"";
        $this->ConvertedValues 	= isset($data['ConvertedValues'])?trim($data['ConvertedValues']," "):"";
        $this->CategoryName 	= isset($data['CategoryName'])?trim($data['CategoryName']," "):"";
        
        $this->SystemUnitLabelId= isset($data['SystemUnitLabelId'])? (!empty($data['SystemUnitLabelId'])?trim($data['SystemUnitLabelId']," "):0) : 0;
    }
}