<?php
/**
 * @author Phin Heng
 * @since  10 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Inventory\Model\BaseItemMasterObject;

class ItemTakeUse extends BaseItemMasterObject
{
    public $SystemQty;
    public $TakeUseQty;
    public $TakeUseDate;
    public $TakeUseUnitMeasurement;
    public $GroupRecord;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->SystemQty   = isset($data['SystemQty'])? (!empty($data['SystemQty'])?trim($data['SystemQty']," "):0) : 0;
        $this->TakeUseQty = isset($data['TakeUseQty'])? (!empty($data['TakeUseQty'])?trim($data['TakeUseQty']," "):0) : 0;
        
        $this->TakeUseUnitMeasurement = isset($data['TakeUseUnitMeasurement'])?trim($data['TakeUseUnitMeasurement']," "):"";
        $this->GroupRecord  = isset($data['GroupRecord'])?trim($data['GroupRecord']," "):"";
        $this->TakeUseDate = isset($data['TakeUseDate'])? (!empty($data['TakeUseDate'])? date("Y-m-d",strtotime(trim($data['TakeUseDate']))):null) : null;
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }
}