<?php
/**
 * @author Phin Heng
 * @since  17 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class ItemReceiveGroupRepository extends BaseRepository
{
	public function getRecord($condition, $columns = array())
    {
        if(is_numeric($condition))
        {
            $condition = array("{$this->tableName}.Id"=>$condition);
        }

    	$supplierTable = TableConstant::INV_SUPPLIER_TABLE;

        $select    = $this->sql->select($this->tableName);

        if(count($columns)>0)
            $select->columns($columns);

        $select->join("{$supplierTable}","{$this->tableName}.SupplierId = {$supplierTable}.Id",[ "SupplierName" => "Name" ],'left');
        
        $select->where(["{$this->tableName}.IsDeleted"=>0]);
        $select->where($condition);

        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($result);
        $item = $resultSet->current();
        
        return $item;
    }

    public function getOutcomeRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        $receiveTable = TableConstant::INV_ITEM_RECEIVE_TABLE;
        
        $select = $this->sql->select($this->tableName);
        $select->columns(
                    [
                        "*",
                        "OutcomeDate" => new Expression("DATE_FORMAT(".$this->tableName.".ReceivedDate,'%d-%b-%Y')")
                    ]
                );

        $select->join("{$receiveTable}","{$this->tableName}.Id = {$receiveTable}.ReceiveGroupId",["Total" => new Expression("SUM(".$receiveTable.".ReceivedQty*".$receiveTable.".ReceivedUnitPrice)")],'left');
        
        if( !empty( $filterString ) ) $select->where($filterString);

        $select->where("{$this->tableName}.IsDeleted = 0 ");
        $select->order($sort.' '.$order);
        $select->limit($rows)->offset($offset);
        $select->group($this->tableName.".ReceivedDate");
        
        $stmt   = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getOutcomeRowCountForGrid($filterString)
    {
        $receiveTable = TableConstant::INV_ITEM_RECEIVE_TABLE;

        $select = $this->sql->select($this->tableName);
        $select->columns(array('Num' => new Expression('COUNT(DISTINCT '.$this->tableName.'.ReceivedDate)')));
        
        // $select->join("{$receiveTable}","{$this->tableName}.ReceiveGroupId = {$receiveTable}.Id",[],'left');
        
        // $select->where("{$this->tableName}.IsDeleted = 0");
        // if(!empty($filterString))$select->where($filterString);
        $select->limit(1);
        
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        $resultSet = new HydratingResultSet();
        $resultSet->initialize($result);
        $item = $resultSet->current();

        return $item['Num'];
    }

    public function getRecordForReport( $condition = "" )
    {
        $receiveTable = TableConstant::INV_ITEM_RECEIVE_TABLE;

        $select = $this->sql->select($this->tableName);
        $select->columns(
                    [
                        "*",
                        "OutcomeDate" => new Expression("DATE_FORMAT(".$this->tableName.".ReceivedDate,'%d-%b-%Y')")
                    ]
                );

        $select->join("{$receiveTable}","{$this->tableName}.Id = {$receiveTable}.ReceiveGroupId",["Total" => new Expression("SUM(".$receiveTable.".ReceivedQty*".$receiveTable.".ReceivedUnitPrice)")],'left');

        $select->where( [ "{$this->tableName}.IsDeleted" => 0] );

        if(!empty($condition))
            $select->where($condition);

        $select->group($this->tableName.".ReceivedDate");

        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($result);
        
        return $resultSet;
    }
}