<?php
/**
 * @author Phin Heng
 * @since  22 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;
use Zend\Db\Sql\Update;

class StockBalanceMasterRepository extends BaseRepository
{
    public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;

        $select = $this->sql->select($this->tableName);
        $select->columns(
                    [
                        "*",
                        "LastReceivedDate" => new Expression("DATE_FORMAT(LastReceivedDate,'".PHPConstant::DATE_FORMAT."')")
                    ]
                );
            
        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",["Name","CategoryName"],'left');
        
        if( !empty( $filterString ) ) $select->where($filterString);

        $select->where("{$this->tableName}.IsDeleted = 0");

        $select->order($sort.' '.$order);
        $select->limit($rows)->offset($offset);
        
        $stmt   = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getRowCountForGrid($filterString)
    {
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;

        $select = $this->sql->select($this->tableName);
        $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT('.$this->tableName.'.Id)')));
        
        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",[],'left');
        
        $select->where("{$this->tableName}.IsDeleted = 0");

        if( !empty( $filterString ) )$select->where($filterString);

        $select->limit(1);
        
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        $resultSet = new HydratingResultSet();
        $resultSet->initialize($result);
        $item = $resultSet->current();

        return $item['Num'];
    }

    public function getAllActiveRecords($condition = "",$columns = array(), $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;

        $select = $this->sql->select($this->tableName);
            
        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",["Name","CategoryName"],'left');
        
        $select->where(["{$this->tableName}.IsDeleted"=>0,"{$this->tableName}.IsActive"=>1]);
        
        if(!empty($condition))
            $select->where($condition);
        $select->order($orderField." ".$orderType);

        if(!empty($groupField))
        {
            $select->group($groupField);
        }
        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($result);
        
        return $resultSet;
    }

    public function getRecord($condition, $columns = array(),$orderField = "",$orderType = "",$groupField = "" )
    {
        if(is_numeric($condition))
        {
            $condition = array("{$this->tableName}.Id"=>$condition);
        }

        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;

        $select = $this->sql->select($this->tableName);
            
        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",["Name","CategoryName"],'left');

        $select->where($condition);

        if( !empty( $orderField ) && !empty( $orderType ) )
        {
            $select->order($orderField." ".$orderType);
        }

        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($result);
        $item = $resultSet->current();
        if( !$item )
        {
            $item = $this->getTableObject();
        }

        return $item;
    }

    public function updateQauntity($condition,$item)
    {
        $update = new Update($this->tableName);

        $update->set($item);
        $update->where($condition);

        $statement = $this->sql->prepareStatementForSqlObject($update);

        $result = $statement->execute();
    }

    public function getCountAlertRecord( $condition = "" )
    {
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;

        $select = $this->sql->select($this->tableName);
        $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT('.$this->tableName.'.Id)')));
            
        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",[],'left');
        
        if( !empty( $condition ) )
            $select->where($condition);

        $select->where("{$this->tableName}.CurrentQty <= {$itemMasterTable}.AlertQty");
        $select->where(["{$itemMasterTable}.IsDeleted" => 0,"{$itemMasterTable}.IsActive" => 1]);

        $select->limit(1);
        
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        $resultSet = new HydratingResultSet();
        $resultSet->initialize($result);
        $item = $resultSet->current();

        return $item['Num'];
    }

    public function getAllAlertRecord($condition = "", $limit = 0 )
    {
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;

        $select = $this->sql->select($this->tableName);
            
        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",["Name","CategoryName"],'left');
        
        $select->where(["{$itemMasterTable}.IsDeleted" => 0,"{$itemMasterTable}.IsActive" => 1]);
        
        if( !empty( $condition ) )
            $select->where($condition);

        $select->where("{$this->tableName}.CurrentQty <= {$itemMasterTable}.AlertQty");

        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($result);
        
        return $resultSet;
    }
}