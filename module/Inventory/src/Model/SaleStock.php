<?php
/**
 * @author Phin Heng
 * @since  10 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Inventory\Model\BaseItemMasterObject;

class SaleStock extends BaseItemMasterObject
{
    public $SystemQty;
    public $StockQty;
    public $StockDate;
    public $StockUnitMeasurement;
    public $GroupRecord;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->SystemQty   = isset($data['SystemQty'])? (!empty($data['SystemQty'])?trim($data['SystemQty']," "):0) : 0;
        $this->StockQty = isset($data['StockQty'])? (!empty($data['StockQty'])?trim($data['StockQty']," "):0) : 0;
        
        $this->StockUnitMeasurement = isset($data['StockUnitMeasurement'])?trim($data['StockUnitMeasurement']," "):"";
        $this->GroupRecord  = isset($data['GroupRecord'])?trim($data['GroupRecord']," "):"";
        $this->StockDate = isset($data['StockDate'])? (!empty($data['StockDate'])? date("Y-m-d",strtotime(trim($data['StockDate']))):null) : null;
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }
}