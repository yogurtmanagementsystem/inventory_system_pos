<?php
/**
 * @author Phin Heng
 * @since  Oct 20 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use PCSPLib\BaseClasses\BaseObject;

class Supplier extends BaseObject
{
	public $Tel;
	public $Address;
	public $Email;
	public $Website;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->Tel 		= isset($data['Tel']) ? trim($data['Tel']) : "";
        $this->Address  = isset($data['Address']) ? trim($data['Address']) : "";
        $this->Email   = isset($data['Email']) ? trim($data['Email']) : "";
        $this->Website  = isset($data['Website']) ? trim($data['Website']) : "";
    }
}