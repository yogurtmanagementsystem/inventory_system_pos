<?php
/**
 * @author Phin Heng
 * @since  21 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class ItemMissingBrokenDetailRepository extends BaseRepository
{
    public function getAllRecords($condition = array(), $columns = array(), $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;
        $warehouseTable  = TableConstant::INV_WAREHOUSE_TABLE;
        $itemMissingBrokeTable  = TableConstant::INV_ITEM_MISSING_BROKEN_TABLE;
        $stockBalanceDetailTable= TableConstant::INV_STOCK_BALANCE_DETAIL_TABLE;
        
        $select = $this->sql->select($this->tableName);
        $select->columns(
                    [
                        "*",
                        "MissingBrokenDate" => new Expression("DATE_FORMAT(".$this->tableName.".MissingBrokenDate,'".PHPConstant::DATE_FORMAT."')")
                    ]
                );
            
        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",["Name","CategoryName","ClassName","GroupName","TypeName"],'left');
        $select->join("{$warehouseTable}","{$this->tableName}.WarehouseId = {$warehouseTable}.Id",["WarehouseName" => "Name"],'left');

        $select->join("{$stockBalanceDetailTable}","{$this->tableName}.StockBalanceDetailId = {$stockBalanceDetailTable}.Id",["LastReceivedDate","ExpiredDate"],'left');

        $select->join("{$itemMissingBrokeTable}","{$this->tableName}.ItemMissingBrokenId={$itemMissingBrokeTable}.Id",["IsManualSet","HasVariance","ItemVarianceValue","MissingBrokenUnitMeasurement","TotalMissingQty" => "MissingBrokenQty"],'left');

        if( count($condition) )
            $select->where($condition);

        $select->where( [ "{$this->tableName}.IsDeleted" => 0 ] )
               ->order($orderField." ".$orderType);

        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($result);

        return $resultSet;
    }
}