<?php
/**
 * @author Phin Heng
 * @since  22 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use PCSPLib\BaseClasses\BaseObject;

class BaseItemMasterObject extends BaseObject
{
    public $ItemMasterId;

    public $DefaultSystemUnit;
    public $OtherUsableUnit;
    public $ConvertedValues;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->ItemMasterId     = isset($data['ItemMasterId'])? (!empty($data['ItemMasterId'])?trim($data['ItemMasterId']," "):0) : 0;
        
        $this->DefaultSystemUnit= isset($data['DefaultSystemUnit'])?trim($data['DefaultSystemUnit']," "):"";
        $this->OtherUsableUnit 	= isset($data['OtherUsableUnit'])?trim($data['OtherUsableUnit']," "):"";
        $this->ConvertedValues 	= isset($data['ConvertedValues'])?trim($data['ConvertedValues']," "):"";
    }
}