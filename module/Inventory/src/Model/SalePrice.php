<?php
/**
 * @author Phin Heng
 * @since  01 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use PCSPLib\BaseClasses\BaseObject;

class SalePrice extends BaseObject
{
    public $StockMasterId;
    public $ItemMasterId;
    public $Price;
    public $SpecialPrice;
    public $SpecialPriceFromDate;
    public $SpecialPriceToDate;
    public $ActivePriceFromDate;
    public $ActivePriceToDate;
    public $IsItemDiscount;
    public $IsGroupPrice;
    public $IsTierPrice;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->StockMasterId = isset($data['StockMasterId'])? (!empty($data['StockMasterId'])?trim($data['StockMasterId']," "):0) : 0;
        $this->ItemMasterId = isset($data['ItemMasterId'])? (!empty($data['ItemMasterId'])?trim($data['ItemMasterId']," "):0) : 0;
        $this->Price = isset($data['Price'])? (!empty($data['Price'])?trim($data['Price']," "):0) : 0;
        $this->SpecialPrice = isset($data['SpecialPrice'])? (!empty($data['SpecialPrice'])?trim($data['SpecialPrice']," "):0) : 0;
        
        $this->SpecialPriceFromDate = isset($data['SpecialPriceFromDate'])? (!empty($data['SpecialPriceFromDate'])? date("Y-m-d",strtotime(trim($data['SpecialPriceFromDate']))):null) : null;
        $this->SpecialPriceToDate = isset($data['SpecialPriceToDate'])? (!empty($data['SpecialPriceToDate'])? date("Y-m-d",strtotime(trim($data['SpecialPriceToDate']))):null) : null;
        $this->ActivePriceFromDate = isset($data['ActivePriceFromDate'])? (!empty($data['ActivePriceFromDate'])? date("Y-m-d",strtotime(trim($data['ActivePriceFromDate']))):null) : null;
        $this->ActivePriceToDate = isset($data['ActivePriceToDate'])? (!empty($data['ActivePriceToDate'])? date("Y-m-d",strtotime(trim($data['ActivePriceToDate']))):null) : null;

        $this->IsItemDiscount = isset($data['IsItemDiscount'])? (!empty($data['IsItemDiscount'])?trim($data['IsItemDiscount']," "):0) : 0;
        $this->IsGroupPrice = isset($data['IsGroupPrice'])? (!empty($data['IsGroupPrice'])?trim($data['IsGroupPrice']," "):0) : 0;
        $this->IsTierPrice = isset($data['IsTierPrice'])? (!empty($data['IsTierPrice'])?trim($data['IsTierPrice']," "):0) : 0;
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }
}