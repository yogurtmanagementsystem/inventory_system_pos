<?php
/**
 * @author Phin Heng
 * @since  21 Nov 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class ItemAdjustmentRepository extends BaseRepository
{
	public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;
        
        $select = $this->sql->select($this->tableName);
        $select->columns(
                    [
                        "*",
                        "AdjustedDate" => new Expression("DATE_FORMAT(AdjustedDate,'".PHPConstant::DATE_FORMAT."')")
                    ]
                );
            
        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",["Name","CategoryName"],'left');
        
        if(!empty($filterString)) $select->where($filterString);
        $select->where("{$this->tableName}.IsDeleted = 0");
        $select->order($sort.' '.$order);
        $select->limit($rows)->offset($offset);
        
        $stmt   = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getRowCountForGrid($filterString)
    {
        $itemMasterTable = TableConstant::INV_ITEM_MASTER_TABLE;

        $select = $this->sql->select($this->tableName);
        $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT('.$this->tableName.'.Id)')));
        
        $select->join("{$itemMasterTable}","{$this->tableName}.ItemMasterId = {$itemMasterTable}.Id",[],'left');
        
        $select->where("{$this->tableName}.IsDeleted = 0");
        if(!empty($filterString))$select->where($filterString);
        $select->limit(1);
        
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        $resultSet = new HydratingResultSet();
        $resultSet->initialize($result);
        $item = $resultSet->current();

        return $item['Num'];
    }
}