<?php
/**
 * @author Phin Heng
 * @since  Oct 20 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Inventory;

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\BaseClasses\BaseObject;
use PCSPLib\TableConstant;
use Zend\Db\Adapter\AdapterInterface;
use Interop\Container\ContainerInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;

return 
[
	'service_manager' => 
    [
        'aliases' => 
        [
			
        ],
        'factories' => 
        [
            Model\SupplierRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::INV_SUPPLIER_TABLE,new ReflectionHydrator(), new Model\Supplier());
            },
            Model\ItemCategoryRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_CATEGORY_TABLE,new ReflectionHydrator(), new BaseObject());
            },
            Model\ItemMasterRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemMasterRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_MASTER_TABLE,new ReflectionHydrator(), new Model\ItemMaster());
            },
            Model\ItemBrokenReasonRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_BROKEN_REASON_TABLE,new ReflectionHydrator(), new BaseObject());
            },
            Model\ItemUnitMeasurementRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemUnitMeasurementRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_UNIT_MEASUREMENT_TABLE,new ReflectionHydrator(), new Model\ItemUnitMeasurement());
            },
            Model\ItemAdjustmentRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemAdjustmentRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_ADJUSTMENT_TABLE,new ReflectionHydrator(), new Model\ItemAdjustment());
            },
            Model\ItemMissingBrokenRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemMissingBrokenRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_MISSING_BROKEN_TABLE,new ReflectionHydrator(), new Model\ItemMissingBroken());
            },
            Model\ItemMissingBrokenDetailRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemMissingBrokenDetailRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_MISSING_BROKEN_DETAIL_TABLE,new ReflectionHydrator(), new Model\ItemMissingBrokenDetail());
            },
            Model\ItemReceiveRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemReceiveRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_RECEIVE_TABLE,new ReflectionHydrator(), new Model\ItemReceive());
            },
            Model\ItemTakeUseRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemTakeUseRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_TAKE_USE_TABLE,new ReflectionHydrator(), new Model\ItemTakeUse());
            },
            Model\ItemReceiveGroupRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemReceiveGroupRepository($container->get(AdapterInterface::class), TableConstant::INV_ITEM_RECEIVE_GROUP_TABLE,new ReflectionHydrator(), new Model\ItemReceiveGroup());
            },
            Model\StockBalanceMasterRepository::class => function(ContainerInterface $container)
            {
                return new Model\StockBalanceMasterRepository($container->get(AdapterInterface::class), TableConstant::INV_STOCK_BALANCE_MASTER_TABLE,new ReflectionHydrator(), new Model\StockBalanceMaster());
            },
            Model\SaleStockRepository::class => function(ContainerInterface $container)
            {
                return new Model\SaleStockRepository($container->get(AdapterInterface::class), TableConstant::INV_SALE_STOCK_TABLE,new ReflectionHydrator(), new Model\SaleStock());
            },
        ],
    ],
	'controllers' => 
    [
        'factories' => 
        [
			Controller\InventoryController::class => function(ContainerInterface $container)
            {
                return new Controller\InventoryController($container);
            },
            Controller\ItemCategoryController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemCategoryController($container);
            },
            Controller\ItemBrokenReasonController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemBrokenReasonController($container);
            },
            Controller\ItemUnitMeasurementController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemUnitMeasurementController($container);
            },
            Controller\SupplierController::class => function(ContainerInterface $container)
            {
                return new Controller\SupplierController($container);
            },
            Controller\ItemMasterController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemMasterController($container);
            },
            Controller\ItemReceiveController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemReceiveController($container);
            },
            Controller\ItemAdjustmentController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemAdjustmentController($container);
            },
            Controller\ItemMissingBrokenController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemMissingBrokenController($container);
            },
            Controller\ItemTakeController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemTakeController($container);
            },
            Controller\StockBalanceMasterController::class => function(ContainerInterface $container)
            {
                return new Controller\StockBalanceMasterController($container);
            },
            Controller\SaleStockController::class => function(ContainerInterface $container)
            {
                return new Controller\SaleStockController($container);
            },
            Controller\ReportController::class => function(ContainerInterface $container)
            {
                return new Controller\ReportController($container);
            },
            Controller\StockAlertController::class => function(ContainerInterface $container)
            {
                return new Controller\StockAlertController($container);
            },
        ],		
    ],
    'router' => 
    [
        'routes' => 
        [
            'inventory' => 
            [
                'type' => Literal::class,
                'options' => 
                [
                    'route' => '/inventory',
                    'defaults' => 
                    [
                        'controller' => Controller\InventoryController::class,
                        'action'     => 'list',
                    ],
                ],
				'may_terminate' => true,
				'child_routes'  => 
                [
					'inventory' => 
                    [
						'type' => Segment::class,
						'options' => 
                        [
						    'route'    => '/inventory[/:action[/:id]]',
							'defaults' => 
                            [
								'controller' => Controller\InventoryController::class,
								'action'     => 'list',
							],
							'constraints' => 
                            [
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id' => '[0-9]+',
							],
						],
					],
                    'item-unit-measurement' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/item-unit-measurement[/:action[/:id]]',
                            'defaults' =>
                                [
                                    'controller' => Controller\ItemUnitMeasurementController::class,
                                    'action'     => 'list',
                                ],
                            'constraints' =>
                                [
                                    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'id' => '[0-9]+',
                                ],
                        ],
                    ],
                    'item-category' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/item-category[/:action[/:id]]',
                            'defaults' =>
                                [
                                    'controller' => Controller\ItemCategoryController::class,
                                    'action'     => 'list',
                                ],
                            'constraints' =>
                                [
                                    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'id' => '[0-9]+',
                                ],
                        ],
                    ],
                    'supplier' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/supplier[/:action[/:id]]',
                            'defaults' =>
                                [
                                    'controller' => Controller\SupplierController::class,
                                    'action'     => 'list',
                                ],
                            'constraints' =>
                                [
                                    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'id' => '[0-9]+',
                                ],
                        ],
                    ],
                    'item-broken-reason' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/item-broken-reason[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\ItemBrokenReasonController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'item-master' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                            [
                                'route'    => '/item-master[/:action[/:id]]',
                                'defaults' =>
                                [
                                    'controller' => Controller\ItemMasterController::class,
                                    'action'     => 'list',
                                ],
                                'constraints' =>
                                [
                                    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'id' => '[0-9]+',
                                ],
                            ],
                    ],
                    'item-receive' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                            [
                                'route'    => '/item-receive[/:action[/:id]]',
                                'defaults' =>
                                [
                                    'controller' => Controller\ItemReceiveController::class,
                                    'action'     => 'list',
                                ],
                                'constraints' =>
                                [
                                    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                    'id' => '[0-9]+',
                                ],
                            ],
                    ],
                    'item-adjustment' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/item-adjustment[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\ItemAdjustmentController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'item-missing-broken' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/item-missing-broken[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\ItemMissingBrokenController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'item-take' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/item-take[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\ItemTakeController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'stock-balance-master' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/stock-balance-master[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\StockBalanceMasterController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'sale-stock' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/sale-stock[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\SaleStockController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'report' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/report[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\ReportController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'stock-alert' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/stock-alert[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\StockAlertController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => 
    [
        'template_path_stack' => 
        [
            'inventory' => __DIR__ . '/../view',
        ],
    ],
    'translator' =>
        [
            'locale' => 'en_US',
            'translation_file_patterns' =>
                [
                    [
                        'type' => 'gettext',
                        'base_dir' => __DIR__ . '/../language',
                        'pattern' => '%s.mo'
                    ]
                ]
        ],
];
