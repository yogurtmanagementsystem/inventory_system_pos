msgid ""
msgstr ""
"Project-Id-Version: ZendSkeletonApplication\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-11-25 23:08+0700\n"
"PO-Revision-Date: 2014-11-25 23:08+0700\n"
"Last-Translator: \n"
"Language-Team: ZF Contibutors <zf-devteam@zend.com>\n"
"Language: km_KH\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: translate\n"
"X-Poedit-Basepath: .\n"
"X-Generator: Poedit 1.6.4\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../src/Users/Controller/ResourcesController.php:101
#: ../src/Users/Controller/UserRoleController.php:193
#: ../src/Users/Controller/UserRoleController.php:195
#: ../src/Users/Controller/UsersController.php:188
#: ../src/Users/Controller/UsersController.php:190
#: ../view/users/resources/list.phtml:113
#: ../view/users/user-role/list.phtml:74 ../view/users/user-role/list.phtml:98
#: ../view/users/users/list.phtml:145 ../view/users/users/list.phtml:169
msgid "ERROR"
msgstr ""

#: ../src/Users/Controller/ResourcesController.php:101
#: ../src/Users/Controller/UserRoleController.php:195
#: ../src/Users/Controller/UsersController.php:190
msgid "ITEM_IN_USED_CANNOT_DELETE"
msgstr "សូមទោសយើងមិនអាចលុបទិន្នន័យនេះទេ ព្រោះវាកំពុងប្រើប្រាស់"

#: ../src/Users/Controller/UserRoleController.php:193
#: ../src/Users/Controller/UsersController.php:188
msgid "SOME_ITEMS_DELETED_EXCEPT_CURRENT_USE"
msgstr "ទិន្នន័យមួយចំនួនបានលុបចោលរួចរាល់ លើកលែងតែទិន្នន័យដែលកំពុងប្រើប្រាស់"

#: ../view/users/resources/list.phtml:10 ../view/users/resources/list.phtml:36
#: ../view/users/user-role/list.phtml:18
#: ../view/users/user-role/list.phtml:177 ../view/users/users/list.phtml:11
#: ../view/users/users/list.phtml:229
msgid "NEW"
msgstr ""

#: ../view/users/resources/list.phtml:13 ../view/users/resources/list.phtml:46
#: ../view/users/user-role/list.phtml:21
#: ../view/users/user-role/list.phtml:191 ../view/users/users/list.phtml:14
#: ../view/users/users/list.phtml:242
msgid "EDIT"
msgstr "កែតំរូវ"

#: ../view/users/resources/list.phtml:16 ../view/users/user-role/list.phtml:24
#: ../view/users/users/list.phtml:17
msgid "DELETE"
msgstr "លុបចោល"

#: ../view/users/resources/list.phtml:43 ../view/users/resources/list.phtml:54
#: ../view/users/user-role/list.phtml:184
#: ../view/users/user-role/list.phtml:187
#: ../view/users/user-role/list.phtml:199
#: ../view/users/user-role/new.phtml:111 ../view/users/users/list.phtml:235
#: ../view/users/users/list.phtml:238 ../view/users/users/list.phtml:251
#: ../view/users/users/new.phtml:99
msgid "WARNING"
msgstr ""

#: ../view/users/resources/list.phtml:43
#: ../view/users/user-role/list.phtml:184 ../view/users/users/list.phtml:235
msgid "SELECT_ROW_TO_EDIT"
msgstr ""

#: ../view/users/resources/list.phtml:54
#: ../view/users/user-role/list.phtml:199 ../view/users/users/list.phtml:251
msgid "SELECT_ROW_TO_DELETE"
msgstr ""

#: ../view/users/resources/list.phtml:58
#: ../view/users/user-role/list.phtml:203 ../view/users/users/list.phtml:255
msgid "MESSAGE_DELETE_ITEM"
msgstr ""

#: ../view/users/resources/list.phtml:113
#: ../view/users/user-role/list.phtml:98 ../view/users/users/list.phtml:169
msgid "ERROR_DELETE"
msgstr ""

#: ../view/users/resources/list.phtml:142 ../view/users/resources/new.phtml:22
#, fuzzy
msgid "ACTION_NAME"
msgstr "មុខតំណែង"

#: ../view/users/resources/list.phtml:143 ../view/users/resources/new.phtml:26
msgid "ACTION_PREFIX"
msgstr ""

#: ../view/users/resources/list.phtml:144 ../view/users/resources/new.phtml:30
msgid "CUSTOM_CONTROLLER"
msgstr ""

#: ../view/users/resources/new.phtml:15
msgid "PARENT"
msgstr ""

#: ../view/users/resources/new.phtml:34
msgid "IDENTIFY"
msgstr ""

#: ../view/users/resources/new.phtml:38
msgid "ORDER"
msgstr ""

#: ../view/users/resources/new.phtml:43 ../view/users/users/list.phtml:222
#: ../view/users/users/new.phtml:49
msgid "ACTIVE"
msgstr "កំពុងប្រើប្រាស់"

#: ../view/users/resources/new.phtml:51 ../view/users/user-role/new.phtml:29
#: ../view/users/users/new.phtml:58
msgid "OK"
msgstr "រក្សាទុក"

#: ../view/users/resources/new.phtml:52 ../view/users/user-role/new.phtml:30
#: ../view/users/users/new.phtml:59
msgid "CANCEL"
msgstr "បិទចោល"

#: ../view/users/user-role/list.phtml:9 ../view/users/user-role/list.phtml:154
#: ../view/users/user-role/new.phtml:16 ../view/users/users/list.phtml:29
#: ../view/users/users/list.phtml:219 ../view/users/users/new.phtml:36
msgid "ROLE_NAME"
msgstr "តួនាទី"

#: ../view/users/user-role/list.phtml:12 ../view/users/users/list.phtml:22
#: ../view/users/users/list.phtml:41
msgid "FILTER"
msgstr "ស្វែងរក"

#: ../view/users/user-role/list.phtml:13 ../view/users/users/list.phtml:44
msgid "CLEAR"
msgstr "សំអាត"

#: ../view/users/user-role/list.phtml:74 ../view/users/users/list.phtml:145
msgid "ERROR_SAVE"
msgstr ""

#: ../view/users/user-role/list.phtml:153 ../view/users/users/list.phtml:213
msgid "ROW"
msgstr "លេខរៀង"

#: ../view/users/user-role/list.phtml:155 ../view/users/user-role/new.phtml:18
msgid "REMARK"
msgstr "កំណត់ចំណាំ"

#: ../view/users/user-role/list.phtml:187 ../view/users/users/list.phtml:238
msgid "SELECT_ONLY_ONE_ROW_TO_EDIT"
msgstr ""

#: ../view/users/user-role/new.phtml:21
msgid "PERMISSION"
msgstr ""

#: ../view/users/user-role/new.phtml:111
msgid "CHOOSE_PERMISSIONS!"
msgstr ""

#: ../view/users/users/list.phtml:27 ../view/users/users/list.phtml:214
#: ../view/users/users/new.phtml:14
#, fuzzy
msgid "FIRST_NAME"
msgstr "ក្រសួង ស្ថាប័ន"

#: ../view/users/users/list.phtml:28 ../view/users/users/list.phtml:215
#: ../view/users/users/new.phtml:18
#, fuzzy
msgid "LAST_NAME"
msgstr "ក្រសួង ស្ថាប័ន"

#: ../view/users/users/list.phtml:216 ../view/users/users/new.phtml:22
#, fuzzy
msgid "USER_NAME"
msgstr "ក្រសួង ស្ថាប័ន"

#: ../view/users/users/list.phtml:217 ../view/users/users/new.phtml:28
msgid "PHONE_NUMBER"
msgstr "លេខទូរស័ព្ទ"

#: ../view/users/users/list.phtml:218 ../view/users/users/new.phtml:32
msgid "EMAIL"
msgstr ""

#: ../view/users/users/list.phtml:220 ../view/users/users/new.phtml:45
msgid "ADDRESS"
msgstr ""

#: ../view/users/users/new.phtml:25
msgid "PASSWORD"
msgstr "លេខសំងាត់"

#: ../view/users/users/new.phtml:26
msgid "CONFIRM_PASSWORD"
msgstr "បញ្ជាក់លេខសំងាត់"

#: ../view/users/users/new.phtml:99
msgid "USER_ALREADY_TAKEN"
msgstr ""

#~ msgid "DISABLE"
#~ msgstr "ឈប់ប្រើប្រាស់"

#~ msgid "CODE"
#~ msgstr "លេខកូដ"

#~ msgid "DISTRICT"
#~ msgstr "ខ័ណ្ឌ"

#~ msgid "PROVINCE"
#~ msgstr "ខេត្ត"

#~ msgid "IS_PLACE_OF_BIRTH"
#~ msgstr "ប្រើសំរាប់តែទីកន្លែងកំណើត"

#~ msgid "COMMUNE"
#~ msgstr "សង្កាត់"

#~ msgid "ANGPHEAP_NAME"
#~ msgstr "អង្គភាព"

#~ msgid "ANGPHEAP_ NAME"
#~ msgstr "អង្គភាព"

#~ msgid "TYPE"
#~ msgstr "ប្រភេទ"

#~ msgid "KRABKHAN_NAME"
#~ msgstr "ប្រភេទក្របខ័ណ្ឌ"

#~ msgid "NMINISTRY_NAME"
#~ msgstr "ក្រសួង ស្ថាប័ន"

#~ msgid "POSITION_TYPE"
#~ msgstr "ប្រភេទ"

#~ msgid "DECLARE_PROPERTY"
#~ msgstr "ត្រូវប្រកាសទ្រព្យសម្បតិ្ត"

#~ msgid "IS_DECLARE_PROPERTY"
#~ msgstr "ត្រូវប្រកាសទ្រព្យសម្បតិ្ត"

#, fuzzy
#~ msgid "POISITION_NAME"
#~ msgstr "មុខតំណែង"

#, fuzzy
#~ msgid "POSITION_ NAME"
#~ msgstr "មុខតំណែង"

#~ msgid "PLACE_OF_BIRTH"
#~ msgstr "ទីកន្លែងកំណើត"

#~ msgid "D_APPRECIATION_INFORMATION"
#~ msgstr "ឃ. ការសរសើរជូនរង្វាន់ ឫ ការដាក់វិន័យ"

#~ msgid "B_GENERAL_EDUCATION"
#~ msgstr "ខ. កំរិតវប្បធម៏ទូទៅ ការបណ្តុះបណ្តាលមុខវិជ្ជាជីវៈ និង ការបណ្តុះបណ្តាលបន្ត"

#~ msgid "A_PERSONAL_INFORMATION"
#~ msgstr "ក. ព័ត៌មានផ្ទាល់ខ្លួន"

#~ msgid "C_WORK_HISTORY"
#~ msgstr "គ. ប្រវត្តិការងារ"

#, fuzzy
#~ msgid "STUDY_LEVEL"
#~ msgstr "គ្រឹស្ថានសិក្សា បណ្តុះបណ្តាល"

#, fuzzy
#~ msgid "STUDY_PLACE"
#~ msgstr "គ្រឹស្ថានសិក្សា បណ្តុះបណ្តាល"

#, fuzzy
#~ msgid "CERTIFICATE_RECEIVED"
#~ msgstr "សញ្ញាប័ត្រដែលទទួលបាន"

#, fuzzy
#~ msgid "START_STUDY_DATE"
#~ msgstr "គ្រឹស្ថានសិក្សា បណ្តុះបណ្តាល"

#, fuzzy
#~ msgid "END_STUDY_DATE"
#~ msgstr "គ្រឹស្ថានសិក្សា បណ្តុះបណ្តាល"

#, fuzzy
#~ msgid "GENERAL_EDUCATION"
#~ msgstr "កំរិតវប្បធម៍ទូទៅ"

#, fuzzy
#~ msgid "KNOWLEDGE_OF_FOREIGN_LANGUAGES"
#~ msgstr "ចំណេះដឹងភាសាបរទេស"

#, fuzzy
#~ msgid "CONTINUEING_TRAINING_OR_COURSE"
#~ msgstr "ការបណ្តុះបណ្តាល និង វគ្គសិក្សាកំពុងបន្ត"

#, fuzzy
#~ msgid "OFFICER_NO"
#~ msgstr "ការបញ្ចូលមន្រ្តីរាជការ"

#, fuzzy
#~ msgid "OFFICER_ID_NO"
#~ msgstr "ការបញ្ចូលមន្រ្តីរាជការ"

#~ msgid "VILLAGE"
#~ msgstr "ភូមិ"

#~ msgid "THANONTARASAK"
#~ msgstr "ឋានន្តរសក្តិ"

#, fuzzy
#~ msgid "SYSTEM_SETTING"
#~ msgstr "Staff Management System"

#~ msgid "SYSTEM_SETTING_GENERAL_EDUCATION"
#~ msgstr "កំរិតវប្បធម៍ទូទៅ និង ការបណ្តុះបណ្តាលបន្ត"

#~ msgid "ST_COURSE_OR_STUDY_LEVEL"
#~ msgstr "វគ្គ ឬ កំរិតសិក្សា"

#~ msgid "ST_TRAINING_LEVEL_UNDER_UPPER"
#~ msgstr "កំរិតបណ្តុះបណ្តាលមុខវិជ្ជាជីវៈ មូលដ្ដាន និង ក្រោយមូលដ្ដាន"

#~ msgid "SYSTEM_TITLE"
#~ msgstr "Staff Management System"
