<?php
/**
 * @author Phin Heng
 * @since  05 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant;

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\BaseClasses\BaseObject;
use PCSPLib\TableConstant;
use Zend\Db\Adapter\AdapterInterface;
use Interop\Container\ContainerInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;

return [
    
    'service_manager' => 
    [
        'aliases' => 
        [
            
        ],
        'factories' => 
        [
            Model\SalePriceRepository::class => function(ContainerInterface $container)
            {
                return new Model\SalePriceRepository($container->get(AdapterInterface::class), TableConstant::SAL_SALE_PRICE_TABLE,new ReflectionHydrator(), new Model\SalePrice());
            },
            Model\DiscountRepository::class => function(ContainerInterface $container)
            {
                return new Model\DiscountRepository($container->get(AdapterInterface::class), TableConstant::SAL_DISCOUNT_VALUE_TABLE,new ReflectionHydrator(), new Model\Discount());
            },
            Model\DiscountTypeRepository::class => function(ContainerInterface $container)
            {
                return new Model\DiscountTypeRepository($container->get(AdapterInterface::class), TableConstant::SAL_DISCOUNT_TYPE_TABLE,new ReflectionHydrator(), new Model\DiscountType());
            },
            Model\IncomeRepository::class => function(ContainerInterface $container)
            {
                return new Model\IncomeRepository($container->get(AdapterInterface::class), TableConstant::SAL_SALE_DETAIL_TABLE,new ReflectionHydrator(), new Model\Income());
            }
        ],
    ],
    'controllers' => 
    [
        'factories' => 
        [
            Controller\AccountantController::class => function(ContainerInterface $container)
            {
                return new Controller\AccountantController($container);
            },
            Controller\SalePriceController::class => function(ContainerInterface $container)
            {
                return new Controller\SalePriceController($container);
            },
            Controller\DiscountController::class => function(ContainerInterface $container)
            {
                return new Controller\DiscountController($container);
            },
            Controller\IncomeController::class => function(ContainerInterface $container)
            {
                return new Controller\IncomeController($container);
            },
        ],      
    ],
    'router' => 
    [
        'routes' => 
        [
            'accountant' => 
            [
                'type' => Literal::class,
                'options' => 
                [
                    'route' => '/accountant',
                    'defaults' => 
                    [
                        'controller' => Controller\AccountantController::class,
                        'action'     => 'list',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => 
                [
                    'accountant' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/accountant[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\AccountantController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'sale-price' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                        [
                            'route'    => '/sale-price[/:action[/:id]]',
                            'defaults' =>
                            [
                                'controller' => Controller\SalePriceController::class,
                                'action'     => 'list',
                            ],
                            'constraints' =>
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'discount' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                            [
                                'route'    => '/discount[/:action[/:id]]',
                                'defaults' =>
                                    [
                                        'controller' => Controller\DiscountController::class,
                                        'action'     => 'list',
                                    ],
                                'constraints' =>
                                    [
                                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '[0-9]+',
                                    ],
                            ],
                    ],  
                    'income' =>
                    [
                        'type' => Segment::class,
                        'options' =>
                            [
                                'route'    => '/income[/:action[/:id]]',
                                'defaults' =>
                                    [
                                        'controller' => Controller\IncomeController::class,
                                        'action'     => 'list',
                                    ],
                                'constraints' =>
                                    [
                                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '[0-9]+',
                                    ],
                            ],
                    ],     
                ],
            ],
        ],
    ],
    'view_manager' => 
    [
        'template_path_stack' => 
        [
            'accountant' => __DIR__ . '/../view',
        ],
    ],
    'translator' =>
    [
        'locale' => 'en_US',
        'translation_file_patterns' =>
            [
                [
                    'type' => 'gettext',
                    'base_dir' => __DIR__ . '/../language',
                    'pattern' => '%s.mo'
                ]
            ]
    ],
];
