<?php
/**
 * @author Phin Heng
 * @since  01 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;
use Zend\Db\Sql\Update;

class SalePriceRepository extends BaseRepository
{
    public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        $tableName = TableConstant::SAL_SALE_ITEM_TABLE;
        $salePriceTable = $this->tableName;

        $select = $this->sql->select($tableName);

        $select->join("{$salePriceTable}","{$tableName}.Id = {$salePriceTable}.SaleItemId",["SalePriceId" => "Id","Price","SalePriceNote" => "Note"],'left');
        
        if(!empty($filterString)) $select->where($filterString);

        $select->where("{$tableName}.IsDeleted = 0 AND {$tableName}.IsActive = 1");
        $select->order($sort.' '.$order);
        $select->limit($rows)->offset($offset);
        
        $stmt   = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($result);

        return $resultSet;
    }
    public function getRowCountForGrid($filterString)
    {
        $tableName = TableConstant::SAL_SALE_ITEM_TABLE;
        $salePriceTable = $this->tableName;

        $select = $this->sql->select($tableName);
        $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT('.$tableName.'.Id)')));
        
        $select->join("{$salePriceTable}","{$tableName}.Id = {$salePriceTable}.SaleItemId",[],'left');
        
        if(!empty($filterString))$select->where($filterString);

        $select->where("{$tableName}.IsDeleted = 0 AND {$tableName}.IsActive = 1");
        $select->limit(1);
        
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result    = $statement->execute();

        $resultSet = new HydratingResultSet();
        $resultSet->initialize($result);
        $item = $resultSet->current();

        return $item['Num'];
    }
    public function getAllRecords($condition = array(), $columns = array(), $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        try
        {
            $saleItemTable = TableConstant::SAL_SALE_ITEM_TABLE;

            $select = $this->sql->select($this->tableName);

            if( count($condition) )
                $select->where($condition);

            $select->join("{$saleItemTable}","{$this->tableName}.SaleItemId = {$saleItemTable}.Id",["ItemName" => "Name","CategoryName","ItemCategoryId","ItemTypeId","TypeName","Image","GroupRecord","IsSaleWeight"],'left');

            $select->where(["{$this->tableName}.IsDeleted" => 0,"{$saleItemTable}.IsDeleted" => 0])->order($orderField." ".$orderType);
            
            if(count($columns))
                $select->columns($columns);
            if(!empty($groupField))
            {
                $select->group($groupField);
            }
            $stmt   = $this->sql->prepareStatementForSqlObject($select);

            $result = $stmt->execute();

            $resultSet = new ResultSet();
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}