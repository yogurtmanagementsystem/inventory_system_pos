<?php
/**
 * @author Phin Heng
 * @since  01 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class DiscountRepository extends BaseRepository
{
    public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        try
        {
        	$discountTypeTable = TableConstant::SAL_DISCOUNT_TYPE_TABLE;

            $select = $this->sql->select($this->tableName);
            
            $select->join($discountTypeTable,"{$this->tableName}.DiscountTypeId = {$discountTypeTable}.Id",["DiscountTypeName" => "Name"],"left");
            
            if(!empty($filterString))
            	$select->where($filterString);

            $select->where("{$this->tableName}.IsDeleted = 0");
            $select->order($sort.' '.$order);
            $select->limit($rows)->offset($offset);
            
            $stmt   = $this->sql->prepareStatementForSqlObject($select);
	        $result = $stmt->execute();

	        $resultSet = new ResultSet();

	        $resultSet->initialize($result);

	        return $resultSet;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}