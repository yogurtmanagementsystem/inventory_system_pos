<?php
/**
 * @author Phin Heng
 * @since  01 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Model;

use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\TableConstant;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Insert;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\HydratingResultSet;

class DiscountTypeRepository extends BaseRepository
{

}