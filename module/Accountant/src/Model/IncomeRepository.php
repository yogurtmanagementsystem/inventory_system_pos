<?php
/**
 * @author Phin Heng
 * @since  01 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class IncomeRepository extends BaseRepository
{
    public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        $saleTable = TableConstant::SAL_SALE_TABLE;
        $saleItemTable = TableConstant::SAL_SALE_ITEM_TABLE;

        $select = $this->sql->select($this->tableName);

        $select->columns(
                [
                    "*",
                    "Qty" => new Expression("SUM(Qty)"),
                    "SubTotal"=>new Expression("SUM(".$this->tableName.".Total)"),
                    "TotalAmount"=>new Expression("SUM(".$this->tableName.".Total1)"),
                    "TotalDiscount"=>new Expression("SUM(".$this->tableName.".TotalDiscount)")
                ]
            );

        $select->join("{$saleTable}","{$saleTable}.Id = {$this->tableName}.SaleId",["InvoiceDate"=>new Expression("DATE_FORMAT(InvoiceDate,'".PHPConstant::DATE_FORMAT."')")],"left");
        
        $select->join("{$saleItemTable}","{$saleItemTable}.Id = {$this->tableName}.SaleItemId",["Name","TypeName","CategoryName","GroupRecord","ItemTypeId","ItemCategoryId"],"left");
                   
        if(!empty($filterString)) $select->where($filterString);
        
        $select->order($sort.' '.$order);
        // $select->limit($rows)->offset($offset);

        $select->group(["{$saleTable}.InvoiceDate","{$saleItemTable}.Id","{$saleItemTable}.ItemTypeId"]);
        
        $stmt   = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getRowCountForGrid($filterString)
    {
        try
        {
            $saleTable = TableConstant::SAL_SALE_TABLE;
            $saleItemTable = TableConstant::SAL_SALE_ITEM_TABLE;

            $select = $this->sql->select($this->tableName);
            $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT(DISTINCT '.$saleItemTable.'.GroupRecord)')));
            
            $select->join("{$saleTable}","{$saleTable}.Id = {$this->tableName}.SaleId",[],"left");
        
            $select->join("{$saleItemTable}","{$saleItemTable}.Id = {$this->tableName}.SaleItemId",[],"left");

            if( !empty( $filterString ) ) $select->where($filterString);

            $select->limit(1);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();          
            
            $resultSet = new HydratingResultSet();
            $resultSet->initialize($result);
            $item = $resultSet->current();

            return $item['Num'];
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}