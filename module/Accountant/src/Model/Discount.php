<?php
/**
 * @author Phin Heng
 * @since  01 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Model;

use PCSPLib\BaseClasses\BaseObject;

class Discount extends BaseObject
{
	public $Value;
	public $DiscountTypeId;
	public $IsDefault;
	
	public function exchangeArray($data)
	{
		parent::exchangeArray($data);
		
		$this->Value 		= isset($data['Value'])?doubleval($data['Value']):0;
		$this->DiscountTypeId = isset($data['DiscountTypeId'])?intval($data['DiscountTypeId']):0;
		$this->IsDefault = isset($data['IsDefault'])?intval($data['IsDefault']):0;
	}

	public function getNoneTableField()
	{
		return [];
	}
}