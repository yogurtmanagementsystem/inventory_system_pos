<?php
/**
 * @author Phin Heng
 * @since  01 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Model;

use PCSPLib\BaseClasses\BaseObject;

class SalePrice extends BaseObject
{
    public $SaleItemId;
    public $Price;
    public $Image;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->SaleItemId = isset($data['SaleItemId'])? (!empty($data['SaleItemId'])?trim($data['SaleItemId']," "):0) : 0;
        $this->Price = isset($data['Price'])? (!empty($data['Price'])?trim($data['Price']," "):0) : 0;
        $this->Image = isset($data["Image"])? trim($data["Image"],""): "";
    }

    public function getNoneTableField()
    {
        return ["Name"];
    }
}