<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Db\Sql\Expression;

use PCSPLib\BaseClasses\BaseController;
use Accountant\Model AS Accountant;
use Sale\Model AS Sale;

use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class IncomeController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(Accountant\IncomeRepository::class);

        parent::__construct($serviceLocator, $table,"InvoiceDate");
    } 

    public function listAction()
    {
        $view = parent::listAction();

        $itemTypeRecord = $this->getService(Sale\ItemTypeRepository::class)->getActiveRecordsForTreeDropDown()->toArray();
        $typeRecord = $this->getService(Sale\ItemTypeRepository::class)->getAllActiveRecords(" ParentId <> 0 ",["Id","Name"])->toArray();
        $categoryRecord = $this->getService(Sale\ItemCategoryRepository::class)->getActiveRecordsForTreeDropDown()->toArray();
        
        $view->setVariables(
                [
                    "itemTypeRecord" => $itemTypeRecord,
                    "typeRecord"     => $typeRecord,
                    "categoryRecord" => $categoryRecord,

                ]
            );

        return $view;
    }
    public function getRecordsForGridAction()
    {
        $page = isset($_POST['pagenum']) ? intval($_POST['pagenum']) : 0;
        $rows = isset($_POST['pagesize'])? intval($_POST['pagesize']) : PHPConstant::DEFAULT_PAGE_SIZE;
        $offset = $page*$rows;
        $sort = (isset($_POST['sortdatafield']) & !empty($_POST['sortdatafield'])) ? strval($_POST['sortdatafield']) : $this->defaultSortGridField;
        $order = (isset($_POST['sortorder']) & !empty($_POST['sortorder'])) ? strval($_POST['sortorder']) : $this->defaultSortGridType;

        $filterString = $this->getFilterCondition();

        $records  = $this->objectRepository->getRecordsForGrid($offset, $rows, $sort, $order, $filterString)->toArray();
            
        $records = $this->getMergeRecordForGrid($records);

        // $totalRow = $this->objectRepository->getRowCountForGrid($filterString);
        $data[] = ['rows'=>$records];
        $result = new JsonModel($data);
        
        return $result;
    }
    public function getMergeRecordForGrid($record)
    {   
        $item = [];
        $tmp  = [];
        $tmpTypeRecord = [];

        $type = $this->getService(Sale\ItemTypeRepository::class)->getAllActiveRecords(" ParentId <> 0 ",["Id","Name"])->toArray();
        foreach( $type as $key => $value )
        {
            $typeRecord["Qty".$value["Id"]] = 0;
        }

        foreach( $record as $key => $value )
        {
            $invoiceDate = $value["InvoiceDate"];
            $itemTypeId  = $value["ItemTypeId"];
            $groupRecord = $value["GroupRecord"];
            $qty = $value["Qty"];
            $subTotal = $value["SubTotal"];
            $totalDiscount = $value["TotalDiscount"];
            $total = $value["TotalAmount"];

            if( !isset( $tmp[$invoiceDate][$groupRecord]["Child"][$itemTypeId] ) )
            {
                if( !isset( $tmp[$invoiceDate][$groupRecord] ) )
                {
                    $tmp[$invoiceDate][$groupRecord]["SubTotal"] = 0;
                    $tmp[$invoiceDate][$groupRecord]["TotalDiscount"] = 0;
                    $tmp[$invoiceDate][$groupRecord]["TotalAmount"] = 0;
                }
                $tmp[$invoiceDate][$groupRecord]["Name"] = $value["Name"];
                $tmp[$invoiceDate][$groupRecord]["CategoryName"] = $value["CategoryName"];
                $tmp[$invoiceDate][$groupRecord]["Child"][$itemTypeId]["Qty"] = 0;
            } 

            $tmp[$invoiceDate][$groupRecord]["SubTotal"] += $subTotal;
            $tmp[$invoiceDate][$groupRecord]["TotalDiscount"] += $totalDiscount;
            $tmp[$invoiceDate][$groupRecord]["TotalAmount"] += $total;

            $tmp[$invoiceDate][$groupRecord]["Child"][$itemTypeId]["Qty"] += $qty;
        }
        foreach( $tmp as $invoiceDate => $value )
        {
            foreach( $value as $groupRecord => $val )
            {
                $child = [];
                $tmpTypeRecord = $typeRecord;
                foreach( $val["Child"] as $typeId => $v )
                {
                    $key = "Qty".$typeId;
                    $child[$key] = $v["Qty"];
                    if( isset( $tmpTypeRecord[$key] ) ) unset( $tmpTypeRecord[$key] );
                }

                $child = array_merge($child,$tmpTypeRecord);

                $arr = [
                    "InvoiceDate" => $invoiceDate,
                    "GroupRecord" => $groupRecord,
                    "Name" => $val["Name"],
                    "SubTotal" => $val["SubTotal"],
                    "TotalDiscount" => $val["TotalDiscount"],
                    "TotalAmount" => $val["TotalAmount"],
                    "CategoryName" => $val["CategoryName"]

                ];
                
                $item[] = array_merge($arr,$child);
            }
        }

        return $item;
    }

    public function getFilterCondition()
    {
        $filtersCount = isset($_POST['filterscount']) ? intval($_POST['filterscount']) : 0;
        
        $saleTable = TableConstant::SAL_SALE_TABLE;
        $saleItemTable = TableConstant::SAL_SALE_ITEM_TABLE;

        $fromDate = isset($_POST["FromDate"])?$_POST["FromDate"]:"";
        $toDate   = isset($_POST["ToDate"])?$_POST["ToDate"]:"";
        $categoryId = isset($_POST["ItemCategoryId"])?$_POST["ItemCategoryId"]:"";
        $typeId  = isset($_POST["ItemTypeId"])?$_POST["ItemTypeId"]:"";

        $filterString = " 1 = 1 ";
        if( !empty( $fromDate ) )
        {
            $filterString .= " AND {$saleTable}.InvoiceDate >= '". date("Y-m-d",strtotime($fromDate))."'";
        }
        if( !empty( $toDate ) )
        {
            $filterString .= " AND {$saleTable}.InvoiceDate <='".date("Y-m-d",strtotime( $toDate ) )."'";
        }
        if( !empty( $categoryId ) )
        {
            $filterString .= " AND {$saleItemTable}.ItemCategoryId IN ({$categoryId}) ";
        }
        if( !empty( $typeId ) )
        {
            $filterString .= " AND {$saleItemTable}.ItemTypeId IN ({$typeId}) ";
        }

        return $filterString;
    }
}