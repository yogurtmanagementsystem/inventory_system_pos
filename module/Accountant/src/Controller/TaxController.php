<?php
/**
 * @author Phin Heng
 * @since  25 Oct 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\JsonModel;

use PCSPLib\BaseClasses\BaseController;
use Accountant\Model AS Accountant;

class TaxController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Accountant\TaxRepository::class);

        parent::__construct($container, $table);
    }

    public function isItemExist()
    {
        $formData = $this->request->getPost();
        $condition = "Name = '".trim($formData['Name']," ")."' AND Id != ".trim($formData['Id']," ");
        
        return $this->objectRepository->isExist($condition);
    }
}