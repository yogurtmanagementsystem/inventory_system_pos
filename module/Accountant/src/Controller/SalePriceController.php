<?php
/**
 * @author Phin Heng
 * @since  07 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\JsonModel;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;
use Inventory\Model AS Inventory;
use Accountant\Model AS Accountant;
use Sale\Model AS Sale;

class SalePriceController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Accountant\SalePriceRepository::class);
        
        parent::__construct($container, $table);
    }

    public function listAction()
    {
        $view = parent::listAction();

        $itemTypeRecord = $this->getService(Sale\ItemTypeRepository::class)->getActiveRecordsForTreeDropDown()->toArray();
        $categoryRecord = $this->getService(Sale\ItemCategoryRepository::class)->getActiveRecordsForTreeDropDown()->toArray();
        
        $view->setVariables(
                [
                    "itemTypeRecord" => $itemTypeRecord,
                    "categoryRecord" => $categoryRecord,

                ]
            );

        return $view;
    }

    public function newAction()
    {
        $view = parent::newAction();
        $saleItemId = isset($_POST["SaleItemId"])?(int)$_POST["SaleItemId"]:0;

        $view->setVariables(
                    [
                        "saleItemId" => $saleItemId,

                    ]
                );

        return $view;
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        
        $tableName = $this->tableName;
        $saleItemTable= TableConstant::SAL_SALE_ITEM_TABLE;
        
        $filterString = " 1 = 1 ";

        if( isset( $formData["FilterType"] ) && $formData["FilterType"] != 1 )
        {
            $filterType = (int)$formData["FilterType"];
            $today = date("Y-m-d");

            $salePriceTable = TableConstant::SAL_SALE_PRICE_TABLE;

            switch( $filterType )
            {
                case 2:

                    $filterString .= " AND {$salePriceTable}.Id IS NOT NULL ";

                    break;
                case 3:

                    $filterString .= " AND {$salePriceTable}.Id IS NULL ";

                    break;
            }
        }

        if( isset( $formData["ItemCategoryId"] ) && !empty( $formData["ItemCategoryId"] ) )
        {
            $filterString .= " AND {$saleItemTable}.ItemCategoryId IN({$formData["ItemCategoryId"]})";
        }
        if( isset( $formData["ItemTypeId"] ) && !empty( $formData["ItemTypeId"] ) )
        {
            $filterString .= " AND {$saleItemTable}.ItemTypeId IN({$formData["ItemTypeId"]})";
        }
        if( isset( $formData["Name"] ) && !empty( $formData["Name"] ) )
        {
            $filterString .= " AND {$saleItemTable}.Name LIKE '%".trim($formData["Name"],"")."%' ";
        }

        return $filterString;
    }
}