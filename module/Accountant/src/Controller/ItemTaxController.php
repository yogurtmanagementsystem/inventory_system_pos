<?php
/**
 * @author Phin Heng
 * @since  07 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\JsonModel;

use PCSPLib\BaseClasses\BaseController;
use Inventory\Model AS Inventory;
use Accountant\Model AS Accountant;

class ItemTaxController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $container)
    {
        $table = $container->get(Accountant\ItemTaxRepository::class);
        
        parent::__construct($container, $table);
    }

    public function newAction()
    {
    	$view = parent::newAction();
        $itemCategoryId = isset($_POST["ItemCategoryId"])?(int)$_POST["ItemCategoryId"]:0;
    	$isCategoryTax = isset($_POST["IsCategoryTax"])?(int)$_POST["IsCategoryTax"]:0;

    	$taxRecord = $this->getService(Accountant\TaxRepository::class)->getAllActiveRecords()->toArray();

    	$view->setVariables(
    				[
    					"taxRecord" => $taxRecord,
                        "itemCategoryId" => $itemCategoryId,
    					"isCategoryTax"  => $isCategoryTax,

    				]
    			);

    	return $view;
    }

    public function saveRecordAction()
    {
        $formData = $this->request->getPost();

        $id = isset($formData['Id']) ? intval($formData['Id']) : 0;
        $itemCategoryId = isset($formData["ItemCategoryId"])?(int)$formData["ItemCategoryId"]:0;
        $taxId = isset($formData["TaxId"])?(int)$formData["TaxId"]:0;
        $isCategoryTax = isset($formData["IsCategoryTax"])?(int)$formData["IsCategoryTax"]:0;
        
        $taxRecord = $this->getService(Accountant\TaxRepository::class)->getRecord($taxId);

        $item = [];
        $condition = [];

        if( !$isCategoryTax )
        {
            $condition["Id"] = $id;
            $item = [
                "ItemTaxId"     => $taxRecord->Id,
                "ItemTaxName"   => $taxRecord->Name,
                "ItemTaxValue"  => $taxRecord->Value,
                "IsItemTax" => $formData["IsItemTax"],
                "ItemTaxNote" => $formData["Note"]
            ];
        }
        else
        {
            $condition["ItemCategoryId"] = $itemCategoryId;
            $item = [
                "CategoryTaxId"     => $taxRecord->Id,
                "CategoryTaxName"   => $taxRecord->Name,
                "CategoryTaxValue"  => $taxRecord->Value,
                "IsCategoryTax"     => $formData["IsCategoryTax"],
                "CategoryTaxNote"   => $formData["Note"]
            ];
        }
        $this->objectRepository->updateRecords($item,$condition);

		$result   = new JsonModel();

		return $result;
    }
}