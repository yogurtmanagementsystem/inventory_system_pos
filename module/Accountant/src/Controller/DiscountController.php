<?php
/**
 * @author Phin Heng
 * @since  05 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Controller;

use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use Accountant\Model AS Accountant;
use PCSPLib\PasswordHelper;
use Zend\View\Model\JsonModel;

class DiscountController extends BaseController
{
    protected $idenMenu = "inventory";
    private $discountTypeRepo;

    public function __construct(ContainerInterface $serviceLocator)
    {
        $this->discountTypeRepo = $serviceLocator->get(Accountant\DiscountTypeRepository::class);
        $table = $serviceLocator->get(Accountant\DiscountRepository::class);

        parent::__construct($serviceLocator, $table);
    }
    public function newAction()
    {
    	$view = parent::newAction();
    	$gridIdName = isset($_POST["gridIdName"])?$_POST["gridIdName"]:0;

        //get discount type
        $discountTypeRecord = $this->discountTypeRepo->getAllActiveRecords()->toArray();

    	$view->setVariables(
    					[
                            'discountTypeRecord' => $discountTypeRecord,
    						"gridIdName"	=>	$gridIdName,

    					]
    				);

    	return $view;
    }

    public function saveRecordAction()
    {
        $id   = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $formData = $this->request->getPost();
        $data = [];
        
        try
        {
            $record = $this->objectRepository->getTableObject();
            $record->exchangeArray($formData);
            $arrayTableField = $record->removeNoneTableField((array)$record);

            if(!$this->isItemExist())
            {
                $discountId = $id;
                $isDefault = isset( $formData["IsDefault"] ) ? (int)$formData["IsDefault"] : 0;
                
                if( $isDefault )
                {
                    $this->objectRepository->updateRecords(["IsDefault" => 0],[]);
                }

                $id = $this->objectRepository->saveRecord($id, $arrayTableField);

                $data = ['Id'=>$id];
            }
            else
            {
                $data = ['msg'=>$this->translator->translate('ITEM_EXIST')];
            }

            $result   = new JsonModel($data);
            return $result;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function defaultRecordAction()
    {
        $id   = isset($_POST['Id']) ? (int)$_POST['Id'] : 0;
        $formData = $this->request->getPost();
        try
        {
            $this->objectRepository->updateRecords(["IsDefault"=>0],[]);
            $this->objectRepository->updateRecords(["IsDefault"=>1], $id);

            $data = array('title'=>null,'msg'=>null);
            $result = new JsonModel($data);

            return $result;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
    public function itemAction()
    {
        $view = parent::listAction();

        $cacheKey = md5("item-discount");
        $record   = [];

        if($this->cache->hasItem($cacheKey))
        {
            $record   = (array)json_decode($this->cache->getItem($cacheKey));
        }

        $view->setVariables(
                [
                    "record" => $record
                ]
            );

        return $view;
    }
    public function changeItemAction()
    {
        $value = isset( $_POST["Value"] ) ? (int)$_POST["Value"] : 0;
        $checked = isset( $_POST["Checked"] ) ? (int)$_POST["Checked"] : 0;

        $cacheKey = md5("item-discount");

        if($this->cache->hasItem($cacheKey))
        {
            $item = $this->cache->getItem($cacheKey);
            $item = (array)json_decode($item);

            if( !$checked )
            {
                if( isset( $item[$value."@"] ) ) unset( $item[$value."@"] );
            }
            else
            {
                $item = array_merge($item, [$value."@"=>$value]);                
            }
            $this->cache->replaceItem($cacheKey, json_encode($item));
        }
        else
        {
            $this->cache->addItem($cacheKey, json_encode([$value."@"=>$value]));
        }

        return new JsonModel();
    }
}