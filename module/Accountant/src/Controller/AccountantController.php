<?php
/**
 * @author Phin Heng
 * @since  Aug 30 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Db\Sql\Expression;

use PCSPLib\BaseClasses\BaseController;
use Accountant\Model AS Accountant;

class AccountantController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(Accountant\SalePriceRepository::class);

        parent::__construct($serviceLocator, $table);
    }

    public function exchangeAction()
    {
    	$view = parent::listAction();

    	$moneyExchange = $this->getExchange();;

    	$view->setVariables(
    			[
    				"moneyExchange" => $moneyExchange,

    			]
    		);

    	return $view;
    }

    public function getExchange($cacheKey = "Exchange")
    {
        $exchange = 4000;
        if($this->cache->hasItem($cacheKey))
        {
            $item   = (array)json_decode($this->cache->getItem($cacheKey));
            $exchange = $item["Exchange"];
        }
        else
        {
            $this->cache->addItem($cacheKey, json_encode(["Exchange" => $exchange]));
        }

        return $exchange;
    }

    public function saveRecordAction()
    {
    	$exchange = isset( $_POST["Exchange"] ) ? $_POST["Exchange"] : 4000;

        $cacheKey = "Exchange";

        if($this->cache->hasItem($cacheKey))
        {
            $item = ["Exchange" => $exchange];

            $this->cache->replaceItem($cacheKey, json_encode($item));
        }
        else
        {
            $this->cache->addItem($cacheKey, json_encode(["Exchange" => $exchange]));
        }

    	return new JsonModel();
    }
}