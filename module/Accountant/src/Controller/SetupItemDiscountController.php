<?php
/**
 * @author Phin Heng
 * @since  05 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Controller;

use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use Accountant\Model AS Accountant;
use PCSPLib\PasswordHelper;
use Zend\View\Model\JsonModel;

class SetupItemDiscountController extends BaseController
{
    protected $idenMenu = "inventory";

    public function __construct(ContainerInterface $serviceLocator)
    {
        $table = $serviceLocator->get(Accountant\SetupItemDiscountRepository::class);

        parent::__construct($serviceLocator, $table);
    }

    public function newAction()
    {
    	$view = parent::newAction();
    	$itemMasterId = isset($_POST["ItemMasterId"])?(int)$_POST["ItemMasterId"]:0;

    	$discountRecord = $this->getService(Accountant\DiscountValueRepository::class)->getAllActiveRecords(["ValueType"=>1])->toArray();

    	$view->setVariables(
    				[
    					"discountRecord" => $discountRecord,
    					"itemMasterId"   => $itemMasterId,

    				]
    			);

    	return $view;
    }

    public function saveRecordAction()
    {
        $id = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $formData = $this->request->getPost();
        $discountIds = isset($formData["DiscountIds"])?$formData["DiscountIds"]:"";
        $discountNames = isset($formData["DiscountNames"])?$formData["DiscountNames"]:"";
        $itemMasterId= isset($formData["ItemMasterId"])?(int)$formData["ItemMasterId"]:0;
        $isActive = isset($_POST["IsActive"])?(int)$_POST["IsActive"]:0;
        $note = isset($_POST["Note"])?$_POST["Note"]:0;

        $record = explode(",", $discountIds);
        $arr = explode(",", $discountNames);

		$object = $this->objectRepository->getTableObject();
		$this->objectRepository->deleteRecord(["ItemMasterId" => $itemMasterId],0);

		foreach( $record as $key => $value )
		{
			$item =[
				"DiscountId"  => $value,
				"ItemMasterId"=> $itemMasterId,
				"IsActive"    => $isActive,
				"Note"		  => $note,
				"DiscountIds" => $discountIds,
				"DiscountName"=> $arr[$key]
			];

			$object->exchangeArray($item);
			$arrayTableField = $object->removeNoneTableField((array)$object);

			$id = $this->objectRepository->saveRecord(0, $arrayTableField);
		}

		$result   = new JsonModel();

		return $result;
    }
}