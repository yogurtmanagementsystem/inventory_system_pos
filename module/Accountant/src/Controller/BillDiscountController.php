<?php
/**
 * @author Phin Heng
 * @since  05 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Accountant\Controller;

use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use Accountant\Model AS Accountant;
use PCSPLib\PasswordHelper;
use Zend\View\Model\JsonModel;

class BillDiscountController extends BaseController
{
    protected $idenMenu = "inventory";
    private $discountTypeRepo;

    
    public function __construct(ContainerInterface $serviceLocator)
    {
        $this->discountTypeRepo = $serviceLocator->get(Accountant\DiscountTypeRepository::class);
        $table = $serviceLocator->get(Accountant\DiscountValueRepository::class);

        parent::__construct($serviceLocator, $table);
    }
    
    public function newAction()
    {
    	$view = parent::newAction();
    	$gridIdName = isset($_POST["gridIdName"])?$_POST["gridIdName"]:0;

        //get discount type
        $discountTypeRecord = $this->discountTypeRepo->getAllActiveRecords(['Id != 3'])->toArray();

    	$view->setVariables(
    					[
                            'discountTypeRecord' => $discountTypeRecord,
    						"gridIdName"	=>	$gridIdName,

    					]
    				);

    	return $view;
    }

    public function getFilterCondition()
    {
        $filtersCount = isset($_POST['filterscount']) ? intval($_POST['filterscount']) : 0;
        $filterString = "";
        $tableName = $this->tableName;

        $otherFilter = '';
        if(isset($_POST['ValueType'])) $filterString .= 'ValueType='.intval($_POST['ValueType']);

        if ($filtersCount > 0)
        {
            //$where = " WHERE (";
            $where = " AND (";
            $tmpDataField = "";
            $tmpFilterOperator = "";
            for ($i = 0; $i < $filtersCount; $i++)
            {
                // get the filter's column.
                $filterDataField = $_POST["filterdatafield" . $i];
                // get the filter's value.
                $filterValue = $_POST["filtervalue" . $i];
                if($filterDataField == "IsActive"){
                    if(isset($_POST["filterid" . $i])){
                        $filterValue = $_POST["filterid" . $i];
                    }
                }
                // get the filter's condition.
                $filterCondition = $_POST["filtercondition" . $i];
                // get the filter's operator.
                $filterOperator = $_POST["filteroperator" . $i];
                if ($tmpDataField == "")
                {
                    $tmpDataField = $filterDataField;
                }
                else if ($tmpDataField <> $filterDataField)
                {
                    $where.= ") AND (";
                }
                else if ($tmpDataField == $filterDataField)
                {
                    if ($tmpFilterOperator == 0)
                    {
                        $where.= " AND ";
                    }
                    else $where.= " OR ";
                }
                $formatStr = $this->phpClass->jqxFilterFormat($filterCondition, $filterValue);

                $where.= " {$tableName}.".$filterDataField . $formatStr;

                if ($i == $filtersCount - 1){
                    $where.= ")";
                }
                $tmpFilterOperator = $filterOperator;
                $tmpDataField = $filterDataField;
            }
            $where = str_replace("IsActive",$this->tableName.".IsActive",$where);
            $filterString .= $where;
        }
        return $filterString;
    }
}