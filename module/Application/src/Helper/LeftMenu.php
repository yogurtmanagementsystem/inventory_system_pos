<?php
/**
 * @author Phin Heng
 * @since  Oct 21 2016
 * @copyright PCSP GROUP COL.TD
 * Left menu class for generat menu 
**/
namespace Application\Helper;

class LeftMenu 
{
	public static $baseUrl;
	public static $trans;
	
	public static function setTranslate($trans) 
	{
	    self::$trans = $trans;
	}

	public static function getTranslator()
	{
		return self::$trans;
	}
	
	public static function setBaseUrl($baseUrl)
	{
		self::$baseUrl = $baseUrl;

		return self::$baseUrl;
	}
	
	public static function getBaseUrl()
	{
		return self::$baseUrl;
	}

	public static function InitLeftMenu($iden)
	{
		$data = array();

		switch ($iden) 
		{
    		case 'inventory':
    			$data = self::getInventoryMenu();break;
    		default:
		}
		return $data;
	}

	public static function getInventoryMenu()
	{
	    $baseUrl = self::getBaseUrl();

	    return 
	    [
	    	[
	    		'id'		=>	'dashboard',
				'url'		=>	$baseUrl.'/inventory',
				'text'		=>	self::getTranslator()->translate("DASHBOARD"),
				'hasChild'	=>	false,
				'img'		=>	$baseUrl."/icon/dashboard.png",
				'type'		=>	'url',
	    	],
	    	[
	            'name'		=>	'user',
	            'id'		=>	'user-treemenu',
	            'url'		=>	'#',
	            'text'		=>	self::getTranslator()->translate('USER_MANAGEMENT'),
	            'hasChild'	=>	true,
	            'img'		=>	$baseUrl."/icon/user-management.png",
	            'type'		=>	'url',
	            'permission'=>
						[
							'type'		=>	'groups',
							'action'	=>	['list'],
							'module'	=>	[
												'user',
												'user-role'
											]		
						],
	            'children'	=>
	            [
	            	[
	                    'name'	=>	'user',
	                    'id'  	=>	'sys-user-List',
	                    'url'	=>	self::getBaseUrl().'/user/user/list',
	                    'text'	=>	self::getTranslator()->translate('USER'),
	                    'img'		=>	$baseUrl."/icon/user.png",
	                    'type'	=>	'url',
	                    'permission'=>[
										'module'	=>	'user',
										'type'		=>	'single',
										'action'	=>	['list'],											
									 ],
	                ],
	                [
	                    'name'	=>	'user-role',
	                    'id'  	=>	'sys-user-role-List',
	                    'url'	=>	self::getBaseUrl().'/user/role/list',
	                    'text'	=>	self::getTranslator()->translate('ROLE'),
	                    'img'		=>	$baseUrl."/icon/user-role.png",
	                    'type'	=>	'url',
	                    'permission'=>[
										'module'	=>	'user-role',
										'type'		=>	'single',
										'action'	=>	['list'],											
									 ],
	                ],
					[
			            'name'	=>	'user-resource',
			            'id'	=>	'user-resource',
			            'url'	=>	'#',
			            'text'	=>	self::getTranslator()->translate('USER_RESOURCE'),
			            'hasChild'=>true,
			            'icon'	=>	'glyphicon glyphicon-minus',
			            'type'	=>	'url',
			            'permission'=>["AutoAllow" => 0],
			            'children'=>
			            [
			                [
			                    'name'	=>	'group-resource',
			                    'id'  	=>	'sys-group-resource-List',
			                    'url'	=>	self::getBaseUrl().'/user/group-resource/list',
			                    'text'	=>	self::getTranslator()->translate('GROUP_RESOURCE'),
			                    'icon'	=>	'glyphicon glyphicon-minus',
			                    'type'	=>	'url',
			                    'permission'=>["AutoAllow" => 1],
			                ],
			                [
			                    'name'	=>	'resource-action',
			                    'id'  	=>	'sys-resource-action-List',
			                    'url'	=>	self::getBaseUrl().'/user/resource-action/list',
			                    'text'	=>	self::getTranslator()->translate('RESOURCE_ACTION'),
			                    'icon'	=>	'glyphicon glyphicon-minus',
			                    'type'	=>	'url',
			                    'permission'=>["AutoAllow" => 1],
			                ],
			                [
			                    'name'	=>	'resource-controllers',
			                    'id'  	=>	'sys-resource-controllers-List',
			                    'url'	=>	self::getBaseUrl().'/user/resource-controllers/list',
			                    'text'	=>	self::getTranslator()->translate('RESOURCE_COTNROLLER'),
			                    'icon'	=>	'glyphicon glyphicon-minus',
			                    'type'	=>	'url',
			                    'permission'=>["AutoAllow" => 1],
			                ],
			                [
			                    'name'	=>	'resource',
			                    'id'  	=>	'sys-resource-List',
			                    'url'	=>	self::getBaseUrl().'/user/resource/list',
			                    'text'	=>	self::getTranslator()->translate('RESOURCE'),
			                    'icon'	=>	'glyphicon glyphicon-minus',
			                    'type'	=>	'url',
			                    'permission'=>["AutoAllow" => 1],
			                ],
		            	]
		        	],
	            ]
	        ],
	     //    [
	     //        'name'		=>	'item-transaction',
	     //        'id'		=>	'item-transaction-treemenu',
	     //        'url'		=>	'#',
	     //        'text'		=>	self::getTranslator()->translate('Product Transaction'),
	     //        'hasChild'	=>	true,
	     //        'img'		=>	$baseUrl."/icon/item-transaction.png",
	     //        'type'		=>	'url',
	     //        'permission'=>
						// [
						// 	'type'		=>	'groups',
						// 	'action'	=>	['list'],
						// 	'module'	=>	[
						// 						'item-receive',
						// 						'item-adjustment',
						// 						'item-take',
						// 						'item-missing-broken'
						// 					]		
						// ],
	     //        'children'	=>
	     //        [
			   //      [
			   //          'id'		=>	'item-receive',
			   //          'url'		=>	$baseUrl.'/inventory/item-receive/list',
			   //          'text'		=>	self::getTranslator()->translate('Product Receipt'),
			   //          'hasChild'	=>	false,
			   //          // 'icon'		=>	'fa-cog',
			   //          'img'		=>	$baseUrl."/icon/item-receive.png",
			   //          'type'		=>	'url',
			   //          'children'  => 	[],
			   //          'permission'=>[
						// 				'module'	=>	'item-receive',
						// 				'type'		=>	'single',
						// 				'action'	=>	['list'],											
						// 			],
			   //      ],
			   //      [
			   //          'id'		=>	'item-adjustment',
			   //          'url'		=>	$baseUrl.'/inventory/item-adjustment/list',
			   //          'text'		=>	self::getTranslator()->translate('Product Adjustment'),
			   //          'hasChild'	=>	false,
			   //          // 'icon'		=>	'fa-cog',
			   //          'img'		=>	$baseUrl."/icon/item-adjustment.png",
			   //          'type'		=>	'url',
			   //          'children'  => 	[],
			   //          'permission'=>[
						// 				'module'	=>	'item-adjustment',
						// 				'type'		=>	'single',
						// 				'action'	=>	['list'],											
						// 			],
			   //      ],
			   //      [
			   //          'id'		=>	'item-take',
			   //          'url'		=>	$baseUrl.'/inventory/item-take/list',
			   //          'text'		=>	self::getTranslator()->translate('Product Take'),
			   //          'hasChild'	=>	false,
			   //          'img'		=>	$baseUrl."/icon/item-take.png",
			   //          'type'		=>	'url',
			   //          'children'  => 	[],
			   //          'permission'=>[
						// 				'module'	=>	'item-take',
						// 				'type'		=>	'single',
						// 				'action'	=>	['list'],											
						// 			],
			   //      ],
			   //      [
			   //          'id'		=>	'item-missing',
			   //          'url'		=>	$baseUrl.'/inventory/item-missing-broken/list',
			   //          'text'		=>	self::getTranslator()->translate('Product Missing Broken'),
			   //          'hasChild'	=>	false,
			   //          'img'		=>	$baseUrl."/icon/item-missing-broken.png",
			   //          'type'		=>	'url',
			   //          'children'  => 	[],
			   //          'permission'=>[
						// 				'module'	=>	'item-missing-broken',
						// 				'type'		=>	'single',
						// 				'action'	=>	['list'],											
						// 			],
			   //      ],
	     //        ]
	     //    ],
	        [
	            'name'		=>	'stock-balance',
	            'id'		=>	'stock-balance-treemenu',
	            'url'		=>	'#',
	            'text'		=>	self::getTranslator()->translate('STOCK_BALANCE'),
	            'hasChild'	=>	true,
	            'img'		=>	$baseUrl."/icon/stock-balance.png",
	            'type'		=>	'url',
	            'permission'=>
						[
							'type'		=>	'groups',
							'action'	=>	['list'],
							'module'	=>	[
												'stock-balance-master',
												'sale-stock',
												'stock-alert'
											]		
						],
	            'children'	=>
	            [
			        [
			            'id'		=>	'stock-balance-master',
			            'url'		=>	$baseUrl.'/inventory/stock-balance-master/list',
			            'text'		=>	self::getTranslator()->translate('STOCK'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/stock-balance-master.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'stock-balance-master',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
			        [
			            'id'		=>	'stock-alert',
			            'url'		=>	$baseUrl.'/inventory/stock-alert/list',
			            'text'		=>	self::getTranslator()->translate('Stock Alert'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/stock-alert.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'stock-alert',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
			        [
			            'id'		=>	'store',
			            'url'		=>	$baseUrl.'/inventory/sale-stock/list',
			            'text'		=>	self::getTranslator()->translate('Sale Stock'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/store.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'sale-stock',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
	            ]
	        ],
	        [
	            'name'		=>	'accountant',
	            'id'		=>	'accountant-treemenu',
	            'url'		=>	'#',
	            'text'		=>	self::getTranslator()->translate('Accountant'),
	            'hasChild'	=>	true,
	            'img'		=>	$baseUrl."/icon/accountant.png",
	            'type'		=>	'url',
	            'permission'=>
						[
							'type'		=>	'groups',
							'action'	=>	['list'],
							'module'	=>	[
												'exchange',
												'sale-price',
												'income',
												'discount'
											]		
						],
	            'children'	=>
	            [
			        [
			            'id'		=>	'exchange',
			            'url'		=>	$baseUrl.'/accountant/accountant/exchange',
			            'text'		=>	self::getTranslator()->translate('Exchange'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/exchange.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'exchange',
										'type'		=>	'single',
										'action'	=>	['exchange'],											
									],
			        ],
			        [
			            'id'		=>	'sale-price',
			            'url'		=>	$baseUrl.'/accountant/sale-price/list',
			            'text'		=>	self::getTranslator()->translate('Sale Price'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/sale-price.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'sale-price',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
			        [
			            'id'		=>	'income',
			            'url'		=>	$baseUrl.'/accountant/income/list',
			            'text'		=>	self::getTranslator()->translate('Income'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/income.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'income',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],			        
			        [
			            'id'		=>	"item-discount",
			            'url'		=>	$baseUrl.'/accountant/discount/item',
			            'text'		=>	self::getTranslator()->translate("Item discount"),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/item-discount.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'discount',
										'type'		=>	'single',
										'action'	=>	['item'],											
									],
			        ],
			        [
			            'id'		=>	'discount',
			            'url'		=>	$baseUrl.'/accountant/discount/list',
			            'text'		=>	self::getTranslator()->translate('Discount'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/discount.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'discount',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
			        
	            ]
	        ],
	        [
	            'name'		=>	'sale-management',
	            'id'		=>	'sale-management-treemenu',
	            'url'		=>	'#',
	            'text'		=>	self::getTranslator()->translate('Sale Management'),
	            'hasChild'	=>	true,
	            'img'		=>	$baseUrl."/icon/manage-sale.png",
	            'type'		=>	'url',
	            'permission'=>
						[
							'type'		=>	'groups',
							'action'	=>	['list','screen'],
							'module'	=>	[
												'sale-item',
												'sale',
												'pos-screen',
												'sale-item-category',
												'sale-item-type'
											]		
						],
	            'children'	=>
	            [
			        [
			            'id'		=>	'sal-item-master',
			            'url'		=>	$baseUrl.'/sale/sale-item/list',
			            'text'		=>	self::getTranslator()->translate('Sale Product'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/pos.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'sale-item',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
			        [
			            'id'		=>	'sale-pos-screen',
			            'url'		=>	$baseUrl.'/pos/pos/screen',
			            'text'		=>	self::getTranslator()->translate('POS Screen'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/pos.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'pos-screen',
										'type'		=>	'single',
										'action'	=>	['screen'],											
									],
			        ],
			        [
			            'id'		=>	'sale',
			            'url'		=>	$baseUrl.'/sale/sale/list',
			            'text'		=>	self::getTranslator()->translate('Sale'),
			            'hasChild'	=>	false,
			            'img'		=>	$baseUrl."/icon/sale.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'sale',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
			        [
			            'name'		=>	'sale-setting',
			            'id'		=>	'sale-setting-treemenu',
			            'url'		=>	'#',
			            'text'		=>	self::getTranslator()->translate('Settings'),
			            'hasChild'	=>	true,
			            'img'		=>	$baseUrl."/icon/setting.png",
			            'type'		=>	'url',
			            'permission'=>
								[
									'type'		=>	'groups',
									'action'	=>	['list'],
									'module'	=>	[
														'sale-item-category',
														'sale-item-type',
														'sale-item-size'
													]		
								],
			            'children'	=>
			            [
					        [
					            'id'		=>	'sale-item-category',
					            'url'		=>	$baseUrl.'/sale/item-category/list',
					            'text'		=>	self::getTranslator()->translate('Product Category'),
					            'hasChild'	=>	false,
					            'img'		=>	$baseUrl."/icon/item-category.png",
					            'type'		=>	'url',
					            'children'  => 	[],
					            'permission'=>[
										'module'	=>	'sale-item-category',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
					        ],  
					        [
					            'id'		=>	'sale-item-type',
					            'url'		=>	$baseUrl.'/sale/item-type/list',
					            'text'		=>	self::getTranslator()->translate('Product Type'),
					            'hasChild'	=>	false,
					            'img'		=>	$baseUrl."/icon/item-type.png",
					            'type'		=>	'url',
					            'children'  => 	[],
					            'permission'=>[
										'module'	=>	'sale-item-type',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
					        ], 
					    //     [
					    //         'id'		=>	'sale-item-size',
					    //         'url'		=>	$baseUrl.'/sale/item-size/list',
					    //         'text'		=>	self::getTranslator()->translate('Item Size'),
					    //         'hasChild'	=>	false,
					    //         'img'		=>	$baseUrl."/icon/item-size.png",
					    //         'type'		=>	'url',
					    //         'children'  => 	[],
					    //         'permission'=>[
									// 	'module'	=>	'sale-item-size',
									// 	'type'		=>	'single',
									// 	'action'	=>	['list'],											
									// ],
					    //     ], 
			            ]
			        ],			        
	            ]
	        ],
	        [
	            'id'		=>	'contact',
	            'url'		=>	$baseUrl.'/contact/contact/list',
	            'text'		=>	self::getTranslator()->translate('Contact'),
	            'hasChild'	=>	false,
	            // 'icon'		=>	'fa-cog',
	            'img'		=>	$baseUrl."/icon/contact.png",
	            'type'		=>	'url',
	            'children'  => 	[],
	            'permission'=>
	            			[
								'module'	=>	'contact',
								'type'		=>	'single',
								'action'	=>	['list'],											
							],
	        ],
	    	[
	            'id'		=>	'inventory-setting',
	            'url'		=>	'#',
	            'text'		=>	self::getTranslator()->translate('Settings'),
	            'hasChild'	=>	true,
	            // 'icon'		=>	'fa-cog',
	            'img'		=>	$baseUrl."/icon/setting.png",
	            'type'		=>	'url',
	            'permission'=>
						[
							'type'		=>	'groups',
							'action'	=>	['list'],
							'module'	=>	[
												'item-master',
												'supplier',
												'item-category',
												'item-missing-broken',
												'item-unit-measurement'
											]		
						],
	            'children'  =>
	            [
			        [
			            'id'		=>	'item-master',
			            'url'		=>	$baseUrl.'/inventory/item-master/list',
			            'text'		=>	self::getTranslator()->translate('Product Master'),
			            'hasChild'	=>	false,
			            // 'icon'		=>	'fa-cog',
			            'img'		=>	$baseUrl."/icon/item-master.png",
			            'type'		=>	'url',
			            'children'  => 	[],
			            'permission'=>[
										'module'	=>	'item-master',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
			        [
	                    'id'  	=>	'syssupplierList',
	                    'url'	=>	$baseUrl.'/inventory/supplier/list',
	                    'text'	=>	self::getTranslator()->translate('Supplier'),
	                    // 'icon'	=>	'fa-home',
	                    'img'		=>	$baseUrl."/icon/supplier.png",
	                    'type'	=>	'url',
	                    'permission'=>[
										'module'	=>	'supplier',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
	            	[
	                    'id'  	=>	'sysItemCategoryList',
	                    'url'	=>	$baseUrl.'/inventory/item-category/list',
	                    'text'	=>	self::getTranslator()->translate('Product Category'),
	                    // 'icon'	=>	'fa-pagelines',
	                    'img'		=>	$baseUrl."/icon/item-category.png",
	                    'type'	=>	'url',
	                    'permission'=>[
										'module'	=>	'item-category',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
			      //   [
	        //             'id'  	=>	'sysItemBrokenReasonList',
	        //             'url'	=>	$baseUrl.'/inventory/item-broken-reason/list',
	        //             'text'	=>	self::getTranslator()->translate('Product Broken Reason'),
	        //             // 'icon'	=>	'fa-code-fork',
	        //             'img'		=>	$baseUrl."/icon/item-missing-broken-reason.png",
	        //             'type'	=>	'url',
	        //             'permission'=>[
									// 	'module'	=>	'item-broken-reason',
									// 	'type'		=>	'single',
									// 	'action'	=>	['list'],											
									// ],
			      //   ],
			        [
	                    'id'  	=>	'sys-item-unit-measurement-list',
	                    'url'	=>	$baseUrl.'/inventory/item-unit-measurement/list',
	                    'text'	=>	self::getTranslator()->translate('Product Unit Measurement'),
	                    // 'icon'	=>	'fa-home',
	                    'img'		=>	$baseUrl."/icon/item-unit-measurement.png",
	                    'type'	=>	'url',
	                    'permission'=>[
										'module'	=>	'item-unit-measurement',
										'type'		=>	'single',
										'action'	=>	['list'],											
									],
			        ],
			    ]
	    	]
	    ];
	}
}