<?php
/**
 * @author Phin Heng
 * @since  06 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Pos\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Db\Sql\Expression;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;
use Sale\Model AS Sale;
use Accountant\Model AS Accountant;
use Contact\Model AS Contact;

class PosController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(Sale\SaleRepository::class);
        
        parent::__construct($serviceLocator, $table);
    }

    public function screenAction()
    {
        $view = parent::listAction();
        $this->layout()->setTemplate('layout/pos-layout');

        $discountRecord = $this->getService(Accountant\DiscountRepository::class)->getAllActiveRecords([],[],"Value")->toArray();
        $contactRecord  = $this->getService(Contact\ContactRepository::class)->getRecord(1);
        
        $cacheKey = md5("item-discount");
        $record   = [];

        if($this->cache->hasItem($cacheKey))
        {
            $record   = (array)json_decode($this->cache->getItem($cacheKey));
        }
        $numDayOfWeek = date("N");

        $isItemDiscount = isset( $record[$numDayOfWeek."@"] ) ? 1 : 0 ;

        $view->setVariables(
                [
                    "discountRecord" => $discountRecord,
                    "contactRecord"  => $contactRecord,
                    "userId"         => $this->userLoginId,
                    "userFullName"   => $this->userFullName,
                    "moneyExchange"  => $this->getExchange(),
                    "isItemDiscount" => $isItemDiscount

                ]
            );
        
        return $view;
    }

    public function getExchange($cacheKey = "Exchange")
    {
        $exchange = 4000;
        
        if($this->cache->hasItem($cacheKey))
        {
            $item   = (array)json_decode($this->cache->getItem($cacheKey));
            $exchange = $item["Exchange"];
        }
        else
        {
            $this->cache->addItem($cacheKey, json_encode(["Exchange" => $exchange]));
        }

        return $exchange;
    }
    public function newContentAction()
    {
        $view = parent::newPopupAction();

        $saleItemTable = TableConstant::SAL_SALE_ITEM_TABLE;

        $categoryId    = isset($_POST["ItemCategoryId"]) ? (int)$_POST["ItemCategoryId"] : 0;

        $condition = " {$saleItemTable}.IsActive = 1 AND {$saleItemTable}.ItemCategoryId=".$categoryId;

        // $record = $this->getService(Accountant\SalePriceRepository::class)->getAllRecords($condition)->toArray();
        $record = $this->getService(Sale\SaleItemRepository::class)->getAllActiveRecords($condition,[],"Name","ASC","GroupRecord")->toArray();
        
        $view->setVariables(
                [
                    "record" => $record,

                ]
            );

        return $view;
    }
    public function newItemAction()
    {
        $view = parent::newPopupAction();

        $saleItemTable = TableConstant::SAL_SALE_ITEM_TABLE;

        $groupRecord = isset( $_POST["GroupRecord"] ) ? $_POST["GroupRecord"] : "";

        $condition = " {$saleItemTable}.GroupRecord = '".$groupRecord."'";

        $record = $this->getService(Sale\SaleItemRepository::class)->getAllRecordForPos($condition)->toArray();

        $view->setVariables(
                [
                    "record" => $record,

                ]
            );

        return $view;
    }
    public function checkInternetAction()
    {
        
        return new JsonModel();
    }

    public function getInvoiceNo()
    {
        $format = '';
        
        $lastRecord = $this->objectRepository->getLastRecord();
        $number = $lastRecord->Id;

        $record[] = ["FormatType" => 2,"Digit" => 8,"StartNum" => 1];

        foreach( $record as $key => $value )
        {
            $formatType = $value["FormatType"];
            switch($formatType)
            {
                // case 1:$format .= $value["Text"];break;
                case 2:
                    $startNum = $value["StartNum"];
                    $digit = $value["Digit"];
                    $num = $startNum+$number;
                    $len = strlen($num)+1;
                    $n = '';
                    for( $i = $len; $i <= $digit ; $i ++)
                    {
                        $n .= "0";
                    }
                    $format .= $n.$num;
                    break;
                case 3:
                    // $format .= date($value->DateFormat);
                    break;
            }
        }
        return $format;
    }
    public function saveRecordAction()
    {
        $id = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $formData = $this->request->getPost();

        $record = isset($formData["Record"])?$formData["Record"]:[];
        $invoiceNo = $this->getInvoiceNo();
        $invoiceDate = date("d/M/Y H:s");
        
        $object = $this->objectRepository->getTableObject();
        $object->exchangeArray($formData);
        $arrayTableField = $object->removeNoneTableField((array)$object);

        $arrayTableField["InvoiceNo"] = $invoiceNo;
        $arrayTableField["InvoiceDate"] = date("Y-m-d");

        $saleId = $this->objectRepository->saveRecord(0, $arrayTableField);

        $saleDetailRepository = $this->getService(Sale\SaleDetailRepository::class);

        $detailObject = $saleDetailRepository->getTableObject();

        $saleTotalDiscount = $formData["TotalDiscount"];
        $saleSubTotal  = $formData["SubTotal"];
        
        foreach( $record as $key => $value )
        {
            $item = $value;
            $item["SaleId"] = $saleId;

            $detailObject->exchangeArray($item);
            $arrayTableField = $detailObject->removeNoneTableField((array)$detailObject);

            $total = $detailObject->Total;

            if( isset( $value["TotalDiscount"] ) && !$value["TotalDiscount"] )
            {
                $totalDiscount = ($saleTotalDiscount*$total)/$saleSubTotal;
                $total = $total-$totalDiscount;

                $arrayTableField["TotalDiscount"] = $totalDiscount;
                $arrayTableField["Total1"] = $total-$totalDiscount;
            }

            $saleDetailId = $saleDetailRepository->saveRecord(0, $arrayTableField);
        }

        $item = ["InvoiceNo" => $invoiceNo,"InvoiceDate" => $invoiceDate];

        return new JsonModel($item);
    }

    public function getRecordsForTreeGridAction()
    {
        $condition = " IsActive = 1 ";
        $records = $this->getService(Sale\ItemCategoryRepository::class)->getRecordsForTreeGrid($condition)->toArray();

        $result = new JsonModel($records);

        return $result;
    }
}