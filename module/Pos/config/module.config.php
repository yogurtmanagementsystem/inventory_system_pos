<?php
/**
 * @author Phin Heng
 * @since  05 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Pos;

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\BaseClasses\BaseObject;
use PCSPLib\TableConstant;
use Zend\Db\Adapter\AdapterInterface;
use Interop\Container\ContainerInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;

return [
    
    'service_manager' => 
    [
        'aliases' => 
        [
            
        ],
        'factories' => 
        [
            
        ],
    ],
    'controllers' => 
    [
        'factories' => 
        [
            Controller\PosController::class => function(ContainerInterface $container)
            {
                return new Controller\PosController($container);
            },
            Controller\LoginController::class => function(ContainerInterface $container)
            {
                return new Controller\LoginController($container);
            },
        ],      
    ],
    'router' => 
    [
        'routes' => 
        [
            'pos' => 
            [
                'type' => Literal::class,
                'options' => 
                [
                    'route' => '/pos',
                    'defaults' => 
                    [
                        'controller' => Controller\PosController::class,
                        'action'     => 'screen',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => 
                [
                    'pos' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/pos[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\PosController::class,
                                'action'     => 'screen',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],    
                    'login' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/login[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\LoginController::class,
                                'action'     => 'index',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],       
                ],
            ],
        ],
    ],
    'view_manager' => 
    [
        'template_path_stack' => 
        [
            'pos' => __DIR__ . '/../view',
        ],
    ],
    'translator' =>
    [
        'locale' => 'en_US',
        'translation_file_patterns' =>
            [
                [
                    'type' => 'gettext',
                    'base_dir' => __DIR__ . '/../language',
                    'pattern' => '%s.mo'
                ]
            ]
    ],
];
