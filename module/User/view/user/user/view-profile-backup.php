<?php
use PCSPLib\PHPConstant;

$jqxPrefix = "_userProfile";
//$url = $this->basepath().'/'.PHPConstant::ENQUIRY_MODULE.'/'.PHPConstant::PROSPECT_CONTROLLER;
$escaper = new \Zend\Escaper\Escaper('utf-8');

$userImage = PHPConstant::USER_IMAGE_BLANK;
?>

<section class="content-custom-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> User</a></li>
        <li>Profile</li>
    </ol>
</section>

<section class="content-header">
    <h1>
        User Profile
        <small></small>
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-lg-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" alt="Profile Image" src="<?php echo $this->basepath($userImage);?>"/>
                    <h3 class="profile-username text-center">Sopheak Chhin</h3>
                    <p class="text-muted text-center">Web Developer</p>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Tel: </b>
                            <span class="pull-right">016232358</span>
                        </li>
                        <li class="list-group-item">
                            <b>Email: </b>
                            <span class="pull-right">it.sopheakchhin@yahoo.com</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#profile">Profile</a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#account">Account</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="profile" class="tab-pane active">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-lg-2 control-label text-right">First Name:</label>
                                <div class="col-lg-10"><input type="text" class="form-control" id="FirstName" name="FirstName"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label text-right">Last Name:</label>
                                <div class="col-lg-10"><input type="text" class="form-control" id="LastName" name="LastName"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-default">Cancel</button>
                                    <input type="submit" class="btn btn-primary" value="Save">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="account" class="tab-pane">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-lg-2 control-label text-right">Old Password:</label>
                                <div class="col-lg-10"><input type="password" class="form-control" id="OldPassword" name="OldPassword"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label text-right">New Password:</label>
                                <div class="col-lg-10"><input type="password" class="form-control" id="NewPassword" name="NewPassword"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label text-right">Confirm New Password:</label>
                                <div class="col-lg-10"><input type="password" class="form-control" id="ConfirmNewPassword" name="ConfirmNewPassword"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-default">Cancel</button>
                                    <input type="submit" class="btn btn-primary" value="Save">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>