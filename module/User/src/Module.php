<?php

namespace User;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\ModuleRouteListener;
use User\AclUtility\Acl;
use Zend\Session\Container;
use Zend\Authentication\AuthenticationService;

class Module implements ConfigProviderInterface
{
  public function getConfig()
  {
      return include __DIR__ . '/../config/module.config.php';
  }

	public function onBootstrap(MvcEvent $e)
  {
    // $this->pcspTestConnection($e);

    $eventManager = $e->getApplication()->getEventManager();
    $moduleRouteListener = new ModuleRouteListener();
    $moduleRouteListener->attach($eventManager);
    
    $eventManager->attach(MvcEvent::EVENT_DISPATCH, array($this, 'boforeDispatch'), 100);
  }

  public function pcspTestConnection($e)
  {
    $application   = $e->getApplication();
      $sm          = $application->getServiceManager();
       
      //try to connect, and if not connected, then catch...
      try 
      {
          $dbInstance = $application->getServiceManager()
                                    ->get('Zend\Db\Adapter\Adapter');
          $dbInstance->getDriver()->getConnection()->connect();
          exit();
      } 
      catch (\Exception $ex) 
      {
          $ViewModel = $e->getViewModel();
          $ViewModel->setTemplate('layout/CopyOflayout');

          $content = new \Zend\View\Model\ViewModel();
          $content->setTemplate('error/mydberrorpagecustompage');
           var_dump($ex);
          $ViewModel->setVariable('content', $sm->get('ViewRenderer')
                                                ->render($content));
           
          exit($sm->get('ViewRenderer')->render($ViewModel));
      }
  }
	
	function boforeDispatch(MvcEvent $event)
  {
      $request = $event->getRequest();
      $response = $event->getResponse();
      $target = $event->getTarget();
      
      $whiteList = 
      [
        'User\Controller\LoginController::login',
        'User\Controller\LoginController::logout'
      ];
      
      $requestUri = $request->getRequestUri();
	    $baseUrl = $request->getBasePath();
      $controller = $event->getRouteMatch()->getParam('controller');
      $action = $event->getRouteMatch()->getParam('action');
      $requestedResourse = $controller . '::' . $action;
	
	    //echo var_export($event->getRouteMatch());exit;
      
      $auth = new AuthenticationService();
	    $session = new Container();
      
      if ($auth->hasIdentity())
      { 
        if (!in_array($requestedResourse, $whiteList)) 
        {
			      $userLogin = $auth->getIdentity();
			      $userRoleId = $userLogin->RoleId;
			
			      $serviceManager = $event->getApplication()->getServiceManager();
            $acl = $serviceManager->get('Acl');
            $acl->initAcl($userRoleId);
			
			      // if (!$acl->hasResource($controller)) 
         //    {
			 	    //    die('Resource:: ' . $controller . ' not defined in ACL Resource. Please go to module\User\src\AclUtility\PreBuildResources.php and add this line of code :: '.$controller);
			      // }
			
         //    $isAllowed = $acl->isAccessAllowed($userRoleId, $controller, $action);
         //    if (!$isAllowed) 
         //    { 
			 	    //     $url = $event->getRouter()->assemble(array(), array('name' => 'auth-deny'));
			  	   //    $response->setHeaders($response->getHeaders()
         //            ->addHeaderLine('Location', $url));
			 	    //     $response->setStatusCode(302);
			 	    //      $response->sendHeaders();
         //    }
        }
      } 
      else 
      {
        if ( $controller == "Pos\Controller\PosController" || $controller == "Pos\Controller\LoginController" )
        // if ( $controller == "Pos\Controller\PosController" )
        {
          if( $action != "index")
          {
            $url = $baseUrl.'/pos/login';
            $response->setHeaders($response->getHeaders()
                    ->addHeaderLine('Location', $url));
            $response->setStatusCode(302);
          }
        }
        elseif ($requestedResourse != 'User\Controller\LoginController::login' && ! in_array($requestedResourse, $whiteList)) 
        {
            $url = $baseUrl.'/login';
            $response->setHeaders($response->getHeaders()
                    ->addHeaderLine('Location', $url));
            $response->setStatusCode(302);
        }
        $response->sendHeaders();
      }
  }
    
  public function getServiceConfig()
  {
      return 
      [
          'factories' => 
          [
			       'Acl' => function ($serviceManager)
              {
				          $dbAdapter = $serviceManager->get('dbAdapter');
                  return new Acl($serviceManager, $dbAdapter);
              },
			       'AuthService' => function ($serviceManager)
              {
  				        $auth = new AuthenticationService();

                  return $auth;
              },
			
			       'dbAdapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
          ],
      ];
  }
}