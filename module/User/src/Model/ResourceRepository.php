<?php
namespace User\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class ResourceRepository extends BaseRepository
{
	public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
	{
	    try
        {
            $select = $this->sql->select($this->tableName);

            $select->join(TableConstant::US_RESOURCE_ACTION_TABLE, "{$this->tableName}.ResourceActionId = ".TableConstant::US_RESOURCE_ACTION_TABLE.".Id", array("ActionName"=>"Name"), "left");
            $select->join(TableConstant::US_RESOURCE_CONTROLLER_TABLE, "{$this->tableName}.ResourceControllerId = ".TableConstant::US_RESOURCE_CONTROLLER_TABLE.".Id", array("ResourceName"=>"Name","GroupResourceName"), "left");
           
            if(!empty($filterString)) $select->where($filterString);
            $select->where("{$this->tableName}.IsDeleted = 0");
            $select->order(TableConstant::US_RESOURCE_CONTROLLER_TABLE.".OrderNum ".$order);
            $select->order($sort.' '.$order);
            $select->limit($rows)->offset($offset);
			
            $stmt   = $this->sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();
            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                return [];
            }
            $resultSet = new ResultSet();
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
	}

    public function getAllActiveRecords($condition = array(),$columns = array(), $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        try
        {
            $condition = array_merge($condition, ["{$this->tableName}.IsDeleted"=>0,"{$this->tableName}.IsActive"=>1]);
            $select = $this->sql->select($this->tableName);
            if(count($columns))
                $select->columns($columns);
            
            $select->join(TableConstant::US_RESOURCE_ACTION_TABLE, "{$this->tableName}.ResourceActionId = ".TableConstant::US_RESOURCE_ACTION_TABLE.".Id", array("ActionName"=>"Name"), "left");
            $select->join(TableConstant::US_RESOURCE_CONTROLLER_TABLE, "{$this->tableName}.ResourceControllerId = ".TableConstant::US_RESOURCE_CONTROLLER_TABLE.".Id", array("ResourceName"=>"Name","GroupResourceName","GroupResourceId"), "left");
           
            $select->where($condition)->order(TableConstant::US_RESOURCE_CONTROLLER_TABLE.".GroupResourceOrderNum ".$orderType);
            $select->where($condition)->order(TableConstant::US_RESOURCE_CONTROLLER_TABLE.".OrderNum ".$orderType);
            $select->where($condition)->order($orderField." ".$orderType);

            if(!empty($groupField))
            {
                $select->group($groupField);
            }
            $stmt   = $this->sql->prepareStatementForSqlObject($select);

            $result = $stmt->execute();

            $resultSet = new ResultSet();

            $resultSet->initialize($result);

            return $resultSet;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}