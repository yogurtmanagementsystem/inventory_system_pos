<?php
namespace User\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class RolePermissionRepository extends BaseRepository
{
    public function getAllPermissionRecord($condition = "")
    {
        try
        {           
            $tblControllerTable = TableConstant::US_RESOURCE_CONTROLLER_TABLE;
            $tblActionTable = TableConstant::US_RESOURCE_ACTION_TABLE;
            $tblResource = TableConstant::US_RESOURCE_TABLE;

            $query = "SELECT 
                            {$this->tableName}.*,
                            {$tblResource}.ResourceName,
                            {$tblResource}.PrefixResource,
                            {$tblResource}.ActionName 
                        FROM 
                            {$this->tableName}
                        LEFT JOIN 
                        (
                            SELECT
                                {$tblResource}.*,
                                {$tblControllerTable}.CustomController AS ResourceName,
                                {$tblControllerTable}.PrefixResource,
                                {$tblActionTable}.Prefix AS ActionName  
                            FROM 
                                {$tblResource}
                            LEFT JOIN
                                {$tblControllerTable}
                                ON 
                                    {$tblControllerTable}.Id = {$tblResource}.ResourceControllerId
                            LEFT JOIN 
                                {$tblActionTable}
                                ON 
                                    {$tblActionTable}.Id = {$tblResource}.ResourceActionId 
                        ) AS {$tblResource}
                        ON 
                            {$tblResource}.Id = {$this->tableName}.ResourceId
                        where 1 = 1 
                    ";
            $query .= $condition;

            $statement = $this->getDBAdaper()->createStatement($query);
            $results = $statement->execute();

            $resultSet = new ResultSet();
            $resultSet->initialize($results);

            return $resultSet;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}