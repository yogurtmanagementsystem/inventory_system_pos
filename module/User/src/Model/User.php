<?php

namespace User\Model;

use PCSPLib\BaseClasses\BaseObject;

class User extends BaseObject
{
    public $FirstName;
    public $LastName;
    public $Image;
    public $Email;
    public $Tel;
    public $Address;
    public $UserName;
    public $Password;
    public $Salt;
    public $RoleId;
	public $UserTypeId;
    public $SexId;
    public $DOB;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->FirstName 	= isset($data['FirstName'])?trim($data['FirstName']):'';
        $this->LastName 	= isset($data['LastName'])?trim($data['LastName']):'';
        $this->Image 		= isset($data['Image'])?trim($data['Image']):'';
        $this->Email 		= isset($data['Email'])?trim($data['Email']):'';
        $this->DOB          = isset($data['DOB'])? (!empty($data['DOB'])?date('Y-m-d',strtotime($data['DOB'])):null) : null;
        $this->Tel 	        = isset($data['Tel'])?trim($data['Tel']):'';
        $this->Address 		= isset($data['Address'])?$data['Address']:'';
        $this->UserName 	= isset($data['UserName'])?trim($data['UserName']):'';
        $this->Password 	= isset($data['Password'])?trim($data['Password']):'';
        $this->Salt 		= isset($data['Salt'])?$data['Salt']:'';
        $this->SexId 	    = isset($data['SexId'])?$data['SexId']:0;
		$this->UserTypeId 	= isset($data['UserTypeId'])? (!empty($data['UserTypeId'])?$data['UserTypeId']:1) : 1;
        $this->RoleId 		= isset($data['RoleId'])? (!empty($data['RoleId'])?$data['RoleId']:0) : 0;
    }

   public function getNoneTableField()
   {
       return ['Name','OrderNum'];
   }

}