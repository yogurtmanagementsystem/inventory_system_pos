<?php

namespace User\Model;

use PCSPLib\BaseClasses\BaseObject;

class ResourceAction extends BaseObject
{
    public $Prefix;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->Prefix 	= isset($data['Prefix'])?trim($data['Prefix']):'';
   }

   public function getNoneTableField()
   {
       return ['Note'];
   }
}