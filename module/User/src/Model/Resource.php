<?php

namespace User\Model;

use PCSPLib\BaseClasses\BaseObject;

class Resource extends BaseObject
{
    public $ResourceActionId;
    public $ResourceControllerId;

    public function exchangeArray($data)
    {
      parent::exchangeArray($data);
      $this->ResourceActionId      = isset($data['ResourceActionId'])?$data['ResourceActionId']:0;
      $this->ResourceControllerId      = isset($data['ResourceControllerId'])?$data['ResourceControllerId']:0;
   }

   public function getNoneTableField()
   {
      return ['Note',"Name"];
   }
}