<?php

namespace User\Model;

use PCSPLib\BaseClasses\BaseObject;

class ResourceControllers extends BaseObject
{
    public $GroupResourceId;
    public $CustomController;
    public $PrefixResource;
    public $GroupResourceName;
    public $GroupResourceOrderNum;

    public function exchangeArray($data)
    {
      parent::exchangeArray($data);
      
      $this->GroupResourceId    = isset($data['GroupResourceId'])?$data['GroupResourceId']:0;
      $this->CustomController   = isset($data['CustomController'])?trim($data['CustomController']):'';
      $this->PrefixResource     = isset($data['PrefixResource'])?trim($data['PrefixResource']):'';
      $this->GroupResourceName 	  = isset($data['GroupResourceName'])?trim($data['GroupResourceName']):'';
      $this->GroupResourceOrderNum= isset($data['GroupResourceOrderNum'])?$data['GroupResourceOrderNum']:0;
   }

   public function getNoneTableField()
   {
      return ['Note'];
   }
}