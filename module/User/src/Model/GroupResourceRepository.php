<?php
namespace User\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class GroupResourceRepository extends BaseRepository
{
    public function getAllActiveRecords($condition = array(),$columns = array(), $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        try
        {
            $condition = array_merge($condition, ["{$this->tableName}.IsDeleted"=>0,"{$this->tableName}.IsActive"=>1]);
            $select = $this->sql->select($this->tableName);
            
            $select->columns(
                    [
                        "*",
                        "ConcatId" => new Expression('CONCAT(Id,"#",OrderNum)')
                    ]
                );

            $select->where($condition)->order($orderField." ".$orderType);

            if(!empty($groupField))
            {
                $select->group($groupField);
            }
            $stmt   = $this->sql->prepareStatementForSqlObject($select);

            $result = $stmt->execute();

            $resultSet = new ResultSet();

            $resultSet->initialize($result);

            return $resultSet;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}