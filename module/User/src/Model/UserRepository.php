<?php
namespace User\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class UserRepository extends BaseRepository
{
	public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
	{
	    try
        {
            $select = $this->sql->select($this->tableName);
            $select->columns(array('*', new Expression('CONCAT(LastName ," ", FirstName) as FullName'), new Expression('DATE_FORMAT(DOB, "'.PHPConstant::DATE_FORMAT.'") as BirthDate')));
            $select->join(TableConstant::US_ROLE_TABLE, "{$this->tableName}.RoleId = ".TableConstant::US_ROLE_TABLE.".Id", array("RoleName"=>"Name"), "left");
            if(!empty($filterString)) $select->where($filterString);
            $select->where("{$this->tableName}.IsDeleted = 0");
            $select->order($sort.' '.$order);
            $select->limit($rows)->offset($offset);
            
            $stmt   = $this->sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();
            
            $resultSet = new ResultSet();
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
	}
	
	public function getAllActiveRecords($condition = '', $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        try
        {
            $select = $this->sql->select($this->tableName);
            $select->columns(array('*', new Expression('CONCAT(LastName ," ", FirstName) as FullName')));
            if(!empty($condition))
            {
                $select->where($condition);
            }
            $select->where(["IsDeleted"=>0,"IsActive"=>1])->order($orderField." ".$orderType);

            if(!empty($groupField))
            {
                $select->group($groupField);
            }
            $stmt   = $this->sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();
            if (! $result instanceof ResultInterface || ! $result->isQueryResult())
            {
                return [];
            }
            $resultSet = new ResultSet();
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}