<?php

namespace User\Model;

use PCSPLib\BaseClasses\BaseObject;

class UserPermission extends BaseObject
{
    public $UserId;
    public $ResourceId;
    public $IsAllow;

    public function exchangeArray($data)
    {
      parent::exchangeArray($data);
      
      $this->UserId      = isset($data['UserId'])?$data['UserId']:0;
      $this->ResourceId  = isset($data['ResourceId'])?$data['ResourceId']:0;
      $this->IsAllow     = isset($data['IsAllow'])?$data['IsAllow']:0;
   }

   public function getNoneTableField()
   {
      return ['Note',"Name","OrderNum"];
   }
}