<?php

namespace User\AclUtility;

class PreBuildResources {
	
	const ALL_CONTROLLERS = [
	
		//==========Application Module==============
		'Application\Controller\IndexController',
		
		//==========User Module==============
		'User\Controller\LoginController',
		'User\Controller\UserController',
		'User\Controller\RoleController',

		'User\Controller\GroupResourceController',
		'User\Controller\ResourceActionController',
		'User\Controller\ResourceControllersController',
		'User\Controller\ResourceController',
		
		//==========Inventory Module===========
		'Inventory\Controller\AdmissionController',
		'Inventory\Controller\InventoryController',
		'Inventory\Controller\WarehouseController',
		'Inventory\Controller\ItemCategoryController',
		'Inventory\Controller\ItemClassController',
		'Inventory\Controller\ItemUnitMeasurementController',
		'Inventory\Controller\ItemMethodController',
		'Inventory\Controller\ItemTypeController',
		'Inventory\Controller\ItemGroupController',
		'Inventory\Controller\ItemBrokenReasonController',
		'Inventory\Controller\SupplierController',
		'Inventory\Controller\ItemVarianceController',

		'Inventory\Controller\ItemMasterController',

		'Inventory\Controller\ItemReceiveController',
		'Inventory\Controller\ItemAdjustmentController',
		'Inventory\Controller\ItemTakeController',
		'Inventory\Controller\ItemMissingBrokenController',
		'Inventory\Controller\StockBalanceMasterController',
		'Inventory\Controller\StockBalanceDetailController',
		'Inventory\Controller\SaleStockController',

		'Inventory\Controller\StockAlertController',
		'Inventory\Controller\ReportController',

		// Sale
		'Sale\Controller\SaleController',
		'Sale\Controller\SalePriceController',
		'Sale\Controller\SaleTypeController',
		'Sale\Controller\ItemCategoryController',
		'Sale\Controller\ItemTypeController',
		'Sale\Controller\SaleItemController',
		'Sale\Controller\ItemSizeController',

		'Pos\Controller\PosController',
		'Pos\Controller\LoginController',

		// Contact
		'Contact\Controller\ContactController',

		// accountant
		'Accountant\Controller\AccountantController',
		'Accountant\Controller\SalePriceController',
		'Accountant\Controller\DiscountController',
		'Accountant\Controller\IncomeController',

		//==========SystemSetting Module===========
		'SystemSetting\Controller\SystemSettingController',
		'SystemSetting\Controller\FormatController',

	];	
}
		    