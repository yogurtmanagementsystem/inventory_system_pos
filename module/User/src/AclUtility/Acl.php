<?php
namespace User\AclUtility;

use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Role\GenericRole as AclRole;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;

use User\Model\RolePermission as RolePermission;
use User\Model\RoleRepository;
use User\Model\RolePermissionRepository;
use PCSPLib\TableConstant;

use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\BaseClasses\BaseObject;

class Acl extends ZendAcl
{
    /**
     * Meaning in ACL
     *
     * Role is refer to Role Name, exaple, "Admin, Editor, Guess..."
     * Resource is refer to Controller, example, "User\Controller\Login, Application\Controller\Index"
     * Permission is refer to Action Name, example, "Add, View, Edit, Delete"
     */
     
    const DEFAULT_ROLE = 'guest';
    
    protected $serviceLocator;
    protected $dbAdapter;
    protected $hydrator;
    protected $rolePrototype;
    
    protected $roleRepository;
    protected $resourceRepository;
    protected $permissionRepository;

    protected $roles;
    protected $permissions;
    protected $resources;
    protected $rolePermissions;
    protected $commonPermission;
    protected $preBuildResources;
    protected $rolePermissionRepository;
    protected $rolePermissionPrototype;

    protected $user_role_id;
    
    public function __construct(ServiceLocatorInterface $serviceLocator, AdapterInterface $dbAdapter)
    {
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $dbAdapter;
        $this->hydrator      = new ReflectionHydrator();
        $this->rolePrototype = new BaseObject();
        $this->rolePermissionPrototype = new RolePermission();
        
        $this->rolePermissionRepository  = new RolePermissionRepository($this->dbAdapter, TableConstant::US_ROLE_PERMISSION_TABLE, $this->hydrator, $this->rolePermissionPrototype);
 
        $this->roleRepository = new BaseRepository($this->dbAdapter, TableConstant::US_ROLE_TABLE, $this->hydrator, $this->rolePrototype);
        $this->preBuildResources = PreBuildResources::ALL_CONTROLLERS;
    }
    
    public function initAcl($user_role_id = 0)
    {
        $this->user_role_id = $user_role_id;

        $this->roles = $this->getAllRoles();
        // var_export($this->roles);die();
        // echo json_encode($this->roles->toArray());exit;
        $this->resources = $this->getAllResources();
        $this->rolePermissions = $this->getRolePermissions();
        
        // we are not putting these resource & permission in table bcz it is common to all user
        $this->commonPermission = [
            /*'User\Controller\LoginController' => [
                'logout',
                'login',
                'access-deny-page'                
            ],*/
        ];
        
        $this->addRoles()
             ->addResources()
             ->addUserPrivilegeResources();
    }

    public function isAccessAllowed($role, $resource, $permission)
    {
        if (! $this->hasResource($resource)) 
        {
            return false;
        }
        // var_dump($resource);exit();
        if ( $this->isAllowed($role, $resource, $permission)) 
        {
            return true;
        }

        return false;
    }

    protected function addRoles()
    {
        //The method is FILO
        $this->addRole(new AclRole(self::DEFAULT_ROLE));
        
        // if (! empty($this->roles))
        // {
        //     foreach ($this->roles as $userRole) 
        //     {
        //         $roleName = $userRole->Id;

        //         if (! $this->hasRole($roleName)) 
        //         {
                    $this->addRole(new AclRole($this->user_role_id), self::DEFAULT_ROLE);
            //     }
            // }
        // }

        return $this;
    }

    protected function addResources()
    {
        if (! empty($this->resources)) 
        {
            foreach ($this->resources as $resource) 
            {
                $resourceName = $resource->Name;
                if ( !$this->hasResource($resourceName)) 
                {
                    $this->addResource(new Resource($resourceName));
                }
            }
        }
        
        // add prebuild all resources
        if (! empty($this->preBuildResources) ) 
        {
            foreach ( $this->preBuildResources as $resource ) 
            {
                if ( !$this->hasResource($resource) ) 
                {
                    $this->addResource(new Resource($resource));
                }
            }
        }
        
        // add common resources
        if (! empty($this->commonPermission)) 
        {
            foreach ($this->commonPermission as $resource => $permissions) 
            {
                if (! $this->hasResource($resource)) 
                {
                    $this->addResource(new Resource($resource));
                }
            }
        }
        
        return $this;
    }

    protected function addUserPrivilegeResources()
    {
        // allow common resource/permission to guest user
        if (! empty($this->commonPermission)) 
        {
            foreach ($this->commonPermission as $resource => $permissions) 
            {
                foreach ($permissions as $permission) 
                {
                    $this->allow(self::DEFAULT_ROLE, $resource, $permission);
                }
            }
        }
        
        if (! empty($this->preBuildResources)) 
        {
            foreach ($this->preBuildResources as $resource) 
            {
                $this->allow(self::DEFAULT_ROLE, $resource, null); 
            }
        }
        
        //$this->deny(self::DEFAULT_ROLE, "User\Controller\RoleController", "list"); 
    
       if (! empty($this->rolePermissions)) 
        {
            foreach ($this->rolePermissions as $key => $value ) 
            {
                //$this->allow($rolePermissions->RoleId, $rolePermissions['resource_name'], $rolePermissions['permission_name']);
                if( $value->IsAllow )
                {
                    $this->allow($value->RoleId, $value->ResourceName, $value->ActionName);
                }
                else
                {
                    $this->deny($value->RoleId, $value->ResourceName, $value->ActionName);
                }
            }
        }
        
        return $this;
    }

    protected function getAllRoles()
    {
        return $this->roleRepository ->getAllRecords(["Id" => $this->user_role_id]);
    }

    protected function getAllResources()
    {
        
    }

    protected function getRolePermissions()
    {
       $record = $this->rolePermissionRepository->getAllPermissionRecord(" AND RoleId = ".$this->user_role_id);

       return $record;
    }
    
    private function debugAcl($role, $resource, $permission)
    {
        echo 'Role:-' . $role . '==>' . $resource . '\\' . $permission . '<br/>';
    }
    
}
