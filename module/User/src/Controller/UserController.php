<?php
namespace User\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use User\Model\RoleRepository;
use PCSPLib\PasswordHelper;
use User\Model AS User;
use SystemSetting\Model as SystemSetting;
use User\Model\UserBranch;

class UserController extends BaseController
{
    protected $idenMenu = "inventory";
    private $passwordHelper;
    private $userRepository;
    private $roleRepository;
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $this->passwordHelper = new PasswordHelper();
        $this->userRepository = $serviceLocator->get(User\UserRepository::class);
        $this->roleRepository = $serviceLocator->get(User\RoleRepository::class);
        
        parent::__construct($serviceLocator, $this->userRepository, 'LastName, FirstName', 'ASC');
    }
    
    public function isUserNameExistAction()
    {
        $formData = $this->request->getPost();
        $condiction = "UserName = '".trim($formData['UserName'])."' AND Id != ".trim($formData['Id']);
        $isExist = $this->userRepository->isExist($condiction);

        if(isset($formData['isAjax']))
        {
            $data = ['success'=>$isExist];
            $result = new JsonModel($data);
            
            return $result;
        }

        return $isExist;
    }

    public function listAction()
    {
        $view = parent::listAction();
        
        $roleRecords = $this->roleRepository->getAllActiveRecords()->toArray();
        
        $view->setVariables(
                [
                    "roleRecords"    =>  $roleRecords, 

                ]
            );    
        
        return $view;
    }
    
    public function newAction()
    {
        $view = parent::newAction();

        $roleRecords = $this->roleRepository->getAllActiveRecords()->toArray();
        
        $view->setVariables(
                [
                    "roleRecords"    =>  $roleRecords, 

                ]
            );    
        
        return $view;
    }
    
    public function saveRecordAction()
    {
        $id = isset($_POST['Id']) ? intval($_POST["Id"]) : 0;

        $formData = $this->request->getPost();
        $data=[];
        $newFileName='';
        $newThumnailFileName='';
        
        try
        {
            if(!$this->isUserNameExistAction())
            {
                $folder = $this->getBasePath().PHPConstant::RESOURCE_FILES_PATH.PHPConstant::USER_PHOTO_FOLDER;
                if(!empty($_FILES["Image"]["name"]))
                {
                    $adapter = new \Zend\File\Transfer\Adapter\Http();
                    $adapter->setValidators([
                        new \Zend\Validator\File\Extension([
                            'extension' => explode(",",PHPConstant::IMAGE_EXTENSION)
                        ]),  
                    ]);
                    if (!$adapter->isValid()) 
                    {
                        $data = ['msg'=>$this->translator->translate('INCORRECT_PHOTO_FILE')];
                        return new JsonModel($data);
                    }

                    $adapter->setDestination($folder);
                    $dt = new \DateTime();
                    foreach ($adapter->getFileInfo() as $file => $info)
                    {
                        $originalFileName = $info['name'];
                        $extension = pathinfo($info['name'], PATHINFO_EXTENSION);

                        $newFileName = PHPConstant::USER_PHOTO_FILE_PREFIX.'_'.$dt->format('Ymd_His').'.'.$extension;
                        // $newThumnailFileName = PHPConstant::SMALL_IMAGE_PREFIX.$newFileName;

                        $upload = $adapter->addFilter('Rename', ['target' => $adapter->getDestination().'/'.$newFileName,'overwrite' => true]);
                        $upload->receive();

                        // $this->phpClass->createThumbnailWithFixSize($folder.$newFileName, $folder.$newThumnailFileName, PHPConstant::THUMNAIL_WIDTH, PHPConstant::THUMNAIL_HEIGHT);
                    }
                }
            
                $record = $this->tableObject;
                $record->exchangeArray($formData);
                $record->Salt = PHPConstant::STATIC_SALT;
                $arrayTableField = $record->removeNoneTableField((array)$record);
            
                if(!empty($formData['Password']) && !empty($formData['ConfirmPassword']) && ($formData['Password']==$formData['ConfirmPassword'])){
                    $arrayTableField["Password"] = $this->passwordHelper->createPassword($record->Password);
                }
                if($id && empty($formData['Password']) && empty($formData['ConfirmPassword']))
                {
                    unset($arrayTableField["Password"]);
                }
                if($formData['Password']!=$formData['ConfirmPassword'])
                {
                    unset($arrayTableField["Password"]);
                }
            
                if(!empty($newFileName))
                    $arrayTableField['Image'] = $newFileName;
                elseif(isset($arrayTableField['Image']))
                    unset($arrayTableField['Image']);

                $id = $this->userRepository->saveRecord($id, $arrayTableField);
                
                $data = ['Id'=>$id];
            }
            else
            {
                $data = ['msg'=>$this->translator->translate('USERNAME_IS_ALREADY_EXIST')];
            }
            
            $result = new JsonModel($data);

            return $result;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
    
    public function viewProfileAction()
    {
        $view = parent::listAction();

        $record  = $this->objectRepository->getRecord($this->userLoginId);

        $view->setVariables(
                        [
                            "record"    =>  $record,
                            
                        ]
                    );

        return $view;
    }
    
    public function saveChangeProfileAction()
    {
        $id = $this->userLoginId;
        $formData = $this->request->getPost();

        if(!empty($formData["DOB"]))
            $formData["DOB"] = date("Y-m-d",strtotime($formData["DOB"]));
        else $formData["DOB"] = null;

        $item = ["LastName"=>$formData["LastName"],"FirstName"=>$formData["FirstName"],"SexId"=>$formData["SexId"],"Tel"=>$formData["Tel"],"Email"=>$formData["Email"],"DOB"=>$formData["DOB"]];

        $id = $this->objectRepository->updateRecords($item,(int)$id);

        return new JsonModel();
    }

    public function saveChangePasswordAction()
    {
        $currentPassword = isset($_POST["CurrentPassword"])?$_POST["CurrentPassword"]:"";
        $newPassword     = isset($_POST["NewPassword"])?$_POST["NewPassword"]:"";
        $userId = $this->userLoginId;

        $currentPassword = $this->passwordHelper->createPassword($currentPassword);

        $bool = $this->objectRepository->isExist(["Id"=>$userId,"Password"=>$currentPassword]);

        $data = [];

        if($bool)
        {
            // password match
            $data = ['msg'=>$this->translator->translate('PASSWORD_HAS_BEEN_SAVE_SUCCESSFULLY')];

            $newPassword = $this->passwordHelper->createPassword($newPassword);
            
            $this->objectRepository->updateRecords(["Password"=>$newPassword], $userId);
        }
        else
        {
            // password not match
            $data = ['msg'=>$this->translator->translate('PASSWORD_NOT_MATCH,PLEASE_CHECK_AGAIN')];
        }
        return new JsonModel($data);
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] )?$formData["Record"]:[];
        
        $tableName = $this->tableName;

        $filterString = " 1 = 1 ";
        
        if( isset( $formData["RoleId"] ) && !empty( $formData["RoleId"] ) )
        {
            $filterString .= " AND {$tableName}.RoleId IN({$formData["RoleId"]})";
        }
        if( isset( $formData["FromDate"] ) && !empty( $formData["FromDate"] ) )
        {
            $fromDate = date("Y-m-d",strtotime( $formData["FromDate"] ) );

            $filterString .= " AND {$tableName}.AddDate >= '{$fromDate}'";
        }
        if( isset( $formData["ToDate"] ) && !empty( $formData["ToDate"] ) )
        {
            $toDate = date("Y-m-d", strtotime( $fromDate["ToDate"] ) );

            $filterString .= " AND {$tableName}.AddDate <= '{$toDate}'";
        }
        if( isset( $formData["DOBFromDate"] ) && !empty( $formData["DOBFromDate"] ) )
        {
            $fromDate = date("Y-m-d",strtotime( $formData["DOBFromDate"] ) );

            $filterString .= " AND {$tableName}.DOB >= '{$fromDate}'";
        }
        if( isset( $formData["DOBToDate"] ) && !empty( $formData["DOBToDate"] ) )
        {
            $toDate = date("Y-m-d", strtotime( $fromDate["DOBToDate"] ) );

            $filterString .= " AND {$tableName}.DOB <= '{$toDate}'";
        }
        if( isset( $formData["Tel"] ) && !empty( $formData["Tel"] ) )
        {
            $filterString .= " AND {$tableName}.Tel LIKE '%{$formData['Tel']}%'";
        }
        if( isset( $formData["Email"] ) && !empty( $formData["Email"] ) )
        {
            $filterString .= " AND {$tableName}.Email LIKE '%{$formData['Email']}%'";
        }
        if( isset( $formData["Address"] ) && !empty( $formData["Address"] ) )
        {
            $filterString .= " AND {$tableName}.Address LIKE '%{$formData['Address']}%'";
        }
        if( isset( $formData["SexId"] ) && (int)$formData["SexId"]  )
        {
            $sexId = (int)$formData["SexId"];

            $filterString .= " AND {$tableName}.SexId ={$sexId}";
        }
        if( count( $record ) )
        {
            $condition = "";
            foreach( $record as $key => $value )
            {
                $s = "";
                if( !empty( $value["LastName"] ) )
                {
                    $s = $tableName.".LastName LIKE '%".trim($value["LastName"],"")."%' ";
                }
                if( !empty( $value["FirstName"] ) )
                {
                    if ( !empty( $s ) ) $s .= " AND ";
                    $s .= $tableName.".FirstName LIKE '%".trim($value["FirstName"])."%' ";
                }
                if( !empty( $value["UserName"] ) )
                {
                    if ( !empty( $s ) ) $s .= " AND ";
                    $s .= $tableName.".UserName LIKE '%".trim($value["UserName"])."%' ";
                }

                if( !empty( $condition ) && !empty( $s ) )$condition .= " OR ";

                if( !empty( $s ) )
                    $condition .= "(".$s.")";
            }
            if( !empty( $condition ) )
            {
                $filterString .= " AND ({$condition}) ";
            }
        }

        return $filterString;
    }
}