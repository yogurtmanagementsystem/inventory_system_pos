<?php
namespace User\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\Helper;
use PCSPLib\PHPClass;
use User\Model AS User;
use PCSPLib\TableConstant;

class RoleController extends BaseController
{
	private $userRepository;
	private $roleRepository;
	protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
		$this->userRepository = $serviceLocator->get(User\UserRepository::class);
		$this->roleRepository = $serviceLocator->get(User\RoleRepository::class);
		
        parent::__construct($serviceLocator, $this->roleRepository, 'Name', 'ASC');
    }
	
	public function isItemExist()
	{
		$formData = $this->request->getPost();
		$condition = "Id <> ".$formData["Id"]." AND Name = '".$formData['Name']."'";
		return $this->roleRepository->isExist($condition);
	}
	
	public function isItemInUsed($id)
	{
		$formData = $this->request->getPost();
		$condition = "RoleId = ".$id;
		$relationTable = TableConstant::USER_TABLE;
		return $this->roleRepository->isInUsed($condition, $relationTable);
	}
	
	public function newAction()
	{
		$view = parent::newAction();
		$id = isset($_POST["Id"])?(int)$_POST["Id"]:0;

		$resourceRecord = $this->getServiceLocator()->get(User\ResourceRepository::class)->getAllActiveRecords([],[],"OrderNum");

		$rolePermissionRecord = $this->getServiceLocator()->get(User\RolePermissionRepository::class)->getAllRecords(["IsAllow"=>1,"RoleId"=>$id]);
		
		$resourceRecord = $this->getMergeRecord($resourceRecord,$rolePermissionRecord);

		$view->setVariables(
						[
							"resourceRecord"	=>	$resourceRecord,

						]
					);

		return $view;
	}

	public function getMergeRecord($record, $rolePermissionRecord )
	{
		$item = [];
		$tmpRecord = [];

		foreach( $rolePermissionRecord as $key => $value )
		{
			$tmpRecord[$value->ResourceId] = 1;
		}

		foreach( $record as $key => $value )
		{
			$arr = $value;
			$arr["Checked"] = isset($tmpRecord[$value->Id])?" checked='checked' ":"";

			$item[$value->GroupResourceId]["GroupResourceName"] = $value->GroupResourceName;
			$item[$value->GroupResourceId]["Child"][$value->ResourceControllerId]["ResourceName"] = $value->ResourceName;
			$item[$value->GroupResourceId]["Child"][$value->ResourceControllerId]["Child"][] = $arr;
		}

		return $item;
	}

	public function saveRecordAction()
	{
		$id = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
		$formData 	= $this->request->getPost();
		$data = [];

        if(!$this->isItemExist())
        {
        	$record = $this->objectRepository->getTableObject();
        	$record->exchangeArray($formData);
        	$arrayTableField = $record->removeNoneTableField((array)$record);

            $id = $this->objectRepository->saveRecord($id, $arrayTableField);
            $data = ['Id'=>$id];
        }
        else
        {
            $data = ['msg'=>$this->translator->translate('ITEM_EXIST')];
        }

		if(isset($data["Id"]))
		{
			$resourceRecord = isset($formData["ResourceRecord"])?$formData["ResourceRecord"]:[];
			$record 	= $this->getServiceLocator()->get(User\ResourceRepository::class)->getAllActiveRecords();

			$tmpRecord  = [];
			foreach( $record as $key => $value )
			{
				$tmpRecord[$value->Id] = $value;
			}

			$rolePermissionRepository = $this->getServiceLocator()->get(User\RolePermissionRepository::class);

			$roleId = $data["Id"];

			$rolePermissionRepository->deleteRecord(["RoleId"=>$roleId], 0);

			foreach( $resourceRecord as $key => $resourceId )
			{
				$item = ["ResourceId"	=>	$resourceId,"RoleId"	=>	$roleId,"IsAllow"	=>	1];

				$rpId = $rolePermissionRepository->saveRecord(0, $item);

				if(isset($tmpRecord[$resourceId]))
					unset($tmpRecord[$resourceId]);
			}
			foreach( $tmpRecord as $resourceId => $value )
			{
				$item = ["ResourceId"	=>	$resourceId,"RoleId"	=>	$roleId,"IsAllow"	=>	0];

				$rpId = $rolePermissionRepository->saveRecord(0, $item);
			}
		}
		
		return new JsonModel($data);
	}
}