<?php
namespace User\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\Helper;
use PCSPLib\PHPClass;
use User\Model AS User;
use PCSPLib\TableConstant;

class ResourceActionController extends BaseController
{
	protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
		$table = $serviceLocator->get(User\ResourceActionRepository::class);
		
        parent::__construct($serviceLocator, $table);
    }
}