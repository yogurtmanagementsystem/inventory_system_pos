<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\ServiceManager\ServiceLocatorInterface;
use Interop\Container\ContainerInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter; 
use Zend\Authentication\Result;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;
use User\Model AS User;

class LoginController extends AbstractActionController
{
	private $serviceLocator;
	private $dbAdapter;
	private $config;
	private $translator;
	private $userPermissionRecord;
	
	public function __construct(ContainerInterface $serviceLocator)
    {
        $this->serviceLocator 	= $serviceLocator;
		$this->dbAdapter 		= $serviceLocator->get('dbAdapter');
        $this->config 			= $serviceLocator->get('Config');
		$this->translator 		= $serviceLocator->get('Zend\Mvc\I18n\Translator');

		$this->userPermissionRecord = new Container('UserPermissionRecord');
    }

	public function loginAction()
	{
		$viewModel = new ViewModel();
		$viewModel->setTerminal(true);
		$formData = $this->request->getPost();
		
		//process login
		if(!empty($formData['userName']) && !empty($formData['password']))
		{		
			$staticSalt = PHPConstant::STATIC_SALT;
			$dbTableAuthAdapter = new DbTableAuthAdapter($this->dbAdapter, TableConstant::US_USER_TABLE, 'UserName', 'Password', "SHA1(CONCAT('$staticSalt',?)) AND IsActive=1");
			$authService = new AuthenticationService();
			$authService->setAdapter($dbTableAuthAdapter);
			
			$authService->getAdapter()
									->setIdentity($formData['userName'])
									->setCredential($formData['password']);
			
			$result = $authService->authenticate();
			
			switch ($result->getCode())
			{
				case Result::FAILURE_IDENTITY_NOT_FOUND:
					//in case not existend user login name
					$viewModel->setVariables(['getMsg'=>'You have input invalid user name. Please try again.','userName'=>$formData['userName'],'password'=>$formData['password']]);
					break;
				case Result::FAILURE_CREDENTIAL_INVALID:
					//in case user password not match
					$viewModel->setVariables(['getMsg'=>'You have input invalid user password. Please try again.','userName'=>$formData['userName'],'password'=>$formData['password']]);
					break;
				case Result::SUCCESS:

					$storage = $authService->getStorage();
					
					$ses = $dbTableAuthAdapter->getResultRowObject(['Id', 'FirstName', 'LastName', 'RoleId', 'AddDate', 'Image']);
					
					$storage->write($ses);

					$roleId = $dbTableAuthAdapter->getResultRowObject(array('RoleId'))->RoleId;
					$permissionRecord = $this->getPermission($roleId);

					$this->userPermissionRecord->Record = $permissionRecord;
					
					return $this->redirect()->toRoute('inventory');
					
					break;
			}			
		}
		return $viewModel;
	}

	public function getPermission($roleId)
	{
		$item = array();
		$resource = $this->serviceLocator->get(User\RolePermissionRepository::class)->getAllPermissionRecord(" AND IsAllow = 1 AND RoleId =".$roleId)->toArray();
		
		foreach( $resource as $key => $value )
		{
			$item[$value["PrefixResource"]][$value["ActionName"]] = (int)$value["IsAllow"];
		}
		
		return $item;
	}
	
	public function logoutAction()
	{
		$auth = new AuthenticationService();
		$session = new Container();
		$auth->clearIdentity();
        $session->getManager()->destroy();
		
		return $this->redirect()->toRoute('login');
		
	}
	
	public function accessDenyPageAction(){
		$viewModel = new ViewModel();
		$viewModel->setTerminal(true);
		
		return $viewModel;
	}
    
}