<?php
namespace User\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\Helper;
use PCSPLib\PHPClass;
use User\Model AS User;
use PCSPLib\TableConstant;

class ResourceControllersController extends BaseController
{
	protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
		$table = $serviceLocator->get(User\ResourceControllersRepository::class);
		
        parent::__construct($serviceLocator, $table);
    }

    public function newAction()
    {
    	$view = parent::newAction();

    	$groupResourceRecord = $this->getService(User\GroupResourceRepository::class)->getAllActiveRecords()->toArray();

    	$view->setVariables(
    			[
    				"groupResourceRecord" => $groupResourceRecord,

    			]
    		);

    	return $view;
    }
}