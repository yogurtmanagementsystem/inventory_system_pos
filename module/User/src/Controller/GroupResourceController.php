<?php
namespace User\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;
use User\Model AS User;

class GroupResourceController extends BaseController
{
	protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
		$table = $serviceLocator->get(User\GroupResourceRepository::class);
		
        parent::__construct($serviceLocator, $table,"OrderNum");
    }

    public function saveRecordAction()
    {
        $id   = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $formData = $this->request->getPost();
        $data = [];
        
        try
        {
            $record = $this->objectRepository->getTableObject();
            $record->exchangeArray($formData);
            $arrayTableField = $record->removeNoneTableField((array)$record);

            if(!$this->isItemExist())
            {
            	$editId = $id;

                $id = $this->objectRepository->saveRecord($id, $arrayTableField);
                $data = ['Id'=>$id];

                if( $editId )
                {
                    $resourceControllerRepository = $this->getService(User\ResourceControllersRepository::class);
                    $resourceControllerRepository->updateRecords(["GroupResourceName" => trim($formData["Name"],""),"GroupResourceOrderNum" => $formData["OrderNum"] ], ["GroupResourceId" => $editId ] );
                }
            }
            else
            {
                $data = ['msg'=>$this->translator->translate('ITEM_EXIST')];
            }

            $result   = new JsonModel($data);
            return $result;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}