<?php
namespace User\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\Helper;
use PCSPLib\PHPClass;
use User\Model AS User;
use PCSPLib\TableConstant;

class ResourceController extends BaseController
{
	protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
		$table = $serviceLocator->get(User\ResourceRepository::class);
		
        parent::__construct($serviceLocator, $table,"OrderNum");
    }

    public function newAction()
    {
    	$view = parent::newAction();

    	$resourceControllerRecord = $this->getServiceLocator()->get(User\ResourceControllersRepository::class)->getAllActiveRecords([],[],"OrderNum")->toArray();
    	$resourceActionRecord = $this->getServiceLocator()->get(User\ResourceActionRepository::class)->getAllActiveRecords([],[],"OrderNum")->toArray();

    	$view->setVariables(
    					[
    						"resourceActionRecord"		=>	$resourceActionRecord,
    						"resourceControllerRecord"	=>	$resourceControllerRecord,

    					]
    				);

    	return $view;
    }
}