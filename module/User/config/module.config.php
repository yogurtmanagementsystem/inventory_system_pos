<?php
/**
 * @author Phin Heng
 * @since  Oct 21 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace User;

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\BaseClasses\BaseObject;
use PCSPLib\TableConstant;
use Zend\Db\Adapter\AdapterInterface;
use Interop\Container\ContainerInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;

return 
[
	'service_manager' => 
    [
        'aliases' => 
        [
			
        ],
        'factories' => 
        [
            Model\RoleRepository::class => function(ContainerInterface $container)
            {
                return new Model\RoleRepository($container->get(AdapterInterface::class), TableConstant::US_ROLE_TABLE,new ReflectionHydrator(), new Model\Role());
            },
            Model\UserRepository::class => function(ContainerInterface $container)
            {
                return new Model\UserRepository($container->get(AdapterInterface::class), TableConstant::US_USER_TABLE,new ReflectionHydrator(), new Model\User());
            },
            Model\ResourceActionRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::US_RESOURCE_ACTION_TABLE,new ReflectionHydrator(), new Model\ResourceAction());
            },
            Model\ResourceRepository::class => function(ContainerInterface $container)
            {
                return new Model\ResourceRepository($container->get(AdapterInterface::class), TableConstant::US_RESOURCE_TABLE,new ReflectionHydrator(), new Model\Resource());
            },
            Model\RolePermissionRepository::class => function(ContainerInterface $container)
            {
                return new Model\RolePermissionRepository($container->get(AdapterInterface::class), TableConstant::US_ROLE_PERMISSION_TABLE,new ReflectionHydrator(), new Model\RolePermission());
            },
            Model\UserPermissionRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::US_USER_PERMISSION_TABLE,new ReflectionHydrator(), new Model\UserPermission());
            },
            Model\GroupResourceRepository::class => function(ContainerInterface $container)
            {
                return new Model\GroupResourceRepository($container->get(AdapterInterface::class), TableConstant::US_GROUP_RESOURCE_TABLE,new ReflectionHydrator(), new BaseObject());
            },
            Model\ResourceControllersRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::US_RESOURCE_CONTROLLER_TABLE,new ReflectionHydrator(), new Model\ResourceControllers());
            },
        ],
    ],
	'controllers' => 
    [
        'factories' => 
        [
            Controller\LoginController::class => function(ContainerInterface $container)
            {
                return new Controller\LoginController($container);
            },
            Controller\UserController::class => function(ContainerInterface $container)
            {
                return new Controller\UserController($container);
            },
            Controller\RoleController::class => function(ContainerInterface $container)
            {
                return new Controller\RoleController($container);
            },
            Controller\GroupResourceController::class => function(ContainerInterface $container)
            {
                return new Controller\GroupResourceController($container);
            },
            Controller\ResourceActionController::class => function(ContainerInterface $container)
            {
                return new Controller\ResourceActionController($container);
            },
            Controller\ResourceControllersController::class => function(ContainerInterface $container)
            {
                return new Controller\ResourceControllersController($container);
            },
            Controller\ResourceController::class => function(ContainerInterface $container)
            {
                return new Controller\ResourceController($container);
            },
        ],
    ],
    'router' => 
    [
        'routes' => 
        [
            'user' => 
            [
                'type' => Literal::class,
                'options' => 
                [
                    'route' => '/user',
                    'defaults' => 
                    [
                        'controller' => Controller\UserController::class,
                        'action'     => 'list',
                    ],
                ],
				// The following allows "/user" to match on its own if no child
				// routes match:
				'may_terminate' => true,
				'child_routes'  => 
                [
					'user' => 
                    [
						'type' => Segment::class,
						'options' => 
                        [
						    'route'    => '/user[/:action[/:id]]',
							'defaults' => 
                            [
								'controller' => Controller\UserController::class,
								'action'     => 'list',
							],
							'constraints' => 
                            [
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id' => '[0-9]+',
							],
						],
					],
					'role' => 
                    [
						'type' => Segment::class,
						'options' => 
                        [
						    'route'    => '/role[/:action[/:id]]',
							'defaults' => 
                            [
								'controller' => Controller\RoleController::class,
								'action'     => 'list',
							],
							'constraints' => 
                            [
								'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
								'id' => '[0-9]+',
							],
						],
					],
                    'group-resource' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/group-resource[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\GroupResourceController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'resource-action' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/resource-action[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\ResourceActionController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'resource-controllers' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/resource-controllers[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\ResourceControllersController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'resource' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/resource[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\ResourceController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                ],
            ],
			'login' => 
            [
                'type'    => Segment::class,
                'options' => 
                [
                    'route' => '/login[/]',
                    'defaults' => 
                    [
                        'controller' => Controller\LoginController::class,
                        'action'     => 'login',						
                    ],
                ],
            ],
			'auth-deny' => 
            [
        		'type'    => Segment::class,
        		'options' => 
                [
        			'route'    => '/auth-deny[/]',
        			'defaults' => 
                    [
        				'controller' => Controller\LoginController::class,
        				'action'     => 'accessDenyPage',
        			],
        		],
        	],
			'logout' => 
            [
        		'type'    => Segment::class,
        		'options' => 
                [
        			'route'    => '/logout[/]',
        			'defaults' => 
                    [
        				'controller' => Controller\LoginController::class,
        				'action'     => 'logout',
        			],
        		],
        	],			
        ],
    ],
    'view_manager' => 
    [
        'template_path_stack' => 
        [
            'user' => __DIR__ . '/../view',
        ],
    ],
    'translator' =>
    [
        'locale' => 'en_US',
        'translation_file_patterns' =>
        [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo'
            ]
        ]
    ],
];