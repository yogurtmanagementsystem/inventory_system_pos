<?php
/**
 * @author Phin Heng
 * @since  09 Jan 2017
 * @copyright PCSP GROUP COL.TD
**/
namespace Contact;

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\BaseClasses\BaseObject;
use PCSPLib\TableConstant;
use Zend\Db\Adapter\AdapterInterface;
use Interop\Container\ContainerInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;

return [
    
    'service_manager' => 
    [
        'aliases' => 
        [
            
        ],
        'factories' => 
        [
            Model\ContactRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::CON_CONTACT_TABLE,new ReflectionHydrator(), new Model\Contact());
            },
        ],
    ],
    'controllers' => 
    [
        'factories' => 
        [
            Controller\ContactController::class => function(ContainerInterface $container)
            {
                return new Controller\ContactController($container);
            },
        ],      
    ],
    'router' => 
    [
        'routes' => 
        [
            'contact' => 
            [
                'type' => Literal::class,
                'options' => 
                [
                    'route' => '/contact',
                    'defaults' => 
                    [
                        'controller' => Controller\ContactController::class,
                        'action'     => 'screen',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => 
                [
                    'contact' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/contact[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\ContactController::class,
                                'action'     => 'screen',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],           
                ],
            ],
        ],
    ],
    'view_manager' => 
    [
        'template_path_stack' => 
        [
            'contact' => __DIR__ . '/../view',
        ],
    ],
    'translator' =>
    [
        'locale' => 'en_US',
        'translation_file_patterns' =>
            [
                [
                    'type' => 'gettext',
                    'base_dir' => __DIR__ . '/../language',
                    'pattern' => '%s.mo'
                ]
            ]
    ],
];
