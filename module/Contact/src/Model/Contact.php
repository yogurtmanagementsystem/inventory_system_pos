<?php
/**
 * @author Phin Heng
 * @since  10 Jan 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Contact\Model;

use PCSPLib\BaseClasses\BaseObject;

class Contact extends BaseObject
{
	public $Tel;
	public $Address;
	public $Email;
    public $Website;
    public $Logo;
    public $WiFiName;
	public $WiFiPassword;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->Tel 	   = isset($data['Tel']) ? trim($data['Tel']) : "";
        $this->Address = isset($data['Address']) ? trim($data['Address']) : "";
        $this->Email   = isset($data['Email']) ? trim($data['Email']) : "";
        $this->Website = isset($data['Website']) ? trim($data['Website']) : "";
        $this->Logo    = isset($data['Logo']) ? trim($data['Logo']) : "";
        $this->WiFiName= isset($data['WiFiName']) ? trim($data['WiFiName']) : "";
        $this->WiFiPassword  = isset($data['WiFiPassword']) ? trim($data['WiFiPassword']) : "";
    }
}