<?php
/**
 * @author Phin Heng
 * @since  06 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Contact\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Db\Sql\Expression;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;

use Contact\Model AS Contact;

class ContactController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(Contact\ContactRepository::class);

        parent::__construct($serviceLocator, $table);
    }

    public function listAction()
    {
    	$view = parent::listAction();

    	$record = $this->objectRepository->getRecord(1);

    	$view->setVariables(
    			[
    				"record" => $record,

    			]
    		);

    	return $view;
    }

    public function saveRecordAction()
    {
        $formData = $this->request->getPost();
        
        $newFileName = '';
        $newThumnailFileName='';

		$folder = $this->getBasePath().PHPConstant::RESOURCE_FILES_PATH;
		// $folder = $this->getBasePath().PHPConstant::RESOURCE_FILES_PATH;

		if(!empty($_FILES["Logo"]["name"]))
		{
			$adapter = new \Zend\File\Transfer\Adapter\Http();
			$adapter->setValidators
			([
                new \Zend\Validator\File\Extension
                ([
					'extension' => explode(",",PHPConstant::IMAGE_EXTENSION)
                ]),  
			]);

			if ( !$adapter->isValid() ) 
			{
				$data = ['msg'=>$this->translator->translate('INCORRECT_PHOTO_FILE')];

				return new JsonModel($data);
			}

			$adapter->setDestination($folder);

			$dt = new \DateTime();

			foreach ( $adapter->getFileInfo() as $file => $info )
			{
				$originalFileName = $info['name'];
				$extension = pathinfo($info['name'], PATHINFO_EXTENSION);

				$newFileName = $dt->format('Ymd_His').'.'.$extension;

				$upload = $adapter->addFilter('Rename', ['target' => $adapter->getDestination().'/'.$newFileName,'overwrite' => true]);
				$upload->receive();
			}
		}
			
		$object = $this->tableObject;
		$object->exchangeArray($formData);

		$arrayTableField = $object->removeNoneTableField((array)$object);
	
		if( !empty( $newFileName ) )
		{
			$arrayTableField['Logo'] = $newFileName;
		}
		// elseif( isset($arrayTableField['Logo'] ) )
		// {
		// 	unset($arrayTableField['Logo']);
		// }

		$id = $this->objectRepository->saveRecord(1, $arrayTableField);
		
		$result = new JsonModel();

		return $result;
	}
}