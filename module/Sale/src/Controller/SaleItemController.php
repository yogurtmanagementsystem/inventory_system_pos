<?php
/**
 * @author Phin Heng
 * @since  06 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;
use Inventory\Model AS Inventory;
use Sale\Model AS Sale;

class SaleItemController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(Sale\SaleItemRepository::class);
        
        parent::__construct($serviceLocator, $table);
    }

    public function listAction()
    {
        $view = parent::listAction();

        $itemTypeRecord = $this->getService(Sale\ItemTypeRepository::class)->getActiveRecordsForTreeDropDown()->toArray();
        $categoryRecord = $this->getService(Sale\ItemCategoryRepository::class)->getActiveRecordsForTreeDropDown()->toArray();
        
        $view->setVariables(
                [
                    "itemTypeRecord" => $itemTypeRecord,
                    "categoryRecord" => $categoryRecord,

                ]
            );

        return $view;
    }

    public function newAction()
    {
        $view = parent::newAction();

        $itemTypeRecord = $this->getService(Sale\ItemTypeRepository::class)->getActiveRecordsForTreeDropDown()->toArray();
        $categoryRecord = $this->getService(Sale\ItemCategoryRepository::class)->getActiveRecordsForTreeDropDown()->toArray();
        
        $view->setVariables(
                [
                    "itemTypeRecord" => $itemTypeRecord,
                    "categoryRecord" => $categoryRecord,

                ]
            );

        return $view;
    }

    public function saveRecordAction()
    {
        $id   = isset($_POST['Id']) ? intval($_POST['Id']) : 0;
        $formData = $this->request->getPost();
        $data = [];
        
        if(!$this->isItemExist())
        {
            $itemTypeIds  = isset($formData["ItemTypeIds"])?$formData["ItemTypeIds"]:"";
            $itemTypeName = isset($formData["TypeName"])?$formData["TypeName"]:"";
            
            $arr1 = explode(",", trim( $itemTypeIds ) );
            $arr2 = explode(",", trim( $itemTypeName ) );

            $object = $this->objectRepository->getTableObject();

            $item = $formData;

            $record = [];

            if( $id )
            {
                $groupRecord = $formData["GroupRecord"];

                $tmp = $this->objectRepository->getAllRecords(["GroupRecord" => $groupRecord]);

                foreach( $tmp as $key => $value )
                {
                    $record[$value->ItemTypeId] = $value;
                }
            }
            else
            {
                $item["GroupRecord"] = date("YmdHis");
            }

            for( $i = 0 ; $i < count( $arr1 ) ; $i ++ )
            {
                $itemTypeId = $arr1[$i];

                $item["ItemTypeId"] = $itemTypeId;
                $item["TypeName"]   = $arr2[$i];
                $id = 0;
                if( isset( $record[$itemTypeId] ) ) 
                {
                    $id = $record[$itemTypeId]->Id;
                    unset($record[$itemTypeId]);
                }
                $object->exchangeArray($item);
                $arrayTableField = $object->removeNoneTableField((array)$object);

                $id = $this->objectRepository->saveRecord($id, $arrayTableField);
            }

            foreach( $record as $key => $value )
            {
                $this->objectRepository->deleteRecord($value->Id,0);
            }
        }
        else
        {
            $data = ['msg'=>$this->translator->translate('ITEM_EXIST')];
        }

        $result   = new JsonModel($data);

        return $result;
    }

    public function uploadImageAction()
    {
        $id = isset($_POST["Id"])?(int)$_POST["Id"]:0;

        if(isset($_FILES["Image"]["name"]) && !empty($_FILES["Image"]["name"]))
        {
            $folder = PHPConstant::RESOURCE_FILES_PATH;

            $adapter = new \Zend\File\Transfer\Adapter\Http();
            $adapter->setValidators(
                                [
                                new \Zend\Validator\File\Extension(
                                        [
                                        'extension' => explode(",",PHPConstant::IMAGE_EXTENSION)
                                        ]   
                                    ),  
                                ]
                            );
            if (!$adapter->isValid()) 
            {
                $data = ['msg'=>$this->translator->translate('Incorrect image file')];
                return new JsonModel($data);
            }

            $adapter->setDestination($folder);
            $dt = new \DateTime();
            foreach ($adapter->getFileInfo() as $file => $info)
            {
                $originalFileName = $info['name'];
                $extension = pathinfo($info['name'], PATHINFO_EXTENSION);

                $newFileName = $dt->format('Ymd_His').'.'.$extension;
                
                $upload = $adapter->addFilter('Rename', ['target' => $adapter->getDestination().'/'.$newFileName,'overwrite' => true]);
                $upload->receive();
            }
        }

        $this->objectRepository->updateRecords( ["Image" => $newFileName],["Id" => $id] );

        return new JsonModel();
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        
        $tableName = $this->tableName;
        $saleItemTable= TableConstant::SAL_SALE_ITEM_TABLE;
        
        $filterString = " 1 = 1 ";

        if( isset( $formData["ItemCategoryId"] ) && !empty( $formData["ItemCategoryId"] ) )
        {
            $filterString .= " AND {$saleItemTable}.ItemCategoryId IN({$formData["ItemCategoryId"]})";
        }
        if( isset( $formData["ItemTypeId"] ) && !empty( $formData["ItemTypeId"] ) )
        {
            $filterString .= " AND {$saleItemTable}.ItemTypeId IN({$formData["ItemTypeId"]})";
        }
        if( isset( $formData["Name"] ) && !empty( $formData["Name"] ) )
        {
            $filterString .= " AND {$saleItemTable}.Name LIKE '%".trim($formData["Name"],"")."%' ";
        }

        return $filterString;
    }
}