<?php
/**
 * @author Phin Heng
 * @since  06 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Controller;

use Zend\Log\Writer\ZendMonitor;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use Sale\Model AS Sale;

class ItemCategoryController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(Sale\ItemCategoryRepository::class);

        parent::__construct($serviceLocator, $table,"OrderNum");
    }

    public function isItemExist()
    {
        $formData = $this->request->getPost();
        
        $condition = "Name = '".trim($formData['Name']," ")."' AND Id != ".trim($formData['Id']," ");
        
        return $this->objectRepository->isExist($condition);
    }

    public function newAction()
    {
        $view = parent::newAction();
        $gridIdName = isset($_POST["gridIdName"])?$_POST["gridIdName"]:0;

        $parentRecord = $this->objectRepository->getActiveRecordsForTreeDropDown()->toArray();
        
        $view->setVariables(
            [
                "gridIdName"    =>  $gridIdName,
                'parentRecord'  => $parentRecord,
                
            ]
        );

        return $view;
    }

    public function getFilterCondition()
    {
        $formData = $this->request->getPost();
        $record   = isset( $formData["Record"] ) ? $formData["Record"] : [];

        $tableName = $this->tableName;
        
        $filterString = "";

        if( count( $record ) )
        {
            foreach( $record as $key => $value )
            {
                if( !empty( $value["Name"] ) )
                {
                    if ( !empty( $filterString ) ) $filterString .= " OR ";

                    $filterString .= " Name LIKE '%".trim( $value["Name"] )."%' ";
                }
            }

            if( !empty( $filterString ) )
            {
                $filterString = "(".$filterString.")";
            }
        }

        return $filterString;
    }

    public function getRecordsForTreeGridAction()
    {
        $condition = $this->getFilterCondition();

        if( isset( $_POST["ParentOnly"] ) && $_POST["ParentOnly"] )
        {
            $condition .= " AND IsActive = 1 ";
        }

        $records = $this->objectRepository->getRecordsForTreeGrid($condition)->toArray();

        $result = new JsonModel($records);

        return $result;
    }

    public function getActiveRecordsForTreeDropDownAction()
    {
        $condition = " IsActive = 1 ";
        
        $records = $this->objectRepository->getRecordsForTreeGrid($condition)->toArray();

        $result = new JsonModel($records);

        return $result;
    }
}