<?php
/**
 * @author Phin Heng
 * @since  06 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Controller;

use Interop\Container\ContainerInterface;

use PCSPLib\BaseClasses\BaseController;
use Sale\Model AS Sale;

class SaleTypeController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(Sale\SaleTypeRepository::class);
        parent::__construct($serviceLocator, $table);
    }
}