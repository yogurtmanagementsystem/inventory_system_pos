<?php
/**
 * @author Phin Heng
 * @since  06 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Controller;

use Interop\Container\ContainerInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Db\Sql\Expression;

use PCSPLib\BaseClasses\BaseController;
use PCSPLib\PHPConstant;
use PCSPLib\TableConstant;

use Sale\Model AS Sale;
use Accountant\Model AS Accountant;
use Contact\Model AS Contact;
use User\Model AS User;

class SaleController extends BaseController
{
    protected $idenMenu = "inventory";
    
    public function __construct(ContainerInterface $serviceLocator)
    {   
        $table = $serviceLocator->get(Sale\SaleRepository::class);

        parent::__construct($serviceLocator, $table);
    }
    public function listAction()
    {
        $view = parent::listAction();

        $userRecord = $this->getService(User\UserRepository::class)->getAllActiveRecords()->toArray();

        $view->setVariables(
                [
                    "userRecord" => $userRecord
                ]
            );

        return $view;
    }
    public function listInvoiceAction()
    {
        $view = parent::newPopupAction();

        $invoiceDate = isset($_POST["InvoiceDate"])?$_POST["InvoiceDate"]:"";
        $userId = isset($_POST["UserId"])?$_POST["UserId"]:"";

        $userRecord = $this->getService(User\UserRepository::class)->getAllActiveRecords()->toArray();

        $view->setVariables(
                [
                    "invoiceDate" => $invoiceDate,
                    "userRecord"  => $userRecord,
                    "userId"  => $userId
                ]
            );

        return $view;
    }

    public function viewAction()
    {
        $view = parent::newPopupAction();

        $id = isset($_POST["Id"])?(int)$_POST["Id"]:0;

        $record = $this->objectRepository->getRecord($id);
        $detailRecord = $this->getService(Sale\SaleDetailRepository::class)->getAllRecords(["SaleId" => $id])->toArray();

        $view->setVariables(
                [
                    "record"       => $record,
                    "detailRecord" => $detailRecord,

                ]
            );

        return $view;
    }

    public function getMergeRecordForView($record)
    {
        $item = [];

        foreach( $record as $key => $value )
        {
            $saleId = $value["SaleId"];

            $item[$saleId] = $value;
            $item[$saleId]["Child"][] = $value;
        }

        return $item;
    }

    public function getFilterCondition()
    {
        $filtersCount = isset($_POST['filterscount']) ? intval($_POST['filterscount']) : 0;
        
        $tableName = $this->tableName;

        $fromDate = isset($_POST["FromDate"])?$_POST["FromDate"]:"";
        $toDate = isset($_POST["ToDate"])?$_POST["ToDate"]:"";
        $userId = isset($_POST["UserId"])?$_POST["UserId"]:"";

        $filterString = " 1 = 1 ";

        if( !empty( $fromDate ) )
        {
            $filterString .= " AND {$tableName}.InvoiceDate >= '". date("Y-m-d",strtotime($fromDate))."'";
        }
        if( !empty( $toDate ) )
        {
            $filterString .= " AND {$tableName}.InvoiceDate <='".date("Y-m-d",strtotime( $toDate ) )."'";
        }
        if( !empty( $userId ) )
        {
            $filterString .= " AND {$tableName}.AddUserId IN({$userId})";
        }

        return $filterString;
    }

    public function getFilterRecordsForGridAction()
    {
        $page = isset($_POST['pagenum']) ? intval($_POST['pagenum']) : 0;
        $rows = isset($_POST['pagesize'])? intval($_POST['pagesize']) : PHPConstant::DEFAULT_PAGE_SIZE;
        $offset = $page*$rows;
        $sort = (isset($_POST['sortdatafield']) & !empty($_POST['sortdatafield'])) ? strval($_POST['sortdatafield']) : $this->defaultSortGridField;
        $order = (isset($_POST['sortorder']) & !empty($_POST['sortorder'])) ? strval($_POST['sortorder']) : $this->defaultSortGridType;

        $invoiceDate = isset($_POST["InvoiceDate"])?$_POST["InvoiceDate"]:"";
        $invoiceNo   = isset($_POST["InvoiceNo"])?$_POST["InvoiceNo"]:"";
        $userId   = isset($_POST["UserId"])?$_POST["UserId"]:"";

        $filterString = " 1 = 1 ";
        if( !empty( $invoiceDate ) )
        {
            $filterString .= " AND InvoiceDate = '".date("Y-m-d",strtotime($invoiceDate))."'";
        }
        if( !empty( $invoiceNo ) )
        {
            $filterString .= " AND InvoiceNo LIKE '%".trim($invoiceNo,"")."%' ";
        }
        if( !empty( $userId ) )
        {
            $filterString .= " AND AddUserId IN ({$userId}) ";
        }

        $records  = $this->objectRepository->getFilterRecordsForGrid($offset, $rows, $sort, $order, $filterString)->toArray();
            
        $totalRow = $this->objectRepository->getRowCountFilterForGrid($filterString);

        $data[]   = ['total'=>$totalRow,'rows'=>$records];

        $result   = new JsonModel($data);
        
        return $result;
    }
    public function getTotalAction()
    {
        $condition = $this->getFilterCondition();

        $total = $this->objectRepository->getTotal($condition);

        return new JsonModel(["Total"=>$total]);
    }
}