<?php
/**
 * @author Phin Heng
 * @since  05 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale;

class Module
{
	public function getConfig()
	{
		return include __DIR__ . '/../config/module.config.php';
	}
}