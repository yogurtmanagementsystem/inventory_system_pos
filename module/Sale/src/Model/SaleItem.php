<?php
/**
 * @author Phin Heng
 * @since  Oct 20 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Model;


use PCSPLib\BaseClasses\BaseObject;

class SaleItem extends BaseObject
{
    public $ItemTypeId;
    public $ItemCategoryId;
    public $ItemCategoryIds;
    public $ItemTypeIds;
    public $CategoryName;
    public $GroupRecord;
    public $IsUsed;
    public $TypeName;
    public $Image;
    public $IsSaleWeight;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->IsSaleWeight = isset($data['IsSaleWeight'])? (!empty($data['IsSaleWeight'])?trim($data['IsSaleWeight']," "):0) : 0;
        
        $this->IsUsed = isset($data['IsUsed'])? (!empty($data['IsUsed'])?trim($data['IsUsed']," "):0) : 0;
        $this->ItemTypeId = isset($data['ItemTypeId'])? (!empty($data['ItemTypeId'])?trim($data['ItemTypeId']," "):0) : 0;
        $this->ItemCategoryId = isset($data['ItemCategoryId'])? (!empty($data['ItemCategoryId'])?trim($data['ItemCategoryId']," "):0) : 0;
        $this->ItemTypeIds = isset($data["ItemTypeIds"])?(!empty($data["ItemTypeIds"]))?trim($data["ItemTypeIds"],""):"":"";
        $this->CategoryName = isset($data["CategoryName"])?(!empty($data["CategoryName"]))?trim($data["CategoryName"],""):"":"";
        $this->ItemCategoryIds = isset($data["ItemCategoryIds"])?(!empty($data["ItemCategoryIds"]))?trim($data["ItemCategoryIds"],""):"":"";
        
        $this->GroupRecord = isset($data["GroupRecord"])?(!empty($data["GroupRecord"]))?trim($data["GroupRecord"],""):"":"";
        $this->TypeName = isset($data["TypeName"])?(!empty($data["TypeName"]))?trim($data["TypeName"],""):"":"";
        $this->Image = isset($data["Image"])?(!empty($data["Image"]))?trim($data["Image"],""):"":"";
    }
}