<?php
/**
 * @author Phin Heng
 * @since  06 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class SaleItemRepository extends BaseRepository
{
	public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        $itemTypeTable = TableConstant::SAL_ITEM_TYPE_TABLE;
        $salePriceTable = TableConstant::SAL_SALE_PRICE_TABLE;
        
        $select = $this->sql->select($this->tableName);
                    
        $select->join("{$itemTypeTable}","{$this->tableName}.ItemTypeId = {$itemTypeTable}.Id",["ItemTypeName" => "Name"],'left');
        $select->join("{$salePriceTable}","{$this->tableName}.Id = {$salePriceTable}.SaleItemId",["Price"],'left');
        
        if(!empty($filterString)) $select->where($filterString);
        $select->where("{$this->tableName}.IsDeleted = 0");
        $select->order($sort.' '.$order);
        $select->limit($rows)->offset($offset);
        
        $stmt   = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getAllRecordForPos($condition)
    {
        try
        {
            $salePriceTable = TableConstant::SAL_SALE_PRICE_TABLE;

            $select = $this->sql->select($this->tableName);

            $select->join("{$salePriceTable}","{$this->tableName}.Id = {$salePriceTable}.SaleItemId",["Price","SalePriceId" => "Id"],'left');

            $select->where(["{$this->tableName}.IsDeleted" => 0,"{$this->tableName}.IsActive" => 1]);
             $select->where("{$salePriceTable}.Price IS NOT NULL");
             $select->where($condition);

            $stmt   = $this->sql->prepareStatementForSqlObject($select);

            $result = $stmt->execute();

            $resultSet = new ResultSet();
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch (\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}