<?php
/**
 * @author Phin Heng
 * @since  06 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Model;

use PCSPLib\BaseClasses\BaseObject;

class SaleDetail extends BaseObject
{
    public $SaleId;
    public $SaleItemId;

    public $Total;
    public $Qty;
    public $Price;

    public $Total1;
    public $TotalDiscount;
    public $DiscountId;
    public $DiscountTypeId;
    public $DiscountValue;
    public $DiscountName;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->SaleId = isset($data['SaleId'])? (!empty($data['SaleId']) ? (int)trim($data['SaleId']," "):0) : 0;
        $this->SaleItemId = isset($data['SaleItemId'])? (!empty($data['SaleItemId']) ? (int)trim($data['SaleItemId']," "):0) : 0;
        
        $this->Total = isset($data['Total'])? (!empty($data['Total']) ? (float)trim($data['Total']," "):0) : 0;
        $this->Qty   = isset($data['Qty'])? (!empty($data['Qty']) ? (float)trim($data['Qty']," "):0) : 0;
        $this->Price = isset($data['Price'])? (!empty($data['Price']) ? (float)trim($data['Price']," "):0) : 0;
        
        $this->Total1 = isset($data['Total1'])? (!empty($data['Total1']) ? (float)trim($data['Total1']," "):0) : 0;
        $this->TotalDiscount = isset($data['TotalDiscount'])? (!empty($data['TotalDiscount']) ? (float)trim($data['TotalDiscount']," "):0) : 0;
        $this->DiscountId   = isset($data['DiscountId'])? (!empty($data['DiscountId']) ? (int)trim($data['DiscountId']," "):0) : 0;
        $this->DiscountTypeId= isset($data['DiscountTypeId'])? (!empty($data['DiscountTypeId']) ? (int)trim($data['DiscountTypeId']," "):0) : 0;
        $this->DiscountValue = isset($data['DiscountValue'])? (!empty($data['DiscountValue']) ? (float)trim($data['DiscountValue']," "):0) : 0;
        $this->DiscountName  = isset($data["DiscountName"])?(!empty($data["DiscountName"]))?trim($data["DiscountName"],""):"":"";
    }

    public function getNoneTableField()
    {
        return ['Name', 'OrderNum', "Note"];
    }
}