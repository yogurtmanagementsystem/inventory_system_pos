<?php
/**
 * @author Phin Heng
 * @since  06 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Model;

use PCSPLib\BaseClasses\BaseObject;

class Invoice extends BaseObject
{
    public $InvoiceNo;
    public $InvoiceTypeId;
    public $InvoiceDate;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->InvoiceNo = isset($data["InvoiceNo"])?(!empty($data["InvoiceNo"]))?trim($data["InvoiceNo"],""):"":"";
        $this->InvoiceTypeId = isset($data['InvoiceTypeId'])? (!empty($data['InvoiceTypeId'])?trim($data['InvoiceTypeId']," "):0) : 0;
        $this->InvoiceDate = isset($data['InvoiceDate'])? (!empty($data['InvoiceDate'])? date("Y-m-d",strtotime(trim($data['InvoiceDate']))):null) : null;
    }

    public function getNoneTableField()
    {
        return ["Name","Note","OrderNum"];
    }
}