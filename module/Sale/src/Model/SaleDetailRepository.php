<?php
/**
 * @author Phin Heng
 * @since  06 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class SaleDetailRepository extends BaseRepository
{
	public function getAllRecords($condition = array(), $columns = array(), $orderField = 'Id', $orderType = 'ASC', $groupField = '')
    {
        $saleItemTable = TableConstant::SAL_SALE_ITEM_TABLE;
        $saleTable = TableConstant::SAL_SALE_TABLE;

        $select = $this->sql->select($this->tableName);

        if( count($condition) )
            $select->where($condition);

        $select->join("{$saleItemTable}","{$this->tableName}.SaleItemId = {$saleItemTable}.Id",["ItemName" => "Name","CategoryName","ItemCategoryId","ItemTypeId","TypeName"],'left');

        $select->join("{$saleTable}","{$this->tableName}.SaleId = {$saleTable}.Id",["InvoiceNo","InvoiceDate","SaleSubTotal" => "SubTotal","SaleTotal" => "Total","SaleTotalDiscount" => "TotalDiscount"],'left');

        $select->where(["{$this->tableName}.IsDeleted"=>0])->order($orderField." ".$orderType);
        if(count($columns))
            $select->columns($columns);
        if(!empty($groupField))
        {
            $select->group($groupField);
        }
        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getRecordsFilterForGrid($offset, $rows, $sort, $order, $filterString)
    {
        $saleItemTable = TableConstant::SAL_SALE_ITEM_TABLE;
        $saleTable = TableConstant::SAL_SALE_TABLE;

        $select = $this->sql->select($this->tableName);

        $select->columns(
                [
                    "*",
                    "Qty" => new Expression("SUM(Qty)")
                ]
            );
        $select->join("{$saleItemTable}","{$saleItemTable}.Id = {$this->tableName}.SaleItemId",["TypeName"],"left");
        $select->join("{$saleTable}","{$saleTable}.Id = {$this->tableName}.SaleId",[],"left");

        if( !empty( $filterString ) ) $select->where($filterString);
        
        $select->order($sort.' '.$order);
        $select->limit($rows)->offset($offset);
        $select->group("{$saleTable}.ItemTypeId");
        
        $stmt   = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getRowCountFilterForGrid($filterString)
    {
        try
        {
            $saleTable = TableConstant::SAL_SALE_TABLE;

            $select = $this->sql->select($this->tableName);
            $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT(DISTINCT SaleItemId)')));
           
           $select->join("{$saleTable}","{$saleTable}.Id = {$this->tableName}.SaleId",[],"left");

            if(!empty($filterString))$select->where($filterString);

            $select->limit(1);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();          
            
            $resultSet = new HydratingResultSet();
            $resultSet->initialize($result);
            $item = $resultSet->current();

            return $item['Num'];
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}