<?php
/**
 * @author Phin Heng
 * @since  10 Dec 2016
 * @copyright Phin Heng
**/
namespace Sale\Model;

use PCSPLib\BaseClasses\BaseObject;

class ItemCategory extends BaseObject
{
    public $ParentId;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->ParentId = isset($data['ParentId'])? (!empty($data['ParentId'])?trim($data['ParentId']," "):0) : 0;
    }
}