<?php
/**
 * @author Phin Heng
 * @since  06 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class ItemTypeRepository extends BaseRepository
{
	public function getRecordsForTreeGrid($condition = "",$fieldValue ="Id")
    {
        try
        {
            $select = $this->sql->select($this->tableName);
            $select->columns(array('*', new Expression($fieldValue.' as id'), new Expression('ParentId as parentid'), new Expression('Name as text')));
            
            $select->where(["IsDeleted"=>0]);

            if( !empty( $condition ) )
                $select->where($condition);

            $stmt   = $this->sql->prepareStatementForSqlObject($select);
            $result = $stmt->execute();

            $resultSet = new ResultSet();
            $resultSet->initialize($result);

            return $resultSet;
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }
}