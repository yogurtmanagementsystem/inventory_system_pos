<?php
/**
 * @author Phin Heng
 * @since  06 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Model;

use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use PCSPLib\BaseClasses\BaseRepository;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use PCSPLib\TableConstant;
use PCSPLib\PHPConstant;

class SaleRepository extends BaseRepository
{
	public function getRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        $select = $this->sql->select($this->tableName);

        $select->columns(
        		[
        			"*",
        			"InvoiceDate" => new Expression("DATE_FORMAT(InvoiceDate,'".PHPConstant::DATE_FORMAT."')"),
        			"SubTotal" => new Expression("SUM(SubTotal)"),
        			"Total" => new Expression("SUM(Total)"),
        			"TotalDiscount" => new Expression("SUM(TotalDiscount)"),
        		]
        	);
                   
        if(!empty($filterString)) $select->where($filterString);
        
        $select->order($sort.' '.$order);
        $select->limit($rows)->offset($offset);
        $select->group("{$this->tableName}.InvoiceDate");
        
        $stmt   = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getRowCountForGrid($filterString)
    {
        try
        {
            $select = $this->sql->select($this->tableName);
            $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT(DISTINCT InvoiceDate)')));
           
            if(!empty($filterString))$select->where($filterString);

            $select->limit(1);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();          
			
			$resultSet = new HydratingResultSet();
            $resultSet->initialize($result);
            $item = $resultSet->current();

            return $item['Num'];
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function getFilterRecordsForGrid($offset, $rows, $sort, $order, $filterString)
    {
        $select = $this->sql->select($this->tableName);

        $select->columns(
                [
                    "*",
                    "InvoiceDate" => new Expression("DATE_FORMAT(InvoiceDate,'".PHPConstant::DATE_FORMAT."')") 
                ]
            );

        if(!empty($filterString)) $select->where($filterString);
        
        $select->order($sort.' '.$order);
        $select->limit($rows)->offset($offset);
        
        $stmt   = $this->sql->prepareStatementForSqlObject($select);
        $result = $stmt->execute();

        $resultSet = new ResultSet();

        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getRowCountFilterForGrid($filterString)
    {
        try
        {
            $select = $this->sql->select($this->tableName);
            $select->columns(array('Num' => new \Zend\Db\Sql\Expression('COUNT(InvoiceDate)')));
           
            if(!empty($filterString))$select->where($filterString);

            $select->limit(1);

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result    = $statement->execute();          
            
            $resultSet = new HydratingResultSet();
            $resultSet->initialize($result);
            $item = $resultSet->current();

            return $item['Num'];
        }
        catch(\Exception $e)
        {
            die($e->getMessage());exit();
        }
    }

    public function getRecordForReport( $condition = "" )
    {
        $select = $this->sql->select($this->tableName);
        $select->columns(
                [
                    "*",
                    "IncomeDate" => new Expression("DATE_FORMAT(InvoiceDate,'".PHPConstant::DATE_FORMAT."')"),
                    "SubTotal" => new Expression("SUM(SubTotal)"),
                    "Total" => new Expression("SUM(Total)"),
                    "TotalDiscount" => new Expression("SUM(TotalDiscount)"),
                ]
            );

        $select->where( [ "{$this->tableName}.IsDeleted" => 0] );

        if( !empty( $condition ) )
            $select->where($condition);

        $select->group($this->tableName.".InvoiceDate");

        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new ResultSet();
        $resultSet->initialize($result);
        
        return $resultSet;
    }
    public function getTotal( $condition )
    {
        $select = $this->sql->select($this->tableName);
        $select->columns(
                [
                    
                    "Total" => new Expression("SUM(Total)")
                ]
            );

        $select->where( [ "{$this->tableName}.IsDeleted" => 0] );

        $select->where($condition);
        $select->limit(1);

        $stmt   = $this->sql->prepareStatementForSqlObject($select);

        $result = $stmt->execute();

        $resultSet = new HydratingResultSet();
        $resultSet->initialize($result);
        $item = $resultSet->current();

        return isset($item['Total'])?$item['Total']:0;
    }
}