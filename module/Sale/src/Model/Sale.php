<?php
/**
 * @author Phin Heng
 * @since  06 DEC 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale\Model;

use PCSPLib\BaseClasses\BaseObject;

class Sale extends BaseObject
{
    public $InvoiceNo;
    public $InvoiceDate;

    public $TotalDiscount;
    public $DiscountId;
    public $DiscountTypeId;
    public $DiscountValue;
    public $DiscountName;

    public $Total;
    public $SubTotal;
    public $KhTotal;
    public $Exchange;

    public function exchangeArray($data)
    {
        parent::exchangeArray($data);

        $this->InvoiceNo = isset($data["InvoiceNo"])?(!empty($data["InvoiceNo"]))?trim($data["InvoiceNo"],""):"":"";
        $this->InvoiceDate = isset($data['InvoiceDate'])? (!empty($data['InvoiceDate'])? trim($data['InvoiceDate']):null) : null;

        $this->SubTotal = isset($data['SubTotal'])? (!empty($data['SubTotal']) ? (float)trim($data['SubTotal']," "):0) : 0;
        $this->Total    = isset($data['Total'])? (!empty($data['Total']) ? (float)trim($data['Total']," "):0) : 0;
        $this->KhTotal  = isset($data['KhTotal'])? (!empty($data['KhTotal']) ? (float)trim($data['KhTotal']," "):0) : 0;
        $this->Exchange = isset($data['Exchange'])? (!empty($data['Exchange']) ? (float)trim($data['Exchange']," "):0) : 0;
        
        $this->TotalDiscount= isset($data['TotalDiscount'])? (!empty($data['TotalDiscount']) ? (float)trim($data['TotalDiscount']," "):0) : 0;
        $this->DiscountId   = isset($data['DiscountId'])? (!empty($data['DiscountId']) ? (int)trim($data['DiscountId']," "):0) : 0;
        $this->DiscountTypeId= isset($data['DiscountTypeId'])? (!empty($data['DiscountTypeId']) ? trim($data['DiscountTypeId']," "):0) : 0;
        $this->DiscountValue = isset($data['DiscountValue'])? (!empty($data['DiscountValue']) ? trim($data['DiscountValue']," "):0) : 0;
        $this->DiscountName  = isset($data["DiscountName"])?(!empty($data["DiscountName"]))?trim($data["DiscountName"],""):"":"";
        
    }

    public function getNoneTableField()
    {
        return ['Name', 'OrderNum', "Note"];
    }
}