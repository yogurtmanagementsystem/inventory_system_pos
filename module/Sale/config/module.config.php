<?php
/**
 * @author Phin Heng
 * @since  05 Dec 2016
 * @copyright PCSP GROUP COL.TD
**/
namespace Sale;

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
use PCSPLib\BaseClasses\BaseRepository;
use PCSPLib\BaseClasses\BaseObject;
use PCSPLib\TableConstant;
use Zend\Db\Adapter\AdapterInterface;
use Interop\Container\ContainerInterface;
use Zend\Hydrator\Reflection as ReflectionHydrator;

return [
    
    'service_manager' => 
    [
        'aliases' => 
        [
            
        ],
        'factories' => 
        [
            Model\SaleRepository::class => function(ContainerInterface $container)
            {
                return new Model\SaleRepository($container->get(AdapterInterface::class), TableConstant::SAL_SALE_TABLE,new ReflectionHydrator(), new Model\Sale());
            },
            Model\SaleDetailRepository::class => function(ContainerInterface $container)
            {
                return new Model\SaleDetailRepository($container->get(AdapterInterface::class), TableConstant::SAL_SALE_DETAIL_TABLE,new ReflectionHydrator(), new Model\SaleDetail());
            },
            Model\ItemCategoryRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemCategoryRepository($container->get(AdapterInterface::class), TableConstant::SAL_ITEM_CATEGORY_TABLE,new ReflectionHydrator(), new Model\ItemCategory());
            },
            Model\ItemTypeRepository::class => function(ContainerInterface $container)
            {
                return new Model\ItemTypeRepository($container->get(AdapterInterface::class), TableConstant::SAL_ITEM_TYPE_TABLE,new ReflectionHydrator(), new Model\ItemType());
            },
            Model\SaleTypeRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::SAL_SALE_TYPE_TABLE,new ReflectionHydrator(), new BaseObject());
            },
            Model\SaleItemRepository::class => function(ContainerInterface $container)
            {
                return new Model\SaleItemRepository($container->get(AdapterInterface::class), TableConstant::SAL_SALE_ITEM_TABLE,new ReflectionHydrator(), new Model\SaleItem());
            },
            Model\ItemSizeRepository::class => function(ContainerInterface $container)
            {
                return new BaseRepository($container->get(AdapterInterface::class), TableConstant::SAL_ITEM_SIZE_TABLE,new ReflectionHydrator(), new BaseObject());
            },
        ],
    ],
    'controllers' => 
    [
        'factories' => 
        [
            Controller\SaleController::class => function(ContainerInterface $container)
            {
                return new Controller\SaleController($container);
            },
            Controller\ItemCategoryController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemCategoryController($container);
            },
            Controller\ItemTypeController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemTypeController($container);
            },
            Controller\SalePriceController::class => function(ContainerInterface $container)
            {
                return new Controller\SalePriceController($container);
            },
            Controller\SaleTypeController::class => function(ContainerInterface $container)
            {
                return new Controller\SaleTypeController($container);
            },
            Controller\SaleItemController::class => function(ContainerInterface $container)
            {
                return new Controller\SaleItemController($container);
            },
            Controller\ItemSizeController::class => function(ContainerInterface $container)
            {
                return new Controller\ItemSizeController($container);
            },
        ],      
    ],
    'router' => 
    [
        'routes' => 
        [
            'sale' => 
            [
                'type' => Literal::class,
                'options' => 
                [
                    'route' => '/sale',
                    'defaults' => 
                    [
                        'controller' => Controller\SaleController::class,
                        'action'     => 'list',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => 
                [
                    'sale' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/sale[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\SaleController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'item-category' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/item-category[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\ItemCategoryController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],  
                    'item-type' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/item-type[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\ItemTypeController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],  
                    'sale-price' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/sale-price[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\SalePriceController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],   
                    'sale-type' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/sale-type[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\SaleTypeController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],              
                    'sale-type' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/sale-type[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\SaleTypeController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],  
                    'sale-item' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/sale-item[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\SaleItemController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],
                    'item-size' => 
                    [
                        'type' => Segment::class,
                        'options' => 
                        [
                            'route'    => '/item-size[/:action[/:id]]',
                            'defaults' => 
                            [
                                'controller' => Controller\ItemSizeController::class,
                                'action'     => 'list',
                            ],
                            'constraints' => 
                            [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+',
                            ],
                        ],
                    ],   
                ],
            ],
        ],
    ],
    'view_manager' => 
    [
        'template_path_stack' => 
        [
            'sale' => __DIR__ . '/../view',
        ],
    ],
    'translator' =>
    [
        'locale' => 'en_US',
        'translation_file_patterns' =>
            [
                [
                    'type' => 'gettext',
                    'base_dir' => __DIR__ . '/../language',
                    'pattern' => '%s.mo'
                ]
            ]
    ],
];
